//
//  UpcomingViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/1/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

class UpcomingBookingsListingViewModel {
  var analyticsCategory = AnalyticsCategories.Upcoming
  
  let currentVehicle = Vehicle.getCurrentVehicle()
  var currentPage: Page?
  
  var bookings = [Booking]() {
    didSet {
      if bookings.count == 0 {
        // If there are no bookings then refresh the center VC to display
        // the Main View Controller
        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.DidUpdateBookings.rawValue, object: nil)
        return
      }
      
      bookingsThatNeedRating = bookings.filter({
        ($0.needsRating ?? false) && ($0.status == .Completed)
      })
    }
  }
  
  var bookingsThatNeedRating: [Booking]?
  
  init() {
    bookings = Booking.getSavedBookingsForCurrentVehicle()
  }
  
  /// This method reloads the booking data.
  /// If the current vehicle bookings are in sync with the server (has a page saved in the persistence manager) then get the already saved bookings.
  func reloadData(completionBlock:(success: Bool, error: NSError?) -> Void) {
    refreshDataSource()
    
    // If bookings for the selected vehicle are synced
    if currentPage != nil {
      completionBlock(success: true, error: nil)
    } else {
      // fetch bookings for current vehicle
      PersistenceManager.sharedManager.loadBookingsForAvailableVehicles(
        [currentVehicle!.id!], indexOfID: 0, completionBlock: { (errors) in
          let success = (errors.count == 0)
          if success {
            self.refreshDataSource()
          }
          completionBlock(success: success, error: errors.first)
      })
    }
  }
  
  var hasBookings: Bool {
    return bookings.count > 0
  }
  
  private func refreshDataSource() {
    bookings = Booking.getSavedBookingsForCurrentVehicle()
    currentPage = PersistenceManager.sharedManager.upcomingBookingsPages[currentVehicle!.id!]
  }
  
  func servicesViewModelForIndexPath(indexPath: NSIndexPath) -> ServicesViewModel {
    let bookingFlowViewModel = BookingFlowViewModel(editedBooking: bookings[indexPath.row])
    let serviceViewModel = ServicesViewModel()
    serviceViewModel.bookingFlowViewModel = bookingFlowViewModel
    return serviceViewModel
  }
  
  //==========================================================
  // MARK: - Rating
  //==========================================================
  
  var bookingToBeRated: Booking? {
    return PersistenceManager.sharedManager.bookingToBeRated
  }
  
  func popBookingThatNeedsRating(booking: Booking? = nil) {
    PersistenceManager.sharedManager.popBookingThatNeedsRating(booking)
  }
  
  //==========================================================
  // MARK: - TABLE VIEW | Data Source
  //==========================================================
  
  var numberOfSections: Int {
    return 1
  }
  
  func numberOfRowsInSecction(section: Int) -> Int {
    return bookings.count
  }
  
  func bookingForCellAtIndexPath(indexPath: NSIndexPath) -> Booking {
    return bookings[indexPath.row]
  }
  
  func sectionInfoForIndex(section: Int) -> String {
    return "Upcoming Orders" // TODO: Localize
  }
  
  func specialistInfoForCellAtIndexPath(indexPath: NSIndexPath) -> (specialistName: String, specialistNumber: String)? {
    let booking = bookingForCellAtIndexPath(indexPath)
    
    guard let specialistName = booking.specialistName,
      specialistNumber = booking.specialistNumber else {
      return nil
    }
    
    return (specialistName: specialistName, specialistNumber: specialistNumber)
  }
  
  //==========================================================
  // MARK: - Pagination related
  //==========================================================
  
  func hasFirstPage() -> Bool {
    guard let currentPageURL = currentPage?.mySelf where !currentPageURL.isEmpty else {
        return false
    }
    return true
  }
  
  func hasNextPage() -> Bool {
    guard let currentPageURL = currentPage?.mySelf,
      nextPageURL = currentPage?.next where !nextPageURL.isEmpty else {
      return false
    }
    
    return currentPageURL != nextPageURL
  }

  func fetchNextPage(completionBlock:(success: Bool, error: NSError?) -> Void) {
    if let nextPage = currentPage?.next, nextPageURL = NSURL(string: nextPage) where !nextPage.isEmpty {
      signedRequest(BlinkMyCarAPI.Next(fullURL: nextPageURL), completion: {
        (data, statusCode, response, error) -> () in
        self.parseBookingsResultData(true, data: data, statusCode: statusCode, completionBlock: completionBlock)
      })
    } else {
      // TODO: Fix error messages
      completionBlock(success: false, error: nil)
    }
  }
  
  func parseBookingsResultData(loadMore: Bool? = false ,data: NSData?, statusCode: Int?, completionBlock: (success: Bool, error: NSError?) -> ()) {
    if statusCode != 200 {
      let error = NSError(
        domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.GetAllBookingsForVehicleFailed.rawValue,
        userInfo: ["error":"Could not get bookings"])
      completionBlock(success: false, error: error)
      return
    }
    
    do {
      if let data = data,
        let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(
          data,
          options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
        var bookings = Booking.bookingsFromJSON(jsonDictionary)
        
        // TODO: Get from the response the next links
        self.currentPage = Page.pageFromJSON(jsonDictionary)
        
        // Sort Bookings by delivery date and time
        bookings = Booking.sortBookings(bookings)
        
        if let loadMore = loadMore where loadMore {
          for booking in bookings {
            self.bookings.append(booking)
          }
        } else {
          self.bookings.removeAll()
          self.bookings = bookings
        }
        completionBlock(success: true, error: nil)
        
        return
      }
    } catch {
      let error = NSError(
        domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.GetAllBookingsForVehicleFailed.rawValue,
        userInfo: ["error":"Could not get bookings"])
      completionBlock(success: false, error: error)
    }
    
    
  }
  
  //==========================================================
  // MARK: - Actions
  //==========================================================
  
  func deleteBookingForIndexPath(indexPath: NSIndexPath, completionBlock: ((success: Bool, error: NSError?) -> Void)?) {
    let booking = bookings[indexPath.row]
    signedRequest(
      BlinkMyCarAPI.CancelBooking(bookingId: booking.bookingId!),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 200 {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.DeleteBookingFailed.rawValue,
            userInfo: ["error":"Could not delete booking"])
          completionBlock?(success: false, error: error)
          return
        }
        
        let cancelledBooking = Booking(bookingId: booking.bookingId, bookingNumber: booking.bookingNumber, address: booking.address,
          vehicle: booking.vehicle, status: BookingStatus.Canceled, services: booking.services,
          deliveryDate: booking.deliveryDate, serviceTime: booking.bookingTime, startDate: booking.startDate, washer: booking.washer, rating: booking.rating, needsRating: booking.needsRating, needsAddressDetails: booking.needsAddressDetails, needKeysDetails: booking.needKeysDetails, addressKeysLocation: booking.addressKeysLocation,  addressLocationType: booking.addressLocationType, totalPrice: booking.totalPrice, paymentMethod: booking.paymentMethod ?? .Stripe, promocode: booking.promocode, specialistName: booking.specialistName, specialistNumber: booking.specialistNumber)
        
        Booking.replaceBooking(booking, with: cancelledBooking)
        
        completionBlock?(success: true, error: nil)
    })
  }
  
  //==========================================================
  // MARK: - Locale
  //==========================================================
  
  var viewControllerTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.viewControllerTitle",
      value: "Blink My Car",
      comment: "Upcoming bookings view controller title")
  }
  
  var actionButtonTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.actionButtonTitle",
      value: "Book For This Vehicle",
      comment: "Book another wash button title in Upcoming VC")
  }
  
  var failToDiscardBookingAlertTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.failToDiscardBookingAlertTitle",
      value: "Cancel Booking",
      comment: "Alert View Title, Shown When we fail to cancel a booking")
  }
  
  var failToDiscardBookingAlertOkButtonTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.failToDiscardBookingAlertOkButtonTitle",
      value: "Ok",
      comment: "Alert View Discard button title, Shown When we fail to cancel a booking")
  }
  
  var failToDiscardBookingAlertMessage: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.failToDiscardBookingAlertMessage",
      value: "Failed to Cancel your booking",
      comment: "Alert View Message, Shown When we fail to cancel a booking")
  }
  
  var discardBookingAlertViewTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.discardBookingAlertViewTitle",
      value: "Cancel Booking",
      comment: "Title of the alert view that appears upon tapping Discard booking button")
  }
  
  var discardBookingAlertViewTitleMessage: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.discardBookingAlertViewTitleMessage",
      value: "Are you sure you want to discard your booking?",
      comment: "Message of the alert view that appears upon tapping Discard booking button")
  }
  
  var discardBookingAlertViewNegativeButtonTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.discardBookingAlertViewNegativeButtonTitle",
      value: "No",
      comment: "Title of negative button in the alert view that appears upon tapping Discard booking button")
  }
  
  var discardBookingAlertViewPostiveButtonTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.discardBookingAlertViewPostiveButtonTitle",
      value: "Yes",
      comment: "Title of positive button in the alert view that appears upon tapping Discard booking button")
  }
  
  var callSpecialistAlertTitle: String {
    return ""
  }
  
  var callSpecialistAlertMessage: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.callSpecialistAlertMessage",
      value: "Call %@?",
      comment: "alert message that displays specialist number before calling")
  }
  
  var callSpecialistAlertPositiveActionButtonTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.callSpecialistAlertPositiveActionButtonTitle",
      value: "Yes",
      comment: "Yes button title for alert message displayed before calling a specialist")
  }
  
  var callSpecialistAlertNegativeActionButtonTitle: String {
    return NSLocalizedString(
      "UpcomingBookingsLisitngViewController.callSpecialistAlertNegativeActionButtonTitle",
      value: "No",
      comment: "No button title for alert message displayed before calling a specialist")
  }
}
