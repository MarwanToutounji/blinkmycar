//
//  TimePickerViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/23/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class TimePickerViewModel {
  var serviceTimes = [ServiceTime]() {
    didSet {
      serviceTimes.sortInPlace({ $0.from < $1.from })
      morningTimes.removeAll(keepCapacity: false)
      afternoonTimes.removeAll(keepCapacity: false)
      for serviceTime in serviceTimes {
        if serviceTime.from < 13 {
          morningTimes.append(serviceTime)
        } else {
          afternoonTimes.append(serviceTime)
        }
      }
    }
  }

  var selectedTime: ServiceTime?

  var morningTimes = [ServiceTime]()
  var afternoonTimes = [ServiceTime]()
  
  var numberOfSections: Int {
    return 2
  }
  
  func numberOfRowForSection(section: Int) -> Int {
    switch section {
    case 0:
      return morningTimes.count
    case 1:
      return afternoonTimes.count
    default:
      return 0
    }
  }
  
  func sectionInfoForIndex(section: Int) -> (title: String, imageName: String) {
    switch section {
    case 0:
      return (title: "Morning", imageName: "sunrise")
    case 1:
      return (title: "Afternoon", imageName: "sunset")
    default:
      return (title: "Service Times", imageName: "sunrise")
    }
  }
  
  func cellInfoForIndexPath(indexPath: NSIndexPath) -> (text: String, color: UIColor) {
    switch indexPath.section {
    case 0:
      let serviceTime = morningTimes[indexPath.row]
      return (text: serviceTime.startTimeStringValue,
              color: (serviceTime.enabled ?? false) ? UIColor.darkGray() : UIColor.lighterGray())
    case 1:
      let serviceTime = afternoonTimes[indexPath.row]
      return (text: serviceTime.startTimeStringValue,
              color: (serviceTime.enabled ?? false) ? UIColor.darkGray() : UIColor.lighterGray())
    default:
      return (text: "N.A",
              color: UIColor.darkGray())
    }
  }
  
  func shouldSelectRowAtIndexPath(indexPath: NSIndexPath) -> Bool {
    if let timeAtIndex = timeForIndexPath(indexPath) {
      return shouldSelectTime(timeAtIndex)
    }
    return false
  }

  func shouldSelectTime(time: ServiceTime) -> Bool {
    if let selectedTime = selectedTime {
      return selectedTime == time && (time.enabled ?? false)
    }
    return false
  }
  
  func canSelectTimeForRow(indexPath: NSIndexPath) -> Bool {
    if let timeSelected = timeForIndexPath(indexPath) {
      return timeSelected.enabled ?? false
    }
    return false
  }
  
  func timeForIndexPath(indexPath: NSIndexPath) -> ServiceTime? {
    switch indexPath.section {
    case 0:
      return morningTimes[indexPath.row]
    case 1:
      return afternoonTimes[indexPath.row]
    default:
      return nil
    }
  }
}
