//
//  RideColorViewModel.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/20/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class RideColorViewModel {
  var title = NSLocalizedString("VehicleInfoViewController.chooseColour",
    comment: "Choose your colour view title")

  var question = NSLocalizedString("VehicleInfoViewController.chooseColourQuestion",
    comment: "Choose your colour question: What Colour is it?")

  private let colors: [RideColor] = [
    RideColor(name: "White", value: UIColor.whiteColor()),
    RideColor(name: "Silver", value: UIColor(red: 217 / 255.0, green: 217 / 255.0, blue: 217 / 255.0, alpha: 1)),
    RideColor(name: "Chrome", value: UIColor(red: 196 / 255.0, green: 207 / 255.0, blue: 212 / 255.0, alpha: 1)),
    RideColor(name: "Black", value: UIColor.blackColor()),
    RideColor(name: "Grey", value: UIColor(red: 53 / 255.0, green: 56 / 255.0, blue: 59 / 255.0, alpha: 1)),
    RideColor(name: "Dark Blue", value: UIColor(red: 19 / 255.0, green: 20 / 255.0, blue: 107 / 255.0, alpha: 1)),
    RideColor(name: "Turquoise", value: UIColor(red: 22 / 255.0, green: 175 / 255.0, blue: 202 / 255.0, alpha: 1)),
    RideColor(name: "Sky Blue", value: UIColor(red: 66 / 255.0, green: 182 / 255.0, blue: 245 / 255.0, alpha: 1)),
    RideColor(name: "Purple", value: UIColor(red: 153 / 255.0, green: 43 / 255.0, blue: 174 / 255.0, alpha: 1)),
    RideColor(name: "Orange", value: UIColor(red: 253 / 255.0, green: 88 / 255.0, blue: 6 / 255.0, alpha: 1)),
    RideColor(name: "Red", value: UIColor(red: 245 / 255.0, green: 0 / 255.0, blue: 22 / 255.0, alpha: 1)),
    RideColor(name: "Wine Red", value: UIColor(red: 113 / 255.0, green: 0 / 255.0, blue: 25 / 255.0, alpha: 1)),
    RideColor(name: "Brown", value: UIColor(red: 82 / 255.0, green: 34 / 255.0, blue: 17 / 255.0, alpha: 1)),
    RideColor(name: "Beige", value: UIColor(red: 185 / 255.0, green: 139 / 255.0, blue: 90 / 255.0, alpha: 1)),
    RideColor(name: "Green", value: UIColor(red: 16 / 255.0, green: 69 / 255.0, blue: 50 / 255.0, alpha: 1)),
    RideColor(name: "Yellow", value: UIColor(red: 252 / 255.0, green: 210 / 255.0, blue: 42 / 255.0, alpha: 1)),
    RideColor(name: "Gold", value: UIColor(red: 193 / 255.0, green: 132 / 255.0, blue: 13 / 255.0, alpha: 1)),
    RideColor(name: "Pink", value: UIColor(red: 225 / 255.0, green: 0 / 255.0, blue: 81 / 255.0, alpha: 1))
  ]

  public func numberOfSections() -> Int {
    return 1
  }

  public func numberOfRows(section: Int) -> Int {
    return colors.count
  }

  public func itemForIndexPath(indexPath: NSIndexPath) -> RideColor {
    return colors[indexPath.row]
  }
  
  public func colorForString(color: String) -> UIColor? {
    var result: [RideColor] = colors.filter { $0.name.lowercaseString == color.lowercaseString }
    if result.count >= 1 {
      return result[0].value
    }
    return nil
  }
}
