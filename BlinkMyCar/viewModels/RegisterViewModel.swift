//
//  AccountInfoViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import libPhoneNumber_iOS

class RegisterViewModel {

  private var phoneUtil = NBPhoneNumberUtil()

  private var loginViewModel = SignInViewModel()

  let validateEmail = {(text: String?) -> Bool in
    if let email = text {
      return email.isValidEmail()
    }
    return false
  }

  let validatePhoneNumber = {(text: String?) -> Bool in
    if let text = text,
      let number = Int(text)
      where !text.isEmpty {
        var errorPointer:NSError?
        var phoneUtil = NBPhoneNumberUtil()
        do {
          let number:NBPhoneNumber = try phoneUtil.parse(text,
            defaultRegion:"LB")
          // Check if country code is included in the given text
          let countryCode = number.countryCode.stringValue
          var hasCountryCode = false
          if countryCode.characters.count+1 < text.characters.count {
            let start = text.startIndex
            let end = text.startIndex.advancedBy(countryCode.characters.count+1)
            var substring = text.substringWithRange(start..<end)
            if substring.rangeOfString(countryCode) != nil {
              hasCountryCode = true
            }
          }
          return errorPointer == nil && !hasCountryCode && phoneUtil.isValidNumber(number)
          
        } catch let error {
          return false
        }
    }
    return false
  }

  let validatePassword = {(text: String?) -> Bool in
    if text == nil || text!.isEmpty {
      return false
    }
    return true
  }

  func getFormattedPhoneNumber(phoneNumber: String?) -> String? {
    return NBPhoneNumber.getFormattedPhoneNumber(phoneNumber)
  }

  func getFormattedPhoneNumberLocal(phoneNumber: String?) -> String {
    return NBPhoneNumber.getFormattedPhoneNumberLocal(phoneNumber)
  }

  func registerUser(email: String, phoneNumber: String,
    password: String, completionBlock: (registrationError: NSError?, loginError: NSError?) -> Void) {
      // 1 - We register the User
      // 2 - We sign him in
      // 3 - We retrieve the user profile and save it

      //~ 1
      APIProvider.sharedProvider.request(BlinkMyCarAPI.Register(firstName: "", lastName: "", email: email, password: password, phoneNumber: phoneNumber)) { (result) -> () in
        switch result {
        case .Success(let response):
          if response.statusCode == 422 {
            let error = NSError(domain: "com.blinkmycar.Registration", code: 422, userInfo: ["error": self.registerationErrorInvalidField])
            completionBlock(registrationError: error, loginError: nil)
            return
          }
          else if response.statusCode == 409 {
            let error = NSError(domain: "com.blinkmycar.Registration", code: 409, userInfo: ["error": self.registerationErrorEmailAlreadyExists])
            completionBlock(registrationError: error, loginError: nil)
            return
          }

          if response.statusCode != 201 {
            let error = NSError(domain: "com.blinkmycar.Registration",
              code: response.statusCode,
              userInfo: ["error": "error not specified"])
            completionBlock(registrationError: error, loginError: nil)
            return
          }

          if let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(response.data, options: NSJSONReadingOptions.AllowFragments) {
            // If the result is a disctionary then this is the returned
            // User object and the registration succeded
            if jsonResult is NSDictionary {
              //~ 2 ~ 3
              self.loginViewModel.loginUser(email, password: password, completionBlock: { (error) -> Void in
                if let loginError = error {
                  completionBlock(registrationError: nil, loginError: loginError)
                  return
                }
                completionBlock(registrationError: nil, loginError: nil)
              })
              return
            }
          }

          completionBlock(registrationError: NSError(domain: "com.blinkmycar.Registration", code: -1, userInfo: ["error": "Could not parse registration response"]), loginError: nil)
          return

        case .Failure(let error):
          let nsError = NSError(domain: "com.blinkmycar.Registration",
            code: error._code,
            userInfo: ["error": "error not specified"])
          completionBlock(registrationError: nsError,
            loginError: nil)
        }
      }
  }

  //===========================================================
  // MARK: U I   T E X T
  //===========================================================

  var viewControllerTitle: String {
    return NSLocalizedString("RegisterViewController.ViewControllerTitle",
      comment: "Register view controller title that appears in the navigation Bar")
  }

  var labelWelcomeText: String {
    return NSLocalizedString("RegisterViewController.WelcomeLabelText", comment: "Text That is displayed above the registration form")
  }

  var labelEmailText: String {
    return NSLocalizedString("RegisterViewController.form.emailLabelText", comment: "Text that is displayed above the email text field in the registration form")
  }

  var labelPhoneText: String {
    return NSLocalizedString("RegisterViewController.form.phoneLabelText", comment: "Text that is displayed above the phone text field in the registration form")
  }

  var labelPasswordText: String {
    return NSLocalizedString("RegisterViewController.form.passwordLabel", comment: "Text that is displayed above the email text field in the registration form")
  }

  var buttonRegisterTitle: String {
    return NSLocalizedString("RegisterViewController.form.registrationButton", comment: "Button text that registers users")
  }

  var buttonLoginLinkTitle: String {
    return NSLocalizedString("RegisterViewController.form.loginLinkButton", comment: "Button text that open login view if user already has an account")
  }
  
  var textFieldEmailErrorMessage: String {
    return NSLocalizedString("RegisterViewController.form.emailErrorMessage", comment: "Error Message that is displayed in case the Email field content is not valid")
  }

  var textFieldPasswordErrorMessage: String {
    return NSLocalizedString("RegisterViewController.form.passwordErrorMessage", comment: "Error Message that is displayed in case the Password field content is empty")
  }

  var textFieldPhoneErrorMessage: String {
    return NSLocalizedString("RegisterViewController.form.phoneErrorMessage", comment: "Error Message that is displayed in case the Phone field content is empty")
  }

  var termsLabelText: String {
    return NSLocalizedString("RegisterViewController.termsAndConditionsLabelText", comment: "Text in Label precedes the Terms and condition button ex:'By registering you agree to our'")
  }

  var termsButtonText: String {
    return NSLocalizedString("RegisterViewController.termsAndConditionsButtonText", comment: "Text in buttons that has content like 'Terms & Conditions'")
  }

  var alertViewConfirmPhoneNumberTitle: String {
    return NSLocalizedString("RegisterViewController.alertViewConfirmPhoneNumberTitle",
      comment: "Title for alert view that is displayed while confirming a user phone number while registering an account")
  }

  var alertViewConfirmPhoneNumberNegativeActionText: String {
    return NSLocalizedString("RegisterViewController.alertViewConfirmPhoneNumberNegativeActionText", comment: "Negative button text, for not confirming a phone number")
  }

  var alertViewConfirmPhoneNumberPositiveActionText: String {
    return NSLocalizedString("RegisterViewController.alertViewConfirmPhoneNumberPositiveActionText", comment: "Positive button text, for confirming a phone number")
  }

  var alertViewFailRegistrationAlertTitle: String {
    return NSLocalizedString("RegisterViewController.alertViewFailRegistrationAlertTitle",
      value: "Ooops",
      comment: "Title for alert views that are displayed after pressing the register button, that are the result of invalid fields or server errors")
  }
  
  var alertViewFailRegistrationAlertMessage: String {
    return NSLocalizedString("RegisterViewController.alertViewFailRegistrationAlertMessage",
      value: "There was a network error, please try again.",
      comment: "Title for alert views that are displayed after pressing the register button, that are the result of invalid fields or server errors")
  }

  var alertViewFailRegistrationAlertInvalidFieldsMessage: String {
    return NSLocalizedString("RegisterViewController.alertViewFailRegistrationAlertInvalidFieldsMessage", comment: "Message that is displayed in alert view for having invalid fields")
  }

  var alertViewFailRegistrationAlertRegistrationErrorMessage: String {
    return NSLocalizedString("RegisterViewController.alertViewFailRegistrationAlertRegistrationErrorMessage", comment: "Message that is displayed in alert view indicating that there was a some kind of error and we couldn't register user")
  }

  var alertViewFailRegistrationAlertActionButtonText: String {
    return NSLocalizedString("RegisterViewController.alertViewFailRegistrationAlertActionButtonText", comment: "Action Button Text for Error alert views in registration")
  }
  
  var alertViewFailRegistrationAlertSignInActionButtonText: String {
    return NSLocalizedString("RegisterViewController.alertViewFailRegistrationAlertSignInActionButtonText", comment: "Sign In Action Button Text for Error alert views in registration")
  }
  
  var registerationErrorInvalidField: String {
    return NSLocalizedString("RegisterViewController.registerationErrorInvalidField",
      comment: "Registeration failure error message if any of the fields were invalid")
  }

  var registerationErrorEmailAlreadyExists: String {
    return NSLocalizedString("RegisterViewController.registerationErrorEmailAlreadyExists",
      comment: "Registeration failure error message if email had been used before")
  }
}
