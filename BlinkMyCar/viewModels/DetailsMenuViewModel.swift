//
//  DetailsMenuViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class DetailsMenuViewModel {
  var vehicle: Vehicle!
  
  init() {
    refreshModel()
  }
  
  func refreshModel() {
    vehicle = Vehicle.getCurrentVehicle()!
  }

  var menuSections = [
    MenuSection.Account,
    MenuSection.CustomerSupport,
    MenuSection.About
  ]

  var numberOfSectionInTableView: Int {
    return menuSections.count
  }

  func numberOfRowsForSection(section: Int) -> Int {
    return menuSections[section].items.count
  }

  func menuItemForIndexPath(indexPath: NSIndexPath) -> MenuItem {
    return menuSections[indexPath.section].items[indexPath.row]
  }

  func cellInformationForIndexPath(indexPath: NSIndexPath) -> (title: String, image: UIImage, imageTintColor: UIColor) {
    let menuItem = menuItemForIndexPath(indexPath)
    return (title: menuItem.title, image: menuItem.image, imageTintColor: menuItem.imageTintColor)
  }

  func sectionTitle(section: Int) -> String {
    return menuSections[section].title
  }
  
  func sectionHeight(section: Int) -> CGFloat {
    return menuSections[section].sectionHeight
  }

  func cellIsLastInLastSection(indexPath: NSIndexPath) -> Bool {
    guard let lastMenuSection = menuSections.last where
      lastMenuSection == menuSections[indexPath.section] && (lastMenuSection.items.count-1) == indexPath.row else {
      return false
    }
    
    return true
  }

  //===================================================
  // MARK: U I   T E X T
  //===================================================

  var viewControllerTitle: String {
    return NSLocalizedString("DetailsMenuTableViewController.viewControllerTitle",
                             value: "Menu",
                             comment: "Details Menu Table View Controller Title")
  }
}


//===================================================
// MARK: - MENU SECTION
//===================================================

enum MenuSection {
  case Account
  case CustomerSupport
  case About

  var title: String {
    return ""
  }
  
  var sectionHeight: CGFloat {
    switch self {
    case .Account:
      return 0.0
    case .About, .CustomerSupport:
      return 40.0
    }
  }

  var items: [MenuItem] {
    switch self {
    case .Account:
      return [
        MenuItem.Account,
        MenuItem.Payment
      ]
    case .CustomerSupport:
      return [
        MenuItem.CustomerSupport
      ]
    case .About:
      return [
        MenuItem.About
      ]
    }
  }
}

//===================================================
// MARK: - MENU ITEM
//===================================================

enum MenuItem {
  case Account
  case Payment
  case CustomerSupport
  case About

  var title: String {
    switch self {
    case .Account:
      return NSLocalizedString("DetailsMenuTableViewController.carProfile",
                               value: "My Account", comment: "")
    case .CustomerSupport:
      return NSLocalizedString("DetailsMenuTableViewController.customerService",
                               value: "Customer Support", comment: "")
    case .About:
      return NSLocalizedString("DetailsMenuTableViewController.about",
                               value: "About", comment: "")
    case .Payment:
      return NSLocalizedString("DetailsMenuTableViewController.payment",
                               value: "Payment", comment: "")
    }
  }

  var image: UIImage {
    switch self {
    case .Account:
      return UIImage(named: "accountSettingsIcon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    case .CustomerSupport:
      return UIImage(named: "customerServiceIcon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    case .About:
      return UIImage(named: "aboutIcon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    case .Payment:
      return UIImage(named: "paymentIcon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    }
  }

  var imageTintColor: UIColor {
    return UIColor.applicationSecondaryColor()
  }
}
