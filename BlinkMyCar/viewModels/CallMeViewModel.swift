//
//  CallMeViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class CallMeViewModel {
  var callingPhoneNumber: String = "961123123"

  let booking: Booking

  init(booking: Booking) {
    self.booking = booking
  }
  
  var viewControllerTitle: String {
    return NSLocalizedString("CallMeViewController.ViewControllerTitle",
      comment: "Call Me view controller title that appears in the navigation Bar")
  }
  
  var headerLabelText: String {
    return NSLocalizedString("CallMeViewController.headerLabelText",
      comment: "Call Me view controller header label text: we will call you in less than one minute")
  }
  
  var okButtonTitle: String {
    return NSLocalizedString("CallMeViewController.okButton",
      comment: "Call Me view controller Ok - Call me button")
  }
  
  var cancelButtonTitle: String {
    return NSLocalizedString("CallMeViewController.cancelButton",
      comment: "Call Me view controller Cancel - Call me button")
  }

  var successAlertTitle: String {
    return NSLocalizedString("CallMeViewController.successAlertTitle",
      comment: "Title of the alert when we submit successfully")
  }

  var successAlertMessage: String {
    return NSLocalizedString("CallMeViewController.successAlertMessage",
      comment: "Message of the alert when we submit successfully")
  }

  var failAlertTitle: String {
    return NSLocalizedString("CallMeViewController.failAlertTitle",
      comment: "Title of the alert when we failt to submit")
  }

  var failAlertMessage: String {
    return NSLocalizedString("CallMeViewController.failAlertMessage",
      comment: "Message of the alert when we failt to submit")
  }

  func sendCallMeRequest(completion: (error: NSError?) -> Void) {
    signedRequest(BlinkMyCarAPI.CustomerSupport(type: "callme",
      body: "",
      bookingId: booking.bookingId!),
      completion: { (data, statusCode, response, error) -> () in
        if statusCode != 201 {
          completion(error: error)
          return
        }

        completion(error: nil)
    })
  }
}
