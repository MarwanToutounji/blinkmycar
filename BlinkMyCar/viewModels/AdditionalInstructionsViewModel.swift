//
//  AdditionalInstructionsViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/31/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

class AdditionalInstructionsViewModel {
  var booking: Booking!
  
  var additionalInformation: String?
  
  init(booking: Booking) {
    self.booking = booking
  }
  
  var isValidInformation: Bool {
    return !(additionalInformation?.trim() ?? "").isEmpty
  }
  
  func submitAdditionalInformation(completion: (success: Bool, error: NSError?) -> Void) {
    signedRequest(
      BlinkMyCarAPI.BookingAdditionalInformation(
        bookingId: booking.bookingId!,
        keysInfo: additionalInformation?.trim() ?? ""))
    { (data, statusCode, response, error) in
      
      if statusCode != 200 {
        let error = NSError(
          domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.SubmitBookingAdditionalInformationFailed.rawValue,
          userInfo: ["error":"Could not Update booking"])
        completion(success: false, error: error)
        return
      }
      
      do {
        if let data = data,
          let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(
            data,
            options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
          let booking = Booking.fromJSON(jsonDictionary) as! Booking
          
          Booking.replaceBooking(self.booking, with: booking)
          
          self.booking = booking
          
          completion(success: true, error: nil)
          return
        }
      } catch {}
      
      let error = NSError(
        domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.SubmitBookingAdditionalInformationFailed.rawValue,
        userInfo: ["error":"Could not Update booking"])
      completion(success: false, error: error)
      
    }
  }
  
  var headerViewTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.headerViewTitle",
      value: "Done!",
      comment: "Additional Info VC header View title")
  }
  
  var headerViewSubtitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.headerViewSubtitle",
      value: "We will confirm your booking shortly",
      comment: "Additional Info VC header View Subtitle")
  }
  
  var textViewPlaceholder: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.textViewPlaceholder",
      value: "e.g. the keys are with the concierge\ne.g. my car is right next to the pharmacy.",
      comment: "Additional Info VC text view preceeding label")
  }
  
  var textViewLabelText: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.textViewLabelText",
      value: "Any other instructions?",
      comment: "Additional Info VC, label preceeding input field")
  }
  
  var cancelButtonTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.cancelButtonTitle",
      value: "Skip",
      comment: "Additional Info VC, cancel button title")
  }
  
  var submitButtonTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.cancelButtonTitle",
      value: "Submit",
      comment: "Additional Info VC, submit button title")
  }
  
  var alertViewFailToSubmitInfoTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.alertViewFailToSubmitInfoTitle",
      value: "Ooops!",
      comment: "Additional Info VC, submit button title")
  }
  
  var alertViewFailToSubmitInfoMessage: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.alertViewFailToSubmitInfoMessage",
      value: "We couldn't submit your additional information, try again later.",
      comment: "Alert View Message, Shown When we fail to Update a booking")
  }
  
  var alertViewFailToSubmitInfoNegativeActionButtonTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.alertViewFailToSubmitInfoNegativeActionButtonTitle",
      value: "Cancel",
      comment: "Alert View Discard button title, Shown When we fail to Update a booking")
  }
  
  var alertViewFailToSubmitInfoRetryActionButtonTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.alertViewFailToSubmitInfoRetryActionButtonTitle",
      value: "Retry",
      comment: "Alert View Retry button title, Shown When we fail to Update a booking")
  }
  
  var alertViewInvalidInformationTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.alertViewInvalidInformationTitle",
      value: "Ooops!",
      comment: "Alert View Title, shows when trying to submit invalid/empty information as additional information for a booking")
  }
  
  var alertViewInvalidInformationMessage: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.alertViewInvalidInformationMessage",
      value: "There is no additional information to submit!",
      comment: "Alert View Message, shows when trying to submit invalid/empty information as additional information for a booking")
  }
  
  var alertViewInvalidInformationActionButtonTitle: String {
    return NSLocalizedString(
      "AdditionalInstructionsViewController.alertViewInvalidInformationActionButtonTitle",
      value: "Ok",
      comment: "Alert View Action Button, shows when trying to submit invalid/empty information as additional information for a booking")
  }
}
