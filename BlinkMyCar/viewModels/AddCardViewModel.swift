//
//  AddCardViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 4/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import Stripe

class AddCardViewModel {
  var analyticsCategory = AnalyticsCategories.InMenuPayment
  
  var isDefault: Bool = true
  
  func addCardRequest(token: STPToken, completionBlock: ((success: Bool, error: NSError?) -> Void)) {
    retrieveStripeKeyIfNeeded { (success, error) -> Void in
      if !success {
        completionBlock(success: false, error: nil)
        return
      }
      
      signedRequest(BlinkMyCarAPI.AddCard(token: token.tokenId, isDefault: self.isDefault)) {
        (data, statusCode, response, error) -> () in
        if statusCode != 201 {
          completionBlock(success: false, error: error)
          return
        }
        
        do {
          if let data = data,
            jsonResult: [String : AnyObject] = try NSJSONSerialization.JSONObjectWithData(data,
              options: NSJSONReadingOptions.AllowFragments) as? [String : AnyObject] {
                let cardInfo = StripePayment.fromJSON(jsonResult) as! StripePayment
                StripePayment.saveCardToList(cardInfo)
            
                // Save the added card as default
                PaymentMethod.saveDefaultPaymentTool(cardInfo)
            
                completionBlock(success: true, error: nil)
                return
          }
        } catch {}
        
        // If this line is reached then there is an erro in the data
        completionBlock(success: false, error: nil)
      }
    }
    
  }
  
  private func retrieveStripeKeyIfNeeded(completion: ((success: Bool, error: NSError?) -> Void)) {
    if Stripe.defaultPublishableKey() == nil {
      PersistenceManager.sharedManager.loadCountryInformation(completion)
      return
    }
    
    completion(success: true, error: nil)
  }

  var addCardViewControllerTitle: String {
    return NSLocalizedString("AddCardViewModel.addCardViewControllerTitle", value: "Add Card",
      comment: "AddCard ViewController adding Card title")
  }

  var addCardButtonTitle: String {
    return NSLocalizedString("AddCardViewModel.addCardButtonTitle", value: "Add",
      comment: "Add Credit Card button title")
  }

  var alertViewErrorAddingCard: String {
    return NSLocalizedString("AddCardViewModel.alertViewErrorAddingCard", value: "Ooops",
      comment: "Error adding Card alert confirmation")
  }

  var alertViewErrorAddingButtonTitle: String {
    return NSLocalizedString("AddCardViewModel.alertViewErrorAddingButtonTitle", value: "Ok",
      comment: "Error adding Card alert confirmation")
  }

  var alertViewErrorAddingAlertMessage: String {
    return NSLocalizedString("AddCardViewModel.alertViewErrorAddingAlertMessage", value: "We could not add your card. Please try again.",
      comment: "Error adding Card alert confirmation")
  }

  var alertViewErrorAddingAlertStipeSetupMessage: String {
    return NSLocalizedString("AddCardViewModel.alertViewErrorAddingAlertStipeSetupMessage", value: "Unfortunately payment gateway is not setup. Please try again.",
      comment: "Error adding Card alert confirmation")
  }
  
  var alertViewFailToAddCardTitle: String {
    return NSLocalizedString(
      "AddCardViewModel.alertViewFailToAddCardTitle",
      value: "Ooops",
      comment: "title for alert view that appears when adding a Payment card fails")
  }
  
  var alertViewFailToAddCardMessage: String {
    return NSLocalizedString(
      "AddCardViewModel.alertViewFailToAddCardMessage",
      value: "We could not add your card. Please try again.",
      comment: "title for alert view that appears when adding a Payment card fails")
  }
  
  var alertViewFailToAddCardActionButtonTitle: String {
    return NSLocalizedString(
      "AddCardViewModel.alertViewFailToAddCardMessage",
      value: "Ok",
      comment: "title for alert view that appears when adding a Payment card fails")
  }

  var alertViewDisclaimerActionButtonTitle: String {
    return NSLocalizedString(
      "AddCardViewModel.alertViewDisclaimerActionButtonTitle",
      value: "Secure Payment",
      comment: "title for disclaimer alert view that appears when adding a Payment card fails")
  }
  
  var alertViewDisclaimerActionButtonMessage: String {
    return NSLocalizedString(
      "AddCardViewModel.alertViewDisclaimerActionButtonMessage",
      value: "We use STRIPE for easy, safe and encrypted payments. STRIPE is an international leading payment processor trusted by thousands of companies like Facebook, Twitter, Adidas, Best Buy and many more.",
      comment: "title for disclaimer alert view that appears when adding a Payment card fails")
  }
  
  var alertViewDisclaimerActionButtonActionButtonTitle: String {
    return NSLocalizedString(
      "AddCardViewModel.alertViewDisclaimerActionButtonActionButtonTitle",
      value: "Ok",
      comment: "title for disclaimer alert view that appears when adding a Payment card fails")
  }
}
