//
//  AddressProfileViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/2/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps

class AddressProfileViewModel {
  var analyticsCategory = AnalyticsCategories.InMenuMyLocations
  
  // If this view model has an address object, Then this means that
  // we are editing an already established address book
  // If not then the corresponding View Controller
  // will behave as it's creating a new address
  var address: Address? = nil
  
  var currentAddress: Address? = nil
  
  var isNewAddress: Bool {
    return address == nil
  }
  
  var addressTypeNotSet: Bool {
    return currentAddress?.type == nil
  }
  
  func actionButtonTitleForMode(mode: AddressProfileViewControllerMode) -> String {
    switch mode {
    case .ViewAddress:
      fallthrough
    case .EditAddressDetails:
      fallthrough
    case .EditAddressCoordinates:
      return "Use this location"
    }
  }
  
  /// **!!!** Currently verification is done on the LocationsMapViewController level
  var areCurrentAddressInformationsVerified: Bool {
    // TODO: Verify Address
    return true
  }
  
  func addTypeToCurrentAddress(addressType: AddressType) {
    self.currentAddress = Address.addressFromAddressAndType(
      self.currentAddress, addressType: addressType)
  }
  
  func updateAddressAPICall(completion: (success: Bool, address: Address?, error: NSError?) -> Void) {
    signedRequest(
      BlinkMyCarAPI.UpdateAddress(id: address!.addressID!, area: address?.area?.name, city: address?.city?.id, comment: currentAddress?.comments, coordinates: currentAddress?.coordinates?.coordinatesToString(), country: address?.country?.name, street: currentAddress?.street, building: currentAddress?.building, userBuilding: currentAddress?.userBuilding, userFloorNumber: currentAddress?.userFloorNumber,  floor: nil, isIndoor: currentAddress?.isIndoor, landmark: address?.landMark, locationType: address?.locationType?.jsonName, name: currentAddress?.name, spotNumber: address?.spotNumber, type: currentAddress?.type?.jsonName),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 200 {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.UpdateAddressFailed.rawValue,
            userInfo: ["error":"Fail to Edit an address"])
          completion(success: false, address: nil, error: error)
          return
        }
        
        do {
          let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!,
            options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject]
          let address = Address.fromJSON(jsonDictionary!) as! Address
          self.address = address
          Address.saveAddress(address)
          
          completion(success: true, address: address, error: nil)
        } catch {
          completion(success: false, address: nil, error: nil)
        }
    })
  }
  
  func affectedBookings() -> [Booking]? {
    let affectedBookings = Booking.getSavedBookingsForAddressId(address!.addressID!).filter({
      if let status = $0.status {
        switch status {
        case .Confirmed, .Pending, .Modified:
          return true
        default:
          return false
        }
      } else {
        return false
      }
    })
    return (affectedBookings.count > 0) ? affectedBookings : nil
  }
  
  func addAddressAPICall(completion: (success:Bool, address: Address?, error: NSError?) -> Void) {
    let area: String? = nil
    let city: String? = "Beirut"
    //TODO make sure that we should be sending the 'addressCarLocation' in 'comment' place to the API
    let comment: String? = self.currentAddress?.comments
    let country: String? = "LB"
    let landmark: String? = self.currentAddress?.landMark
    let locationType: String? = nil
    let name: String? = self.currentAddress?.name
    let spotNumber: String? = nil
    let type: String? = self.currentAddress?.type?.addressTypeName()
    let floor: String? = nil
    let isIndoor: Bool? = self.currentAddress?.isIndoor
    let coordinates: String? = self.currentAddress?.coordinates?.coordinatesToString()
    let building: String? = self.currentAddress?.building
    let userBuilding: String? = self.currentAddress?.userBuilding
    let userFloorNumber: String? = self.currentAddress?.userFloorNumber
    let street: String? = self.currentAddress?.street
    
    signedRequest(
      BlinkMyCarAPI.AddAddress(
        area: area, city: city , comment: comment, coordinates: coordinates,
        country: country, street: street, building: building, userBuilding: userBuilding, userFloorNumber: userFloorNumber, floor: floor, isIndoor: isIndoor, landmark: landmark,
        locationType: locationType, name: name, spotNumber: spotNumber, type: type),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 201 {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.CreateAddressFailed.rawValue,
            userInfo: ["error":"Fail to Edit an address"])
          completion(success: false, address: nil, error: error)
          return
        }
        
        do {
          let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!,
            options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject]
          let address = Address.fromJSON(jsonDictionary!) as! Address
          self.address = address
          Address.saveAddress(address)
          
          completion(success: true, address: address, error: nil)
        } catch {
          completion(success: false, address: nil, error: nil)
        }
    })
  }
  
  func updateBookingsForUpdatedAddressInformation(affectedBookings: [Booking]) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
      { () -> Void in
        PersistenceManager.sharedManager.updateBookingsAddressInfo(
          self.address!,
          bookingIndex: 0,
          bookings: affectedBookings,
          completionBlock: { (address: Address, success: Bool, error: NSError?) -> Void in
            if !success {
              Address.saveAddressIdForUpdatingRelatedBookings(address.addressID!)
              return
            }
            
            NSNotificationCenter.defaultCenter().postNotificationName(
              BMCNotificationNames.DidUpdateBookings.rawValue,
              object: nil)
        })
    })
  }

  func isAddressServiceable(address: Address?) -> Bool {
    return areCoordinatesServiceable(address?.coordinates)
  }

  func areCoordinatesServiceable(coordinates: CLLocationCoordinate2D?) -> Bool {
    if let coordinates = coordinates, let overlay = Country.getCountry()?.overlay
      where overlay.polygons.count > 0 {
      for polygon in overlay.polygons {
        let dynamicPath: GMSMutablePath = GMSMutablePath()
        for polygonCoordinate in polygon {
          dynamicPath.addCoordinate(polygonCoordinate)
        }

        if GMSGeometryContainsLocation(coordinates, dynamicPath, true) {
          return true
        }
      }
    }
    return false
  }
  
  func viewedAddressesForFunctionality(functionality: AddressProfileViewController.ViewControllerFunctionality) -> [Address] {
    switch functionality {
    case .BookAService:
      return Address.getAddressesWithCoordinates()
    case .Default:
      guard let address = address else {
        return [Address]()
      }
      return [address]
    }
  }

  //===================================================
  // MARK: U I   T E X T
  //===================================================
  
  var alertViewAddressNotServiceableNotificationAlertTitle: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewServiceAreaNotificationAlertTitle",
      value: "",
      comment: "Alert title that shows when user selects OTHER from the AREA dropdown and inputs a custom-area then presses save.")
  }
  
  var alertViewAddressNotServiceableNotificationAlertMessage: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewServiceAreaNotificationAlertMessage",
      value: "",
      comment: "Alert title that shows when user selects OTHER from the AREA dropdown and inputs a custom-area then presses save.")
  }
  
  var alertViewAddressNotServiceableNotificationAlertButtonText: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewServiceAreaNotificationAlertButtonText",
      value: "",
      comment: "Alert title that shows when user selects OTHER from the AREA dropdown and inputs a custom-area then presses save.")
  }
  
  var alertViewFailToAddOrUpdateAddressAlertTitle: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewFailToAddOrUpdateAddressAlertTitle",
      comment: "Alert title that shows when error occures")
  }
  
  var alertViewFailToAddOrUpdateAddressAlertMessage: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewFailToAddOrUpdateAddressAlertMessage",
      comment: "Alert message that shows when error occures")
  }
  
  var alertViewFailToAddOrUpdateAddressActionButtonText: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewFailToAddOrUpdateAddressActionButtonText",
      comment: "Alert action button text that shows when error occures")
  }
  
  var alertViewAffectedBookingsTitle: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewAffectedBookingsTitle",
      value: "Related Bookings",
      comment: "Alert title that shows when changing the address Details Affect Bookings")
  }
  
  var alertViewAffectedBookingsMessage: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewAffectedBookingsMessage",
      value: "Your changes will affect your upcoming bookings.",
      comment: "Alert Message that shows when changing the address Details Affect Bookings")
  }
  
  var alertViewAffectedBookingsActionButtonText: String {
    return NSLocalizedString("AddressProfileViewModel.alertViewAffectedBookingsActionButtonText",
      value: "Ok",
      comment: "Alert Action Button Titlte, that shows when changing the address Details Affect Bookings")
  }
}
