//
//  CustomerServiceViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/28/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class CustomerServiceViewModel {
  
  var data: [FilterValue] = []
  var description: String?
  var selectedSupportType: String?
  
  init() {
   self.initializeWithDummyData()
   self.canCallNow()
  }
  
  private func initializeWithDummyData() {
    var value: FilterValue = FilterValue()
    value.label = "Feedback"
    value.value = "FEEDBACK"
    data.append(value)
    
    value = FilterValue()
    value.label = "Complaint"
    value.value = "COMPLAINT"
    data.append(value)
    
    value = FilterValue()
    value.label = "Other"
    value.value = "OTHER"
    data.append(value)
  }
  
  var requestTypeViewModel : PickerTextFieldViewModel? {
    get{
      return PickerTextFieldViewModel(
        components: data, allItem: false)
    }
  }

  func sendCustomerRequest(completion: (error: NSError?) -> Void) {
    signedRequest(BlinkMyCarAPI.CustomerSupport(type: selectedSupportType!, body: description!, bookingId: nil),
      completion: { (data, statusCode, response, error) -> () in
        if statusCode != 201 {
          completion(error: error)
          return
        }

        completion(error: nil)
    })
  }
  
  func canCallNow() -> Bool {
    let date = NSDate()
    let calendar = NSCalendar.currentCalendar()
    let components = calendar.components([.Hour, .Minute], fromDate: date)
    let hour = components.hour

    if hour >= 9 && hour <= 17 {
      return true
    }
    
    return false
  }
  
  func valueForRequestType(requestTypeLabelValue: String) -> String? {
    var filterResult: [FilterValue] = data.filter { $0.label == requestTypeLabelValue }
    if filterResult.count >= 1 {
      return filterResult[0].value as? String
    }
    return nil
  }
  //===========================================================
  // MARK: U I   T E X T
  //===========================================================

  var phoneNumber: String {
    return GeneralSettings().telephoneNumber
  }
  
  var callingPhoneNumber: String {
    let temp = phoneNumber.stringByReplacingOccurrencesOfString("+", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
    return temp.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
  }
  
  var callingPeriod: String {
    return "( 9AM - 6PM )"
  }
  
  var viewControllerTitle: String {
    return NSLocalizedString(
      "CustomerServiceViewController.ViewControllerTitle",
      value: "Customer Support",
      comment: "Customer Service view controller title that appears in the navigation Bar")
  }
  
  var giveUsACallLabelText: String {
    return NSLocalizedString("CustomerServiceViewController.giveUsACallLabelText",
      comment: "Customer Service view controller give us a call label title")
  }
  
  var callNowButtonTitle: String {
    return NSLocalizedString("CustomerServiceViewController.callNowButtonTitle",
      comment: "Customer Service view controller call now button title")
  }

  var saveButtonTitle: String {
    return NSLocalizedString("CustomerServiceViewController.saveButtonTitle",
      value: "Submit",
      comment: "Customer Service view controller save bottom at the button title")
  }

  var requestTypeLabelText: String {
    return NSLocalizedString("CustomerServiceViewController.requestTypeLabelText",
      comment: "Customer Service view controller request Type label title, above requestType textfield")
  }
  
  var descriptionLabelText: String {
    return NSLocalizedString("CustomerServiceViewController.descriptionLabelText",
      comment: "Customer Service view controller description Label title, above description textfield")
  }
  
  var descriptionTextFieldPlaceHolder: String {
    return NSLocalizedString("CustomerServiceViewController.descriptionTextFieldPlaceHolder",
      comment: "Customer Service view controller description Text placeholder")
  }

  var successAlertTitle: String {
    return NSLocalizedString("CustomerServiceViewController.successAlertTitle",
      comment: "Title of the alert when we submit successfully")
  }

  var successAlertMessage: String {
    return NSLocalizedString("CustomerServiceViewController.successAlertMessage",
      comment: "Message of the alert when we submit successfully")
  }

  var failAlertTitle: String {
    return NSLocalizedString("CustomerServiceViewController.failAlertTitle",
      comment: "Title of the alert when we failt to submit")
  }

  var failAlertMessage: String {
    return NSLocalizedString("CustomerServiceViewController.failAlertMessage",
      comment: "Message of the alert when we failt to submit")
  }
  
  var cantSubmitAlertTitle: String {
    return NSLocalizedString("CustomerServiceViewController.cantSubmitAlertTitle",
      comment: "Title of the alert when we cant submit because there is no data")
  }
  
  var cantSubmitAlertMessage: String {
    return NSLocalizedString("CustomerServiceViewController.cantSubmitAlertMessage",
      comment: "Message of the alert when we cant submit because there is no data")
  }

  var cantSubmitAlertOkButton: String {
    return NSLocalizedString("CustomerServiceViewController.cantSubmitAlertOkButton",
      comment: "Ok button label of the alert when we cant submit because there is no data")
  }

  var callAlertTitle: String {
    return NSLocalizedString("CustomerServiceViewController.callAlertTitle",
      value: "Call Us",
      comment: "Alert View Title, Shown When we the user taps on call us")
  }

  var callAlertMessage: String {
    return NSLocalizedString("CustomerServiceViewController.callAlertMessage",
      value: "You are about to call the customer service. Would you like to proceed?",
      comment: "Alert View message, Shown When we the user taps on call us")
  }

  var callAlertYesButton: String {
    return NSLocalizedString("CustomerServiceViewController.callAlertYesButton",
      value: "Yes",
      comment: "Alert View yes button, Shown When we the user taps on call us")
  }

  var callAlertNoButton: String {
    return NSLocalizedString("CustomerServiceViewController.callAlertNoButton",
      value: "No",
      comment: "Alert View no button, Shown When we the user taps on call us")
  }
}
