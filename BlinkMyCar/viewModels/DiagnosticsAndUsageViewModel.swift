//
//  DiagnosticsAndUsageViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public class DiagnosticsAndUsageViewModel {
  public enum DiagnosticsAndUsageViewModelItems {
    case SendDataUsage
    
    var sendDataUsageText: String {
      return NSLocalizedString("DiagnosticsAndUsageViewController.sendDataUsageText",
        comment: "Diagnostics And Usage - send data usage toggle")
    }
    
    func toString() -> String {
      switch(self) {
      case SendDataUsage:
        return sendDataUsageText
      }
    }
  }
  
    var data: [DiagnosticsAndUsageViewModelItems] = []
    var sendDataUsage: Bool = false
  
  init() {
    data.append(.SendDataUsage)
  }
  
  func itemAtRow(row: Int) -> DiagnosticsAndUsageViewModelItems {
    return data[row]
  }
  
  func setValueFor(item: DiagnosticsAndUsageViewModelItems, value: Bool) {
    switch item {
    case .SendDataUsage:
      var settings = GeneralSettings()
      settings.sendUsageData = value
    }
  }
  
  var viewControllerTitle: String {
    return NSLocalizedString("DiagnosticsAndUsageViewController.viewControllerTitle",
      comment: "Diagnostics And Usage View title")
  }
  
  var headerMessage: String {
    return NSLocalizedString("DiagnosticsAndUsageViewController.headerDetailedMessage",
      comment: "Diagnostics And Usage View title")
  }
  
}

//MARK: Table Functionality Handler
extension DiagnosticsAndUsageViewModel {
  
  func numberOfRows() -> Int {
    return data.count
  }
  
  func cellForRow(indexPath: NSIndexPath)  -> String {
    let item: DiagnosticsAndUsageViewModelItems = self.itemAtRow(indexPath.row)
    return item.toString()
  }
}