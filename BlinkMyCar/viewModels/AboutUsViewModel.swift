//
//  AboutUsViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/6/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class AboutUsViewModel {
  let fbUrl: String = "https://www.facebook.com/blinkmycar"
  let instaUrl: String = "https://instagram.com/blinkmycar/"
  let twitterUrl: String = "https://twitter.com/blinkmycar"
  let termsAndConditionsUrl: String = "http://www.blinkmycar.com/terms-and-conditions-mobile"
  let privacyPolicyUrl: String = "http://www.blinkmycar.com/privacy-policy-mobile"
  
  var vcTitle: String {
    return NSLocalizedString("AboutUsViewController.title",
      comment: "About Us View controller title")
  }
  
  var playButtonTitle: String {
    return NSLocalizedString("AboutUsViewController.playButtonTitle",
      comment: "About Us View controller play button text")
  }
  
  var mainText: String {
    return NSLocalizedString("AboutUsViewController.mainText",
      comment: "About Us View controller text found below the video in the header")
  }
  
  var secondaryParagraphText: String {
    return NSLocalizedString("AboutUsViewController.secondaryParagraphText",
      comment: "About Us View controller paragraph found below the main text")
  }
  
  var termsAndConditions: String {
    return NSLocalizedString("AboutUsViewController.termsAndConditions",
      comment: "About Us View controller terms And Conditions")
  }
  
  var privacyPolicy: String {
    return NSLocalizedString("AboutUsViewController.privacyPolicy",
      comment: "About Us View controller privacy Policy")
  }
  
  var legalNotices: String {
    return NSLocalizedString("AboutUsViewController.legalNotices",
      comment: "About Us View controller legal Notices")
  }
}
