//
//  ChooseYourRideViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/8/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class ChooseYourRideViewModel {
  var analyticsCategory = AnalyticsCategories.InappChooseVehicle
  
  var shouldDisplayFreeWashAlert: Bool {
    return Vehicle.getSavedVehicles().count <= 0
  }
  
  var freeWashAlertViewTitle: String {
    return NSLocalizedString("ChooseYourRideViewController.freeWashAlertViewTitle",
      value: "FIRST WASH FOR FREE",
      comment: "title for alert message that appears when creating a new ride, if the user is creating his first ride")
  }
  
  var freeWashAlertViewMessage: String {
    return NSLocalizedString("ChooseYourRideViewController.freeWashAlertViewMessage",
      value: "Add your vehicle and book your FREE wash now!",
      comment: "message for alert message that appears when creating a new ride, if the user is creating his first ride")
  }
  
  var freeWashAlertViewActionButtonTitle: String {
    return NSLocalizedString("ChooseYourRideViewController.freeWashAlertViewActionButtonTitle",
      value: "Ok",
      comment: "action button title for alert message that appears when creating a new ride, if the user is creating his first ride")
  }
  
  var chooseARideTitle: String {
    return NSLocalizedString("ChooseYourRideViewController.titleLabel",
      comment: "The text label in the nav bar")
  }
  
  var carTitle: String {
    return NSLocalizedString("ChooseYourRideViewController.carTitleLabel",
      comment: "The text label for the item")
  }
  
  var bikeTitle: String {
    return NSLocalizedString("ChooseYourRideViewController.bikeTitleLabel",
      comment: "The text label for the item")
  }
  
  var fleetTitle: String {
    return NSLocalizedString("ChooseYourRideViewController.fleetTitleLabel",
      comment: "The text label for the item")
  }
}
