//
//  WashInProgressViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 8/7/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public class WashInProgressViewModel {
  public var booking: Booking!

  public var washerName: String {
    if let washer = booking.washer {
      return washer.isEmpty ? "Ibrahim" : washer
    } else {
      return "Moustapha"
    }
  }

  public var vehicleDisplayInformation: String {
    return booking.vehicle?.displayedModel ?? "My Car"
  }

  public var viewControllerTitle: String {
    return NSLocalizedString("WashInProgressViewController.viewControllerTitle",
      comment: "Wash In Progress View Controller Title")
  }

  public var washerActionDescription: String {
    return NSLocalizedString("WashInProgressViewController.washerActionDescription",
      comment: "A text that comes between the washer name and the car name stating that the washer is pampering your vehicle")
  }

  public var descriptionText: String {
    return NSLocalizedString("WashInProgressViewController.descriptionText",
      comment: "A text that comes under the amount of water saved")
  }

  public var washIsCompleted: Bool {
    return booking.status == .Completed
  }
}
