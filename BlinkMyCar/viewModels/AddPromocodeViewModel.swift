//
//  AddPromocodeViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/30/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

class AddPromocodeViewModel {
  var preSelectedPromocode: String? = nil
  
  var viewControllerTitle: String {
    return NSLocalizedString(
      "AddPromocodeViewController.viewControllerTitle",
      value: "Promo Code",
      comment: "Add Promo code view controller title")
  }
  
  var emptyPromocodeAlertViewTitle: String {
    return NSLocalizedString(
      "AddPromocodeViewController.emptyPromocodeAlertViewTitle",
      value: "Ooops",
      comment: "Title for alert view that appears when trying to apply an empty promocode")
  }
  
  var emptyPromocodeAlertViewMessage: String {
    return NSLocalizedString(
      "AddPromocodeViewController.emptyPromocodeAlertViewMessage",
      value: "Add a promocode so we can apply it to your booking!",
      comment: "Message for alert view that appears when trying to apply an empty promocode")
  }
  
  var emptyPromocodeAlertViewActionButtonTitle: String {
    return NSLocalizedString(
      "AddPromocodeViewController.emptyPromocodeAlertViewMessage",
      value: "Ok",
      comment: "title for action button alert view that appears when trying to apply an empty promocode")
  }
  
  var alertMessageOkActionButtonTitle: String {
    return NSLocalizedString(
      "AddPromocodeViewController.alertMessageOkActionButtonTitle",
      value: "Ok",
      comment: "title for action button alert view")
  }
  
  var labelText: String {
    return NSLocalizedString(
      "AddPromocodeViewController.labelText",
      value: "ADD A PROMO CODE",
      comment: "Label that appears just above the promocode text field")
  }
  
  var textFieldPlaceholder: String {
    return NSLocalizedString(
      "AddPromocodeViewController.textFieldPlaceholder",
      value: "PROMOCODE",
      comment: "promocode text field placeholder")
  }
  
  var actionButtonTitle: String {
    return NSLocalizedString(
      "AddPromocodeViewController.actionButtonTitle",
      value: "Apply",
      comment: "Apply button title")
  }
}
