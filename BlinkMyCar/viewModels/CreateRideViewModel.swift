//
//  CreateRideViewModel.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public enum CreateRideViewMode {
  case Make
  case Model
  case Color
  case PlateNumber
  
  var stepName: String {
    switch self {
    case .Make:
      return "Make"
    case .Model:
      return "Model"
    case .Color:
      return "Color"
    case .PlateNumber:
      return "Plate Number"
    }
  }
}

public class CreateRideViewModel {
  public var analyticsCategory = AnalyticsCategories.InappChooseVehicle
  
  public var fullProcess: Bool = true
  
  public var viewMode: CreateRideViewMode = .Make {
    didSet {
      prevViewMode = oldValue
    }
  }
  public var prevViewMode: CreateRideViewMode = .Make
  private var rideType: Make.VehicleType
  public var makeViewModel: RideMakeViewModel
  public var modelViewModel: RideModelViewModel? = nil
  private var plateNumberViewModel: PlateNumberViewModel? = nil
  private lazy var colorViewModel = RideColorViewModel()
  private var _isDataLoaded = false

  public var imagePath: String? = nil
  public var selectedMake: Make? = nil
  public var selectedModel: String? = nil
  public var selectedColor: RideColor? = nil
  public var userSetcarName: String? = nil
  public var plateNumber: String? = nil
  public var plateNumberFormatted: String? {
    get {
      if let plateNumber = plateNumber {
        return plateNumber
      }
      return nil
    }
  }
  public var uploadImageData: NSData? = nil
  //Mode Related Varaibles
  public var makeName: String?
  
  public var saveButtonTitle: String {
    get {
      return plateNumberViewModel?.saveButton ?? ""
    }
  }

  public var skipButtonTitle: String {
    get {
      return plateNumberViewModel?.skipButton ?? ""
    }
  }
  
  public var isComplete: Bool {
    get {
      if let _ = selectedMake, _ = selectedModel, _ = selectedColor {
        return true
      }
      return false
    }
  }

  public var title:String {
    get {
      switch viewMode {
      case .Make:
        return makeViewModel.title
      case .Model:
        return modelViewModel!.title
      case .Color:
        return colorViewModel.title
      case .PlateNumber:
        return plateNumberViewModel!.title
      }
    }
  }
  
  public var analyticsScreenName: String {
    switch viewMode {
    case .Make:
      return "Choose Brand"
    case .Model:
      return "Choose Model"
    case .Color:
      return "Choose Color"
    case .PlateNumber:
      return "Plate Number"
    }
  }

  public var carName:String {
    get {
      if let carName = self.userSetcarName {
        return carName
      }
      switch viewMode {
      case .Make:
        if rideType == Make.VehicleType.Car {
          return NSLocalizedString("VehicleInfoViewController.myCarName",
            comment: "The car name placeholder")
        }else {
          //Motorcycle
          return NSLocalizedString("VehicleInfoViewController.myMotorcycleName",
            comment: "The Motorcycle name placeholder")
        }
      case .Model, .Color, .PlateNumber:
        return NSLocalizedString("VehicleInfoViewController.myVehicleName",
          comment: "My ANY_VEHICLE_NAME") + " \(selectedMake!.name)"
      }
    }
  }
  
  public var searchTextPlaceHolder:String {
    get {
      switch viewMode {
      case .Make:
        return makeViewModel.searchTextPlaceHolder
      case .Model:
        return modelViewModel!.searchTextPlaceHolder
      case .Color, .PlateNumber:
        return ""
      }
    }
  }

  public var question:String {
    get {
      switch viewMode {
      case .Make:
        return makeViewModel.question
      case .Model:
        return modelViewModel!.question
      case .Color:
        return colorViewModel.question
      case .PlateNumber:
        return plateNumberViewModel!.question
      }
    }
  }
  
  public var rideIconName: String {
    get {
      switch(rideType) {
      case Make.VehicleType.Car:
        return "iconCar"
      case Make.VehicleType.Motorcycle:
        return "iconMotorcycle"
      }
    }
  }
  
  public var plateNumberPlaceHolder: String {
    get {
      return plateNumberViewModel?.plateNumberPlaceHolder ?? ""
    }
  }
  
  public var plateNumberLabel: String {
    get {
      return plateNumberViewModel?.plateNumberLabel ?? ""
    }
  }

  public var isDataLoaded: Bool {
    return _isDataLoaded
  }

  public var noDataViewState: VehicleNoDataView.State {
    switch viewMode {
    case .Make:
      return .Make
    default:
      return .Model
    }
  }
  
  public init(rideType: Make.VehicleType) {
    self.rideType = rideType
    makeViewModel = RideMakeViewModel(rideType: rideType)
    plateNumberViewModel = PlateNumberViewModel()
  }

  public func loadData(completion: (success: Bool) -> Void) {
    makeViewModel.loadData { (success: Bool) -> Void in
      self._isDataLoaded = true
      completion(success: success)
    }
  }

  //TODO: Change this method's signature to include more details about possible
  //      errors
  public func createRide(completion: (Bool) -> Void) {
    //TODO: add the right format for the plate number, current one is: Symbole + " " + Number
    var plateInfo: String = ""
    if let plateNumber = plateNumber {
      plateInfo += (plateInfo != "") ? " " : ""
      plateInfo += plateNumber
    }
    
    signedRequest(
      BlinkMyCarAPI.AddVehicle(color: selectedColor!.name,
        make: selectedMake!.name,
        model: selectedModel!,
        name: carName,
        plateNumber: plateInfo,
        type: rideType.rawValue,
        year: nil),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 201 {
          completion(false)
          return
        }

        do {
          if let data = data,
            jsonResult: [String : AnyObject] = try NSJSONSerialization.JSONObjectWithData(data,
              options: NSJSONReadingOptions.AllowFragments) as? [String : AnyObject] {
                var vehicle = Vehicle.fromJSON(jsonResult) as! Vehicle
                if let _ = self.uploadImageData {
                  uploadRequest(BlinkMyCarAPI.Vehicle(id: vehicle.id!), data: (self.uploadImageData ?? NSData()), progress: { (percentage, json, error) -> Void in
                    //TODO: Handle errors
                    if let _ = error {
                      completion(false)
                      return
                    }
                    if percentage != 1 {
                      return
                    }
                    
                    if let jsonData = json as? NSData,
                      jsonResult: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(jsonData,
                        options: NSJSONReadingOptions.AllowFragments) as? [String : AnyObject] {
                      vehicle = Vehicle.fromJSON(jsonResult) as! Vehicle
                      Vehicle.saveVehicle(vehicle)
                      Vehicle.saveCurrentVehicle(vehicle)
                      completion(true)
                    } else {
                      completion(false)
                    }
                  })
                } else {
                  Vehicle.saveVehicle(vehicle)
                  Vehicle.saveCurrentVehicle(vehicle)
                  completion(true)
                }
          }
        } catch {
          completion(false)
        }
    })
  }

  public func goBack() -> Void {
    prevViewMode = viewMode
    switch viewMode {
    case .Model:
      viewMode = .Make
    case .Color:
      viewMode = .Model
    case .PlateNumber:
      viewMode = .Color
    default:
      return
    }
  }
  
  var noData: Bool {
    get {
      switch viewMode {
      case .Make:
        return makeViewModel.numberOfSections() == 0
      case .Model:
        return modelViewModel!.numberOfRows(0) == 0
      default:
        return false
      }
    }
  }
  
  public func numberOfSections() -> Int {
    switch viewMode {
    case .Make:
      return makeViewModel.numberOfSections()
    case .Model:
      return modelViewModel!.numberOfSections()
    case .Color:
      return colorViewModel.numberOfSections()
    case .PlateNumber:
      return 1
    }
  }

  public func numberOfRows(section: Int) -> Int {
    switch viewMode {
    case .Make:
      return makeViewModel.numberOfRows(section)
    case .Model:
      return modelViewModel!.numberOfRows(section)
    case .Color:
      return colorViewModel.numberOfRows(0)
    case .PlateNumber:
      return 0
    }
  }

  public func itemForIndexPath(indexPath: NSIndexPath) -> AnyObject {
    switch viewMode {
    case .Make:
      return makeViewModel.itemForIndexPath(indexPath)
    case .Model:
      return modelViewModel!.itemForIndexPath(indexPath)
    case .Color:
      return colorViewModel.itemForIndexPath(indexPath)
    case .PlateNumber:
      return ""
    }
  }

  public func sectionTitle(section: Int) -> String {
    switch viewMode {
    case .Make:
      return makeViewModel.sectionTitle(section)
    case .Model, .Color, .PlateNumber:
      return ""
    }
  }

  public func isLastItem(indexPath: NSIndexPath) -> Bool {
    switch viewMode {
    case .Make:
      return makeViewModel.isLastItem(indexPath)
    case .Model:
      return modelViewModel!.isLastItem(indexPath)
    case .Color, .PlateNumber:
      return true
    }
  }

  public func search(searchFor: String) {
    switch viewMode {
    case .Make:
      return makeViewModel.search(searchFor)
    case .Model:
      return modelViewModel!.search(searchFor)
    case .Color, .PlateNumber:
      return
    }
  }

  /// return wether should continue process
  public func selectItemAtIndexPath(indexPath: NSIndexPath?, value: String = "") -> Bool {
    switch viewMode {
    case .Make:
      viewMode = .Model
      selectedMake = (indexPath != nil) ? makeViewModel.makeAtIndexPath(indexPath!) :
        Make(name: value, models: [String](), type: rideType, imagePath: "")
      makeViewModel.search("")
      modelViewModel = RideModelViewModel(make: selectedMake!)
      return true
    case .Model:
      viewMode = .Color
      selectedModel = (indexPath != nil) ? modelViewModel!.itemForIndexPath(indexPath!) : value
      modelViewModel!.search("")
      return true
    case .Color:
      viewMode = .PlateNumber
      selectedColor = colorViewModel.itemForIndexPath(indexPath!)
      return true
    case .PlateNumber:
      return false
    }
  }


  //=================================================
  // MARK: UI STRINGS
  //=================================================

  var alertViewFailToAddRideAlertTitle: String {
    return NSLocalizedString("CreateRideViewController.FailToAddRideAlertViewTitle",
      comment: "Title in alert view that shows If registering the ride failed")
  }

  var alertViewFailToAddRideAlertMessage: String {
    return  NSLocalizedString("CreateRideViewController.FailToAddRideAlertViewMessage",
      comment: "Message of alert view that shows If registering the ride failed")
  }

  var alertViewFailToAddRideAlertActionButtonText: String {
    return NSLocalizedString("CreateRideViewController.FailToAddRideAlertViewActionButtonText",
      comment: "Action Button text of alert view that shows If registering the ride failed")
  }

  var alertViewFailToLoadModelsAlertTitle: String {
    return NSLocalizedString("CreateRideViewController.alertViewFailToLoadModelsAlertTitle",
      comment: "Title in alert view that shows when loading models fails")
  }
  
  var alertViewFailToLoadModelsAlertMessage: String {
    return  NSLocalizedString("CreateRideViewController.alertViewFailToLoadModelsAlertMessage",
      comment: "Message of alert view that shows when loading models fails")
  }
  
  var alertViewFailToLoadModelsAlertActionButtonText: String {
    return NSLocalizedString("CreateRideViewController.alertViewFailToLoadModelsAlertActionButtonText",
      comment: "Action Button text of alert view that shows when loading models fails")
  }
  
  var searchResultNoRecords: String {
    return NSLocalizedString("CreateRideViewController.searchResultNoRecords",
      comment: "Short message that appears when there are no results (models/makes) to display")
  }

  var alertViewEmptyValueAlertTitle: String {
    return NSLocalizedString("CreateRideViewController.alertViewEmptyValueAlertTitle",
      value: "Ooops",
      comment: "Title for alert view that appears when trying to add an empty value for a Make or Model")
  }

  var alertViewEmptyValueAlertMessage: String {
    return NSLocalizedString("CreateRideViewController.alertViewEmptyValueAlertMessage",
      value: "Please write a value before proceeding.",
      comment: "Message for alert view that appears when trying to add an empty value for a Make or Model")
  }

  var alertViewEmptyValueAlertActionButtonText: String {
    return NSLocalizedString("CreateRideViewController.alertViewEmptyValueAlertActionButtonText",
      value: "Ok",
      comment: "Title for Action Button in alert view that appears when trying to add an empty value for a Make or Model")
  }
}
