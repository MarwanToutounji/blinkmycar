//
//  RideMakeViewModel.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/18/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

typealias IndexEntry = (Character?, [Make])

public class RideMakeViewModel {
  private let rideType: Make.VehicleType
  private var allMakes: [Make] = []
  private var dataSourceEntries: [IndexEntry] = []
  private var selectedMake: Make? = nil

  var title = NSLocalizedString("VehicleInfoViewController.chooseBrand",
    comment: "Choose your brand view title")

  var searchTextPlaceHolder = NSLocalizedString("VehicleInfoViewController.searchTextPlaceHolder",
    comment: "The search textfield place holder")

  var question = NSLocalizedString("VehicleInfoViewController.chooseBrandQuestion",
    comment: "Choose your brand question: What brand is it?")

  public init(rideType: Make.VehicleType) {
    self.rideType = rideType
  }

  public func loadData(completion:((success: Bool) -> Void)) {
    signedRequest(BlinkMyCarAPI.CarMakesAndModels, completion: { (data, statusCode, response, error) -> () in
      if statusCode != 200 {
        completion(success: false)
        return
      }

      let jsonDictionary = try! NSJSONSerialization.JSONObjectWithData(data!,
        options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject]
      self.allMakes = Make.arrayMakeFromJSON(jsonDictionary!).filter({
        $0.type == self.rideType
      }).sort({
        $1.name > $0.name
      })

      self.dataSourceEntries = self.buildIndex(self.allMakes)
      completion(success: true)
    })
  }

  private func buildIndex(makes: [Make]) -> [IndexEntry] {
    //Get the First Letter
    let letters = makes.map {
      Character($0.name.substringToIndex($0.name.startIndex.advancedBy(1)).uppercaseString)
    }
    //Make Letters Array Distinct
    let distinctLetters = distinct(letters)

    //Map Letters Distinct array to each word
    return distinctLetters.map { (letter) -> IndexEntry in
      return (letter, makes.filter {
        Character($0.name.substringToIndex($0.name.startIndex.advancedBy(1)).uppercaseString) == letter
        })
    }
  }

  //Make Distinct
  private func distinct<T: Equatable>(source: [T]) -> [T] {
    var unique = [T]()
    for item in source {
      if !unique.contains(item) {
        unique.append(item)
      }
    }
    return unique
  }

  public func sectionTitle(section: Int) -> String {
    let char: Character? = dataSourceEntries[section].0
    if let char = char {
      return String(char)
    }
    return ""
  }

  public func makeFor(name: String) -> Make? {
    let sectionFilter = dataSourceEntries.filter { $0.0 == Character(name.substringToIndex(name.startIndex.advancedBy(1)).uppercaseString) }
    if sectionFilter.count >= 0 {
      let makeFilter = sectionFilter[0].1.filter{ $0.name == name }
      if makeFilter.count > 0 {
        return makeFilter[0]
      }
    }
    return nil
  }
  
  public func makeAtIndexPath(indexPath: NSIndexPath) -> Make {
    return dataSourceEntries[indexPath.section].1[indexPath.row]
  }

  public func itemForIndexPath(indexPath: NSIndexPath) -> String {
    return makeAtIndexPath(indexPath).name
  }

  public func numberOfSections() -> Int {
    return dataSourceEntries.count ?? 0
  }

  public func numberOfRows(section: Int) -> Int {
    return dataSourceEntries[section].1.count ?? 0
  }

  public func isLastItem(indexPath: NSIndexPath) -> Bool {
    let sectionItemsCount: Int = dataSourceEntries[indexPath.section].1.count ?? 0
    if (sectionItemsCount - 1) == indexPath.row {
      return true
    }
    return false
  }

  public func search(searchFor: String) {
    if searchFor.characters.count == 0 {
      dataSourceEntries = buildIndex(allMakes)
      return
    }
    let filteredMakes = allMakes.filter {
      $0.name.rangeOfString(searchFor, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil
      } ?? []
    dataSourceEntries = buildIndex(filteredMakes)
  }

  public func modelViewModelForIndexPath(indexPath: NSIndexPath) -> RideModelViewModel {
    return RideModelViewModel(make: dataSourceEntries[indexPath.section].1[indexPath.row])
  }
}
