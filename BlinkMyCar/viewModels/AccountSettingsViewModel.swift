//
//  AccountSettingsViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/27/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

//===========================================================
// MARK: - AccountSettingsItems (ENUM DECLARATION)
//===========================================================

public enum AccountSettingsItem {
  case Locations
  case Vehicles
  case Profile
  case BookingHistory
  case SendDataUsage
  case EmailNotifications
  
  func displayedText() -> String {
    switch(self) {
    case Locations:
      return NSLocalizedString(
        "AccountSettingsViewController.Locations",
        value: "My Locations",
        comment: "My Account Settings - my locations item text")
    case Vehicles:
      return NSLocalizedString(
        "AccountSettingsViewController.Vehicles",
        value: "My Vehicles",
        comment: "My Account Settings - my vehicles item text")
    case Profile:
      return NSLocalizedString(
        "AccountSettingsViewController.Profile",
        value: "My Profile",
        comment: "My Account Settings - user profile item text")
    case BookingHistory:
      return NSLocalizedString(
        "AccountSettingsViewController.BookingHistory",
        value: "Booking History",
        comment: "My Account Settings - booking history item text")
    case SendDataUsage:
      return NSLocalizedString(
        "AccountSettingsViewController.sendDataUsageText",
        value: "Diagnostics & Usage",
        comment: "My Account Settings - send data usage item text")
    case EmailNotifications:
      return NSLocalizedString(
        "AccountSettingsViewController.emailNotificationsText",
        value: "Email Notifications",
        comment: "My Account Settings - email notifications item text")
    }
  }
  
  var targetViewController: UIViewController? {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    switch self {
    case .Locations:
      return storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.AddressesTableViewController.rawValue)
    case .Vehicles:
      return storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.VehicleListingViewController.rawValue)
    case .Profile:
      return storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.UserProfileViewController.rawValue)
    case .BookingHistory:
      return storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.BookingHistoryViewController.rawValue)
    case .SendDataUsage:
      return storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.DiagnosticsAndUsageViewController.rawValue)
    case .EmailNotifications:
      return nil
    }
  }
  
  var icon: UIImage? {
    var image: UIImage? = nil
    
    switch self {
    case .Profile:
      image = UIImage(named: "profileIcon")
    case .Vehicles:
      image = UIImage(named: "carProfileIcon")
    case .Locations:
      image = UIImage(named: "addressBookIcon")
    case .BookingHistory:
      image = UIImage(named: "bookingHistoryIcon")
    case .SendDataUsage:
      image = UIImage(named: "statsIcon")
    case .EmailNotifications:
      image = UIImage(named: "mailIcon")
    }
    
    return image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
  }
}


//===========================================================
// MARK: - AccountSettingsViewModel (CLASS DECLARATION)
//===========================================================

public class AccountSettingsViewModel {
  var analyticsCategory = AnalyticsCategories.MyAccount
  
  var data: [AccountSettingsItem] = [
    .Locations,
    .Vehicles,
    .Profile,
    .BookingHistory,
    .SendDataUsage
  ]
  var emailNotifications: Bool = false
  var sendDataUsage: Bool = false
  
  init() {
    //TODO: enable Email notifications when we have Push notifications. For now Disable .EmailNotications for now
    //data.append(.EmailNotifications)
  }
  
  func itemAtRow(row: Int) -> AccountSettingsItem {
    return data[row]
  }
  
  func leftImageForRow(row: Int) -> UIImage? {
    return itemAtRow(row).icon
  }
  
  func setValueFor(item: AccountSettingsItem, value: Bool) {
    switch item {
    case .SendDataUsage:
      var settings = GeneralSettings()
      settings.sendUsageData = value
    default:
      break
    }
  }
}

//===========================================================
// MARK: Table Functionality Handler
//===========================================================

extension AccountSettingsViewModel {
  func isLast(row: Int) -> Bool {
    return (data.count - 1) == row
  }
  
  func numberOfRows() -> Int {
    return data.count
  }
  
  func cellForRow(indexPath: NSIndexPath)  -> (title: String, leftImage: UIImage?, showToggle: Bool, toggleValue: Bool) {
    let item: AccountSettingsItem = self.itemAtRow(indexPath.row)
    let toggleValue = item == .SendDataUsage ? GeneralSettings().sendUsageData : false
    return (item.displayedText(), item.icon, (item == .EmailNotifications), toggleValue)
  }
}

//==============================================================
// MARK: - Localization
//==============================================================

extension AccountSettingsViewModel {
  var vcTitle: String {
    return NSLocalizedString(
      "AccountSettingsViewController.title",
      value: "My Account",
      comment: "Account settings view controller title")
  }
  
  var logoutText: String {
    return NSLocalizedString(
      "AccountSettingsViewController.logoutText",
      value: "Sign out",
      comment: "Account settings view controller update profile text")
  }
  
  var alertViewLogoutConfirmationTitle: String {
    return NSLocalizedString(
      "AccountSettingsViewController.alertViewLogoutConfirmationTitle",
      value: "Sign out Confirmation",
      comment: "Account settings view controller Logout confirmation alert title")
  }
  
  var alertViewLogoutConfirmationMessage: String {
    return NSLocalizedString(
      "AccountSettingsViewController.alertViewLogoutConfirmationMessage",
      value: "Are you sure you want to Sign out?",
      comment: "Account settings view controller Logout confirmation alert message")
  }
  
  var alertViewLogoutConfirmationPostiveButtonTitle: String {
    return NSLocalizedString(
      "AccountSettingsViewController.alertViewLogoutConfirmationPostiveButtonTitle",
      value: "Yes",
      comment: "Account settings view controller Logout confirmation alert ok button title")
  }
  
  var alertViewLogoutConfirmationNegativeButtonTitle: String {
    return NSLocalizedString(
      "AccountSettingsViewController.alertViewLogoutConfirmationNegativeButtonTitle",
      value: "No",
      comment: "Account settings view controller Logout confirmation alert cancel butotn title")
  }
}
