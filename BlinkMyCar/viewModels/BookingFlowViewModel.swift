//
//  BookingFlowViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/21/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

private enum BookingFlowMode {
  case Create
  case Edit
}

enum BookingStep: Int {
  case PickServices = 1
  case PickLocation = 2
  case ChooseDate = 3
  case ChooseTime = 4
  case Confirmation = 5
  
  var title: String {
    switch self {
    case .PickServices:
      return NSLocalizedString(
        "BookingStep.PickLocationTitle",
        value: "Pick Services",
        comment: "")
    case .PickLocation:
      return NSLocalizedString(
        "BookingStep.PickLocationTitle",
        value: "Locate\nYour Car",
        comment: "Title that appears under the step number")
    case .ChooseDate:
      return NSLocalizedString(
        "BookingStep.ChooseDateTitle",
        value: "Pick\na Date",
        comment: "Title that appears under the step number")
    case .ChooseTime:
      return NSLocalizedString(
        "BookingStep.ChooseTimeTitle",
        value: "Pick a\nStart Time",
        comment: "Title that appears under the step number")
    case .Confirmation:
      return NSLocalizedString(
        "BookingStep.ConfirmationTitle",
        value: "Confirm",
        comment: "")
    }
  }
}

struct BookingFlowViewModel {
  let currentVehicle = Vehicle.getCurrentVehicle()
  var editedBooking: Booking? 
  
  var selectedServices: [Service]?
  var selectedAddress: Address?
  var selectedDate: ServiceDate?
  var selectedTime: ServiceTime?
  
  var shouldDisplayCancelBookingFlowButton = true
  
  init(editedBooking: Booking? = nil, selectedServices: [Service]? = nil, selectedAddress: Address? = nil, selectedDate: ServiceDate? = nil, selectedTime: ServiceTime? = nil) {
    self.editedBooking = editedBooking
    self.selectedAddress = selectedAddress
    self.selectedServices = selectedServices
    self.selectedDate = selectedDate
    self.selectedTime = selectedTime
  }
  
  //==========================================================
  // MARK: - Flow Utilities
  //==========================================================
  
  var hasSelectedServices: Bool {
    return (selectedServices?.count ?? 0) > 0
  }
  
  //==========================================================
  // MARK: - Public Utilities
  //==========================================================
  
  var haveEditedServices: (value:Bool, selectedServices: [Service]?) {
    guard let selectedServices = selectedServices else {
      return (false, nil)
    }
    
    if let originalServices = editedBooking?.services {
      return (!Service.areServicesArrayTheSame(selectedServices, secondArray: originalServices), selectedServices)
    } else {
      return (false, selectedServices)
    }
  }
  
  var haveEditedAddress: (value:Bool, selectedAddress: Address?) {
    guard let selectedAddress = selectedAddress else {
      return (false, nil)
    }
    
    if let originalAddress = editedBooking?.address {
      return (selectedAddress.addressID! != originalAddress.addressID!, selectedAddress)
    } else {
      return (false, selectedAddress)
    }
  }
  
  var haveEditedDate: (value:Bool, selectedDate: ServiceDate?) {
    guard let selectedDate = selectedDate?.date else {
      return (false, nil)
    }
    
    if let originalDate = editedBooking?.deliveryDate?.date {
      return (selectedDate.compareDate(originalDate) != .OrderedSame, self.selectedDate)
    } else {
      return (false, self.selectedDate)
    }
  }
  
  var haveEditedTime: (value:Bool, selectedTime: ServiceTime?) {
    guard let selectedFromTime = selectedTime?.from else {
      return (false, nil)
    }
    
    if let originalFromTime = editedBooking?.bookingTime?.from {
      return (selectedFromTime != originalFromTime, selectedTime)
    } else {
      return (false, selectedTime)
    }
  }
}
