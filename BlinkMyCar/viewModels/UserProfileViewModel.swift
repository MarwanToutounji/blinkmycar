//
//  UserProfileViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/27/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import libPhoneNumber_iOS

class UserProfileViewModel {
  private var phoneUtil = NBPhoneNumberUtil()
  
  let validateEmail = {(text: String?) -> Bool in
    if let email = text {
      return email.isValidEmail()
    }
    return false
  }
  
  let validatePhoneNumber = {(text: String?) -> Bool in
    if (text != nil && !text!.isEmpty && Int(text!) != nil) {
      var errorPointer:NSError?
      var phoneUtil = NBPhoneNumberUtil()

      var number:NBPhoneNumber
      do {
        number = try phoneUtil.parse(text!,
        defaultRegion:"LB")
        // Check if country code is included in the given text
        let countryCode = number.countryCode.stringValue
        var hasCountryCode = false
        if (countryCode.characters.count+1) < (text!).characters.count {
          let start = text!.startIndex
          let end = text!.startIndex.advancedBy(countryCode.characters.count+1)
          var substring = text!.substringWithRange(start..<end)
          if substring.rangeOfString(countryCode) != nil {
            hasCountryCode = true
          }
        }

        return errorPointer == nil && !hasCountryCode && phoneUtil.isValidNumber(number)
      } catch {
        print("Error parsing number")
      }
    }
    return false
  }
  
  func getFormattedPhoneNumber(phoneNumber: String?) -> String? {
    return NBPhoneNumber.getFormattedPhoneNumber(phoneNumber)
  }
  
  func getFormattedPhoneNumberLocal(phoneNumber: String?) -> String {
    return NBPhoneNumber.getFormattedPhoneNumberLocal(phoneNumber)
  }

  var viewControllerTitle: String {
    return NSLocalizedString("UserProfileViewController.ViewControllerTitle",
      comment: "User Profile view controller title that appears in the navigation Bar")
  }

  var submitButtonTitle: String {
    return NSLocalizedString("UserProfileViewController.submitButtonTitle",
      comment: "User Profile view controller Save / Submit button title")
  }
  
  var labelFirstNameText: String {
    return NSLocalizedString("UserProfileViewController.form.firstNameLabelText", comment: "Text that is displayed above the first name text field in the registration form")
  }
  
  var labelLastNameText: String {
    return NSLocalizedString("UserProfileViewController.form.lastNameLabelText", comment: "Text that is displayed above the last name text field in the registration form")
  }

  var labelEmailText: String {
    return NSLocalizedString("UserProfileViewController.form.emailLabelText", comment: "Text that is displayed above the email text field in the registration form")
  }
  
  var labelPhoneText: String {
    return NSLocalizedString("UserProfileViewController.form.phoneLabelText", comment: "Text that is displayed above the phone text field in the registration form")
  }
  
  var textFieldEmailErrorMessage: String {
    return NSLocalizedString("UserProfileViewController.form.emailErrorMessage", comment: "Error Message that is displayed in case the Email field content is not valid")
  }
  
  var textFieldPhoneErrorMessage: String {
    return NSLocalizedString("UserProfileViewController.form.phoneErrorMessage", comment: "Error Message that is displayed in case the Phone field content is empty")
  }
  
  var alertViewFailToUpdateUserProfileAlertActionButtonText: String {
    return NSLocalizedString("UserProfileViewController.api.alertViewFailToUpdateUserProfileAlertActionButtonText", comment: "OK button displayed in the alert view when API call fails")
  }

  var alertViewFailToUpdateUserProfileAlertTitle: String {
    return NSLocalizedString("UserProfileViewController.api.alertViewFailToUpdateUserProfileAlertTitle", comment: "Title displayed in the alert view when API call fails")
  }
  
  var alertViewFailToUpdateUserProfileAlertMessage: String {
    return NSLocalizedString("UserProfileViewController.api.alertViewFailToUpdateUserProfileAlertMessage", comment: "Message displayed in the alert view when API call fails")
  }

  var alertViewInvalidFormDataAlertActionButtonText: String {
    return NSLocalizedString("UserProfileViewController.form.alertViewInvalidFormDataAlertActionButtonText", comment: "OK button displayed in the alert view when Form has invalid data")
  }
  
  var alertViewInvalidFormDataAlertTitle: String {
    return NSLocalizedString("UserProfileViewController.form.alertViewInvalidFormDataAlertTitle", comment: "Title displayed in the alert view when Form has invalid data")
  }
  
  var alertViewInvalidFormDataAlertMessage: String {
    return NSLocalizedString("UserProfileViewController.form.alertViewInvalidFormDataAlertMessage", comment: "Message displayed in the alert view when Form has invalid data")
  }

  var user: User?
  var emailValue: String = ""
  var phoneNumberValue: String = ""
  var firstNameValue: String = ""
  var lastNameValue: String = ""
  
  init() {
    self.loadUserData()
  }
  
  func loadUserData() {
    self.user = User.getSavedUser()
    self.emailValue = self.user?.email ?? ""
    self.phoneNumberValue = self.user?.mobileNumber ?? ""
    self.firstNameValue = self.user?.firstName ?? ""
    self.lastNameValue = self.user?.lastName ?? ""
  }
  
  func updateProfile(completion: (Bool) -> Void) {
    signedRequest(
      BlinkMyCarAPI.UpdateProfile(firstName: firstNameValue,
        lastName: lastNameValue,
        email: emailValue,
        phoneNumber: phoneNumberValue),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 200 {
          completion(false)
          return
        }

        do {
        if let data = data,
          jsonResult: [String : AnyObject] = try NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) as? [String : AnyObject] {
              let user = User.fromJSON(jsonResult) as! User
              User.saveUser(user)
              //reload view model's user data
              self.loadUserData()
        }
        } catch {}
        
        completion(true)
    })
  }
}