//
//  LocationsMapViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan on 4/5/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

class LocationsMapViewModel: NSObject {
  var alertViewFillRequiredFieldsTitle: String {
    return NSLocalizedString("AddressDetailsView.alertViewFillRequiredFieldsTitle",
      value: "Required Info",
      comment: "title for alert view that appears when the user adds or updates an address and required fields are not set")
  }
  
  
  var alertViewFillRequiredFieldsMessage: String {
    return NSLocalizedString("AddressDetailsView.alertViewFillRequiredFieldsMessage",
      value: "Add your building and car parking details so we can find your car faster!",
      comment: "message for alert view that appears when the user adds or updates an address and required fields are not set")
  }
  
  var alertViewFillRequiredFieldsActionButtonTitle: String {
    return NSLocalizedString("AddressDetailsView.alertViewFillRequiredFieldsActionButtonTitle",
      value: "Ok",
      comment: "action button title for alert view that appears when the user adds or updates an address and required fields are not set")
  }
}
