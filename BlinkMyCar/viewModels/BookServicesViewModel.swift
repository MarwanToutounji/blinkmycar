//
//  BookServicesViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/21/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation


public class BookServicesViewModel {
  let bookingSteps: [BookingStep] = [.PickLocation, .ChooseDate, .ChooseTime]
  
  var selectedAddress: Address?
  var selectedDate: ServiceDate?
  var selectedTime: ServiceTime?
  
  var bookingFlowViewModel: BookingFlowViewModel {
    didSet {
      extractInformationFromBookingFlowViewModel()
    }
  }
  
  func extractInformationFromBookingFlowViewModel() {
    if let editedBooking = bookingFlowViewModel.editedBooking {
      selectedAddress = editedBooking.address
      selectedDate = editedBooking.deliveryDate
      selectedTime = editedBooking.bookingTime
    }
  }
  
  var currentBookingStep: BookingStep = .PickLocation
  
  init(bookingFlowViewModel: BookingFlowViewModel) {
    self.bookingFlowViewModel = bookingFlowViewModel
    extractInformationFromBookingFlowViewModel()
  }
  
  var viewControllerTitle: String! {
    switch currentBookingStep {
    case .PickLocation:
      return NSLocalizedString(
        "BookServicesViewController.viewControllerTitle.PickLocationStep",
        value: "Pick a Location",
        comment: "Book Services View Controller Title For Step")
    case .ChooseDate:
      return NSLocalizedString(
        "BookServicesViewController.viewControllerTitle.ChooseDateStep",
        value: "Pick a Date",
        comment: "Book Services View Controller Title For Step")
    case .ChooseTime:
      return NSLocalizedString(
        "BookServicesViewController.viewControllerTitle.ChooseTimeStep",
        value: "Pick a Start Time",
        comment: "Book Services View Controller Title For Step")
    default:
      return nil
    }
  }
  
  var analyticsScreenName: String {
    switch currentBookingStep {
    case .PickLocation:
      return "Pick A Location"
    case .ChooseDate:
      return "Pick A Date"
    case .ChooseTime:
      return "Pick A Time"
    default:
      return "Book Services"
    }
  }
  
  var indexForCurrentBookingStep: Int {
    return bookingSteps.indexOf(currentBookingStep)!
  }
  
  func bookingProcessStepsStrings() -> [String] {
    return bookingSteps.map({ $0.title })
  }
  
  func bookingProcessViewControllers() -> [UIViewController] {
    var vcs = [UIViewController]()
    for step in bookingSteps {
      vcs.append(viewControllerForStep(step))
    }
    return vcs
  }
  
  func viewControllerForStepIndex(index: Int) -> UIViewController {
    return viewControllerForStep(bookingSteps[index])
  }
  
  private func viewControllerForStep(bookingStep: BookingStep, setSelectionIfAvailable: Bool = true) -> UIViewController! {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    switch bookingStep {
    case .PickLocation:
      let vc = storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.AddressesTableViewController.rawValue) as! AddressesTableViewController
      if setSelectionIfAvailable {
        vc.viewModel.selectedAddress = selectedAddress
      }
      return vc
    case .ChooseDate:
      let vc = storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.DatePickerTableViewController.rawValue) as! DatePickerTableViewController
      if setSelectionIfAvailable {
        vc.viewModel.selectedDate = selectedDate
      }
      return vc
    case .ChooseTime:
      let vc = storyboard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.TimePickerViewController.rawValue) as! TimePickerViewController
      if setSelectionIfAvailable {
        vc.viewModel.selectedTime = selectedTime
      }
      return vc
    default:
      return nil
    }
  }
  
  /// Can only move back
  /// - Returns: Number of steps to move back
  func numberOfStepsComparedToCurrentStep(forBookingStep bookingStep: BookingStep) -> Int {
    let numberOfSteps = currentBookingStep.rawValue - bookingStep.rawValue
    // We would be moving forward and that's not allowed. for now.
    return (numberOfSteps < 0) ? 0 : numberOfSteps
  }
  
  func next() -> (success: Bool, index: Int, isLastStep: Bool)! {
    switch currentBookingStep {
    case .PickLocation:
      if let _ = selectedAddress {
        currentBookingStep = .ChooseDate
        return (success: true, index: indexForCurrentBookingStep, isLastStep: false)
      }
    case .ChooseDate:
      if let _ = selectedDate {
        currentBookingStep = .ChooseTime
        return (success: true, index: indexForCurrentBookingStep, isLastStep: false)
      }
    case .ChooseTime:
      if let _ = selectedTime {
        return (success: true, index: indexForCurrentBookingStep, isLastStep: true)
      }
    default:
      return nil
    }
    
    return (success: false, index: indexForCurrentBookingStep, isLastStep: false)
  }
  
  func previous() -> (success: Bool, index: Int)! {
    switch currentBookingStep {
    case .PickLocation:
      selectedAddress = nil
      selectedDate = nil
      selectedTime = nil
      return (success: false, index: indexForCurrentBookingStep)
    case .ChooseDate:
      selectedDate = nil
      selectedTime = nil
      currentBookingStep = .PickLocation
      return (success: true, index: indexForCurrentBookingStep)
    case .ChooseTime:
      selectedTime = nil
      currentBookingStep = .ChooseDate
      return (success: true, index: indexForCurrentBookingStep)
    default:
      return nil
    }
  }
  
  //==============================================================
  // MARK: -  H E L P E R S
  //==============================================================
  
  var haveNoAddressesAvailable: Bool {
    return Address.getAddresses().count <= 0
  }
  
  func confirmationViewModel() -> BookingConfirmationViewModel {
    let newBookingFlowVM = BookingFlowViewModel(
      editedBooking: bookingFlowViewModel.editedBooking,
      selectedServices: bookingFlowViewModel.selectedServices,
      selectedAddress: selectedAddress,
      selectedDate: selectedDate,
      selectedTime: selectedTime)
    
    let confirmationViewModel = BookingConfirmationViewModel(bookingFlowViewModel: newBookingFlowVM)
    return confirmationViewModel
  }
}
