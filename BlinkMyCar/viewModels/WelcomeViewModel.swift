//
//  WelcomeViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class WelcomeViewModel {
  var welcomeLabelText: String {
    return NSLocalizedString("WelcomeViewViewController.WelcomeLabel",
      comment: "The text label below the logo on the welcome screen")
  }
}
