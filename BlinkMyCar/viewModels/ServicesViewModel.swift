//
//  ServicesViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 5/27/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

/**
 Services are organized by category (String).
 In case several services are bundled together, they are displayed 
 in the view controller as Radio Buttons
*/

// TODO: rethink on the way we are rearranging the services by category and how we specify bundled services

public class ServicesViewModel {
  let currentVehicle = Vehicle.getCurrentVehicle()
  
  var displaySelectedServicesForEditedBooking: Bool = false
  
  var bookingFlowViewModel: BookingFlowViewModel? {
    didSet {
      extractInformationFromBookingFlowViewModel()
    }
  }
  
  
  init() {
    // Do additional stuff here
  }
  
  init(services: [Service]?) {
    retrieveSelectedServices(services)
  }
  
  init(bookingFlowViewModel: BookingFlowViewModel?) {
    self.bookingFlowViewModel = bookingFlowViewModel
    extractInformationFromBookingFlowViewModel()
  }
  
  private var services: [Service] = [Service]() {
    didSet {
      servicesByCategory = Service.servicesByCategory(services)
      
      // TODO: In case there were selected services check if they are still in
      // the list. If not remove them from selection.
      
      addAddOnsForSelectedServices()
    }
  }
  
  var servicesByCategory: (orderedCategories: [Category], dataSource: [Category : Any])?
  
  var selectedServices = [Service]()
  
  var didLoadAtleastOnce = false
  
  var hasServices: Bool {
    return services.count > 0
  }
  
  var hasSelectedServices: Bool {
    return selectedServices.count > 0
  }
  
  var priceInformation: (actualPrice: String?, discountedPrice: String?, note: String?) {
    var price: Float = 0
    for service in selectedServices {
      price += service.price
    }
    return (actualPrice: price.displayAsUsDollarsPrice, discountedPrice: nil,
            note: "(VAT Included)")
  }
  
  func reloadServices(completionBlock: (success: Bool, error: NSError?) -> Void) {
    services = Service.getServicesForVehicleType(currentVehicle!.type!)
    didLoadAtleastOnce = true
    
    if PersistenceManager.sharedManager.didLoadServices {
      
      completionBlock(success: true, error: nil)
    } else {
      PersistenceManager.sharedManager.loadServices({ (success, error) in
        if success {
          // Filter Services By Vehicle Type
          self.services = Service.getServices().filter({ $0.vehicleType == self.currentVehicle!.type })
        }
        completionBlock(success: success, error: error)
      })
    }
  }
  
  func canReloadServices(sender: AnyObject?) -> Bool {
    if !didLoadAtleastOnce && !isSyncing {
      return true
    }
    
    if let _ = sender {
      return true
    }
    
    return !isSyncing
  }
  
  var isSyncing: Bool {
    return PersistenceManager.sharedManager.isSyncing
  }
  
  var numberOfSections: Int {
    return servicesByCategory?.orderedCategories.count ?? 0
  }
  
  func numberOfRowsInSection(section: Int) -> Int {
    guard let servicesByCategory = servicesByCategory else { return 0 }
    let category = servicesByCategory.orderedCategories[section]
    
    if let arr = servicesByCategory.dataSource[category] as? [Any] {
      return arr.count
    } else if let arr = servicesByCategory.dataSource[category] as? [Service] {
      return arr.count
    }
    
    return 0
  }
  
  func sectionInfoForIndex(section: Int) -> (title: String, imageName: String) {
    guard let servicesByCategory = servicesByCategory else {
      return (title: "Category", imageName: "cleanIcon")
    }
    
    let sectionTitle = servicesByCategory.orderedCategories[section]
    var sectionImageName = ""
    switch sectionTitle.lowercaseString {
    case "cleaning":
      sectionImageName = "cleanIcon"
    default:
      // TODO: Get the Maintenance or a general service Icon
      sectionImageName = "cleanIcon"
    }
    
    return (title: sectionTitle, imageName: sectionImageName)
  }
  
  func canSelectRowAtIndexPath(indexPath: NSIndexPath) -> Bool {
    guard let servicesByCategory = servicesByCategory else {
      return false
    }
    
    let category = servicesByCategory.orderedCategories[indexPath.section]
    // Ony supports Check box cells
    if let services = servicesByCategory.dataSource[category] as? [Service] {
      let service = services[indexPath.row]
      return service.availability == .inStock
    } else {
      // Not supported
      return true
    }
  }
  
  func cellInformationForIndexPath(indexPath: NSIndexPath) -> (rowData:Any?, isServiceSelected: Bool? , selectedService: Service?) {
    guard let servicesByCategory = servicesByCategory else {
      return (nil, nil , nil)
    }
    let category = servicesByCategory.orderedCategories[indexPath.section]
    
    if let arr = servicesByCategory.dataSource[category] as? [Any] {
      if let dataForRow = arr[indexPath.row] as? [Service] {
        for service in dataForRow {
          if selectedServices.contains(service) {
            return (arr[indexPath.row], true, service)
          }
        }
      }
      if let dataForRow = arr[indexPath.row] as? Service {
        if selectedServices.contains(dataForRow) {
          return (arr[indexPath.row], true, nil)
        }
      }
      return (arr[indexPath.row], nil, nil)
    } else if let arr = servicesByCategory.dataSource[category] as? [Service] {
      let service = arr[indexPath.row]
      if selectedServices.contains(service) {
        return (arr[indexPath.row], true, nil)
      }
      return (arr[indexPath.row], nil, nil)
    }
    
    return (nil, nil , nil)
  }
  
  func serviceDescriptionForIndexPath(indexPath: NSIndexPath) -> (title: String?, description: String?) {
    guard let servicesByCategory = servicesByCategory else {
      return (nil, nil)
    }
    let category = servicesByCategory.orderedCategories[indexPath.section]
    
    if let arr = servicesByCategory.dataSource[category] as? [Any] {
      if let dataForRow = arr[indexPath.row] as? Service {
        return (dataForRow.name, dataForRow.description)
      }
      return (nil, nil)
    } else if let arr = servicesByCategory.dataSource[category] as? [Service] {
      let service = arr[indexPath.row]
      return (service.name, service.description)
    }
    
    return (nil, nil)
  }
  
  func isLastCellInLastSection(indexPath: NSIndexPath) -> Bool {
    guard let servicesByCategory = servicesByCategory else {
      return false
    }
    
    if indexPath.section == ((servicesByCategory.orderedCategories.count ?? 0) - 1) {
      let category = servicesByCategory.orderedCategories[indexPath.section]
      if let arr = servicesByCategory.dataSource[category] as? [Any] {
        if (arr.count - 1) <= indexPath.row {
          return true
        }
      } else if let arr = servicesByCategory.dataSource[category] as? [Service] {
        if (arr.count - 1) <= indexPath.row {
          return true
        }
      }
    }
    
    return false
  }
  
  func heightForRowAtIndexPath(indexPath: NSIndexPath) -> CGFloat {
    let cellInformation = cellInformationForIndexPath(indexPath)
    
    // switch case for typeCast
    switch cellInformation.rowData {
    case is [Service]:
      return ServiceRadioButtonsTableViewCell.height
    case is Service:
      fallthrough
    default:
      return ServiceCheckBoxTableViewCell.height
    }
  }
  
  
  /// Select / Deselect a service
  /// If the selected service has add ons, the selection state is also affected (Basically if deselected)
  ///
  /// - Returns: Boolean indicating if the service will affect addOns or not
  ///
  func didSelectService(service: Service, withIndexPath indexPath: NSIndexPath, withStatus selected: Bool) -> (addOnsAffected: Bool, addOnsIndexPaths: [NSIndexPath]?) {
    guard let servicesByCategory = servicesByCategory else {
      return (addOnsAffected: false, addOnsIndexPaths: nil)
    }
    let category = servicesByCategory.orderedCategories[indexPath.section]
    
    switch selected {
    case true:
      selectedServices.append(service)
      if service.addOns.count > 0 {
        // Add add-Ons to data source
        if let arr = servicesByCategory.dataSource[category] as? [Any] {
          var array = arr
          for (index, item) in service.addOns.enumerate() {
            array.insert(item, atIndex: indexPath.row+(index+1))
          }
          self.servicesByCategory!.dataSource[category] = array
        } else if let arr = servicesByCategory.dataSource[category] as? [Service] {
          var array = arr
          array.insertContentsOf(service.addOns, at: indexPath.row+1)
          self.servicesByCategory!.dataSource[category] = array
        }
        
        var indexPaths = [NSIndexPath]()
        for index in 0..<service.addOns.count {
          indexPaths.append(NSIndexPath(forRow: indexPath.row+index+1, inSection: indexPath.section))
        }
        return (addOnsAffected: true,
                addOnsIndexPaths: indexPaths)
      }
      return (addOnsAffected: false, addOnsIndexPaths: nil)
    case false:
      selectedServices = selectedServices.filter({ ($0.parentId != service.id) && ($0.id != service.id) })
      // Remove any selected add ons if available
      if service.addOns.count > 0 {
        // Add add-Ons to data source
        if let arr = servicesByCategory.dataSource[category] as? [Any] {
          var array = arr
          for _ in service.addOns {
            array.removeAtIndex(indexPath.row+1)
          }
          self.servicesByCategory!.dataSource[category] = array
        } else if let arr = servicesByCategory.dataSource[category] as? [Service] {
          var array = arr
          for _ in service.addOns {
            array.removeAtIndex(indexPath.row+1)
          }
          self.servicesByCategory!.dataSource[category] = array
        }
        
        var indexPaths = [NSIndexPath]()
        for index in 0..<service.addOns.count {
          indexPaths.append(NSIndexPath(forRow: indexPath.row+index+1, inSection: indexPath.section))
        }
        return (addOnsAffected: true,
                addOnsIndexPaths: indexPaths)
      }
      return (addOnsAffected: false, addOnsIndexPaths: nil)
    }
  }
  
  func isAlreadyInTheBookingFlow() -> Bool {
    return bookingFlowViewModel != nil
  }
  
  func getBookingFlowViewModelForNextStep() -> BookingFlowViewModel {
    let newBookingFlowViewModel = BookingFlowViewModel(
      editedBooking: bookingFlowViewModel?.editedBooking,
      selectedServices: selectedServices,
      selectedAddress: bookingFlowViewModel?.selectedAddress,
      selectedDate: bookingFlowViewModel?.selectedDate,
      selectedTime: bookingFlowViewModel?.selectedTime)
    return newBookingFlowViewModel
  }
  
  var bookServicesViewModel: BookServicesViewModel {
    let vm = BookServicesViewModel(
      bookingFlowViewModel: getBookingFlowViewModelForNextStep())
    return vm
  }
  
  
  //==========================================================
  // MARK: - Rating
  //==========================================================
  
  var bookingToBeRated: Booking? {
    return PersistenceManager.sharedManager.bookingToBeRated
  }
  
  func popBookingThatNeedsRating(booking: Booking? = nil) {
    PersistenceManager.sharedManager.popBookingThatNeedsRating(booking)
  }
  
  
  //========================================================
  // MARK: - HELPERS
  //========================================================
  
  /// Check If selected services have adds ons and add them to services by category
  func addAddOnsForSelectedServices() {
    if selectedServices.count > 0 {
      // !!!: If an add-on has add-ons the following logic will give erronious results
      if let tempServicesByCategory = servicesByCategory {
        for (key, services) in tempServicesByCategory.dataSource {
          if let arr = services as? [Any] {
            var array = arr
            // the array (arr) can contain either [Service] or Service
            for (rowIndex, item) in arr.enumerate().reverse() {
              if let services = item as? [Service] { // Radio buttons
                for service in services {
                  if selectedServices.contains(service) && service.addOns.count > 0 {
                    for (index, service) in service.addOns.enumerate() {
                      array.insert(service, atIndex: (rowIndex+1)+index )
                    }
                    break
                  }
                }
              } else if let service = item as? Service {
                if selectedServices.contains(service) && service.addOns.count > 0 {
                  for (index, service) in service.addOns.enumerate() {
                    array.insert(service, atIndex: (rowIndex+1)+index)
                  }
                }
              }
            }
            
            servicesByCategory!.dataSource[key] = array
          } else if let arr = services as? [Service] {
            var services = arr
            for (rowIndex, service) in arr.enumerate().reverse() {
              if selectedServices.contains(service) && service.addOns.count > 0 {
                for (index, service) in service.addOns.enumerate() {
                  services.insert(service, atIndex: (rowIndex+1)+index)
                }
              }
            }
            servicesByCategory!.dataSource[key] = services
          }
        }
      }
    }
  }
  
  private func extractInformationFromBookingFlowViewModel() {
    if let editedBooking = bookingFlowViewModel?.editedBooking where displaySelectedServicesForEditedBooking {
      self.retrieveSelectedServices(editedBooking.services)
    }
  }
  
  private func retrieveSelectedServices(selectedServices: [Service]?) {
    self.selectedServices.removeAll()
    let services = Service.getServicesForVehicleType(currentVehicle!.type!)
    if let bookingServices = selectedServices {
      for bookingService in bookingServices {
        if services.filter({ $0.sku == bookingService.sku }).first != nil {
          self.selectedServices.append(bookingService)
        }
      }
    }
  }
  
  //========================================================
  // MARK: - Localisation
  //========================================================
  
  let title = NSLocalizedString(
    "ServicesViewController.viewControllerTitle",
    value: "Blink My Car",
    comment: "Services view controller title")
  
  let actionButtonTitle = NSLocalizedString(
    "ServicesViewController.actionButtonTitle",
    value: "Pick a Location",
    comment: "Service VC action button title")
  
  let descriptionAlertActionButtonTitle = NSLocalizedString(
    "ServicesViewController.descriptionAlertActionButtonTitle",
    value: "Ok",
    comment: "action button title for alert that displayes a Service description")
}

