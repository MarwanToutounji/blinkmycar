//
//  RateBookingViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/28/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

class RateBookingViewModel {
  var booking: Booking!
  
  var vehicle: Vehicle {
    return booking.vehicle!
  }
  
  var bookingDate: String {
    return booking.deliveryDate?.displayTextForServiceDate() ?? ""
  }
  
  var bookingServices: String {
    guard let services = booking.services else {
      return ""
    }
    
    var text = ""
    var separator = ""
    for service in services {
      text += "\(separator)\(service.name)"
      separator = "\n"
    }
    return text
  }
  
  var selectedRating: Int?
  
  init(booking: Booking) {
    self.booking = booking
  }
  
  func rateService(completion: (success: Bool, error: NSError?) -> Void) {
    signedRequest(BlinkMyCarAPI.Rating(bookingId: booking.bookingId!, rating: selectedRating!, comment: nil), completion: { (data, statusCode, response, error) -> () in
      if statusCode != 201 && statusCode != 409{
        completion(success: false, error: error)
        return
      }
      
      
      if self.booking.deliveryDate!.isServiceDateOlderThan24hours {
        // As the booking is completed and rated then no need to keep it stored
        // If it was old
        Booking.deleteBookingWithId(self.booking.bookingId!)
      }
      
      completion(success: true, error: error)
    })
  }
  
  var ratingControlTitle: String {
    return NSLocalizedString(
      "RateBookingViewController",
      value: "Rate the Service",
      comment: "Label that appears just above the rating stars")
  }
  
  var alertViewFailToSubmitRatingTitle: String {
    return NSLocalizedString(
      "RateBookingViewController.alertViewFailToSubmitRatingTitle",
      value: "Ooops",
      comment: "Fail to submit rating alert view title")
  }
  
  var alertViewFailToSubmitRatingMessage: String {
    return NSLocalizedString(
      "RateBookingViewController.alertViewFailToSubmitRatingMessage",
      value: "We faced a problem sending your request. Please try again later.",
      comment: "Fail to submit rating alert view Message")
  }
  
  var alertViewFailToSubmitRatingActionButtonTitle: String {
    return NSLocalizedString(
      "RateBookingViewController.alertViewFailToSubmitRatingActionButtonTitle",
      value: "Ok",
      comment: "Fail to submit rating alert view action button title (Ok)")
  }
  
  var alertViewMustRateTitle: String {
    return NSLocalizedString(
      "RatingViewController.alertViewMustRateTitle",
      value: "Rate Service",
      comment: "Must Rate Alert View Title")
  }
  
  var alertViewMustRateMessage: String {
    return NSLocalizedString(
      "RatingViewController.alertViewMustRateMessage",
      value: "Please rate our service and help us improve!",
      comment: "Must Rate Alert View Message")
  }
  
  var alertViewMustRateActionButtonTitle: String {
    return NSLocalizedString(
      "RatingViewController.alertViewMustRateActionButtonTitle",
      value: "Ok",
      comment: "Must Rate Alert View Action Button Title")
  }
}
