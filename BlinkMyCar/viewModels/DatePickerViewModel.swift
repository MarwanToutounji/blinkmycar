//
//  DatePickerViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/22/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

struct DateShortcut {
  let name: String
  let date: ServiceDate
  let enabled: Bool

  init(name: String, date: ServiceDate, enabled: Bool = true) {
    self.name = name
    self.date = date
    self.enabled = enabled
  }

  var displayName: String {
    return name
  }
}

class DatePickerViewModel {
  private let MINIMUM_DISPLAYED_DATES = 7
  
  var timer: NSTimer?
  let bookingTimeOutTime: NSTimeInterval = 300

  var dates: [ServiceDate]? {
    didSet {
      _dates = dates
      _dates?.sortInPlace({ $0.daysFromToday < $1.daysFromToday })
      refreshShortcutsArray()
    }
  }

  var selectedDate: ServiceDate?

  private var _dates: [ServiceDate]?
  
  func reloadDateAndTimes(completionBlock: ((success: Bool, error: NSError?) -> Void)?) {
    signedRequest(
      BlinkMyCarAPI.ServiceDateTime(countryId: "LB"),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 200 {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.RetrieveDatesAndTimesFailed.rawValue,
            userInfo: ["error":"Could not get dates and times"])
          completionBlock?(success: false, error: error)
          return
        }
        
        let jsonDictionary = try! NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject]
        
        self.dates = ServiceDate.serviceDatesFromJSON(jsonDictionary!)
        completionBlock?(success: true, error: error)
    })
  }

  private func fillDisabledDates() {
    if let dates = _dates {
      var correctedDates = [ServiceDate]()
      for (index, date) in dates.enumerate() {
        correctedDates.append(date)

        // if last element in array then finish the loop
        if (index + 1) == dates.count {
          continue
        }

        let fillDates = getDatesBetweenTwoDates(date.date!, date2: dates[index+1].date!)
        var fillServiceDates = [ServiceDate]()
        for fillDate in fillDates {
          let serviceDate = ServiceDate(date: fillDate, daysFromToday: nil, dayOfWeek: nil, times: nil)
          fillServiceDates.append(serviceDate)
        }

        for fillServiceDate in fillServiceDates {
          correctedDates.append(fillServiceDate)
        }
      }

      // Making sure to always Display at least seven dates
      let daysToAdd = MINIMUM_DISPLAYED_DATES - correctedDates.count
      for _ in 0 ..< daysToAdd {
        let addedDate = correctedDates.last!.date!.nextDay
        let serviceDate = ServiceDate(date: addedDate, daysFromToday: nil, dayOfWeek: nil, times: nil)
        correctedDates.append(serviceDate)
      }

      _dates = correctedDates
    }
  }

  private func getDatesBetweenTwoDates(date1: NSDate, date2: NSDate) -> [NSDate] {
    var fillInDates = [NSDate]()

    let calendar = NSCalendar.currentCalendar()
    _ = [NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year]

    let components = NSDateComponents()
    components.day = 1

    var currentDate = date1
    while !currentDate.sameDate(date2) {
      if currentDate.sameDate(date1) {
        currentDate = calendar.dateByAddingComponents(components, toDate: currentDate, options: [])!
        continue
      }

      fillInDates.append(currentDate)
      currentDate = calendar.dateByAddingComponents(components, toDate: currentDate, options: [])!
    }

    return fillInDates
  }

  private var shortcuts = [DateShortcut]()

  var numberOfSections: Int {
    return (shortcuts.count > 0) ? 1 : 0
  }

  var numberOfRows: Int {
    return shortcuts.count
  }

  func dateLabelForRow(row: Int) -> String {
    return shortcuts[row].displayName
  }

  func dateEnabledForRow(row: Int) -> Bool {
    return shortcuts[row].enabled
  }

  func dateForRow(row: Int) -> ServiceDate {
    return shortcuts[row].date
  }

  func shouldSelectRow(row: Int) -> Bool {
    // In case we are editing a booking then the previousely
    // selected date should be highlighted
    let rowDate = shortcuts[row].date
    if let selectedDateDate = selectedDate?.date,
      let rowDateDate = rowDate.date {
        return selectedDateDate.sameDate(rowDateDate) && rowDate.isServiceable
    }
    return false
  }

  var calendarStartDate: NSDate {
    return dates!.first!.date!
  }

  var calendarEndDate: NSDate {
    return dates!.last!.date!
  }

  func serviceDateForDate(date: NSDate) -> ServiceDate? {
    let filteredDates = dates!.filter({ (serviceDate: ServiceDate) in
      let areTheSame = date.sameDate(serviceDate.date!)
      return areTheSame
    })
    return filteredDates.first
  }

  func isDateSelectable(date: NSDate) -> Bool {
    let serviceDate = serviceDateForDate(date)
    return serviceDate != nil
  }

  var indexDates = 0;

  private func refreshShortcutsArray() {
    shortcuts.removeAll(keepCapacity: false)
    if (_dates?.count ?? 0) > 0 {
      indexDates = 0
      let dayOfTheWeek = NSDate().dayOfTheWeek
      switch dayOfTheWeek {
      case .Monday:
        for serviceDate in _dates! {
          if serviceDate.daysFromToday < 6 {
            let status = dateSatus(serviceDate)
            if serviceDate.date!.dayOfTheWeek == .Monday {
              shortcuts.append(DateShortcut(name: "Today, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            } else if serviceDate.date!.dayOfTheWeek == .Tuesday {
              shortcuts.append(DateShortcut(name: "Tomorrow, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            }
          }
        }
      case .Tuesday:
        for serviceDate in _dates! {
          if serviceDate.daysFromToday < 6 {
            let status = dateSatus(serviceDate)
            if serviceDate.date!.dayOfTheWeek == .Tuesday {
              shortcuts.append(DateShortcut(name: "Today, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            } else if serviceDate.date!.dayOfTheWeek == .Wednesday {
              shortcuts.append(DateShortcut(name: "Tomorrow, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            }
          }
        }
      case .Wednesday:
        for serviceDate in _dates! {
          if serviceDate.daysFromToday < 6 {
            let status = dateSatus(serviceDate)
            if serviceDate.date!.dayOfTheWeek == .Wednesday {
              shortcuts.append(DateShortcut(name: "Today, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            } else if serviceDate.date!.dayOfTheWeek == .Thursday {
              shortcuts.append(DateShortcut(name: "Tomorrow, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            }
          }
        }
      case .Thursday:
        for serviceDate in _dates! {
          if serviceDate.daysFromToday < 6 {
            let status = dateSatus(serviceDate)
            if serviceDate.date!.dayOfTheWeek == .Thursday {
              shortcuts.append(DateShortcut(name: "Today, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            } else if serviceDate.date!.dayOfTheWeek == .Friday {
              shortcuts.append(DateShortcut(name: "Tomorrow, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            }
          }
        }
      case .Friday:
        for serviceDate in _dates! {
          if serviceDate.daysFromToday < 6 {
            let status = dateSatus(serviceDate)
            if serviceDate.date!.dayOfTheWeek == .Friday {
              shortcuts.append(DateShortcut(name: "Today, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            } else if serviceDate.date!.dayOfTheWeek == .Saturday {
              shortcuts.append(DateShortcut(name: "Tomorrow, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            }
          }
        }
      case .Saturday:
        for serviceDate in _dates! {
          if serviceDate.daysFromToday < 6 {
            let status = dateSatus(serviceDate)
            if serviceDate.date!.dayOfTheWeek == .Saturday {
              shortcuts.append(DateShortcut(name: "Today, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            }
          }
        }
      case .Sunday:
        for serviceDate in _dates! {
          if serviceDate.daysFromToday < 6 {
            let status = dateSatus(serviceDate)
            if serviceDate.date!.dayOfTheWeek == .Sunday {
              shortcuts.append(DateShortcut(name: "Today, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            } else if serviceDate.date!.dayOfTheWeek == .Monday {
              shortcuts.append(DateShortcut(name: "Tomorrow, \(serviceDate.date!.stringValueMonthAndDay)\(status.displayString)",
                date: serviceDate, enabled: status.isEnabled))
              indexDates += 1
            }
          }
        }
      }

      fillDisabledDates()
      completeDates()
    }
  }

  private func completeDates() {
    if let dates = _dates {
      for i in indexDates ..< dates.count {
        let tempServiceDate = dates[i]
        let status = dateSatus(tempServiceDate)

        shortcuts.append(DateShortcut(
          name: "\(tempServiceDate.date!.stringValueToAttributedText)\(status.displayString)" ,
          date: tempServiceDate,
          enabled: status.isEnabled)
        )
      }
    }
  }

  private func dateSatus(serviceDate: ServiceDate) -> (isEnabled: Bool, displayString: String) {
    let isEnabled = serviceDate.times != nil
    var displayName = ""
    if isEnabled {
      if !serviceDate.isServiceable {
        if !serviceDate.isDateStillAvailable {
          displayName = " (Closed)"
        } else {
          displayName = " (Fully Booked)"
        }
      } else {
        displayName = ""
      }
    } else {
      displayName = " (Closed)"
    }
    return (isEnabled: (isEnabled && serviceDate.isServiceable), displayString: displayName)
  }
}

//=======================================================
// MARK: - BOOKING PROCESS TIMER
//=======================================================

extension DatePickerViewModel {
  func startTimer(target: AnyObject, selector: Selector) {
    stopTimer()
    
    timer = NSTimer(
      timeInterval: bookingTimeOutTime,
      target: target,
      selector: selector,
      userInfo: nil,
      repeats: false)
    NSRunLoop.currentRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
  }
  
  func stopTimer() {
    if timer != nil {
      timer!.invalidate()
      timer = nil
    }
  }
}

extension DatePickerViewModel {
  var failToRetrieveDatesAlertTitle: String {
    return NSLocalizedString(
      "BookAWashDateViewController.failToRetrieveDatesAlertTitle",
      value: "Oooops",
      comment: "Title for alert view that appears when retrieving booking dates fails")
  }
  
  var failToRetrieveDatesAlertMessage: String {
    return NSLocalizedString(
      "DatePickerViewModel.failToRetrieveDatesAlertMessage",
      value: "We could not refresh the available dates!",
      comment: "Message for alert view that appears when retrieving booking dates fails")
  }
  
  var failToRetrieveDatesCancelActionButtonTitle: String {
    return NSLocalizedString(
      "DatePickerViewModel.failToRetrieveDatesCancelActionButtonTitle",
      value: "Cancel",
      comment: "Cancel Action Button title for alert view that appears when retrieving booking dates fails")
  }
  
  var failToRetrieveDatesRetryActionButtonTitle: String {
    return NSLocalizedString(
      "DatePickerViewModel.failToRetrieveDatesCancelActionButtonTitle",
      value: "Retry",
      comment: "Retry Action Button title for alert view that appears when retrieving booking dates fails")
  }
  
  var alertViewBookingTimeOutAlertTitle: String {
    return NSLocalizedString("BookAWashDateViewController.alertViewBookingTimeOutAlertTitle",
                             value: "Booking Process",
                             comment: "Title for alert view that appears when the booking process times out")
  }
  
  var alertViewBookingTimeOutAlertMessage: String {
    return NSLocalizedString("BookAWashDateViewController.alertViewBookingTimeOutAlertMessage",
                             value: "Ooops! The booking process timed out, continue by selecting the wash date!",
                             comment: "Message for alert view that appears when the booking process times out")
  }
  
  var alertViewBookingTimeOutAlertActionButtonText: String {
    return NSLocalizedString("BookAWashDateViewController.alertViewBookingTimeOutAlertActionButtonText",
                             value: "Ok",
                             comment: "Action button Title for alert view that appears when the booking process times out")
  }
}
