//
//  LegalTermsViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 9/9/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

typealias LibraryAcknowledgements = (libraryName: String, acknowledgement: String)

class AcknowledgementsViewModel {
  var acknowledgements: [LibraryAcknowledgements] = [LibraryAcknowledgements]()

  init() {
    if let ackFile = NSBundle.mainBundle().pathForResource("AppAcknowledgements", ofType: "plist"),
      let dict = NSDictionary(contentsOfFile: ackFile)  {
      acknowledgements = retrieveAcknowledgements(dict)
    }
  }

  func retrieveAcknowledgements(originalDict: NSDictionary) -> [LibraryAcknowledgements] {
    var result = [LibraryAcknowledgements]()

    let tempAckDict = originalDict.valueForKey("Acknowledgements") as! [NSDictionary]
    for innerDict in tempAckDict {
      let name = innerDict.valueForKey("Title") as! String
      let content = innerDict.valueForKey("Content") as! String
      let ack = (libraryName: name, acknowledgement: content)
      result.append(ack)
    }

    return result
  }

  func cellInfoForRow(row: Int) -> LibraryAcknowledgements {
    return acknowledgements[row]
  }

  var numberOfSections: Int {
    return (acknowledgements.count > 0) ? 1 : 0
  }

  var numberOfRows: Int {
    return acknowledgements.count
  }

  var viewControllerTitle: String {
    return NSLocalizedString("AcknowledgementsTableViewController.viewControllerTitle", value: "Legal Terms",
      comment: "Legal terms acknowledgements view controller title")
  }

  func acknowledgementViewModelForRow(row: Int) -> AcknowledgementViewModel {
    let info = cellInfoForRow(row)
    let ackViewModel = AcknowledgementViewModel()
    ackViewModel.acknowledgementText = info.acknowledgement
    ackViewModel.acknowledgementName = info.libraryName
    return ackViewModel
  }
}