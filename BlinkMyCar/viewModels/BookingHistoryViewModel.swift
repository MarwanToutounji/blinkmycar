//
//  BookingHistoryViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/6/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class BookingHistoryViewModel {
  enum BMCDateTimeFormatting: String {
    case TimeFormat = "hh:mm"
    case DateFormat = "dd.MM.yyyy"
  }
  
  var currentPage: Page?
  
  let dateFormat: String = BMCDateTimeFormatting.DateFormat.rawValue
  let timeFormat: String = BMCDateTimeFormatting.TimeFormat.rawValue
  var bookings: [Booking] = []

  let title: String = {
    return NSLocalizedString("BookingHistoryViewController.viewControllerTitle",
      comment: "Booking history view controller title")
    }()

  var bookingHistoryNoRecordsMessage: String {
    return NSLocalizedString("BookingHistoryViewController.bookingHistoryNoRecordsMessage",
      comment: "Short message that appears when there are no bookings to display")
  }

  var noData: Bool {
    return !(bookings.count > 0)
  }

  func rideIconName(rideType: Make.VehicleType) -> String {
    switch(rideType) {
    case Make.VehicleType.Car:
      return "iconCar"
    case Make.VehicleType.Motorcycle:
      return "iconMotorcycle"
    }
  }

  func loadBookingHistory(completion: (success: Bool) -> Void) {
    signedRequest(BlinkMyCarAPI.AllBookings, completion: { (data, statusCode, response, error) -> () in
      if statusCode != 200 {
        completion(success: false)
        return
      }

      do {
        if let bookingHistoryJSON = try NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject] {
          
          self.bookings = Booking.bookingsFromJSON(bookingHistoryJSON)
          self.currentPage = Page.pageFromJSON(bookingHistoryJSON)
          
          completion(success: true)
          return
        }
      } catch {}
      
      completion(success:false)
    })
  }
  
  func formattedDate(date: NSDate?) -> String {
    if let date = date {
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = dateFormat
      return dateFormatter.stringFromDate(date)
    }
    return ""
  }
  
  func formattedTime(date: NSDate?) -> String {
    if let date = date {
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = timeFormat
      return dateFormatter.stringFromDate(date)
    }
    return ""
  }
  
  //=====================================================
  // MARK: - Paging related
  //=====================================================
  
  func hasNextPage() -> Bool {
    guard let currentPageURL = currentPage?.mySelf,
      nextPageURL = currentPage?.next where !nextPageURL.isEmpty else {
        return false
    }
    
    return currentPageURL != nextPageURL
  }
  
  func fetchNextPage(completionBlock:(success: Bool, error: NSError?) -> Void) {
    if let nextPage = currentPage?.next, nextPageURL = NSURL(string: nextPage) where !nextPage.isEmpty {
      signedRequest(BlinkMyCarAPI.Next(fullURL: nextPageURL), completion: {
        (data, statusCode, response, error) -> () in
        self.parseBookingsResultData(true, data: data, statusCode: statusCode, completionBlock: completionBlock)
      })
    } else {
      // TODO: Fix error messages
      completionBlock(success: false, error: nil)
    }
  }
  
  func parseBookingsResultData(loadMore: Bool? = false ,data: NSData?, statusCode: Int?, completionBlock: (success: Bool, error: NSError?) -> ()) {
    if statusCode != 200 {
      let error = NSError(
        domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.GetAllBookingsFailed.rawValue,
        userInfo: ["error":"Could not get bookings"])
      completionBlock(success: false, error: error)
      return
    }
    
    do {
      if let data = data,
        let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(
          data,
          options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
        var bookings = Booking.bookingsFromJSON(jsonDictionary)
        
        // Get from the response the next links
        self.currentPage = Page.pageFromJSON(jsonDictionary)
        
        // Sort Bookings by delivery date and time
        bookings = Booking.sortBookings(bookings)
        
        if let loadMore = loadMore where loadMore {
          for booking in bookings {
            self.bookings.append(booking)
          }
        } else {
          self.bookings.removeAll()
          self.bookings = bookings
        }
        completionBlock(success: true, error: nil)
        return
      }
    } catch {}
    
    let error = NSError(
      domain: "com.blinkmycar.network",
      code: BlinkMyCarErrorCodes.GetAllBookingsFailed.rawValue,
      userInfo: ["error":"Could not get bookings"])
    completionBlock(success: false, error: error)
  }
}

extension BookingHistoryViewModel {
  func numberOfRows() -> Int {
    return bookings.count
  }
  
  func bookingForRow(row: Int) -> Booking {
    return bookings[row]
  }
}
