//
//  AcknowledgementViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 9/10/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class AcknowledgementViewModel {
  var acknowledgementName: String = ""
  var acknowledgementText: String = ""
}
