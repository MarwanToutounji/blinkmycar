//
//  BookingConfirmationViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/27/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import Moya

enum ConfirmationTableViewSection {
  case Other
  case DateAndCarLocation
  case Services
  case PaymentTool
  case Promocode
  
  var title: String {
    switch self {
    case .DateAndCarLocation:
      return NSLocalizedString(
        "ConfirmationTableViewSection.DateAndCarLocation",
        value: "Date and Car Location",
        comment: "Section title in booking confirmation VC for Date and car location")
    case .Services:
      return NSLocalizedString(
        "ConfirmationTableViewSection.Services",
        value: "Services",
        comment: "Section title in booking confirmation VC for Services")
    case .PaymentTool:
      return NSLocalizedString(
        "ConfirmationTableViewSection.PaymentTool",
        value: "Payment",
        comment: "Section title in booking confirmation VC for Payment")
    case .Promocode:
      return NSLocalizedString(
        "ConfirmationTableViewSection.Promocode",
        value: "Promocode",
        comment: "Section title in booking confirmation VC for Promocode")
    case .Other:
      return NSLocalizedString(
        "ConfirmationTableViewSection.Other",
        value: "",
        comment: "Section title in booking confirmation VC for unidentified section")
    }
  }
}

class BookingConfirmationViewModel {
  var serviceBundles: [ServiceBundle] = [ServiceBundle]()
  var defaultPaymentMethod = PaymentMethod.getDefaultPaymentMethod()
  var promocode: String? = nil
  
  private var sections: [ConfirmationTableViewSection] = [.DateAndCarLocation, .Services]
  
  var bookingFlowViewModel: BookingFlowViewModel {
    didSet {
      extractInformationFromBookingFlowViewModel()
    }
  }
  
  func extractInformationFromBookingFlowViewModel() {
    var serviceBundleArray = [ServiceBundle]()
    
    if let selectedServices = bookingFlowViewModel.selectedServices {
      let mainServices = selectedServices.filter({ $0.isCore == true })
      for mainService in mainServices {
        let addOnsForCoreService = selectedServices.filter({ $0.parentId == mainService.id })
        let serviceBundle = ServiceBundle()
        serviceBundle.mainService = mainService
        serviceBundle.addOnsSerices = addOnsForCoreService
        serviceBundleArray.append(serviceBundle)
      }
    }
    
    if let editedBooking = bookingFlowViewModel.editedBooking {
      promocode = editedBooking.promocode
    }
    
    serviceBundles = serviceBundleArray
  }
  
  init(bookingFlowViewModel: BookingFlowViewModel) {
    self.bookingFlowViewModel = bookingFlowViewModel
    extractInformationFromBookingFlowViewModel()
  }
  
  func setSelectedServicesListener() {
    
  }
  
  var actionButtonTitle: String {
    return NSLocalizedString(
      "BookingConfirmationViewController",
      value: "Confirm",
      comment: "Title for button that confirms a booking")
  }
  
  func refreshSections() {
    sections = [.DateAndCarLocation, .Services]
    sections.insert(.Promocode, atIndex: 0)
    if defaultPaymentMethod?.defaultPaymentTool() != nil {
      sections.insert(.PaymentTool, atIndex: 0)
    }
  }
  
  var numberOfSections: Int {
    // Refresh the sections to be displayed here
    refreshSections()
    return sections.count
  }
  
  func numberOfRowsInSection(section: Int) -> Int {
    let tableViewSection = sections[section]
    switch tableViewSection {
    case .DateAndCarLocation:
      return 2
    case .Services:
      return serviceBundles.count
    case .PaymentTool:
      return defaultPaymentMethod?.defaultPaymentTool() != nil ? 1 : 0
    case .Promocode:
      return 1
    default:
      return 0
    }
  }
  
  func refreshDefaultPaymentTool() {
    defaultPaymentMethod = PaymentMethod.getDefaultPaymentMethod()
  }
  
  var additionalInformationViewModel: AdditionalInstructionsViewModel {
    let additionalInfoViewModel = AdditionalInstructionsViewModel(booking: self.bookingFlowViewModel.editedBooking!)
    additionalInfoViewModel.additionalInformation = bookingFlowViewModel.editedBooking?.addressKeysLocation
    return additionalInfoViewModel
  }
  
  //==========================================================
  // MARK: - API | Network calls
  //==========================================================
  
  var getTotalRequest: Cancellable?
  
  func getPriceForSelectedServices(completionBlock:((success: Bool, price: String?, discountedPrice: String?, note: String?, error: NSError?) -> Void)) -> (price: String?, discountedPrice: String?, note: String?) {
    if getTotalRequest != nil {
      getTotalRequest!.cancel()
    }
    
    let note = "(VAT Included)"
    
    getTotalRequest = signedRequest(
      BlinkMyCarAPI.getTotal(
      addressId: bookingFlowViewModel.selectedAddress!.addressID!,
      deliveryDate: bookingFlowViewModel.selectedDate!.date!,
      deliveryTime: bookingFlowViewModel.selectedTime!,
      rideId: bookingFlowViewModel.currentVehicle!.id!,
      service: bookingFlowViewModel.selectedServices!,
      promocode: promocode),
      completion: { (data, statusCode, response, error) in
        
        defer {
          self.getTotalRequest = nil
        }
        
        guard statusCode == 200 else {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.GetTotalFailed.rawValue,
            userInfo: ["error":"Could not get total"])
          completionBlock(success: false, price: nil, discountedPrice: nil, note: nil, error: error)
          return
        }
        
        guard let data = data else {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.GetTotalFailed.rawValue,
            userInfo: ["error":"Could not get total"])
          completionBlock(success: false, price: nil, discountedPrice: nil, note: nil, error: error)
          return
        }
        
        do {
          if let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
            
            let priceInfo = Booking.totalPriceFromJSON(jsonDictionary)
            if let price = priceInfo.price,
              let discountedPrice = priceInfo.discountedPrice {
              completionBlock(
                success: !(priceInfo.hasError ?? false),
                price: price.displayAsUsDollarsPrice,
                discountedPrice: (discountedPrice > price) ? discountedPrice.displayAsUsDollarsPrice : nil,
                note: note,
                error: NSError(domain: "com.blinkmycar.network",
                  code: BlinkMyCarErrorCodes.DeleteAddressFailed.rawValue,
                  userInfo: ["error":priceInfo.errorMessage ?? "Something went wrong, Please try again!"]))
            } else {
              completionBlock(success: false, price: "$0", discountedPrice: nil, note: nil, error: nil)
            }
          } else {
            completionBlock(success: false, price: "$0", discountedPrice: nil, note: nil, error: nil)
          }
        } catch {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.GetTotalFailed.rawValue,
            userInfo: ["error":"Could not get total"])
          completionBlock(success: false, price: nil, discountedPrice: nil, note: nil, error: error)
        }
    })
    
    var totalPrice: Float = 0
    for service in bookingFlowViewModel.selectedServices! {
      totalPrice += service.price
    }
    return (price: totalPrice.displayAsUsDollarsPrice, discountedPrice: nil, note: note)
  }
  
  func addUpdateBooking(completionBlock: ((success: Bool, isNew: Bool, error: NSError?) -> Void)?) {
    // If the edit or add was successful the page saved in the Persistence Manager is 
    // Unvalidated, for we have to refetch the first page to be able to 
    // prevent duplicates in the upcoming listing
    
    if bookingFlowViewModel.editedBooking?.bookingId != nil {
      editBooking({ (success, error) in
        if success {
          PersistenceManager.sharedManager.unvalidatePageForVehicleID(self.bookingFlowViewModel.currentVehicle?.id ?? "")
        }
        completionBlock?(success: success, isNew: false, error: error)
      })
    } else {
      addBooking { (success, error) in
        if success {
          PersistenceManager.sharedManager.unvalidatePageForVehicleID(self.bookingFlowViewModel.currentVehicle?.id ?? "")
        }
        completionBlock?(success: success, isNew: true, error: error)
      }
    }
  }
  
  private func addBooking(completionBlock: ((success: Bool, error: NSError?) -> Void)?) {
    let vehicle = bookingFlowViewModel.currentVehicle!
    let address = bookingFlowViewModel.selectedAddress!
    let date = bookingFlowViewModel.selectedDate!
    let time = bookingFlowViewModel.selectedTime!
    let service = bookingFlowViewModel.selectedServices!
    
    signedRequest(
      BlinkMyCarAPI.CreateBooking(
        addressId: address.addressID!,
        deliveryDate: date.date!,
        deliveryTime: time,
        rideId: vehicle.id!,
        service: service,
        paymentMethod: defaultPaymentMethod!,
        promocode: promocode),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 201 {
          var err: NSError? = error
          if err == nil {
            err = BlinkMyCarAPI.retrieveErrorFromData(data)
            if err == nil {
              err = NSError(domain: "com.blinkmycar.network",
                code: BlinkMyCarErrorCodes.CreateBookingFailed.rawValue,
                userInfo: ["error":"Something went wrong we couldn't create your booking. Please try again!"])
            }
          }
          completionBlock?(success: false, error: err)
          return
        }
        
        do {
          if let data = data, let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
            let booking = Booking.fromJSON(jsonDictionary) as! Booking
            Booking.saveBooking(booking)
            self.bookingFlowViewModel.editedBooking = booking
            
            completionBlock?(success: true, error: nil)
            return
          }
        } catch {}
        
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.CreateBookingFailed.rawValue,
          userInfo: ["error":"Something went wrong we couldn't create your booking. Please try again!"])
        completionBlock?(success: false, error: error)
    })
  }
  
  func editBooking(completionBlock: ((success: Bool, error: NSError?) -> Void)?) {
    let vehicle = bookingFlowViewModel.currentVehicle
    var address: Address? = nil
    var date: ServiceDate? = nil
    var time: ServiceTime? = nil
    var service: [Service]? = nil
    var paymentMethod: PaymentMethod? = nil
    var promocode: String? = nil
    
    if bookingFlowViewModel.haveEditedAddress.value {
      address = bookingFlowViewModel.selectedAddress
    }
    if bookingFlowViewModel.haveEditedDate.value {
      date = bookingFlowViewModel.selectedDate
    }
    if bookingFlowViewModel.haveEditedTime.value {
      time = bookingFlowViewModel.selectedTime
    }
    if bookingFlowViewModel.haveEditedServices.value {
      service = bookingFlowViewModel.selectedServices
    }
    if paymentMethod != defaultPaymentMethod! {
      paymentMethod = defaultPaymentMethod
    }
    if (bookingFlowViewModel.editedBooking?.promocode ?? "") != (promocode ?? "") {
      promocode = self.promocode ?? ""
    }
    
    signedRequest(
      BlinkMyCarAPI.UpdateBooking(
        bookingId: bookingFlowViewModel.editedBooking!.bookingId!,
        addressId: address?.addressID,
        deliveryDate: date?.date,
        deliveryTime: time,
        rideId: vehicle!.id,
        service: service,
        paymentMethod: paymentMethod,
        promocode: promocode),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 200 {
          var err: NSError? = error
          if err == nil {
            err = BlinkMyCarAPI.retrieveErrorFromData(data)
            if err == nil {
              err = NSError(domain: "com.blinkmycar.network",
                code: BlinkMyCarErrorCodes.UpdateBookingFailed.rawValue,
                userInfo: ["error":"Something went wrong we couldn't update your booking. Please try again!"])
            }
          }
          completionBlock?(success: false, error: err)
          return
        }
        
        do {
          if let data = data, let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
            let booking = Booking.fromJSON(jsonDictionary) as! Booking
            
            Booking.replaceBooking(self.bookingFlowViewModel.editedBooking!, with: booking)
            
            self.bookingFlowViewModel.editedBooking = booking
            
            completionBlock?(success: true, error: nil)
            return
          }
        } catch {}
        
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.UpdateBookingFailed.rawValue,
          userInfo: ["error":"Something went wrong we couldn't update your booking. Please try again!"])
        completionBlock?(success: false, error: error)
    })
  }
  
  func hasDefaultPaymentMethod(completionBlock: (success: Bool, error: NSError?) -> Void) {
    let paymentListingViewModel = PaymentViewModel()
    paymentListingViewModel.hasDefaultPaymentMethod(completionBlock)
  }
  
  //==========================================================
  // MARK: - Table View Data Source
  //==========================================================
  func isLastCellInLastSection(indexPath: NSIndexPath) -> Bool {
    // Last section
    if indexPath.section == (sections.count-1) {
      // Last row in section
      if indexPath.row == (numberOfRowsInSection(indexPath.section)-1) {
        return true
      }
    }
    return false
  }
  
  func cellInformationForIndexPath(indexpath: NSIndexPath) -> (section: ConfirmationTableViewSection, info: Any?) {
    let sectionValue = sections[indexpath.section]
    switch sectionValue {
    case .DateAndCarLocation:
      var text: String? = nil
      switch indexpath.row {
      case 0:
        if let selectedDate = bookingFlowViewModel.selectedDate {
          text = "\(selectedDate.displayTextForServiceDate()). "
        }
        
        if let selectedTime = bookingFlowViewModel.selectedTime {
          if text == nil {
            text = ""
          }
          text! += selectedTime.startTimeStringValue
        }
      case 1:
        if let selectedAddress = bookingFlowViewModel.selectedAddress {
          text = selectedAddress.fullAddress
        }
      default:
        break
      }
      return (section: .DateAndCarLocation, info: text)
    case .Services:
      return (section: .Services, info: serviceBundles[indexpath.row])
    case .PaymentTool:
      return (section: .PaymentTool, info: defaultPaymentMethod?.defaultPaymentTool())
    case .Promocode:
      var info: String
      if let promocode = self.promocode where !promocode.isEmpty {
        info = promocode
      } else {
        info = NSLocalizedString(
          "BookingConfirmationViewController.promocodePlaceholder",
          value: "Add a promocode",
          comment: "Text that appears if there was no promocode already")
      }
      return (section: .Promocode, info: info)
    case .Other:
      fallthrough
    default:
      return (section: .Other, info: nil)
    }
  }
  
  func sectionForCellIndexPath(indexPath: NSIndexPath) -> ConfirmationTableViewSection {
    return sections[indexPath.section]
  }
  
  func sectionTitle(section: Int) -> String {
    return sections[section].title
  }
  
  var servicesViewModel: ServicesViewModel {
    let serviceViewModel = ServicesViewModel(services: bookingFlowViewModel.selectedServices)
    serviceViewModel.bookingFlowViewModel = bookingFlowViewModel
    return serviceViewModel
  }
}


//===============================================================
// MARK: - Locals
//===============================================================

extension BookingConfirmationViewModel {
  var viewControllerTitle: String {
    return NSLocalizedString(
      "BookingConfirmationViewController",
      value: "Confirm",
      comment: "BookingConfirmationViewController title")
  }
  
  var alertViewFailToCheckForPaymentMethodTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.alertViewFailToCheckForPaymentMethodTitle",
      value: "Ooops",
      comment: "title for alert view that appears when an error occures while checking for default payment")
  }
  
  var alertViewFailToCheckForPaymentMethodMessage: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.alertViewFailToCheckForPaymentMethodMessage",
      value: "We could not check your default payment method. Please try again.",
      comment: "title for alert view that appears when an error occures while checking for default payment")
  }
  
  var alertViewFailToCheckForPaymentMethodButtonTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.alertViewFailToCheckForPaymentMethodButtonTitle",
      value: "Ok",
      comment: "title for alert view that appears when an error occures while checking for default payment")
  }
  
  var bookingSuccessAlertViewTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.bookingSuccessAlertViewTitle",
      value: "BMC",
      comment: "Title for alert view that shows up when booking a wash is successful")
  }
  
  var bookingSuccessAlertViewMessage: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.bookingSuccessAlertViewMessage",
      value: "We will confirm your booking shortly.\nDiscounts will be applied upon service completion.",
      comment: "Message for alert view that shows up when booking a wash is successful")
  }
  
  var bookingSuccessAlertViewPostiveButtonTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.bookingSuccessAlertViewPostiveButtonTitle",
      value: "Ok",
      comment: "Dismiss Button title for alert view that shows up when booking a wash is successful")
  }
  
  var failToCreateBookingAlertTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.failToCreateBookingAlertTitle",
      comment: "Alert View Title, Shown When we fail to Create a booking")
  }
  
  var failToCreateBookingAlertOkButtonTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.failToCreateBookingAlertOkButtonTitle",
      comment: "Alert View Discard button title, Shown When we fail to Create a booking")
  }
  
  var failToCreateBookingAlertMessage: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.failToCreateBookingAlertMessage",
      comment: "Alert View Message, Shown When we fail to Create a booking")
  }
  
  var failToUpdateBookingAlertTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.failToUpdateBookingAlertTitle",
                             comment: "Alert View Title, Shown When we fail to Update a booking")
  }
  
  var failToUpdateBookingAlertOkButtonTitle: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.failToUpdateBookingAlertOkButtonTitle",
                             comment: "Alert View Discard button title, Shown When we fail to Update a booking")
  }
  
  var failToUpdateBookingAlertMessage: String {
    return NSLocalizedString(
      "BookAWashConfirmationViewController.failToUpdateBookingAlertMessage",
                             comment: "Alert View Message, Shown When we fail to Update a booking")
  }
}
