//
//  AddressesViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/22/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class AddressesViewModel {
  var analyticsCategory = AnalyticsCategories.InMenuMyLocations

  var addresses: [Address]? = nil
  var selectedAddress: Address?

  var disableNotServiceableAddresses = false

  var noData: Bool {
    return !((addresses?.count ?? 0) > 0)
  }
  
  public init() {
    addresses = Address.getAddresses()
  }

  var numberOfSections: Int {
    return (addresses?.count > 0) ? 1 : 0
  }

  var numberOfRows: Int {
    if let addresses = addresses {
      return addresses.count
    }
    return 0
  }

  func addressTitleForRow(row: Int) -> String {
    return addresses?[row].fullAddress ?? "Address"
  }
  
  func addressTypeForRow(row: Int) -> String {
    if let type = addresses?[row].type {
      return type.addressTypeName()
    }
    return ""
  }

  func addressNoteForRow(row: Int) -> String? {
    if addressForRow(row).coordinates == nil {
      return addressHasNoCoordinatesNote
    }
    return nil
  }

  func addressForRowIsSelected(row: Int) -> Bool {
    if let addressId = addresses?[row].addressID,
    let selectedAddressId = selectedAddress?.addressID {
      return addressId == selectedAddressId
    }
    return false
  }

  func addressImageForRow(row: Int) -> UIImage {
    if let addresses = addresses {
      return  addresses[row].type!.icon.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    }
    return UIImage()
  }

  func addressForRow(row: Int) -> Address {
    if let addresses = addresses {
      return addresses[row]
    }
    return Address()
  }

  func addressForRowIsServiceable(row: Int) -> Bool {
    if let address = addresses?[row] {
      return addressServiceable(address)
    }
    return false
  }

  func addressServiceable(address: Address) -> Bool {
    return AddressProfileViewModel().isAddressServiceable(address)
  }

  func deleteAddressForRow(row: Int, completionBlock: ((success: Bool, error: NSError?) -> Void)?) {
    if let addresses = addresses {
      signedRequest(BlinkMyCarAPI.DeleteAddress(id: addresses[row].addressID!),
        completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
          if statusCode != 204 {
            let error = NSError(domain: "com.blinkmycar.network",
              code: BlinkMyCarErrorCodes.DeleteAddressFailed.rawValue,
              userInfo: ["error":"Could not Update booking"])
            completionBlock?(success: false, error: error)
            return
          }

          Address.deleteAddressWithId(addresses[row].addressID!)

          self.addresses!.removeAtIndex(row)
          completionBlock?(success: true, error: nil)
      })
    } else {
      completionBlock?(success: true, error: nil)
    }
  }

  var addAddressesNoRecordsMessage: String {
    return NSLocalizedString("AddressesTableViewController.addAddressesNoRecordsMessage",
      comment: "Short message that appears when there are no addresses to display")
  }
  
  var addAddressesButtonText: String {
    return NSLocalizedString("AddressesTableViewController.addAddressesButtonText",
      comment: "The Add Addresses button Text")
  }

  var viewControllerTitle: String {
    return NSLocalizedString("AddressesTableViewController.viewControllerTitle",
      comment: "Title that appears when this view controller is present inside a UINavigationController")
  }

  var addressHasNoCoordinatesNote: String {
    return NSLocalizedString("AddressesTableViewController.addressHasNoCoordinatesNote", value: "Specify existing location on map.",
      comment: "Text that appears under the address name to notify the user that this address has no coordinates and thus is not serviceable")
  }

  var alertViewErrorLoadingAddressesTitle: String {
    return NSLocalizedString("AddressesTableViewController.alertViewErrorLoadingAddressesTitle",
      comment: "Error loading addresses alert title")
  }
  
  var alertViewErrorLoadingAddressesMessage: String {
    return NSLocalizedString("AddressesTableViewController.alertViewErrorLoadingAddressesMessage",
      comment: "Error loading addresses alert title")
  }
  
  var alertViewErrorLoadingAddressesButtonTitle: String {
    return NSLocalizedString("AddressesTableViewController.alertViewErrorLoadingAddressesButtonTitle",
      comment: "Error loading addresses alert title")
  }

  public func loadData(completion: (success: Bool, error: NSError?) -> Void) {
    loadAddresses { (success: Bool, error: NSError?) -> Void in
      if !success {
        completion(success: false, error: error)
        return
      }
      
      completion(success: true, error: nil)
    }
  }

  public func loadAddresses(completion: (success: Bool, error: NSError?) -> Void) {
    if !PersistenceManager.sharedManager.didLoadAddresses {
      PersistenceManager.sharedManager.loadAddresses(
        { (success: Bool, error: NSError?) -> Void in
          if !success {
            completion(success: false, error: error)
            return
          }
          self.addresses = Address.getAddresses()
          completion(success: true, error: nil)
      })
    } else {
      addresses = Address.getAddresses()
      completion(success: true, error: nil)
    }
  }

  /**
   Checks if the addresses were synced with the server, if not then we cannot really trust
   and be sure that the saved addresses are still valid. 
  */
  public var haveValidAddresses: Bool {
    return PersistenceManager.sharedManager.didLoadAddresses
  }


  //==========================================================
  // MARK: U I   S T R I N G S
  //==========================================================

  var alertViewTitleCouldNotLoadAddresses: String {
    return NSLocalizedString("AddressesTableViewCOntroller.alertViewTitleCouldNotLoadAddresses", comment: "Alert View Title, appears when we fail to load the addresses from server in the Addresses listing")
  }

  var alertViewMessageCouldNotLoadAddresses: String {
    return NSLocalizedString("AddressesTableViewCOntroller.alertViewMessageCouldNotLoadAddresses",
      comment: "Alert View Message, appears when we fail to load the addresses from server in the Addresses listing")
  }

  var alertViewCancelButtonTitle: String {
    return NSLocalizedString(
      "AddressesTableViewCOntroller.alertViewCancelButtonTitle",
      value: "Cancel",
      comment: "Alert View Cancel button, just dismisses the alert view")
  }
  
  var alertViewOkButtonTitle: String {
    return NSLocalizedString(
      "AddressesTableViewCOntroller.alertViewOkButtonTitle",
      value: "Ok",
      comment: "Alert View Ok button")
  }

  var alertViewTryAgainButtonTitle: String {
    return NSLocalizedString("AddressesTableViewCOntroller.alertViewTryAgainButtonTitle",
      comment: "Alert View Try Again button")
  }

  var alertViewTitleGeneralError: String {
    return NSLocalizedString("AddressesTableViewCOntroller.alertViewTitleGeneralError",
      comment: "Alert View Title, appears when we an unknown error occures while fetching info from server")
  }

  var alertViewMessageGeneralError: String {
    return NSLocalizedString("AddressesTableViewCOntroller.alertViewMessageGeneralError",
      comment: "Alert View Message, appears when we an unknown error occures while fetching info from server")
  }

  var alertViewTitleCouldNotDeleteAddress: String {
    return NSLocalizedString("AddressesTableViewCOntroller.alertViewTitleCouldNotDeleteAddress",
      comment: "Alert View Title, appears when we fail to delete an address")
  }

  var alertViewMessageCouldNotDeleteAddress: String {
    return NSLocalizedString("AddressesTableViewCOntroller.alertViewMessageCouldNotDeleteAddress",
      comment: "Alert View Message, appears when we fail to delete an address")
  }

  var alertViewTitleCouldNotProceedNeedToSyncWithServer: String {
    return NSLocalizedString(
      "AddressesTableViewController.alertViewTitleCouldNotProceedNeedToSyncWithServer",
      value: "Locations",
      comment: "Alert View Title, appears when trying to do an action that requires to sync addresses and areas with the server")
  }
  
  var alertViewMessageCouldNotProceedNeedToSyncWithServer: String {
    return NSLocalizedString(
      "AddressesTableViewController.alertViewMessageCouldNotProceedNeedToSyncWithServer",
      value: "We need to sync with the server before proceeding.",
      comment: "Alert View Message, appears when trying to do an action that requires to sync addresses and areas with the server")
  }
  
  var deleteAddressAlertViewTitle: String {
    return NSLocalizedString(
      "AddressesTableViewController.deleteAddressAlertViewTitle",
      value: "Delete Address",
      comment: "Title for Alert view That shows up before deleting an address")
  }
  
  var deleteAddressAlertViewMessage: String {
    return NSLocalizedString(
      "AddressesTableViewController.deleteAddressAlertViewMessage",
      value: "Are you sure you want to discard your address?",
      comment: "Message for Alert view That shows up before deleting an address")
  }
  
  var deleteAddressAlertViewCancelActionButtonTitle: String {
    return NSLocalizedString(
      "AddressesTableViewController.deleteAddressAlertViewCancelActionButtonTitle",
      value: "Cancel",
      comment: "Cancel button Title for Alert view That shows up before deleting an address")
  }
  
  var deleteAddressAlertViewOkActionButtonTitle: String {
    return NSLocalizedString(
      "AddressesTableViewController.deleteAddressAlertViewOkActionButtonTitle",
      value: "Ok",
      comment: "Ok Button Title for Alert view That shows up before deleting an address")
  }
}
