//
//  PaymentViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 4/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import Stripe

class PaymentViewModel {
  var analyticsCategory = AnalyticsCategories.InMenuPayment
  
  var defaultPaymentMethod: PaymentMethod? = PaymentMethod.getDefaultPaymentMethod()
  
  var dataSource: (paymentMethods: [PaymentMethod],
    paymentToolsByMethodName: [String: [PaymentTool]]) =
    (PaymentMethod.allAvailableMethods, PaymentMethod.getPaymentToolsByMethodName())
  
  //===========================================================
  // MARK: - DATA SOURCE Methods
  //===========================================================
  
  func numberOfSections() -> Int {
    return dataSource.paymentMethods.count
  }
  
  func numberOfRowsForSection(section: Int) -> Int {
    let keyForSection: String = dataSource.paymentMethods[section].name
    return dataSource.paymentToolsByMethodName[keyForSection]?.count ?? 0
  }
  
  func paymentToolForIndexPath(indexPath: NSIndexPath) -> PaymentTool? {
    let keyForSection: String = dataSource.paymentMethods[indexPath.section].name
    return dataSource.paymentToolsByMethodName[keyForSection]![indexPath.row]
  }
  
  func isPaymentToolAtIndexPathDefaultTool(indexPath: NSIndexPath) -> Bool {
    guard let defaultPaymentTool = PaymentMethod.getDefaultPaymentTool(),
      let concernedPaymentTool = paymentToolForIndexPath(indexPath) else {
        return false
    }
    
    if defaultPaymentTool.method == concernedPaymentTool.method {
      switch defaultPaymentTool.method {
      case .Cash:
        // Since the cash method only has one option
        return true
      case .Stripe:
        return (concernedPaymentTool as! StripePayment).isDefault
      }
    } else {
      return false
    }
  }
  
  func reloadData(completionBlock: (success: Bool, error: NSError?) -> Void) {
    // Load localy saved cards
    dataSource = (PaymentMethod.allAvailableMethods, PaymentMethod.getPaymentToolsByMethodName())
    
    defaultPaymentMethod = PaymentMethod.getDefaultPaymentMethod()
    
    // Check if cards are synced with the server
    // Note: Do any addiotional loading for other payment methods here if available
    if PersistenceManager.sharedManager.didLoadCards {
      completionBlock(success: true, error: nil)
    } else {
      PersistenceManager.sharedManager.loadCards({ (success, error) -> Void in
        if success {
          self.dataSource = (PaymentMethod.allAvailableMethods, PaymentMethod.getPaymentToolsByMethodName())
        }
        completionBlock(success: success, error: error)
      })
    }
  }
  
  //===========================================================
  // MARK: - ACTIONS
  //===========================================================

  func deleteCardForIndexPath(indexPath: NSIndexPath, completionBlock: ((success: Bool, error: NSError?) -> Void)) {
    //TODO: Make API call
    if let stripePayment = paymentToolForIndexPath(indexPath) as? StripePayment{
      signedRequest(BlinkMyCarAPI.DeleteCard(cardId: stripePayment.cardId)) {
        (data, statusCode, response, error) -> () in
        if statusCode != 204 {
          let error = NSError(domain: "com.blinkmycar.network",
                              code: BlinkMyCarErrorCodes.DeletePaymentToolFailed.rawValue,
                              userInfo: ["error":"Fail to Delete card"])
          completionBlock(success: false, error: error)
          return
        }
        
        StripePayment.deleteCardWithIdFromList(stripePayment.cardId)
        
        self.reloadData({ (success, error) in
          completionBlock(success: true, error: nil)
        })
      }
    }
  }
  
  func setAsDefaultPaymentToolAtIndexPath(indexPath: NSIndexPath, completionBlock: (success: Bool, error: NSError?) -> Void) {
    let errorBlockHandling = { () -> Void in
      let error = NSError(domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.SetPaymentToolAsDefaultFailed.rawValue,
        userInfo: ["error":"Could not Update default paymentTool"])
      completionBlock(success: false, error: error)
    }
    
    if let paymentTool = paymentToolForIndexPath(indexPath) {
      switch paymentTool.method {
      case .Cash:
        PaymentMethod.saveDefaultPaymentTool(paymentTool)
        self.defaultPaymentMethod = PaymentMethod.getDefaultPaymentMethod()
        completionBlock(success: true, error: nil)
      case .Stripe:
        if let stripePayment = paymentTool as? StripePayment {
          if stripePayment.isDefault {
            self.setDefaultPaymentToolLocally(stripePayment)
            self.reloadData({ (success, error) -> Void in
              completionBlock(success: success, error: error)
            })
          } else {
            signedRequest(BlinkMyCarAPI.SetCardAsDefault(cardId: stripePayment.cardId),
                          completion:
              { (data, statusCode, response, error) -> () in
                if statusCode != 200 {
                  errorBlockHandling()
                  return
                }
                
                do {
                  if let data = data,
                    let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data,
                      options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
                    let updatedDefaultCard = StripePayment.fromJSON(jsonDictionary) as! StripePayment
                    self.setDefaultPaymentToolLocally(updatedDefaultCard)
                    self.reloadData({ (success, error) -> Void in
                      completionBlock(success: success, error: error)
                    })
                    return
                  }
                } catch {}
                
                errorBlockHandling()
            })
          }
        }
      }
    }
  }
  
  func setDefaultPaymentToolLocally(defaultPaymentTool: StripePayment) {
    PaymentMethod.saveDefaultPaymentTool(defaultPaymentTool)
    StripePayment.saveDefaultCard(defaultPaymentTool)
    self.defaultPaymentMethod = PaymentMethod.getDefaultPaymentMethod()
  }

  // Only Cash Opetion cannot be deleted
  func canDeletePaymentToolAtIndexPath(indexPath: NSIndexPath) -> Bool {
    guard let paymentTool = paymentToolForIndexPath(indexPath) else {
      return false
    }
    
    switch paymentTool.method {
    case .Cash:
      return false
    case .Stripe:
      if let defaultPaymentMethod = defaultPaymentMethod where defaultPaymentMethod == .Stripe {
        return !(paymentTool as! StripePayment).isDefault
      }
      return true
    }
  }
  
  func canCommitDeletingPaymentToolAtIndexPath(indexPath: NSIndexPath) -> Bool {
    guard let paymentTool = paymentToolForIndexPath(indexPath) else {
      return false
    }
    
    switch paymentTool.method {
    case .Cash:
      return false
    case .Stripe:
      return true
    }
  }
  
  //===========================================================
  // MARK: - HELPERS
  //===========================================================
  
  func hasDefaultPaymentMethod(completion: (success: Bool, error: NSError?) -> Void) {
    self.reloadData { (success, error) -> Void in
      // If there is a payment tool then the payment method is already set
      if self.defaultPaymentMethod?.defaultPaymentTool() != nil {
        completion(success: true, error: nil)
      } else {
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.MissingDefaultPaymentMethod.rawValue,
          userInfo: nil)
        completion(success: false, error: error)
      }
    }
  }
}

//===========================================================
// MARK: - COPYRIGHTING
//===========================================================

extension PaymentViewModel {
  var addPaymentButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.addPaymentButtonTitle",
      value: "Add Payment",
      comment: "add payment button title")
  }
  
  var defualtLabelText: String {
    return NSLocalizedString("PaymentListingViewController.defualtLabelText",
      value: "Default", comment: "Payment Listing - Default Item label")
  }

  var paymentListingViewControllerTitle: String {
    return NSLocalizedString("PaymentListingViewController.paymentListingViewControllerTitle",
      value: "Payment",
      comment: "Payment Listing - Default Item label")
  }

  var alertViewDeleteConfirmationTitle: String {
    return NSLocalizedString("PaymentListingViewController.deleteAlertTitle", value: "Delete Confirmation",
      comment: "Payment Card Listing delete alert confirmation")
  }
  var alertViewDeleteConfirmationMessage: String {
    return NSLocalizedString("PaymentListingViewController.deleteAlertMessage", value: "Are you sure you want to delete this card?",
      comment: "Payment Card Listing delete alert confirmation")
  }
  var alertViewDeleteConfirmationNegativeButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.deleteAlertNegativeButton", value: "No",
      comment: "Payment Card Listing delete alert confirmation")
  }
  var alertViewDeleteConfirmationPostiveButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.deleteAlertPostionButton", value: "Yes",
      comment: "Payment Card Listing delete alert confirmation")
  }
  
  var alertViewErrorLoadingPaymentsTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewErrorLoadingPaymentsTitle",
      value: "Ooops",
      comment: "title of alert view that appears when loading payments/cards fail")
  }
  
  var alertViewErrorLoadingPaymentsMessage: String {
    return NSLocalizedString("PaymentListingViewController.alertViewErrorLoadingPaymentsMessage",
      value: "There was a network error, please try again.",
      comment: "message of alert view that appears when loading payments/cards fail")
  }
  
  var alertViewErrorLoadingPaymentsNegativeActionButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewErrorLoadingPaymentsNegativeActionButtonTitle",
      value: "Cancel",
      comment: "cancel action button title of alert view that appears when loading payments/cards fail")
  }
  
  var alertViewErrorLoadingPaymentsPositiveActionButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewErrorLoadingPaymentsPositiveActionButtonTitle",
      value: "Retry",
      comment: "Retry action button title of alert view that appears when loading payments/cards fail")
  }

  var alertViewCantDeleteTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewCantDeleteTitle", value: "Ooops",
      comment: "Payment Card Listing cant delete last item alert")
  }
  var alertViewCantDeleteMessage: String {
    return NSLocalizedString("PaymentListingViewController.alertViewCantDeleteMessage", value: "Can't delete Card",
      comment: "Payment Card Listing cant delete last item alert")
  }
  var alertViewCantDeleteButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewCantDeleteButtonTitle", value: "Ok",
      comment: "Payment Card Listing cant delete delete last alert ")
  }

  var alertViewCantDeleteLastCardTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewCantDeleteLastCardTitle", value: "Ooops",
      comment: "Payment Card Listing cant delete last item alert")
  }
  var alertViewCantDeleteLastCardMessage: String {
    return NSLocalizedString("PaymentListingViewController.alertViewCantDeleteLastCardMessage", value: "Unfortunately we can't delete your last card.",
      comment: "Payment Card Listing cant delete last item alert")
  }

  var alertViewCantDeleteDefaultCardMessage: String {
    return NSLocalizedString("PaymentListingViewController.alertViewCantDeleteDefaultCardMessage", value: "You can't delete your default card.",
      comment: "Payment Card Listing cant delete default card alert")
  }

  var alertViewCantDeleteLastCardButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewCantDeleteLastCardButtonTitle", value: "Ok",
      comment: "Payment Card Listing cant delete delete last alert ")
  }

  var deleteCardErrorMessage: String {
    return NSLocalizedString("PaymentListingViewController.deleteCardErrorMessage", value: "",
      comment: "Delete Payment Card error message")
  }

  var alertViewErrorDeletingTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewErrorDeletingTitle", value: "Ooops",
      comment: "Error deleting Payment Card from Listing alert title")
  }
  
  var alertViewErrorDeletingMessage: String {
    return NSLocalizedString("PaymentListingViewController.alertViewErrorDeletingMessage",
      value: "We could not delete your card. Please try again.",
      comment: "Error deleting Payment Card from Listing alert message")
  }

  var alertViewErrorDeletingButtonTitle: String {
    return NSLocalizedString("PaymentListingViewController.alertViewErrorDeletingButtonTitle", value: "Ok",
      comment: "Error deleting Payment Card from Listing alert button title")
  }
  
  var titleForFailToChangeDefaultCardAlert: String {
    return NSLocalizedString("PaymentListingViewController.titleForFailToChangeDefaultCardAlert",
      value: "Ooops",
      comment: "title for Alert view that appears when setting a card as default payment method fails")
  }
  
  var messageForFailToChangeDefaultCardAlert: String {
    return NSLocalizedString("PaymentListingViewController.messageForFailToChangeDefaultCardAlert",
      value: "We could not update your default payment method. Please try again.",
      comment: "message for Alert view that appears when setting a card as default payment method fails")
  }
  
  var actionButtonTitleForFailToChangeDefaultCardAlert: String {
    return NSLocalizedString("PaymentListingViewController.mactionButtonTitleForFailToChangeDefaultCardAlert",
      value: "Ok",
      comment: "action button title for Alert view that appears when setting a card as default payment method fails")
  }
}
