//
//  SignInViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/10/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class SignInViewModel {
  
  var email: String?
  
  let validateEmail = {(text: String?) -> Bool in
    if let email = text {
      return email.isValidEmail()
    }
    return false
  }

  let validatePassword = {(text: String?) -> Bool in
    if text == nil || text!.isEmpty {
      return false
    }
    return true
  }

  func loginUser(email: String, password: String, completionBlock: (error: NSError?) -> Void) {
    APIProvider.sharedProvider.request(BlinkMyCarAPI.OAuth(
      username: email,
      password: password)) { (result) -> () in
        switch result {
        case .Success(let response):
          if response.statusCode != 200 {
            // The Client credentials are invalid
            completionBlock(error: NSError(domain: "com.blinkmycar.network",
              code: BlinkMyCarErrorCodes.SignInCredentialsInvalid.rawValue,
              userInfo: ["error":"The client credentials are invalid"])
            )
            return
          }

          if let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(
            response.data,
            options: NSJSONReadingOptions.AllowFragments) {
              if jsonResult is NSDictionary {
                let dicationary = jsonResult as! NSDictionary
                var accessToken = AccessToken()
                accessToken.readFromDictionary(dicationary)

                self.retrieveUserProfile(completionBlock)

                return
              }
          }

          completionBlock(error: NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.SignInFailed.rawValue, userInfo: nil))
          return
        case .Failure:
          completionBlock(error: NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.SignInFailed.rawValue, userInfo: nil))
        }
    }
  }

  func retrieveUserProfile(completionBlock: (error: NSError?) -> Void) {
    // Retrieve the User
    //----------------------------
    signedRequest(BlinkMyCarAPI.Profile,
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in

        // IF THIS STEP FAIL THEN LOGIN FAILS
        if error != nil || statusCode != 200 {
          AccessToken().deleteToken()
          completionBlock(error: NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.ProfileAPIFailed.rawValue, userInfo: nil))
          return
        }

        if let
          data = data,
          jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) {
              if jsonResult is [String : AnyObject] {
                let dictionary = jsonResult as! [String : AnyObject]
                let user = User.fromJSON(dictionary) as! User
                User.saveUser(user)

                // Retrieve the User Vehicles
                //----------------------------
                signedRequest(BlinkMyCarAPI.AllVehicles,
                  completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in

                    // IF THIS STEP FAIL THEN LOGIN FAILS
                    if error != nil || statusCode != 200 {
                      AccessToken().deleteToken()
                      User.deleteSavedUser()
                      completionBlock(error: NSError(domain: "com.blinkmycar.network",
                        code: BlinkMyCarErrorCodes.ProfileAPIFailed.rawValue, userInfo: nil))
                      return
                    }

                    if let data = data,
                      jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data,
                        options: NSJSONReadingOptions.AllowFragments) {
                          let dictionary = jsonResult as! [String : AnyObject]
                          let vehicles = Vehicle.vehiclesFromJSON(dictionary)
                          // TODO: UPDATE THE DB
                          // For now we save in the user defaults

                          Vehicle.saveVehicles(vehicles)
                          if let vehicle = vehicles.first {
                            Vehicle.saveCurrentVehicle(vehicle)
                          }

                          // Retrieve the Country information
                          //-----------------------------------
                          signedRequest(BlinkMyCarAPI.CountryInformation(countryId: "LB"),
                            completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
                              if statusCode != 200 {
                                AccessToken().deleteToken()
                                User.deleteSavedUser()
                                Vehicle.deleteCurrentVehicle()
                                Vehicle.deleteSavedVehicles()

                                let error = NSError(domain: "com.blinkmycar.network",
                                  code: BlinkMyCarErrorCodes.RetrieveCountryInformationFailed.rawValue, userInfo: nil)
                                completionBlock(error: error)
                                return
                              }

                              let jsonDictionary = try! NSJSONSerialization.JSONObjectWithData(data!,
                                options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject]
                              let country = Country.fromJSON(jsonDictionary!) as! Country
                              Country.saveCountry(country)

                              completionBlock(error: nil)

                          })
                          return
                    }


                    completionBlock(error: NSError(domain: "com.blinkmycar.network",
                      code: BlinkMyCarErrorCodes.ProfileAPIFailed.rawValue, userInfo: nil))
                })
                return
              }
        }

        AccessToken().deleteToken()
        completionBlock(error: NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.ProfileAPIFailed.rawValue, userInfo: nil))
        return
    })
  }

  //===========================================================
  // MARK: U I   T E X T
  //===========================================================

  var viewControllerTitle: String {
    return NSLocalizedString("SignInViewController.viewControllerTitle", comment: "SignIn View Controller Title that appears in the navigation controller")
  }

  var labelEmailText: String {
    return NSLocalizedString("SignInViewController.LabelEmail", comment: "Text That is displayed above the email text field")
  }

  var labelPasswordText: String {
    return NSLocalizedString("SignInViewController.LabelPasswordText", comment: "Text That is displayed above the password text field")
  }

  var buttonSignInTitle: String {
    return NSLocalizedString("SignInViewController.ButtonSignInTitle", comment: "Text That is displayed In the Sign In button")
  }
  
  var buttonForgotPasswordTitle: String {
    return NSLocalizedString("SignInViewController.buttonForgotPasswordTitle", comment: "Text That is displayed below Sign In button - forget password")
  }

  var textFieldEmailErrorMessage: String {
    return NSLocalizedString("SignInViewController.TextFieldEmailErrorMessage", comment: "Error Message that is displayed in case the Email field content is not valid")
  }

  var textFieldPasswordErrorMessage: String {
    return NSLocalizedString("SignInViewController.TextFieldPasswordErrorMessage", comment: "Error Message that is displayed in case the Password field content is empty")
  }

  var registerButtonText: String {
    return NSLocalizedString("SignInViewController.RegisterButtonText", comment: "Text that is displayed in the button that takes the user to the registration View")
  }

  var registerLabelText: String {
    return NSLocalizedString("SignInViewController.RegisterLabelText", comment: "Text that is displayed in the label above the register button")
  }

  var alertViewTitle: String {
    return NSLocalizedString("SignInViewController.AlertViewTitle", comment: "Alert view title, the alert view holds error messages related to the Sign in process")
  }

  var alertViewServerErrorMessage: String {
    return NSLocalizedString("SignInViewController.AlertViewServerErrorMessage", comment: "Error message from the server informing user that he couldn't login. Username or password are wrong")
  }

  var alertViewInvalidEmailAndOrPassword: String {
    return NSLocalizedString("SignInViewController.AlertViewInvalidEmailAndOrPassword", comment: "Error message that is displayed after checking the validation of the email and password fields before calling the server")
  }

  var alertViewDismissActionButtonText: String {
    return NSLocalizedString("SignInViewController.AlertViewDismissActionButtonText",
      comment: "Alert view dismiss button")
  }

  var invalidCredentialsErrorMessage: String {
    return NSLocalizedString("SignInViewController.tempFormErrorMessage",
      comment: "Error message that is displayed after having a server error (invalid username or password) and is hidden after starting editing the usernam pr password fields")
  }
}
