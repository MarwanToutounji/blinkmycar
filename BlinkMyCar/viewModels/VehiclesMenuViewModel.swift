//
//  VehiclesMenuViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class VehiclesMenuViewModel {
  var analyticsCategory = AnalyticsCategories.RightMenu

  var vehicles: [Vehicle] = []

  init() {
    loadData()
  }
  
  func loadData() {
    vehicles = Vehicle.getSavedVehicles()
    sortVehicles()
  }
  
  var selectedVehicle: Vehicle? {
      return Vehicle.getCurrentVehicle()
  }

  func sortVehicles() {
    vehicles.sortInPlace({ $0.name < $1.name })
  }

  func reloadVehicles() {
    vehicles = Vehicle.getSavedVehicles()
    sortVehicles()
  }

  var vehicleListingViewControllerTitle: String {
    return NSLocalizedString("VehiclesListingViewController.vehicleListingViewControllerTitle",
      value: "My Vehicles",
      comment: "Text that appears in the navigation Bar when the Vehicle listing VC is displayed")
  }

  var switchVehicleViewControllerTitle: String {
    return NSLocalizedString("VehiclesMenuViewController.switchVehicleViewControllerTitle",
      value: "Change Vehicle",
      comment: "Text that appears in the navigation Bar when the Vehicle menu VC is displayed")
  }

  var addVehicleButtonText: String {
    return NSLocalizedString("VehiclesMenuViewController.addVehicleButtonText",
      comment: "Text That appears in the Add Vehicle Button in the side menu")
  }

  
  var alertViewDeleteConfirmationTitle: String {
    return NSLocalizedString("VehiclesListingViewController.deleteAlertTitle",
      comment: "Vehicles Listing delete alert confirmation")
  }
  var alertViewDeleteConfirmationMessage: String {
    return NSLocalizedString("VehiclesListingViewController.deleteAlertMessage",
      comment: "Vehicles Listing delete alert confirmation")
  }
  var alertViewDeleteConfirmationNegativeButtonTitle: String {
    return NSLocalizedString("VehiclesListingViewController.deleteAlertNegativeButton",
      comment: "Vehicles Listing delete alert confirmation")
  }
  var alertViewDeleteConfirmationPostiveButtonTitle: String {
    return NSLocalizedString("VehiclesListingViewController.deleteAlertPostionButton",
      comment: "Vehicles Listing delete alert confirmation")
  }

  var alertViewCantDeleteTitle: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewCantDeleteTitle",
      comment: "Vehicles Listing cant delete last item alert")
  }
  var alertViewCantDeleteMessage: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewCantDeleteMessage",
      comment: "Vehicles Listing cant delete last item alert")
  }
  var alertViewCantDeleteButtonTitle: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewCantDeleteButtonTitle",
      comment: "Vehicles Listing cant delete delete last alert ")
  }

  var alertViewCantDeleteLastVehicleTitle: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewCantDeleteLastVehicleTitle",
      comment: "Vehicles Listing cant delete last item alert")
  }
  var alertViewCantDeleteLastVehicleMessage: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewCantDeleteLastVehicleMessage",
      comment: "Vehicles Listing cant delete last item alert")
  }
  var alertViewCantDeleteLastVehicleButtonTitle: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewCantDeleteLastVehicleButtonTitle",
      comment: "Vehicles Listing cant delete delete last alert ")
  }
  
  var deleteVehicleErrorMessage: String {
    return NSLocalizedString("VehiclesListingViewController.deleteVehicleErrorMessage",
      comment: "Delete a Vehicle error message")
  }
  
  var alertViewErrorDeletingTitle: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewErrorDeletingTitle",
      comment: "Error deleting Vehicle from Listing alert title")
  }

  var alertViewErrorDeletingButtonTitle: String {
    return NSLocalizedString("VehiclesListingViewController.alertViewErrorDeletingButtonTitle",
      comment: "Error deleting Vehicle from Listing alert button title")
  }
}

//MARK: TABLE VIEW HELPERS
extension VehiclesMenuViewModel {
  func numberOfRows() -> Int {
    return vehicles.count
  }

  /// Cannot delete a vehicle if there is only one vehicle
  func canDeleteVehicleAtIndex(row: Int) -> Bool {
    if numberOfRows() <= 1 {
      return false
    }

    let vehicle = vehicles[row]
    if let vehicleId = vehicle.id {
      let allBookingsForVehicle = Booking.getSavedBookingsForVehicleId(vehicleId)
      var bookings = allBookingsForVehicle.filter({
        $0.status == .Pending ||
        $0.status == .Confirmed
      })

      bookings += allBookingsForVehicle.filter({
          $0.status == .Modified ||
          $0.status == .Processing
      })
      return bookings.count <= 0
    }

    return true
  }
  
  func cellForRow(indexPath: NSIndexPath)  -> Vehicle {
    return vehicles[indexPath.row]
  }
  
  func deleteVehicleForRow(row: Int, completionBlock: ((success: Bool, error: NSError?) -> Void)?) {
    let vehicleToDeleteId: String = vehicles[row].id!
    
    signedRequest(BlinkMyCarAPI.DeleteVehicle(id: vehicleToDeleteId),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 204 {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.DeleteVehicleFailed.rawValue,
            userInfo: ["error":self.deleteVehicleErrorMessage])
          completionBlock?(success: false, error: error)
          return
        }

        let vehicle = self.vehicles[row]

        //Delete locally
        self.vehicles.removeAtIndex(row)
        Vehicle.deleteVehicle(vehicleToDeleteId)

        // If the deleted Vehicle id the current selected vehicle
        // Change the current selected Vehicle
        let currentVehicle = Vehicle.getCurrentVehicle()!
        if vehicle.id == currentVehicle.id {
          Vehicle.saveCurrentVehicle(Vehicle.getSavedVehicles().first!)
        }

        completionBlock?(success: true, error: nil)
    })
  }
}