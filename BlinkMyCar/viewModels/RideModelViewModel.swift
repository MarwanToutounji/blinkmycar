//
//  RideModelViewModel.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/19/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public class RideModelViewModel {
  var title = NSLocalizedString("VehicleInfoViewController.chooseModel",
    comment: "Choose your model view title")

  var searchTextPlaceHolder = NSLocalizedString("VehicleInfoViewController.searchTextPlaceHolder",
    comment: "The search textfield place holder")

  var question = NSLocalizedString("VehicleInfoViewController.chooseModelQuestion",
    comment: "Choose your model question: What model is it?")

  private let vehicleMake: Make
  private var dataSourceEntries: [String]

  public init(make: Make) {
    vehicleMake = make
    dataSourceEntries = vehicleMake.models
  }

  public func numberOfSections() -> Int {
    return dataSourceEntries.count > 0 ? 1 : 0
  }

  public func numberOfRows(section: Int) -> Int {
    return dataSourceEntries.count
  }

  public func itemForIndexPath(indexPath: NSIndexPath) -> String {
    return dataSourceEntries[indexPath.row]
  }

  public func isLastItem(indexPath: NSIndexPath) -> Bool {
    return indexPath.row == (dataSourceEntries.count - 1)
  }

  public func search(searchFor: String) {
    if searchFor.characters.count == 0 {
      dataSourceEntries = vehicleMake.models
    } else {
      dataSourceEntries = vehicleMake.models.filter {
        $0.rangeOfString(searchFor, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil
        } ?? []
    }
  }
}
