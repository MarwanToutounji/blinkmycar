//
//  HomeViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class HomeViewModel {
  var leftMenuBarButtonText : String {
    return NSLocalizedString("HomeViewController.leftMenuBarButtonText",
      comment: "The details menu button text")
  }

  var hasBookings: Bool {
    return Booking.getSavedBookings().count > 0
  }

  var hasCompletedWasheThatNeedsRating: Bool {
    return Booking.bookingThatNeedRating != nil
  }

  var completedWashThatNeedsRating: Booking! {
    return Booking.bookingThatNeedRating
  }

  var areBookingsForCurrentVehicleSynced: Bool {
    return PersistenceManager.sharedManager.areBookingsForCurrentVehicleUpToDate()
  }

  var currentVehicleHasSavedBookings: Bool {
    return Booking.getSavedBookingsForCurrentVehicle().count > 0
  }
}
