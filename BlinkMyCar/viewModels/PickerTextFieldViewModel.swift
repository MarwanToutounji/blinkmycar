//
//  PickerTextFieldViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/23/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

struct KWDPickerTextFieldNotificationsName {
  static let DidUpdateModelViewSelection = "PickerTextFieldNotificationsNameDidUpdateModelViewSelection"
}

class PickerTextFieldViewModel {
  private var components: [FilterValue] = [FilterValue]()
  private var _selectedComponent: FilterValue?
  
  convenience init() {
    self.init(components: nil, allItem: true)
  }
  
  init(components: [FilterValue]?, allItem: Bool) {
    // Insert the All Values
    var allFilterValue: FilterValue?
    
    if allItem {
      allFilterValue = FilterValue()
      allFilterValue!.label = "All"
      allFilterValue!.value = nil
      _selectedComponent = allFilterValue
    }
    
    if components != nil {
      self.components = components!
      if self.components.count > 0 {
        _selectedComponent = self.components[0]
      }
    }
    if let allFilterValue = allFilterValue {
      self.components.insert(allFilterValue, atIndex: 0)
    }
  }
  
  //===========================================================
  // MARK: GETTERS & SETTERS
  //===========================================================
  
  var selectedComponent: FilterValue {
    get {
      return _selectedComponent ?? FilterValue()
    }
    set {
      _selectedComponent = newValue
    }
  }
  
  func setSelectedComponent(component: FilterValue?) {
    var component = component
    if component == nil {
      component = components.first
    }
    
    selectedComponent = component!
    NSNotificationCenter.defaultCenter().postNotificationName(
      KWDPickerTextFieldNotificationsName.DidUpdateModelViewSelection,
      object: nil)
  }
  
  //===========================================================
  // MARK: PUBLIC
  //===========================================================
  
  var numberOfComponents: Int {
    return ((components.count ?? 0)>0) ?1:0
  }
  
  var numberOfRows: Int {
    return components.count ?? 0
  }
  
  func titleForRow(row: Int) -> String! {
    return components[row].label ?? ""
  }
  
  func setSelectedComponentAtRow(row: Int) {
    selectedComponent = components[row]
  }
  
  var selectedText: String {
    return selectedComponent.label ?? ""
  }
  
  var selectedRow: Int {
    return components.indexOf(selectedComponent) ?? 0
  }
}