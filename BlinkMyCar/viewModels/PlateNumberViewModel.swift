//
//  PlateNumberViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/22/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public class PlateNumberViewModel {

  var title = NSLocalizedString("VehicleInfoViewController.plateNumber",
    comment: "Plate Number")
  
  var question = NSLocalizedString("VehicleInfoViewController.plateNumberQuestion",
    comment: "Enter Plate number: Would you happen to now your plate number ?")
  
  let saveButton: String = NSLocalizedString("VehicleInfoViewController.saveButton",
    comment: "Save button label text: Save")
  
  let skipButton: String = NSLocalizedString("VehicleInfoViewController.skipButton",
    comment: "Save button label text: Save")
  
  let plateNumberPlaceHolder: String = NSLocalizedString("VehicleInfoViewController.plateNumberPlaceHolder",
    comment: "Plate Numebr text field placeholder")
  
  let plateNumberLabel: String = NSLocalizedString("VehicleInfoViewController.plateNumberLabel",
    comment: "Plate Numebr text field placeholder")
}
