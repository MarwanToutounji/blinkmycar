//
//  VehicleProfileViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/5/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public enum VehicleProfileItem {
  case Brand
  case Model
  case Color
  case PlateNumber

  private var brand: String { return NSLocalizedString("VehicleProfileViewController.brandTableItem",
    comment: "Car profile controller table item: brand") }
  private var model: String { return NSLocalizedString("VehicleProfileViewController.modelTableItem",
    comment: "Car profile controller table item: model") }
  private var color: String { return NSLocalizedString("VehicleProfileViewController.colorTableItem",
    comment: "Car profile controller table item: color") }
  private var plateNumber: String { return NSLocalizedString("VehicleProfileViewController.plateNumberTableItem",
    comment: "Car profile controller table item: plate number") }

  func toString() -> String {
    switch(self) {
    case Brand:
      return brand
    case Model:
      return model
    case Color:
      return color
    case PlateNumber:
      return plateNumber
    }
  }
  
  func rowNumber() -> Int {
    switch(self) {
    case Brand:
      return 0
    case Model:
      return 1
    case Color:
      return 2
    case PlateNumber:
      return 3
    }
  }
}

class VehicleProfileViewModel {
  var data: [VehicleProfileItem] = []
  var vehicle: Vehicle?
  var colorModel: RideColorViewModel = RideColorViewModel()
  var rideType: Make.VehicleType {
    get {
      return vehicle!.type!
    }
  }
  
  var carNameValue: String?
  var brandValue: String?
  var modelValue: String?
  var colorValue: String?
  var plateNumberValue: String?
  var plateNumberFullValue: String? {
    get {
      return (plateNumberValue != nil) ? "\(plateNumberValue!)" : nil
    }
  }
  var uploadImageData: NSData? = nil
  
  var canUpdate: Bool {
    get {
      return (carNameValue != nil && carNameValue != "") || (brandValue != nil && brandValue != "")
        || (modelValue != nil && modelValue != "") || (colorValue != nil && colorValue != "")
        || (plateNumberFullValue != nil && plateNumberFullValue != "")
        || (uploadImageData != nil && uploadImageData != "")
    }
  }
  
  var rideIconName: String {
    get {
      switch(rideType) {
      case Make.VehicleType.Car:
        return "iconCar"
      case Make.VehicleType.Motorcycle:
        return "iconMotorcycle"
      }
    }
  }
  
  init() {
    data.append(.Brand)
    data.append(.Model)
    data.append(.Color)
    data.append(.PlateNumber)
  }
  
  func itemAtRow(row: Int) -> VehicleProfileItem {
    return data[row]
  }
  
  func valueForItemAtRow(row: Int) -> String {
    switch(self.itemAtRow(row)) {
    case .Brand:
      return brandValue ?? vehicle!.make ?? ""
    case .Model:
      return modelValue ?? vehicle!.model ?? ""
    case .Color:
      return colorValue ?? vehicle?.color ?? ""
    case .PlateNumber:
      return plateNumberFullValue ?? vehicle?.plateNumber ?? ""
    }
  }
  
  func isColorRowNumber(row: Int) -> Bool {
    return row == VehicleProfileItem.Color.rowNumber()
  }

  var vehicleMake: Make {
    return Make(name: vehicle!.make!, models: [String](), type: vehicle!.type!, imagePath: "")
  }
  
}
//MARK: A P I  R E Q U E S T
extension VehicleProfileViewModel {
  func updateRide(completion: (Bool) -> Void) {
    signedRequest(
      BlinkMyCarAPI.UpdateVehicle(id: vehicle!.id!, color: colorValue, make: brandValue, model: modelValue, name: carNameValue, plateNumber: plateNumberFullValue, type: vehicle!.type!.rawValue, year: nil),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 200 {
          completion(false)
          return
        }
        
        if let data = data,
          jsonResult: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) as? [String : AnyObject] {
              var vehicle = Vehicle.fromJSON(jsonResult) as! Vehicle
              self.vehicle = vehicle
              // Save Updated Vehicle locally
              self.saveLocally(vehicle)
              // Update Affected bookings
              self.updateBookingsForUpdatedVehicleInformation()

              if let imageData = self.uploadImageData {
                uploadRequest(BlinkMyCarAPI.Vehicle(id: vehicle.id!), data: (imageData ?? NSData()), progress: { (percentage, json, error) -> Void in
                  //TODO: Handle errors
                  if let _ = error {
                    completion(false)
                    return
                  }
                  if percentage != 1 {
                    return
                  }
                  if let jsonData = json as? NSData,
                    jsonResult: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(jsonData,
                      options: NSJSONReadingOptions.AllowFragments) as? [String : AnyObject] {
                    vehicle = Vehicle.fromJSON(jsonResult) as! Vehicle

                    // Save Updated Vehicle locally
                    self.saveLocally(vehicle)

                    completion(true)
                  } else {
                    completion(false)
                  }
                })
              }
              else{
                completion(true)
              }
        }
    })
  }

  func updateBookingsForUpdatedVehicleInformation() {
    let affectedBookings = Booking.getSavedBookingsForVehicleId(vehicle!.id!)
    PersistenceManager.sharedManager.updateBookingsVehicleInfo(self.vehicle!,
      bookingIndex: 0,
      bookings: affectedBookings) { (vehicle, success, error) -> Void in
        if !success {
          Vehicle.saveVehicleIdForUpdatingRelatedBookings(vehicle.id!)
          return
        }

        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.DidUpdateBookings.rawValue,
          object: nil)
    }
  }

  func saveLocally(vehicle: Vehicle) {
    Vehicle.saveVehicle(vehicle)
    if vehicle.id! == Vehicle.getCurrentVehicle()!.id! {
      Vehicle.saveCurrentVehicle(vehicle)
    }
    //Update view model vehicle object
    self.vehicle = vehicle
  }
}
//MARK: Table Functionality Handler
extension VehicleProfileViewModel {
  
  func numberOfRows() -> Int {
    return data.count
  }
  
  func cellForRow(indexPath: NSIndexPath)  -> (title: String, value: String, color: UIColor?) {
    let item: VehicleProfileItem = self.itemAtRow(indexPath.row)
    let value: String = self.valueForItemAtRow(indexPath.row)
    if item == .Color {
      return (item.toString(),value,colorModel.colorForString(value))
    }
    return (item.toString(),value,nil)
  }
}
//MARK: L O C A T I Z A T I O N
extension VehicleProfileViewModel {
  var viewControllerTitle: String {
    return NSLocalizedString("VehicleProfileViewController.ViewControllerTitle",
      comment: "Car profile controller title that appears in the navigation Bar")
  }
}

//MARK: LOCALIZATION
extension VehicleProfileViewModel {
  var alertViewFailToLoadModelsAlertTitle: String {
    return NSLocalizedString("VehicleProfileViewModel.alertViewFailToLoadModelsAlertTitle",
      comment: "Title in alert view that shows when loading models fails")
  }
  
  var alertViewFailToLoadModelsAlertMessage: String {
    return  NSLocalizedString("VehicleProfileViewModel.alertViewFailToLoadModelsAlertMessage",
      comment: "Message of alert view that shows when loading models fails")
  }
  
  var alertViewFailToLoadModelsAlertActionButtonText: String {
    return NSLocalizedString("VehicleProfileViewModel.alertViewFailToLoadModelsAlertActionButtonText",
      comment: "Action Button text of alert view that shows when loading models fails")
  }

  var alertViewFailToUploadImageAlertTitle: String {
    return NSLocalizedString("VehicleProfileViewModel.alertViewFailToUploadImageAlertTitle", value: "Update",
      comment: "Title in alert view that shows when uploading picture fails")
  }

  var alertViewFailToUploadImageAlertMessage: String {
    return  NSLocalizedString("VehicleProfileViewModel.alertViewFailToUploadImageAlertMessage", value: "Ooops!! we couldn't upload the vehicle picture!!",
      comment: "Message of alert view that shows when uploading picture fails")
  }

  var alertViewFailToUploadImageAlertActionButtonText: String {
    return NSLocalizedString("VehicleProfileViewModel.alertViewFailToUploadImageAlertActionButtonText", value: "Ok",
      comment: "Action Button text of alert view that shows when uploading picture fails")
  }

  var alertViewFailToUpdateVehicleProfileAlertTitle: String {
    return NSLocalizedString("VehicleProfileViewModel.alertViewFailToUpdateVehicleProfileAlertTitle", value: "Update",
      comment: "Title in alert view that shows when updating a vehicle profile fails")
  }

  var alertViewFailToUpdateVehicleProfileAlertMessage: String {
    return  NSLocalizedString("VehicleProfileViewModel.alertViewFailToUpdateVehicleProfileAlertMessage", value: "Ooops!! we couldn't update your vehicle information!!",
      comment: "Message of alert view that shows when updating a vehicle profile fails")
  }

  var alertViewFailToUpdateVehicleProfileAlertActionButtonText: String {
    return NSLocalizedString("VehicleProfileViewModel.alertViewFailToUpdateVehicleProfileAlertActionButtonText", value: "Ok",
      comment: "Action Button text of alert view that shows when updating a vehicle profile fails")
  }
}
