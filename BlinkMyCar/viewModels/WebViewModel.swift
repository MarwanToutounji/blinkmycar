//
//  WebViewModel.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/14/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

class WebViewModel {
  
  var url: String?
  var title: String?
}