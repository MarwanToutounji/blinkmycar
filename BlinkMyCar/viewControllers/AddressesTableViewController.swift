//
//  AddressesTableViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/22/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import SwiftLoader

class AddressesTableViewCell: UITableViewCell {
  @IBOutlet weak var addressTypeImageView: UIImageView!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var addressNote: UILabel!
  @IBOutlet weak var separator: UIView!

  override func awakeFromNib() {
    super.awakeFromNib()

    addressTypeImageView.tintColor = UIColor.applicationMainColor()
    separator.backgroundColor = UIColor.lighterMidGray()
  }
  
  func setAddressLabelText(text: String?, addressType: String?) {
    if let addressType = addressType where !addressType.isEmpty {
      let separator = "\n"
      var displayString = addressType
      if let text = text {
        displayString += separator + text
      }
      
      let attributedString = NSMutableAttributedString(string: displayString)
      let rangeOfAddressType = (addressType as NSString).rangeOfString(addressType)
      attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldBMCFont(size: 18), range: rangeOfAddressType)
      attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.applicationSecondaryColor(), range: rangeOfAddressType)
      addressLabel.attributedText = attributedString
    } else {
      addressLabelText = text
    }
    
  }

  var addressLabelText: String? {
    get {
      return addressLabel.text
    }

    set {
      addressLabel.text = newValue
    }
  }

  var addressNoteText: String? {
    get {
      return addressNote.text
    }

    set {
      addressNote.text = newValue
    }
  }


  var addressImage: UIImage? {
    get {
      return addressTypeImageView.image
    }

    set {
      addressTypeImageView.image = newValue
    }
  }

  class var reusableIdentifier: String {
    return "AddressesTableViewCell"
  }

}

protocol AddressesTableViewControllerDelegate {
  func addressesTableViewDidSelectAddress(viewController: AddressesTableViewController, address: Address, withMode mode: AddressProfileViewControllerMode?)
  func addressesTableViewDidTapEditActionButton(viewController: AddressesTableViewController, forAddress address: Address)
  func addressesTableViewDidTapAddAddresses(viewController: AddressesTableViewController)
}

class AddressesTableViewController: UITableViewController {

  var viewModel = AddressesViewModel()

  var addressesTableViewDelegate: AddressesTableViewControllerDelegate?

  private var didAppearOnce: Bool = false

  required init!(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.viewControllerTitle
    self.sendScreenView("My Locations")

    if addressesTableViewDelegate == nil {
      addressesTableViewDelegate = self
    }

    self.tableView.tableFooterView = addAddressesView()

    // Register for the notifications related to any addresses updates
    NSNotificationCenter.defaultCenter().addObserver(
      self, selector: #selector(AddressesTableViewController.reloadData),
      name: BMCNotificationNames.DidUpdateAddresses.rawValue,
      object: nil)
  }

  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)

    if !didAppearOnce {
      didAppearOnce = true
      reloadData()
    }
  }

  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  func initNavigationItems() {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if viewModel.addresses?.count > 0 {
      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: self, action: #selector(AddressesTableViewController.goToEditMode))
    } else {
      navigationItem.rightBarButtonItem = nil
    }
  }

  //=================================================================
  // MARK: - Table view data source
  //=================================================================

  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }

  override func tableView(tableView: UITableView,
    numberOfRowsInSection section: Int) -> Int {
      return viewModel.numberOfRows
  }

  override func tableView(tableView: UITableView,
    cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCellWithIdentifier(AddressesTableViewCell.reusableIdentifier)
        as! AddressesTableViewCell
      cell.setAddressLabelText(viewModel.addressTitleForRow(indexPath.row),
        addressType: viewModel.addressTypeForRow(indexPath.row))
      cell.addressImage = viewModel.addressImageForRow(indexPath.row)
      cell.addressNoteText = viewModel.addressNoteForRow(indexPath.row)
      
      cell.addressLabel.numberOfLines = 4
      if !(cell.addressNoteText ?? "").isEmpty {
        cell.addressLabel.numberOfLines = 1
      }

      cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
      if viewModel.disableNotServiceableAddresses {
        if !viewModel.addressForRowIsServiceable(indexPath.row) {
          cell.accessoryType = UITableViewCellAccessoryType.None
        }
      }

      if viewModel.addressForRowIsSelected(indexPath.row) {
        tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
      }

      return cell
  }

  //=================================================================
  // MARK: - Table view delegate
  //=================================================================

  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Existing Location Tap", label: "", value: nil)
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    let selectedAddress = viewModel.addressForRow(indexPath.row)
    addressesTableViewDelegate?.addressesTableViewDidSelectAddress(self, address: selectedAddress, withMode: nil)
  }
  
  func deleteAddressAlert(indexPath: NSIndexPath) {
    let alertController = UIAlertController(
      title: viewModel.deleteAddressAlertViewTitle,
      message: viewModel.deleteAddressAlertViewMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(
      title: viewModel.deleteAddressAlertViewCancelActionButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(
      title: viewModel.deleteAddressAlertViewOkActionButtonTitle,
      style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        SwiftLoader.show(animated: true)
        self.viewModel.deleteAddressForRow(indexPath.row, completionBlock:
          { (success: Bool, error: NSError?) -> Void in
            SwiftLoader.hide()
            
            if !success {
              self.showCouldNotDeleteAddressAlert()
              return
            }
            
            if self.viewModel.numberOfSections > 0 {
              self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            } else {
              self.tableView.deleteSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
            }
            self.handleNoDataView()
        })
    }
    
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
    let view = UITableViewRowAction(style: .Normal, title: "Edit") { action, index in
      self.trackEvent(self.viewModel.analyticsCategory.rawValue, action: "Edit Location Tap", label: "", value: nil)
      let selectedAddress = self.viewModel.addressForRow(indexPath.row)
      self.addressesTableViewDelegate?.addressesTableViewDidTapEditActionButton(self, forAddress: selectedAddress)
    }
    view.backgroundColor = UIColor.applicationMainColor()
    
    let delete = UITableViewRowAction(style: .Normal, title: "Delete") { action, index in
      self.trackEvent(self.viewModel.analyticsCategory.rawValue, action: "Delete Location Tap", label: "", value: nil)
      self.deleteAddressAlert(index)
    }
    delete.backgroundColor = UIColor.redColor()
    
    return [delete, view]
  }
  
  override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
  }

  //=================================================================
  // MARK: - Actions
  //=================================================================

  func didTapAddAddress() {
    if viewModel.haveValidAddresses {
      self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Add New Location Tap", label: "", value: nil)
      addressesTableViewDelegate?.addressesTableViewDidTapAddAddresses(self)
    } else {
      showCouldNotProcedeNeedToSyncWithServerAlert()
    }
  }

  func didTapCancel() {
    stopEditing()
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func didTapDoneButton() {
    stopEditing()
  }
  
  func stopEditing() {
    if tableView.editing {
      tableView.beginUpdates()
      tableView.editing = false
      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: self, action: #selector(AddressesTableViewController.goToEditMode))
      tableView.endUpdates()
    }
  }
  func goToEditMode() {
    if !tableView.editing {
      let doneButton = UIBarButtonItem(barButtonSystemItem: .Done,
        target: self, action: #selector(AddressesTableViewController.didTapDoneButton))
      self.navigationItem.rightBarButtonItem = doneButton
      tableView.beginUpdates()
      tableView.editing = true
      tableView.endUpdates()
    }
  }

  func reloadData() {
    SwiftLoader.show(animated: true)
    viewModel.loadData { (success: Bool, error: NSError?) -> Void in
      if success {
        self.tableView.reloadData()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
      } else {
        if let error = error {
          switch error.code {
          case BlinkMyCarErrorCodes.RetrieveAddressesFailed.rawValue:
            self.showCouldNotLoadAddressesAlert()
          default:
            self.showGeneralAddressesAlert()
          }
        }
      }

      self.handleNoDataView()

      SwiftLoader.hide()
      self.initNavigationItems()
    }
  }

  
  func errorLoadingAddressesAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewErrorLoadingAddressesTitle,
      message: viewModel.alertViewErrorLoadingAddressesMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewErrorLoadingAddressesButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }

  
  //=================================================================
  // MARK: - Helpers
  //=================================================================
  
  func handleNoDataView() {
    if self.viewModel.noData {
      let headerView: UIView = UIView(frame: CGRectMake(0, 0,tableView.frame.width, 200))

      //create a lable size to fit the Table View
      let messageLbl: UILabel = BMCListItemLabel()
      //set the message
      messageLbl.text = viewModel.addAddressesNoRecordsMessage
      //center the text
      messageLbl.textAlignment = NSTextAlignment.Center
      messageLbl.numberOfLines = 0
      messageLbl.alpha = 0
      headerView.addSubview(messageLbl)

      messageLbl.alignLeading("30", trailing: "-30", toView: headerView)
      messageLbl.alignCenterYWithView(headerView, predicate: "15")

      UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut,
        animations: { () -> Void in
          self.tableView.tableHeaderView = headerView
          messageLbl.alpha = 1
      }, completion: nil)
    }
    else {
      UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut,
        animations: { () -> Void in
          self.tableView.tableHeaderView = nil
        }, completion: nil)
    }

  }
  
  
  private func addAddressesView() -> UIView {
    let container = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 104))
    let addAddressButton = BMCLinkButton(type: UIButtonType.Custom)
    addAddressButton.addTarget(self, action: #selector(AddressesTableViewController.didTapAddAddress),
      forControlEvents: UIControlEvents.TouchUpInside)
    addAddressButton.setTitle(viewModel.addAddressesButtonText, forState: UIControlState.Normal)
    container.addSubview(addAddressButton)

    addAddressButton.constrainHeight("44")
    addAddressButton.alignLeading("30", trailing: "-30", toView: container)
    addAddressButton.alignCenterYWithView(container, predicate: "0")

    return container
  }


  func showCouldNotLoadAddressesAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewTitleCouldNotLoadAddresses,
      message: viewModel.alertViewMessageCouldNotLoadAddresses,
      preferredStyle: UIAlertControllerStyle.Alert)

    let cancelAction = UIAlertAction(title: viewModel.alertViewCancelButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(title: viewModel.alertViewTryAgainButtonTitle,
      style: UIAlertActionStyle.Default)
      { (UIAlertAction) -> Void in
        self.reloadData()
    }

    alertController.addAction(okAction)
    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func showGeneralAddressesAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewTitleGeneralError,
      message: viewModel.alertViewMessageGeneralError,
      preferredStyle: UIAlertControllerStyle.Alert)

    let cancelAction = UIAlertAction(title: viewModel.alertViewCancelButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(title: viewModel.alertViewTryAgainButtonTitle,
      style: UIAlertActionStyle.Default)
      { (UIAlertAction) -> Void in
        self.reloadData()
    }

    alertController.addAction(okAction)
    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func showCouldNotDeleteAddressAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewTitleCouldNotDeleteAddress,
      message: viewModel.alertViewMessageCouldNotDeleteAddress,
      preferredStyle: UIAlertControllerStyle.Alert)

    let cancelAction = UIAlertAction(title: viewModel.alertViewOkButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)

    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func showCouldNotProcedeNeedToSyncWithServerAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewTitleCouldNotProceedNeedToSyncWithServer,
      message: viewModel.alertViewMessageCouldNotProceedNeedToSyncWithServer,
      preferredStyle: UIAlertControllerStyle.Alert)

    let cancelAction = UIAlertAction(title: viewModel.alertViewCancelButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(title: viewModel.alertViewOkButtonTitle,
      style: UIAlertActionStyle.Default)
      { (UIAlertAction) -> Void in
        self.reloadData()
    }

    alertController.addAction(okAction)
    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

extension AddressesTableViewController: AddressesTableViewControllerDelegate {
  func addressesTableViewDidSelectAddress(viewController: AddressesTableViewController, address: Address, withMode mode: AddressProfileViewControllerMode?) {
    if viewModel.haveValidAddresses {
      let vc = storyboard?.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.AddressProfileViewController.rawValue) as! AddressProfileViewController
      vc.delegate = self
      if let mode = mode {
        vc.initialRequiredMode = mode
      }
      let addressProfileViewModel = AddressProfileViewModel()
      addressProfileViewModel.address = address
      addressProfileViewModel.analyticsCategory = viewModel.analyticsCategory
      vc.viewModel = addressProfileViewModel
      self.navigationController?.pushViewController(vc, animated: true)
    } else {
      showCouldNotProcedeNeedToSyncWithServerAlert()
    }
  }
  
  func addressesTableViewDidTapEditActionButton(viewController: AddressesTableViewController, forAddress address: Address) {
    addressesTableViewDidSelectAddress(viewController, address: address, withMode: AddressProfileViewControllerMode.EditAddressDetails)
  }

  func addressesTableViewDidTapAddAddresses(viewController: AddressesTableViewController) {
    
    let vc = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.AddressProfileViewController.rawValue) as! AddressProfileViewController
    vc.viewModel.analyticsCategory = viewModel.analyticsCategory
    vc.delegate = self
    self.navigationController?.pushViewController(vc, animated: true)
  }
}

extension AddressesTableViewController: AddressProfileViewControllerDelegate {
  func didAddAddressSuccessfully(address: Address) {
    self.navigationController?.popToRootViewControllerAnimated(true)
  }
  func didEditAddressSuccessfully(address: Address) {
    // Do nothing here, Should not reach here
    
  }
}
