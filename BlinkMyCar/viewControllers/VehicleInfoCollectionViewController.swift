//
//  VehicleInfoCollectionViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/26/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

protocol VehicleInfoCollectionViewControllerDelegate {
  func didSelectColor(collectionView:VehicleInfoCollectionViewController, selectedColor: UIColor?)
}

class VehicleInfoCollectionViewController: UIViewController {

  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var collectionView: ColorsCollectionView!

  var delegate: VehicleInfoCollectionViewControllerDelegate?

  var colors = [UIColor]()
  var selectedColor: UIColor?

  var descriptionLabel: String? {
    didSet{
      if topLabel != nil {
        topLabel.text = descriptionLabel
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    collectionView.colorsCollection = colors
    collectionView.colorsDelegate = self
    collectionView.selectedColor = selectedColor
    collectionView.reloadData()
  }

}

extension VehicleInfoCollectionViewController: ColorsCollectionViewDelegate {
  func colorsCollectionViewDidSelectColor(color: UIColor?) {
    selectedColor = color
    delegate?.didSelectColor(self, selectedColor: selectedColor)
  }
}
