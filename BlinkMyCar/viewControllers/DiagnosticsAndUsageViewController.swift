//
//  DiagnosticsAndUsageViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class DiagnosticsAndUsageViewController: UIViewController {

  @IBOutlet weak var headerMessageLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  
  var viewModel: DiagnosticsAndUsageViewModel = DiagnosticsAndUsageViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupContent()
    self.applyTheme()
  }
  
  private func setupContent() {
    self.title = self.viewModel.viewControllerTitle
    self.sendScreenView("Diagnostics")
    self.headerMessageLabel.text = self.viewModel.headerMessage
  }
  
  private func applyTheme() {
    self.view.backgroundColor = UIColor.veryLightGray()
    tableView.backgroundColor = view.backgroundColor
  }
  
  func didTapCancel() {
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
}

//MARK: Table View DataSource and delegate implemetation
extension DiagnosticsAndUsageViewController: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows()
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: SwitchTableViewCell = tableView.dequeueReusableCellWithIdentifier(SwitchTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! SwitchTableViewCell
    let title: String = viewModel.cellForRow(indexPath)
    cell.itemTitleLabel.text = title
    cell.toggleView.setOn(GeneralSettings().sendUsageData, animated: false)
    cell.delegate = self
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //TODO: do action on selection
    _ = tableView.dequeueReusableCellWithIdentifier(SwitchTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! SwitchTableViewCell
    let item: DiagnosticsAndUsageViewModel.DiagnosticsAndUsageViewModelItems = self.viewModel.itemAtRow(indexPath.row)
    switch (item) {
    case .SendDataUsage:
      //Do nothing for now
      break
    }
    //de-select cell
    tableView.deselectRowAtIndexPath(indexPath, animated: false)
  }
}

//MARK: C E L L  D E L E G A T E S
extension DiagnosticsAndUsageViewController: SwitchTableViewCellDelegate {
  func didToggle(cell: SwitchTableViewCell, value: Bool) {
    let indexPath: NSIndexPath? = tableView.indexPathForCell(cell)
    if let indexPath = indexPath {
      let item = self.viewModel.itemAtRow(indexPath.row)
      self.viewModel.setValueFor(item, value: value)
    }
  }
}
