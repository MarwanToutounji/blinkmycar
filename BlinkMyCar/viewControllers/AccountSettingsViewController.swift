//
//  AccountSettingsViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/27/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class AccountSettingsViewController: UIViewController {
  @IBOutlet weak var settingsTableView: UITableView!
  @IBOutlet weak var logoutButton: BMCLinkButton!
  
  var viewModel: AccountSettingsViewModel = AccountSettingsViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupContent()
    self.applyTheme()
    navigationItem.leftBarButtonItem = UINavigationBar.closeBarButton(self, selector: "didTapCancel")
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.title = self.viewModel.vcTitle
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    self.title = ""
  }

  private func setupContent() {
    self.sendScreenView("Account Settings")
    self.logoutButton.textColor = UIColor.bmcRed()
    self.logoutButton.setTitle(self.viewModel.logoutText, forState: .Normal)
  }

  private func applyTheme() {
    self.view.backgroundColor = UIColor.veryLightGray()
    settingsTableView.backgroundColor = view.backgroundColor
    settingsTableView.tableFooterView?.backgroundColor = UIColor.veryLightGray()
  }
  
  func didTapCancel() {
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func didTapLogoutInFooter(sender: AnyObject) {
    self.trackEvent(AnalyticsCategories.MyAccount.rawValue, action: "Signout Tap", label: "", value: nil)
    logoutConfirmationAlert()
  }
  
  func navigateToViewController(viewController: UIViewController?) -> Bool {
    guard let viewController = viewController else {
      return false
    }
    
    self.navigationController?.pushViewController(viewController, animated: true)
    
    return true
  }
  
  func logoutConfirmationAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewLogoutConfirmationTitle,
      message: viewModel.alertViewLogoutConfirmationMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.alertViewLogoutConfirmationNegativeButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(title: viewModel.alertViewLogoutConfirmationPostiveButtonTitle, style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
      self.logoutAction()
    }
    
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func logoutAction() {
    self.dismissViewControllerAnimated(true) { () -> Void in
      NSNotificationCenter.defaultCenter().postNotificationName(BMCNotificationNames.Logout.rawValue, object: nil)
    }
  }
}

//MARK: Table View DataSource and delegate implemetation
extension AccountSettingsViewController: UITableViewDataSource, UITableViewDelegate {

  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows()
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: AccountSettingsTableViewCell = tableView.dequeueReusableCellWithIdentifier(AccountSettingsTableViewCell.reusableIdentifier, forIndexPath: indexPath) as!AccountSettingsTableViewCell
    let cellInfo = viewModel.cellForRow(indexPath)
    cell.itemTitleLabel.text = cellInfo.title
    cell.showToggle = cellInfo.showToggle
    cell.leftImage = cellInfo.leftImage
    cell.toggleView.setOn(cellInfo.toggleValue, animated: false)
    cell.delegate = self
    cell.separator.alpha = viewModel.isLast(indexPath.row) ? 0 : 1
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: false)
    
    let item: AccountSettingsItem = self.viewModel.itemAtRow(indexPath.row)
    sendAnalyticsForAccountSetting(item)
    navigateToViewController(item.targetViewController)
  }
  
  func sendAnalyticsForAccountSetting(setting: AccountSettingsItem) {
    var action = ""
    switch setting {
    case .BookingHistory:
      action = "Booking History Tap"
    case .Locations:
      action = "My Locations Tap"
    case .Profile:
      action = "My Profile Tap"
    case .Vehicles:
      action = "My Vehicles Tap"
    default:
      action = ""
    }
    
    if !action.isEmpty {
      self.trackEvent(viewModel.analyticsCategory.rawValue, action: action, label: "", value: nil)
    }
  }
}

//MARK: C E L L  D E L E G A T E S
extension AccountSettingsViewController: AccountSettingsTableViewCellDelegate {
  func didToggle(cell: AccountSettingsTableViewCell, value: Bool) {
    let indexPath: NSIndexPath? = settingsTableView.indexPathForCell(cell)
    if let indexPath = indexPath {
      let item = self.viewModel.itemAtRow(indexPath.row)
      self.viewModel.setValueFor(item, value: value)
      
    }
  }
}
