//
//  AdditionalInstructionsViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/27/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout
import SwiftLoader

protocol AdditionalInstructionsViewControllerDelegate {
  func additionalInfoVCDidSubmitRating(additionalInfoVC: AdditionalInstructionsViewController, booking: Booking, success: Bool, error: NSError?)
  func additionalInfoVCDidCancelRating(additionalInfoVC: AdditionalInstructionsViewController, booking: Booking)
}

public class AdditionalInstructionsViewController: UIViewController {
  
  let animationDuration: NSTimeInterval = 0.3
  
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var headerView: UIView!
  @IBOutlet weak var textFieldContainer: UIView!
  @IBOutlet weak var actionViewsContainer: UIView!
  @IBOutlet weak var headerTitle: BMCAdditonalInfoTitleLabel!
  @IBOutlet weak var headerSubtitle: BMCAdditonalInfoSubtitleLabel!
  @IBOutlet weak var textViewLabel: BMCAdditonalInfoQuestionLabel!
  @IBOutlet weak var textView: BMCAdditionalInfoTextView!
  @IBOutlet weak var cancelButton: BMCOrangeButton!
  @IBOutlet weak var submitButton: BMCOrangeButton!
  
  @IBOutlet weak var contentViewCenterYToSuperviewConstraint: NSLayoutConstraint!
  @IBOutlet weak var contentViewTopToSuperviewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var contentViewBottomToSuperviewBottomConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var textFieldContainerTopToHeaderViewBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var textFieldContainerBottomToActionViewsContainerConstraint: NSLayoutConstraint!
  @IBOutlet weak var textFieldContainerTopToSuperviewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var textFieldContainerBottomToSuperviewBottomConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var textFieldHeightConstraint: NSLayoutConstraint!
  
  
  //================================================================
  // Mark: VARIABLES
  //================================================================
  
  var viewModel: AdditionalInstructionsViewModel!
  var delegate: AdditionalInstructionsViewControllerDelegate?
  
  //================================================================
  // Mark: - LIFECYCLE
  //================================================================
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    
    self.sendScreenView("Instructions")
    
    self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    
    // Give curve to main View border
    contentView.makeCurvedEdges()
    
    // Set Background Colors
    contentView.backgroundColor = UIColor.whiteColor()
    headerView.backgroundColor = UIColor.applicationMainColor()
    textFieldContainer.backgroundColor = UIColor.whiteColor()
    actionViewsContainer.backgroundColor = UIColor.orange()
    
    // Set Labels Text
    headerTitle.text = viewModel.headerViewTitle
    headerSubtitle.text = viewModel.headerViewSubtitle
    textViewLabel.text = viewModel.textViewLabelText
    
    // Set Action Buttons Title
    cancelButton.setTitle(viewModel.cancelButtonTitle, forState: .Normal)
    submitButton.setTitle(viewModel.submitButtonTitle, forState: .Normal)
    
    // Init text field
    textView.placeholderText = viewModel.textViewPlaceholder
    textView.backgroundColor = UIColor.whiteColor()
    textView.returnKeyType = .Done
    textView.textContainerInset = UIEdgeInsetsMake(10,0,10,0);
    textView.delegateForTextViewWithPlaceholder = self
    
    // For the labels sizes to take their final values
    self.view.layoutIfNeeded()
    
    // Register notification for handeling keyboard
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(AdditionalInstructionsViewController.keyboardWillShow(_:)),
      name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(AdditionalInstructionsViewController.keyboardWillHide(_:)),
      name: UIKeyboardWillHideNotification, object: nil)
    
    // Display Additional info if available
    if !(viewModel.additionalInformation?.trim() ?? "").isEmpty {
      textView.text = viewModel.additionalInformation
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  //================================================================
  // Mark: - ACTIONS
  //================================================================
  
  func keyboardWillShow(notification: NSNotification) {
    if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let frame = value.CGRectValue()
      contentViewTopToSuperviewTopConstraint.constant = 0
      contentViewBottomToSuperviewBottomConstraint.constant = -frame.height-20
      self.view.removeConstraint(contentViewCenterYToSuperviewConstraint)
      self.view.addConstraint(contentViewTopToSuperviewTopConstraint)
      self.view.addConstraint(contentViewBottomToSuperviewBottomConstraint)
      
      contentView.removeConstraints([
        textFieldContainerTopToHeaderViewBottomConstraint,
        textFieldContainerBottomToActionViewsContainerConstraint,
        ])
      contentView.addConstraints([
        textFieldContainerTopToSuperviewTopConstraint,
        textFieldContainerBottomToSuperviewBottomConstraint
        ])
      
      if textFieldHeightConstraint != nil {
        textView.removeConstraint(textFieldHeightConstraint)
      }
      
      UIView.animateWithDuration(animationDuration) {
        self.view.layoutIfNeeded()
      }
    }
  }
  
  func keyboardWillHide(notification: NSNotification) {
    self.view.removeConstraint(contentViewTopToSuperviewTopConstraint)
    self.view.removeConstraint(contentViewBottomToSuperviewBottomConstraint)
    self.view.addConstraint(contentViewCenterYToSuperviewConstraint)
    
    contentView.removeConstraints([
      textFieldContainerTopToSuperviewTopConstraint,
      textFieldContainerBottomToSuperviewBottomConstraint
      ])
    contentView.addConstraints([
      textFieldContainerTopToHeaderViewBottomConstraint,
      textFieldContainerBottomToActionViewsContainerConstraint,
      ])
    
    textFieldHeightConstraint = textView.constrainHeight("130").first as! NSLayoutConstraint
    
    UIView.animateWithDuration(
      animationDuration,
      animations: {
        self.view.layoutIfNeeded()
      }, completion: nil)
  }
  
  @IBAction func didTapSubmitButton(sender: UIButton?) {
    self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Submit Instructions Tap", label: "", value: nil)
    
    if viewModel.isValidInformation {
      SwiftLoader.show(animated: true)
      viewModel.submitAdditionalInformation({ (success, error) in
        SwiftLoader.hide()
        if !success {
          self.displayFailToSubmitAdditionalInformationAlert()
          return
        }
        
        if self.delegate != nil {
          self.delegate?.additionalInfoVCDidSubmitRating(self,
            booking: self.viewModel.booking,
            success: true,
            error: nil)
        } else {
          self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.DidUpdateBookings.rawValue, object: nil)
      })
    } else {
      displayInvalidInformationAlert()
    }
  }
  
  @IBAction func didTapCancelButton(sender: UIButton?) {
    self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Skip Instructions Tap", label: "", value: nil)
    
    if delegate != nil {
      self.delegate?.additionalInfoVCDidCancelRating(self, booking: viewModel.booking)
    } else {
      self.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  //================================================================
  // Mark: - ALERTS
  //================================================================
  
  func displayFailToSubmitAdditionalInformationAlert() {
    let alertController = UIAlertController(
      title: self.viewModel.alertViewFailToSubmitInfoTitle,
      message: self.viewModel.alertViewFailToSubmitInfoMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelButton = UIAlertAction(
      title: self.viewModel.alertViewFailToSubmitInfoNegativeActionButtonTitle,
      style: UIAlertActionStyle.Cancel) { (alertAction) in
        self.didTapCancelButton(nil)
    }
    let retryButton = UIAlertAction(
      title: self.viewModel.alertViewFailToSubmitInfoRetryActionButtonTitle,
      style: UIAlertActionStyle.Default) { (alertAction) in
        self.didTapSubmitButton(nil)
    }
    
    alertController.addAction(cancelButton)
    alertController.addAction(retryButton)
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayInvalidInformationAlert() {
    let alertController = UIAlertController(
      title: self.viewModel.alertViewInvalidInformationTitle,
      message: self.viewModel.alertViewInvalidInformationMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okButton = UIAlertAction(
      title: self.viewModel.alertViewInvalidInformationActionButtonTitle,
      style: UIAlertActionStyle.Default,
      handler: nil)
    
    alertController.addAction(okButton)
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

//================================================================
// Mark: - PRESENTING THE VC
//================================================================

extension AdditionalInstructionsViewController {
  class func showFromViewController(
    viewController: UIViewController,
    viewModel: AdditionalInstructionsViewModel,
    delegate: AdditionalInstructionsViewControllerDelegate!) {
    
    guard let additionalInfoVC = viewController.storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.AdditionalInstructionsViewController.rawValue) as? AdditionalInstructionsViewController else {
      return
    }
    additionalInfoVC.viewModel = viewModel
    additionalInfoVC.delegate = delegate
    
    additionalInfoVC.modalPresentationStyle = .OverCurrentContext
    additionalInfoVC.modalTransitionStyle = .CrossDissolve
    viewController.presentViewController(additionalInfoVC, animated: true, completion: nil)
    
    return
  }
}

extension AdditionalInstructionsViewController: TextViewWithPlaceholderDelegate {
  func textView(
    textView: UITextView,
    shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    if text == "\n" {
      textView.resignFirstResponder()
    }
    return true
  }
  
  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    return true
  }
  
  public func textViewDidBeginEditing(textView: UITextView) {
    textView.scrollRectToVisible(textView.frame, animated: true)
  }
  
  func textViewDidEndEditing(textView: UITextView) {
    viewModel.additionalInformation = (textView as! BMCAdditionalInfoTextView).valueForText()
    return
  }
  
  func textViewShouldReturn(textField: UITextField) -> Bool {
    return true
  }
}
