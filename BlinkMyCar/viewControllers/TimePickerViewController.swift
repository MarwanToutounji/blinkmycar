//
//  TimePickerViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/27/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

protocol TimePickerViewControllerDelegate {
  func timePickerDidSelectServiceTime(serviceTime: ServiceTime?)
}

class TimePickerViewController: UITableViewController {
  
  var viewModel: TimePickerViewModel = TimePickerViewModel()
  
  var delegate: TimePickerViewControllerDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
  
    tableView.registerClass(TimePickerTableViewCell.self, forCellReuseIdentifier: "tableViewCell")
    // register section header view
    tableView.registerNib(
      TableSectionView.nib,
      forHeaderFooterViewReuseIdentifier: TableSectionView.identifier)
  }
  
  func reloadData() {
    tableView.reloadData()
  }
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowForSection(section)
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("tableViewCell")! as! TimePickerTableViewCell
    cell.selectionStyle = .None
    
    let cellInfo = viewModel.cellInfoForIndexPath(indexPath)
    cell.textLabel?.text = cellInfo.text
    cell.textLabel?.textColor = cellInfo.color
    
    if viewModel.shouldSelectRowAtIndexPath(indexPath) {
      tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
    }
    
    return cell
  }
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 70
  }
  
  override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableHeaderFooterViewWithIdentifier(
      TableSectionView.identifier) as! TableSectionView
    let sectionInfo = viewModel.sectionInfoForIndex(section)
    cell.setTitle(sectionInfo.title, imageName: sectionInfo.imageName)
    return cell
  }
  
  override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 60
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let timeForRow = viewModel.timeForIndexPath(indexPath)
    let shouldRemainSelected = viewModel.canSelectTimeForRow(indexPath)
    viewModel.selectedTime = shouldRemainSelected ? timeForRow : nil
    delegate?.timePickerDidSelectServiceTime(viewModel.selectedTime)
    if !shouldRemainSelected {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
}


class TimePickerTableViewCell: UITableViewCell {
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    if selected {
      self.textLabel?.font = UIFont.boldBMCFont(size: 20)
    } else {
      self.textLabel?.font = UIFont.lightBMCFont(size: 17)
    }
  }
}
