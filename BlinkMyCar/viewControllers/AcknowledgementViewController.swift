//
//  AcknowledgementViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 9/10/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class AcknowledgementViewController: UIViewController {
  @IBOutlet weak var textView: UITextView!

  var viewModel = AcknowledgementViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.acknowledgementName

    textView.frame = self.view.bounds
    textView.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleWidth]
    textView.textContainerInset = UIEdgeInsetsMake(self.sizeForPercent(3.125), self.sizeForPercent(3.125), self.sizeForPercent(3.125), self.sizeForPercent(3.125))
    textView.userInteractionEnabled = true
    textView.selectable = true
    textView.editable = false
    textView.scrollEnabled = true
    textView.showsHorizontalScrollIndicator = false
    textView.showsVerticalScrollIndicator = true
    textView.backgroundColor = .clearColor()
    textView.spellCheckingType = .No
    textView.font = UIFont.lightBMCFont(size: self.sizeForPercent(4.063))

    textView.text = viewModel.acknowledgementText
  }

  private func sizeForPercent(percent: CGFloat) -> CGFloat {
    if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
      return ceil(((self.view.frame.size.width * 0.7) * (percent / 100)))
    } else {
      return ceil(self.view.frame.size.width * (percent / 100))
    }
  }
}
