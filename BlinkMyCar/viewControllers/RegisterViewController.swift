//
//  RegisterViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import ORStackView
import SwiftLoader

class RegisterViewController: UIViewController, ValidationTextFieldDelegate {
  private static var sideMargin: CGFloat = 60
  let largeVerticalSpacing: CGFloat = 24
  let mediumVerticalSpacing: CGFloat = 14
  let smallVerticalSpacing: CGFloat = 10

  private var labelWelcome: BMCSecondaryTitleLabel!
  private var labelEmail: BMCFormLabel!
  private var labelPhone: BMCFormLabel!
  private var labelPassword: BMCFormLabel!
  private var textFieldEmail: ValidationTextField!
  private var viewPhoneFields: UIView!
  private var textFieldPhoneCode: BMCTextField!
  private var textFieldPhone: BMCTextField!
  private var labelPhoneInvalid: BMCErrorLabel!
  private var textFieldPassword: ValidationTextField!
  private var buttonSave: BMCOrangeButton!
  private var loginButton: BMCLinkButton!

  private var constraintViewPhoneFieldBottomToTextView: [NSLayoutConstraint]!
  private var constraintViewPhoneFieldBottomToErrorLabel: [NSLayoutConstraint]!

  @IBOutlet weak var labelTerms: UILabel!
  @IBOutlet weak var scrollStackView: ORStackScrollView!
  @IBOutlet weak var termsAndConditionLabel: BMCLabel!
  @IBOutlet weak var termsAndConditionButton: BMCLinkButton!

  @IBOutlet weak var constraintStackViewBottomToRegistrationLabelTop: NSLayoutConstraint!
  @IBOutlet weak var constraintStackViewBottomToSuperviewBottom: NSLayoutConstraint!

  private let viewModel: RegisterViewModel;

  init() {
    viewModel = RegisterViewModel()
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    viewModel = RegisterViewModel()
    super.init(coder: aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Registration")
    
    termsAndConditionButton.textColor = UIColor.lightGray()
    initNavigationCntrollerButtons()
    initLayout()

    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(RegisterViewController.keyboardWillShow(_:)),
      name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(RegisterViewController.keyboardWillHide(_:)),
      name: UIKeyboardWillHideNotification, object: nil)
    
    self.view.addTapGesture(self)
  }

  private func initNavigationCntrollerButtons() {
    let cancelButton = UIBarButtonItem(barButtonSystemItem: .Cancel,
      target: self, action: #selector(RegisterViewController.didTapCancelButton(_:)))
    self.navigationItem.leftBarButtonItem = cancelButton
  }

  private func initLayout() {
    labelWelcome = BMCSecondaryTitleLabel()
    labelWelcome.textAlignment = NSTextAlignment.Center
    labelEmail = BMCFormLabel()
    labelPhone = BMCFormLabel()
    labelPassword = BMCFormLabel()

    textFieldEmail = UIView.loadFromNib(ValidationTextField.nibName,
      owner: self) as! ValidationTextField
    textFieldPassword = UIView.loadFromNib(ValidationTextField.nibName,
      owner: self) as! ValidationTextField
    
    buttonSave = BMCOrangeButton()
    loginButton = BMCLinkButton()
    
    loginButton.textColor = UIColor.lightGray()
    
    buttonSave.addTarget(self,
      action: #selector(RegisterViewController.didTapSaveButton(_:)),
      forControlEvents: UIControlEvents.TouchUpInside)
    loginButton.addTarget(self,
      action: #selector(RegisterViewController.didTapLoginButton(_:)),
      forControlEvents: UIControlEvents.TouchUpInside)
    
    textFieldEmail.keyboardType = UIKeyboardType.EmailAddress

    viewPhoneFields = UIView()
    viewPhoneFields.clipsToBounds = true
    textFieldPhoneCode = BMCTextField()
    textFieldPhoneCode.enable(false)
    textFieldPhoneCode.text = "+961"
    textFieldPhoneCode.textAlignment = NSTextAlignment.Center
    textFieldPhone = BMCTextField()
    textFieldPhone.keyboardType = UIKeyboardType.NumberPad
    textFieldPhone.delegate = self
    textFieldPhone.text = " "
    labelPhoneInvalid = BMCErrorLabel()
    labelPhoneInvalid.numberOfLines = 0
    labelPhoneInvalid.text = viewModel.textFieldPhoneErrorMessage
    viewPhoneFields.addSubview(labelPhoneInvalid)
    viewPhoneFields.addSubview(textFieldPhoneCode)
    viewPhoneFields.addSubview(textFieldPhone)
    textFieldPhone.constrainHeight("44")
    textFieldPhoneCode.constrainHeight("44")
    textFieldPhoneCode.constrainWidth("80")
    textFieldPhoneCode.alignLeadingEdgeWithView(viewPhoneFields, predicate: "0")
    textFieldPhoneCode.alignTopEdgeWithView(viewPhoneFields, predicate: "0")
    textFieldPhone.constrainLeadingSpaceToView(textFieldPhoneCode, predicate: "5")
    textFieldPhone.alignTopEdgeWithView(viewPhoneFields, predicate: "0")
    textFieldPhone.alignTrailingEdgeWithView(viewPhoneFields, predicate: "0")
    labelPhoneInvalid.alignLeading("0", trailing: "0", toView: viewPhoneFields)
    labelPhoneInvalid.constrainTopSpaceToView(textFieldPhoneCode, predicate: "10")
    constraintViewPhoneFieldBottomToTextView = viewPhoneFields.alignBottomEdgeWithView(textFieldPhoneCode, predicate: "0") as! [NSLayoutConstraint]
    constraintViewPhoneFieldBottomToErrorLabel = viewPhoneFields.alignBottomEdgeWithView(labelPhoneInvalid, predicate: "0") as! [NSLayoutConstraint]
    viewPhoneFields.removeConstraints(constraintViewPhoneFieldBottomToErrorLabel)


    labelWelcome.text = viewModel.labelWelcomeText
    labelEmail.text = viewModel.labelEmailText
    labelPhone.text = viewModel.labelPhoneText
    labelPassword.text = viewModel.labelPasswordText
    labelEmail.setRequired(true)
    labelPhone.setRequired(true)
    labelPassword.setRequired(true)
    textFieldEmail.errorMessage = viewModel.textFieldEmailErrorMessage
    textFieldPassword.errorMessage = viewModel.textFieldPasswordErrorMessage
    buttonSave.setTitle(viewModel.buttonRegisterTitle, forState: UIControlState.Normal)
    loginButton.setTitle(viewModel.buttonLoginLinkTitle, forState: UIControlState.Normal)
    labelTerms.text = viewModel.termsLabelText
    termsAndConditionButton.setTitle(viewModel.termsButtonText, forState: .Normal)

    textFieldPassword.secureTextEntry = true

    scrollStackView.stackView.addSubview(labelWelcome, withPrecedingMargin: largeVerticalSpacing/2,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(labelEmail, withPrecedingMargin: largeVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(textFieldEmail, withPrecedingMargin: smallVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(labelPhone, withPrecedingMargin: mediumVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(viewPhoneFields, withPrecedingMargin: smallVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(labelPassword, withPrecedingMargin: mediumVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(textFieldPassword, withPrecedingMargin: smallVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(buttonSave, withPrecedingMargin: largeVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(loginButton, withPrecedingMargin: smallVerticalSpacing,
      sideMargin: RegisterViewController.sideMargin)
    scrollStackView.stackView.addSubview(UIView(), withPrecedingMargin: largeVerticalSpacing,
      sideMargin: 0)

    scrollStackView.stackView.sendSubviewToBack(labelPhoneInvalid)

    buttonSave.constrainHeight("44")

    textFieldEmail.validationTextFieldDelegate = self
    textFieldPassword.validationTextFieldDelegate = self

    textFieldEmail.validationMethod = viewModel.validateEmail
    textFieldPassword.validationMethod = viewModel.validatePassword

    textFieldEmail.returnKeyType = UIReturnKeyType.Next
    textFieldPhone.returnKeyType = UIReturnKeyType.Next
    textFieldPassword.returnKeyType = UIReturnKeyType.Done
  }

  //==========================================================================
  // MARK: - N O T I F I C A T I O N S
  //==========================================================================

  func keyboardWillShow(notification: NSNotification) {
    if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let frame = value.CGRectValue()
      constraintStackViewBottomToSuperviewBottom.constant = -frame.height
    }

    layoutForVisibleKeyboard()
  }

  func keyboardWillHide(notification: NSNotification) {
    layoutForHiddenKeyboard()
  }

  func layoutForVisibleKeyboard() {
    self.view.removeConstraint(constraintStackViewBottomToRegistrationLabelTop)
    self.view.addConstraint(constraintStackViewBottomToSuperviewBottom)
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.scrollStackView.stackView.removeSubview(self.labelWelcome)
      self.scrollStackView.stackView.removeSubview(self.buttonSave)
      self.view.layoutIfNeeded()
    })
  }

  func layoutForHiddenKeyboard() {
    self.view.removeConstraint(constraintStackViewBottomToSuperviewBottom)
    self.view.addConstraint(constraintStackViewBottomToRegistrationLabelTop)
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.scrollStackView.stackView.insertSubview(
        self.labelWelcome, atIndex: 0, withPrecedingMargin: 30,
        sideMargin: RegisterViewController.sideMargin)
      self.scrollStackView.stackView.insertSubview(
        self.buttonSave, atIndex: 7, withPrecedingMargin: 30,
        sideMargin: RegisterViewController.sideMargin)
      self.view.layoutIfNeeded()
    })
  }


  //==========================================================================
  // MARK: - V A L I D A T I O N   T E X T   F I E L D   D E L E G A T E
  //==========================================================================

  func validationTextFieldDidBeginEditing(textField: UITextField) {
  }

  func validationTextFieldDidEndEditing(textField: UITextField) {
  }

  func validationTextFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case textFieldEmail.textField:
      textFieldPhone.becomeFirstResponder()
    case textFieldPassword.textField:
      textField.resignFirstResponder()
    default:
      break
    }
    return true
  }

  func validationTextFieldShouldLayoutIfNeeded(animationBlock: (() -> Void)) {
    UIView.animateWithDuration(0.5,
      animations: {() -> Void in
        self.view.layoutIfNeeded()
        animationBlock()
    })
  }

  //========================================================
  // MARK: - A C T I O N S
  //========================================================

  @IBAction func didTapTermsAndConditionButton(sender: AnyObject?) {
    let vc: WebViewController = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.WebViewController.rawValue)
      as! WebViewController
    vc.viewModel = WebViewModel()
    let aboutViewModel = AboutUsViewModel()
    vc.viewModel.url = aboutViewModel.termsAndConditionsUrl
    vc.viewModel.title = aboutViewModel.termsAndConditions
    self.presentViewController(
      UINavigationController(rootViewController: vc), animated: true,
      completion: nil)
  }

  func didTapCancelButton(sender: AnyObject?) {
    dismissKeyboard()
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }

  func didTapSaveButton(sender: AnyObject?) {
    saveAccountInfo()
  }

  func didTapLoginButton(sender: AnyObject?) {
    self.trackEvent(AnalyticsCategories.Register.rawValue, action: "Signin Tap", label: "", value: nil)
    presentLoginVC()
  }
  
  func presentLoginVC() {
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidTapLoginFromRegisterVC.rawValue,
      object: nil, userInfo: [ BMCNotificationDataKeys.BMCUserEmailKey.rawValue : textFieldEmail.text ?? ""])
  }
  
  func saveAccountInfo() {
    if (validateFields()) {
      confirmPhoneNumber()
    } else {
      showFieldsInvalidationAlert()
    }
  }

  func noAlertActionHandler(action: UIAlertAction!) {
    textFieldPhone.becomeFirstResponder()
    textFieldPhone.text = ""
  }

  func yesAlertActionHandler(action: UIAlertAction!) {
    SwiftLoader.show(animated: true)
    viewModel.registerUser(textFieldEmail.text!,
      phoneNumber: textFieldPhone.text!,
      password: textFieldPassword.text!) { (registrationError: NSError?, loginError: NSError?) -> Void in
        SwiftLoader.hide()
        if let error = registrationError {
          self.showFailToRegisterAlert(error)
          return
        } else {
          self.trackEvent(AnalyticsCategories.Register.rawValue, action: "Successful Registration", label: "", value: nil)
          if let _ = loginError {
            NSNotificationCenter.defaultCenter().postNotificationName(
              BMCNotificationNames.FailToSignInAfterRegistration.rawValue,
              object: nil)
            return
          }
        }
        
        self.trackEvent(AnalyticsCategories.Register.rawValue, action: "Successful Signin", label: "", value: nil)
        
        // Go to next vc if registration and logging in succeded
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier(
          ViewControllerStoryboardIdentifier.ChooseYourRideViewController.rawValue) as! ChooseYourRideViewController
        vc.viewModel.analyticsCategory = AnalyticsCategories.RegistrationChooseVehicle
        self.navigationController?.pushViewController(vc, animated: true)
    }
  }

  //======================================================
  // MARK: - H E L P E R S
  //======================================================

  func dismissKeyboard() {
    textFieldEmail.resignFirstResponder()
    textFieldPassword.resignFirstResponder()
    textFieldPhone.resignFirstResponder()
  }

  func validateFields() -> Bool {

    let isEmailValid = textFieldEmail.validate()
    let isPasswordValid = textFieldPassword.validate()
    textFieldPhone.text = textFieldPhone.text?.trim()
    let isPhoneValid = viewModel.validatePhoneNumber(textFieldPhone.text)
    if isEmailValid
      && isPhoneValid
      && isPasswordValid {
        return true
    }
    return false
  }

  func confirmPhoneNumber() {
    let alertController = UIAlertController(
      title: viewModel.alertViewConfirmPhoneNumberTitle,
      message: viewModel.getFormattedPhoneNumber(textFieldPhone.text),
      preferredStyle: UIAlertControllerStyle.Alert)

    let noAction = UIAlertAction(title: viewModel.alertViewConfirmPhoneNumberNegativeActionText,
      style: UIAlertActionStyle.Cancel, handler: noAlertActionHandler)
    alertController.addAction(noAction)

    let yesAction = UIAlertAction(title: viewModel.alertViewConfirmPhoneNumberPositiveActionText,
      style: UIAlertActionStyle.Default, handler: yesAlertActionHandler)
    alertController.addAction(yesAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func showFieldsInvalidationAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailRegistrationAlertTitle,
      message: viewModel.alertViewFailRegistrationAlertInvalidFieldsMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewFailRegistrationAlertActionButtonText,
      style: UIAlertActionStyle.Destructive, handler: nil)
    alertController.addAction(okAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func showFailToRegisterAlert(error: NSError?) {
    var code: Int = 0
    if let error = error {
      code = error.code
    }
    
    let alertViewTitle = viewModel.alertViewFailRegistrationAlertTitle
    var alertViewMessage = viewModel.alertViewFailRegistrationAlertMessage
    var addSignInActionButton = false
    
    if code == 409 {
      alertViewMessage = viewModel.registerationErrorEmailAlreadyExists
      addSignInActionButton = true
    }
    
    let alertController = UIAlertController(
      title: alertViewTitle,
      message: alertViewMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    if addSignInActionButton {
      let signInAction = UIAlertAction(title: viewModel.alertViewFailRegistrationAlertSignInActionButtonText,
        style: UIAlertActionStyle.Destructive) { (UIAlertAction) -> Void in
          self.presentLoginVC()
      }
      alertController.addAction(signInAction)
    }
    
    let okAction = UIAlertAction(title: viewModel.alertViewFailRegistrationAlertActionButtonText,
      style: UIAlertActionStyle.Destructive, handler: nil)
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func showPhoneNumberErrorLabel() {
    viewPhoneFields.removeConstraints(constraintViewPhoneFieldBottomToTextView)
    viewPhoneFields.addConstraints(constraintViewPhoneFieldBottomToErrorLabel)
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.scrollStackView.stackView.layoutIfNeeded()
    })

  }

  func hidePhoneNumberErrorLabel() {
    viewPhoneFields.removeConstraints(constraintViewPhoneFieldBottomToErrorLabel)
    viewPhoneFields.addConstraints(constraintViewPhoneFieldBottomToTextView)
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.scrollStackView.stackView.layoutIfNeeded()
    })
  }
}

extension RegisterViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if textField.text == " " {
      textField.text = ""
    }
    return true
  }

  func textFieldDidEndEditing(textField: UITextField) {
    if (viewModel.validatePhoneNumber(textFieldPhone.text)) {
      hidePhoneNumberErrorLabel()
    } else {
      showPhoneNumberErrorLabel()
    }
  }
}

//======================================================
// MARK: - ViewTapDelegate
//======================================================
extension RegisterViewController: ViewTapDelegate {
  func didTapToCancel() {
    dismissKeyboard()
  }
}
