//
//  AboutUsViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/6/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class AboutUsViewController: UIViewController {
  var viewModel: AboutUsViewModel = AboutUsViewModel()
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var playButton: BMCTranseculantButton!
  @IBOutlet weak var mainLabel: BMCAboutUsBoldLabel!
  @IBOutlet weak var secondaryLabel: BMCListItemLabel!
  
  
  @IBOutlet weak var termsAndConditionsButton: BMCLinkButton!
  @IBOutlet weak var privacyPolicyButton: BMCLinkButton!
  @IBOutlet weak var legalNoticesButton: BMCLinkButton!
  
  @IBOutlet weak var twitterImageView: UIImageView!
  @IBOutlet weak var instaImageView: UIImageView!
  @IBOutlet weak var fbImageView: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    applyTheme()
    setupContent()
    navigationItem.leftBarButtonItem = UINavigationBar.closeBarButton(self, selector: "cancel")
    title = viewModel.vcTitle
    self.sendScreenView("About Us")
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
  }
  
  func setupContent() {
    mainLabel.text = viewModel.mainText
    secondaryLabel.text = viewModel.secondaryParagraphText
    termsAndConditionsButton.setTitle(viewModel.termsAndConditions, forState: UIControlState.Normal)
    privacyPolicyButton.setTitle(viewModel.privacyPolicy, forState: UIControlState.Normal)
    legalNoticesButton.setTitle(viewModel.legalNotices, forState: UIControlState.Normal)
  }
  
  func applyTheme() {
    termsAndConditionsButton.textColor = UIColor.applicationSecondaryColor()
    privacyPolicyButton.textColor = UIColor.applicationSecondaryColor()
    legalNoticesButton.textColor = UIColor.applicationSecondaryColor()
  }

  func cancel() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func openUrl(url: String) {
    let url = NSURL(string: url)!
    UIApplication.sharedApplication().openURL(url)
  }
}

//MARK: A C T I O N S
extension AboutUsViewController {
  
  @IBAction func facebookTouchUpInside(sender: AnyObject) {
    openUrl(viewModel.fbUrl)
  }
  
  @IBAction func instaTouchUpInside(sender: AnyObject) {
    openUrl(viewModel.instaUrl)
  }
  
  @IBAction func twitterTouchUpInside(sender: AnyObject) {
    openUrl(viewModel.twitterUrl)
  }
  
  @IBAction func termsAndConditionsTouchUpInside(sender: AnyObject) {
    let vc: WebViewController = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.WebViewController.rawValue)
      as! WebViewController
    vc.viewModel = WebViewModel()
    vc.viewModel.url = viewModel.termsAndConditionsUrl
    vc.viewModel.title = viewModel.termsAndConditions
    self.presentViewController(
      UINavigationController(rootViewController: vc), animated: true,
      completion: nil)
  }
  
  @IBAction func privacyPolicyTouchUpInside(sender: AnyObject) {
    let vc: WebViewController = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.WebViewController.rawValue)
      as! WebViewController
    vc.viewModel = WebViewModel()
    vc.viewModel.url = viewModel.privacyPolicyUrl
    vc.viewModel.title = viewModel.privacyPolicy
    self.presentViewController(
      UINavigationController(rootViewController: vc), animated: true,
      completion: nil)
  }
  
  @IBAction func legalNoticesTouchUpInside(sender: AnyObject) {
    let vc = storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.AcknowledgementsTableViewController.rawValue) as! AcknowledgementsTableViewController
    self.presentViewController(
      UINavigationController(rootViewController: vc), animated: true,
      completion: nil)
  }
}
