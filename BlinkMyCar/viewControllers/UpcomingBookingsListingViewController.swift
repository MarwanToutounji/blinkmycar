//
//  UpcomingBookingsLisitngViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/1/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import SwiftLoader
import DrawerController

class UpcomingBookingsListingViewController: UIViewController {
  lazy var refreshController = UIRefreshControl()
  
  @IBOutlet weak var carProfileView: VehicleBasicInformationView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var actionButton: BMCOrangeButton!
  
  var viewModel = UpcomingBookingsListingViewModel()
  
  var isPageRefreshing: Bool = false

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Upcoming")
    
    // Register cells in table view
    tableView.registerClass(
      UpcomingBookingTableViewCell.self,
      forCellReuseIdentifier: UpcomingBookingTableViewCell.identifier)
    // Register section header
    tableView.registerNib(
      TitleTableViewSectionView.nib,
      forHeaderFooterViewReuseIdentifier: TitleTableViewSectionView.identifier)
    
    // Set Properties for dynamic cell height
    tableView.estimatedRowHeight = 30
    tableView.rowHeight = UITableViewAutomaticDimension
    // Set Properties for dynamic section header height
    tableView.sectionHeaderHeight = UITableViewAutomaticDimension
    tableView.estimatedSectionHeaderHeight = 30
    
    // Implement the pull table view to refresh
    refreshController.addTarget(self, action: #selector(self.handlePullToRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
    tableView.addSubview(refreshController)
    
    // Init UI
    self.view.backgroundColor = UIColor.veryLightGray()
    tableView.tableFooterView = UIView(frame: CGRectZero)
    tableView.backgroundColor = UIColor.clearColor()
    tableView.allowsSelection = false
    tableView.separatorStyle = .None
    
    // Init Vehicle Info View
    carProfileView.selectionBackgroundColor = UIColor.darkPurple()
    carProfileView.selectedState = true
    carProfileView.compactView(compactSize)
    carProfileView.vehicle = viewModel.currentVehicle!
    
    // Initialize Action Button
    actionButton.setTitle(viewModel.actionButtonTitle, forState: .Normal)
    actionButton.addTarget(
      self,
      action: #selector(UpcomingBookingsListingViewController.didTapActionButton),
      forControlEvents: .TouchUpInside)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    if !PersistenceManager.sharedManager.isSyncing {
      refreshUpcomingVCIfNeeded()
    }
  }
  
  func refreshUpcomingVCIfNeeded() {
    SwiftLoader.show(animated: true)
    viewModel.reloadData { (success, error) in
      SwiftLoader.hide()
      
      if success {
        if self.viewModel.hasBookings {
          self.displayBookingToBeRatedIfAvailable(nil)
          self.tableView.reloadData()
        } else {
          // There are no bookings to be displayed, in that case we need
          // To call the refresh root VC notification so this vc is replaced
          // With a ServicesVC instead
          NSNotificationCenter.defaultCenter().postNotificationName(
            BMCNotificationNames.ShouldRefreshApplicationRootVC.rawValue,
            object: nil)
        }
      } else {
        // TODO: Show reload button
        self.displayBookingToBeRatedIfAvailable(nil)
      }
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  //=================================================================
  // MARK: - ACTION
  //=================================================================
  
  /// Triggers global Syncrinization task for all fields in the app
  func handlePullToRefresh(refreshControl: UIRefreshControl) {
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.RefreshApplication.rawValue, object: nil)
    SwiftLoader.refreshIfNeeded()
    refreshController.endRefreshing()
  }
  
  func didTapActionButton() {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Book Again Tap", label: "", value: nil)
    BookingFlowViewController.startBookingFlowFromViewController(self)
  }
  
  func handleCancelEditBooking() {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
}


//=================================================================
// MARK: - TABLE VIEW | Data Source
//=================================================================

extension UpcomingBookingsListingViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSecction(section)
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(UpcomingBookingTableViewCell.identifier) as! UpcomingBookingTableViewCell
    cell.setBooking(viewModel.bookingForCellAtIndexPath(indexPath))
    cell.delegate = self
    return cell
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableHeaderFooterViewWithIdentifier(
      TitleTableViewSectionView.identifier) as! TitleTableViewSectionView
    let sectionTitle = viewModel.sectionInfoForIndex(section)
    cell.setTitle(sectionTitle)
    return cell
  }
}

//=================================================================
// MARK: - Scroll View Delegate
//=================================================================

extension UpcomingBookingsListingViewController {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height))
    {
      if !self.isPageRefreshing {
        if self.viewModel.hasNextPage() {
          self.isPageRefreshing = true
          SwiftLoader.show(animated: true)
          //MAKE NEXT PAGE API CALL AND REFRESH VIEW
          self.viewModel.fetchNextPage({ (success, error) -> Void in
            SwiftLoader.hide()
            self.isPageRefreshing = false
            if success {
              self.tableView.reloadData()
            }
          })
        } else {
          // if there is no next page then fetch the first page again
          if !viewModel.hasFirstPage() {
            self.isPageRefreshing = true
            SwiftLoader.show(animated: true)
            self.viewModel.reloadData({ (success, error) in
              SwiftLoader.hide()
              self.isPageRefreshing = false
              if success {
                self.tableView.reloadData()
              }
            })
          }
        }
      }
    }
  }
}

//=================================================================
// MARK: - Cell Delegate
//=================================================================

extension UpcomingBookingsListingViewController: UpcomingBookingTableViewCellDelegate {
  func upcomingBookingTableViewCellDidTapCancel(cell: UpcomingBookingTableViewCell) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Cancel Booking Tap", label: "", value: nil)
    let cellIndexPath = tableView.indexPathForCell(cell)!
    discardBookingAlert(cellIndexPath)
  }
  
  func upcomingBookingTableViewCellDidTapEdit(cell: UpcomingBookingTableViewCell) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Edit Booking Tap", label: "", value: nil)
    
    let cellIndexPath = tableView.indexPathForCell(cell)
    BookingFlowViewController.startBookingFlowFromViewController(
      self,
      editedBooking: viewModel.bookingForCellAtIndexPath(cellIndexPath!))
  }
  
  func upcomingBookingTableViewCellDidTapCallSpecialist(cell: UpcomingBookingTableViewCell) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Call Specialist Tap", label: "", value: nil)
    
    let cellIndexPath = tableView.indexPathForCell(cell)
    if let cellIndexPath = cellIndexPath, specialistInfo = viewModel.specialistInfoForCellAtIndexPath(cellIndexPath), url = NSURL(string: "tel://\(specialistInfo.specialistNumber)") {
      displayCallSpecialistConfirmationAlert(url)
    }
  }
}

//=================================================================
// MARK: - Navigation Delegate
//=================================================================

extension UpcomingBookingsListingViewController : ServicesViewControllerNavigationDelegate {
  func servicesViewControllerShouldTransferRightBarButton() -> Bool {
    return true
  }
}

//=========================================================
// MARK: - Rate View Delegate
//=========================================================

extension UpcomingBookingsListingViewController: RateBookingViewControllerDelegate {
  func rateBookingVCDidSubmitRating(
    rateBookingVC: RateBookingViewController,
    booking: Booking,
    success: Bool, error: NSError?) {
    rateBookingVC.dismissViewControllerAnimated(true) {
      self.displayBookingToBeRatedIfAvailable(booking)
    }
  }
  
  func rateBookingVCDidCancelRating(
    rateBookingVC: RateBookingViewController,
    booking: Booking) {
    rateBookingVC.dismissViewControllerAnimated(true) {
      self.displayBookingToBeRatedIfAvailable(booking)
    }
  }
  
  private func displayBookingToBeRatedIfAvailable(ignoredBooking: Booking?) {
    if let ignoredBooking = ignoredBooking {
      viewModel.popBookingThatNeedsRating(ignoredBooking)
    }
    
    if let bookingToBeRated = viewModel.bookingToBeRated{
      RateBookingViewController.showFromViewController(
        self,
        rateBookingViewModel: RateBookingViewModel(booking: bookingToBeRated),
        delegate: self)
    }
  }
}

//=========================================================
// MARK: - Alert Views
//=========================================================

extension UpcomingBookingsListingViewController {
  private func failToDiscardBookingAlert() {
    let alertController = UIAlertController(
      title: viewModel.failToDiscardBookingAlertTitle,
      message: viewModel.failToDiscardBookingAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(
      title: viewModel.failToDiscardBookingAlertOkButtonTitle,
      style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayCallSpecialistConfirmationAlert(url: NSURL) {
    let alertController = UIAlertController(
      title: viewModel.callSpecialistAlertTitle,
      message: String(format: viewModel.callSpecialistAlertMessage, url.host ?? "Specialist"),
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(
      title: viewModel.callSpecialistAlertNegativeActionButtonTitle,
      style: UIAlertActionStyle.Default, handler: nil)
    let callAction = UIAlertAction(
      title: viewModel.callSpecialistAlertPositiveActionButtonTitle,
      style: UIAlertActionStyle.Default) { (_) in
        UIApplication.sharedApplication().openURL(url)
    }
    alertController.addAction(cancelAction)
    alertController.addAction(callAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func discardBookingAlert(cellIndexPath: NSIndexPath) {
    let alertController = UIAlertController(
      title: viewModel.discardBookingAlertViewTitle,
      message: viewModel.discardBookingAlertViewTitleMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(
      title: viewModel.discardBookingAlertViewNegativeButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(
      title: viewModel.discardBookingAlertViewPostiveButtonTitle,
      style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        SwiftLoader.show(animated: true)
        self.viewModel.deleteBookingForIndexPath(cellIndexPath) {
          (success, error) in
          SwiftLoader.hide()
          
          if !success {
            self.failToDiscardBookingAlert()
            return
          }
          
          NSNotificationCenter.defaultCenter().postNotificationName(
            BMCNotificationNames.DidUpdateBookings.rawValue,
            object: nil)
          
        }
    }
    
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}
