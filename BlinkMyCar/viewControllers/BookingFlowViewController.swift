//
//  BookingFlowViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/19/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import UIKit


class BookingFlowViewController: Dismissable {
  let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  
  var viewModel: BookingFlowViewModel
  
  private var navigationController: UINavigationController!
  
  private init(bookingFlowViewModel: BookingFlowViewModel) {
    self.viewModel = bookingFlowViewModel
  }
  
  private func getRootViewController() -> UIViewController {
    navigationController = UINavigationController(rootViewController: instanceOfStartingBookingFlowViewController())
    return navigationController
  }
  
  private func instanceOfStartingBookingFlowViewController() -> UIViewController {
    var viewController: UIViewController!
    if viewModel.hasSelectedServices {
      let bookServicesViewController = storyboard.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.BookServicesViewController.rawValue) as! BookServicesViewController
      bookServicesViewController.viewModel.bookingFlowViewModel = viewModel
      viewController = bookServicesViewController
    } else {
      let servicesViewController = storyboard.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.ServicesViewController.rawValue) as! ServicesViewController
      servicesViewController.viewModel.bookingFlowViewModel = viewModel
      viewController = servicesViewController
    }
    
    if let dismissableViewController = viewController as? Dismissable {
      let cancelBookingFlow = UIBarButtonItem(
        barButtonSystemItem: UIBarButtonSystemItem.Cancel,
        target: viewController, action: #selector(dismissableViewController.dismiss(_:)))
      viewController.navigationItem.rightBarButtonItem = cancelBookingFlow
    } else {
      let cancelBookingFlow = UIBarButtonItem(
        barButtonSystemItem: UIBarButtonSystemItem.Cancel,
        target: self, action: #selector(self.dismiss(_:)))
      viewController.navigationItem.rightBarButtonItem = cancelBookingFlow
    }
    
    return viewController
  }
  
  @objc func dismiss(sender: AnyObject? = nil) {
    navigationController.dismissViewControllerAnimated(true, completion: nil)
  }
  
  //=======================================================================
  
  class func startBookingFlowFromViewController(
    viewController: UIViewController,
    editedBooking: Booking? = nil) -> BookingFlowViewController {
    
    let bookingFlowViewModel = BookingFlowViewModel(editedBooking: editedBooking)
    let bookingFlowViewController = BookingFlowViewController( bookingFlowViewModel: bookingFlowViewModel)
    
    viewController.presentViewController(
      bookingFlowViewController.getRootViewController(),
      animated: true, completion: nil)
    
    return bookingFlowViewController
  }
  
  class func startBookingFlowFromViewController(
    viewController: UIViewController,
    selectedServices: [Service]) -> BookingFlowViewController {
    
    let bookingFlowViewModel = BookingFlowViewModel(selectedServices: selectedServices)
    let bookingFlowViewController = BookingFlowViewController(bookingFlowViewModel: bookingFlowViewModel)
    
    viewController.presentViewController(
      bookingFlowViewController.getRootViewController(),
      animated: true, completion: nil)
    
    return bookingFlowViewController
  }
}
