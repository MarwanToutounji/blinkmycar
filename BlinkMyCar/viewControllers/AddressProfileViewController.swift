//
//  AddressProfileViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import ORStackView
import SwiftLoader
import CoreLocation
/**
 The mode, basically, indicated the view state of the VC.
 For example, the VC will be loaded on .EditAddressCoordinates
 in two cases:
 - When we are creating a new address
 - When we are viewing an address that doesn't have coordinates (old version)
 **/

enum AddressProfileViewControllerMode {
  case ViewAddress
  case EditAddressCoordinates
  case EditAddressDetails
}


public protocol AddressProfileViewControllerDelegate {
  func didAddAddressSuccessfully(address: Address)
  func didEditAddressSuccessfully(address: Address)
}

/**
 This class is to be used for creating a new address
 or viewing / editing an existing address
 **/

public class AddressProfileViewController: UIViewController {
  // Discussion: This directly affects the placement/visibility
  // of Action/Save Button at the bottom
  public enum ViewControllerFunctionality {
    case BookAService // Select/Edit/Create an address for booking a service
    case Default // Viewing/Editing/Adding an address
  }
  
  public var delegate: AddressProfileViewControllerDelegate?
  
  private var animationDuration: NSTimeInterval = 0.3
  
  private var animateChanges = false
  
  @IBOutlet weak var actionButtonsView: UIView!
  @IBOutlet weak var actionButton: UIButton!
  @IBOutlet weak var constraintActionButtonsViewTopToSuperviewBottom: NSLayoutConstraint!
  @IBOutlet weak var constraintActionButtonsViewBottomToSuperviewBottom: NSLayoutConstraint!
  var locationsMapViewController: LocationsMapViewController!
  
  var constraintParkingDetailsBottomToActionsViewTop: NSLayoutConstraint!
  var constraintParkingDetailsBottomToSuperviewBottom: NSLayoutConstraint!
  
  var viewModel: AddressProfileViewModel = AddressProfileViewModel()
  
  var initialRequiredMode: AddressProfileViewControllerMode?
  private var mode: AddressProfileViewControllerMode = .ViewAddress {
    didSet {
      refreshViewForMode()
    }
  }
  
  var functionality: ViewControllerFunctionality = .Default
  
  var didLoad = false

  private var showBottomActionButton: Bool {
    get {
      switch self.functionality {
      case .BookAService:
        switch mode {
        case .ViewAddress:
          fallthrough
        case .EditAddressDetails:
          return true
        case .EditAddressCoordinates:
          return false
        }
      case .Default:
        switch mode {
        case .ViewAddress:
          fallthrough
        case .EditAddressDetails:
          fallthrough
        case .EditAddressCoordinates:
          return false
        }
      }
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    
    self.sendScreenView("Map Screen")
    
    // ====== Influences the search bar behavior ======
    edgesForExtendedLayout = UIRectEdge.None
    
    // ======
    // Add the locations map VC to the View
    // ======
    locationsMapViewController = storyboard!.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.LocationsMapViewController.rawValue)
      as! LocationsMapViewController
    locationsMapViewController.delegate = self
    locationsMapViewController.canSelectMarkers = (functionality == .BookAService)
    
    // the view controller can be editing an existing address coordinates
    // regardless of the mode
    /**
     * If this view is used for booking services then all addresses with coordinates
     * are displayed. If not, this view is used for creating/editing/viewing
     * an address. So only one address is displayed in case of view or edit.
     */
    let viewedAddresses: [Address] = viewModel.viewedAddressesForFunctionality(functionality)
    let selectedAddress: Address? = viewModel.address
    locationsMapViewController.setAddresses(
      viewedAddresses,
      withSelectedAddress: selectedAddress)
    let locationsMapView = self.addViewController(locationsMapViewController, toView: self.view)
    locationsMapView.alignTopEdgeWithView(self.view, predicate: "0")
    locationsMapView.alignLeading("0", trailing: "0", toView: self.view)

    constraintParkingDetailsBottomToSuperviewBottom = locationsMapView.alignBottomEdgeWithView(self.view, predicate: "0").first as! NSLayoutConstraint
    self.view.removeConstraint(constraintParkingDetailsBottomToSuperviewBottom)
    constraintParkingDetailsBottomToActionsViewTop = locationsMapView.constrainBottomSpaceToView(actionButtonsView, predicate: "0").first as! NSLayoutConstraint

    // ====== Add selector to action button ======
    actionButton.addTarget(
      self,
      action: #selector(AddressProfileViewController.didTapOnActionButton),
      forControlEvents: .TouchUpInside)
    
    
    // ====== Styling ======
    actionButtonsView.backgroundColor = UIColor.orange()
    
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(AddressProfileViewController.keyboardWillShow(_:)),
      name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(AddressProfileViewController.keyboardWillHide(_:)),
      name: UIKeyboardWillHideNotification, object: nil)
    
    didLoad = true
  }
  
  override public func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    // ====== Set the View Controller Mode ======
    // The mode affect the UI that's why we are initialising it here
    initViewMode()
  }
  
  public override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    self.view.endEditing(true)
  }
  
  func initViewMode() {
    // If there is no address(Having an address means that we are viewing or
    // editing an address), Then this means that we are creating a new address
    
    let originalMode = initialRequiredMode ?? mode
    if viewModel.address == nil {
      mode = .EditAddressCoordinates
    } else {
      if viewModel.address?.coordinates == nil {
        mode = .EditAddressCoordinates
      } else {
        mode = viewModel.isAddressServiceable(viewModel.address!) ? originalMode : .EditAddressCoordinates
      }
    }
  }
  
  func dismissViewController() {
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  //============================================================================
  // MARK: - Triggered Actions
  //============================================================================
  
  func keyboardWillShow(notification: NSNotification) {
    if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let frame = value.CGRectValue()
      // TODO: CheckAgain
      if !locationsMapViewController.searchController.searchBar.isFirstResponder() {
        layoutForKeyboardHeight(frame.height)
      }
    }
  }
  
  func keyboardWillHide(notification: NSNotification) {
    // TODO: CheckAgain
    if !locationsMapViewController.searchController.searchBar.isFirstResponder() {
      layoutForNoKeyboard()
    }

  }
  
  func didTapOnActionButton() {
    switch mode {
    case .EditAddressCoordinates:
      return
    case .EditAddressDetails:
      locationsMapViewController.addressDetailsViewDidTapSaveButton()
    case .ViewAddress:
      delegate?.didAddAddressSuccessfully(viewModel.address!)
    }
  }
  
  func handleActionForCurrentState(sender: AnyObject?) {
    switch mode {
    case .EditAddressDetails:
      viewModel.currentAddress = locationsMapViewController.getCurrentAddress()
      if viewModel.areCurrentAddressInformationsVerified {
        // If there is an address then we are currently editing an
        // already created address. Otherwise, we are creating a new one
        if viewModel.isNewAddress {
          setAddressTypeIfNeeded({
            self.addAddress()
          })
        } else {
          updateAddress()
        }
      } else {
        //TODO: Display alert
        // **!!!** Currently the alert view is displayed on the level of 
        // LocationsMapViewController
      }
    default:
      break
    }
  }
  
  func setAddressTypeIfNeeded(completion: () -> Void) {
    if viewModel.addressTypeNotSet {
      let actionSheet = UIAlertController(
        title: "What's your address type ?", message: nil,
        preferredStyle: .ActionSheet)
      let homeAction = UIAlertAction(
        title: AddressType.Home.addressTypeName(),
        style: .Default) { (_) in
          self.viewModel.addTypeToCurrentAddress(AddressType.Home)
          completion()
      }
      let officeAction = UIAlertAction(
        title: AddressType.Office.addressTypeName(),
        style: .Default) { (_) in
          self.viewModel.addTypeToCurrentAddress(AddressType.Office)
          completion()
      }
      let otherAction = UIAlertAction(
        title: AddressType.Other.addressTypeName(),
        style: .Default) { (_) in
          self.viewModel.addTypeToCurrentAddress(AddressType.Other)
          completion()
      }
      actionSheet.addAction(homeAction)
      actionSheet.addAction(officeAction)
      actionSheet.addAction(otherAction)
      
      self.presentViewController(actionSheet, animated: true, completion: nil)
    } else {
      completion()
    }
  }
}

//==============================================================================
// MARK: - Network API calls
//==============================================================================

extension AddressProfileViewController {
  func addAddress() {
    SwiftLoader.show(animated: true)
    
    self.viewModel.addAddressAPICall() { (succeeded: Bool, address: Address?, error: NSError?) -> Void in
      SwiftLoader.hide()
      if succeeded {
        if !self.viewModel.isAddressServiceable(address) {
          
          self.showAddressNotServiceableNotificationAlert({ () -> () in
            self.notifiyAddressUpdate(address)
            self.delegate?.didAddAddressSuccessfully(address!)
          })
          
        } else {
          self.notifiyAddressUpdate(address)
          self.delegate?.didAddAddressSuccessfully(address!)
        }
      } else {
        self.showFailToUpdateAddressesAlert()
      }
    }
  }
  
  func updateAddress() {
    SwiftLoader.show(animated: true)
    self.viewModel.updateAddressAPICall({ (success: Bool, address: Address?, error: NSError?) -> Void in
      SwiftLoader.hide()
      if success {
        self.affectedBookingsAlert(address)
      } else {
        self.showFailToUpdateAddressesAlert()
      }
    })
  }
}

//==============================================================================
// MARK: - Helpers
//==============================================================================

extension AddressProfileViewController {

  func notifiyAddressUpdate(address: Address?) {
    let userInfo: [String : AnyObject]? = (address != nil) ? ["address": address!] : nil
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidUpdateAddresses.rawValue,
      object: nil, userInfo: userInfo)
  }
  
  func showAddressNotServiceableNotificationAlert(completionBlock: (()->())? = nil) {
    let alertController = UIAlertController(
      title: viewModel.alertViewAddressNotServiceableNotificationAlertTitle,
      message: viewModel.alertViewAddressNotServiceableNotificationAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewAddressNotServiceableNotificationAlertButtonText,
      style: UIAlertActionStyle.Default) { (_) -> Void in
        if let completionBlock = completionBlock {
          completionBlock()
        } else {
          self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func affectedBookingsAlert(address: Address?) {
    if let affectedBookings = viewModel.affectedBookings() {
      let alertController = UIAlertController(
        title: viewModel.alertViewAffectedBookingsTitle,
        message: viewModel.alertViewAffectedBookingsMessage,
        preferredStyle: UIAlertControllerStyle.Alert)
      
      let okAction = UIAlertAction(title: viewModel.alertViewAffectedBookingsActionButtonText,
        style: UIAlertActionStyle.Default,
        handler: { (let alertAction) -> Void in
          self.updatingAddressSucceeded(affectedBookings)
          self.delegate?.didEditAddressSuccessfully(address!)
      })
      alertController.addAction(okAction)
      
      self.presentViewController(alertController, animated: true, completion: nil)
    } else {
      if !self.viewModel.isAddressServiceable(self.viewModel.address) {
        self.showAddressNotServiceableNotificationAlert({ () -> () in
          self.updatingAddressSucceeded()
          self.delegate?.didEditAddressSuccessfully(address!)
        })
      } else {
        updatingAddressSucceeded()
        self.delegate?.didEditAddressSuccessfully(address!)
      }
    }
  }
  
  func showFailToUpdateAddressesAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToAddOrUpdateAddressAlertTitle,
      message: viewModel.alertViewFailToAddOrUpdateAddressAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewFailToAddOrUpdateAddressActionButtonText,
      style: UIAlertActionStyle.Destructive, handler: nil)
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func updatingAddressSucceeded(affectedBookings: [Booking]? = nil) {
    if let affectedBookings = affectedBookings {
      viewModel.updateBookingsForUpdatedAddressInformation(affectedBookings)
    }

    let userInfo: [String : AnyObject]? = (self.viewModel.address != nil) ? ["address": self.viewModel.address!] : nil

    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidUpdateAddresses.rawValue, object: nil, userInfo: userInfo)

    self.navigationController?.popToRootViewControllerAnimated(true)
  }
  
  
  func refreshViewForMode() {
    if self.didLoad {
      let serviceable = self.viewModel.areCoordinatesServiceable(self.locationsMapViewController.lastKnownCoordinates ?? self.viewModel.address?.coordinates ?? nil)
      switch mode {
      case .ViewAddress:
        refreshActionViewVisibility(forMode: mode, animated: animateChanges) { () -> Void in
          self.locationsMapViewController.originalMode = LocationsMapViewMode.ParkingDetailsPeek
          self.locationsMapViewController.parkingDetailsEditable = false
          self.locationsMapViewController.displayMapCenterIndicator = false
          self.locationsMapViewController.displaySearchBar(false)
          self.locationsMapViewController.displayMarkers(serviceable)
          self.locationsMapViewController.updateLocateYourselfVisibility(false)

          self.title = self.viewModel.address!.type!.addressTypeName()
        }
      case .EditAddressCoordinates:
        refreshActionViewVisibility(forMode: mode, animated: animateChanges) { () -> Void in
          self.locationsMapViewController.originalMode = LocationsMapViewMode.MapFullScreen
          self.locationsMapViewController.parkingDetailsEditable = true
          self.locationsMapViewController.displayMapCenterIndicator = true
          self.locationsMapViewController.displaySearchBar(true)
          self.locationsMapViewController.displayMarkers(serviceable, hideSelectedMarker: true)
          self.locationsMapViewController.updateLocateYourselfVisibility(true)
          self.locationsMapViewController.displayMapCenterIndicatorCollapsed = false

          self.locationsMapViewController.updateMapCenterIndicator(serviceable)
          self.title = "Pick A Location"
        }
      case .EditAddressDetails:
        refreshActionViewVisibility(forMode: mode, animated: animateChanges) { () -> Void in
          self.locationsMapViewController.originalMode = LocationsMapViewMode.MapViewPeek
          self.locationsMapViewController.parkingDetailsEditable = true
          self.locationsMapViewController.displayMapCenterIndicator = true
          self.locationsMapViewController.displaySearchBar(false)
          self.locationsMapViewController.hideMarkers()
          self.locationsMapViewController.updateLocateYourselfVisibility(false)
          self.locationsMapViewController.displayMapCenterIndicatorCollapsed = true

          self.locationsMapViewController.updateMapCenterIndicator(serviceable)
          
          self.title = "Parking Details"
        }
      }
      
      // mode is handled internally
      actionButton.setTitle(viewModel.actionButtonTitleForMode(mode), forState: .Normal)
    }
  }
  
  func hideActionView(animated: Bool = false, completion: (() -> Void)? = nil) {
    self.view.removeConstraint(constraintActionButtonsViewBottomToSuperviewBottom)
    self.view.addConstraint(constraintActionButtonsViewTopToSuperviewBottom)
    
    if animated {
      UIView.animateWithDuration(animationDuration,
        animations: { () -> Void in
          self.view.layoutIfNeeded()
        }) { (_) -> Void in
          completion?()
      }
    } else {
      self.view.layoutIfNeeded()
      completion?()
    }
  }
  
  func showActionView(animated: Bool = false, completion: (() -> Void)? = nil) {
    self.view.removeConstraint(constraintActionButtonsViewTopToSuperviewBottom)
    self.view.addConstraint(constraintActionButtonsViewBottomToSuperviewBottom)
    
    if animated {
      UIView.animateWithDuration(animationDuration,
        animations: { () -> Void in
          self.view.layoutIfNeeded()
        }) { (_) -> Void in
          completion?()
      }
    } else {
      self.view.layoutIfNeeded()
      completion?()
    }
  }
  
  func refreshActionViewVisibility(forMode
    mode: AddressProfileViewControllerMode,
    animated: Bool, completion: (() -> Void)? = nil)
  {
    if showBottomActionButton {
      showActionView(animated, completion: completion)
    } else {
      hideActionView(animated, completion: completion)
    }
  }
  
  func layoutForKeyboardHeight(height: CGFloat) {
    constraintParkingDetailsBottomToSuperviewBottom.constant = -height
    
    self.view.removeConstraint(constraintParkingDetailsBottomToActionsViewTop)
    self.view.addConstraint(constraintParkingDetailsBottomToSuperviewBottom)
    
    locationsMapViewController.originalMode = LocationsMapViewMode.ParkingDetailsFullScreen
    hideActionView(true)
  }
  
  func layoutForNoKeyboard() {
    self.view.removeConstraint(constraintParkingDetailsBottomToSuperviewBottom)
    self.view.addConstraint(constraintParkingDetailsBottomToActionsViewTop)
    constraintParkingDetailsBottomToSuperviewBottom.constant = 0
    refreshViewForMode()
  }
}

//==============================================================================
// MARK: - LocationsMapViewDelegate
//==============================================================================

extension AddressProfileViewController: LocationsMapViewDelegate {
  func locationsMapViewDidTapCenterIndicator() {
    if !locationsMapViewController.haveLocationPermission() {
      return
    }
    locationsMapViewController.setMarkerForSelectedAddressMarker()
    mode = .EditAddressDetails
  }
  
  func locationsMapViewCanTransitionToMode(mode: LocationsMapViewMode) -> Bool {
    if mode == .ParkingDetailsPeek && self.mode == AddressProfileViewControllerMode.EditAddressDetails {
      self.mode = .EditAddressCoordinates
      return false
    }
    
    return true
  }

  func locationsMapViewDidTapEditParkingDetailsButton() {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Edit Location On Map Tap", label: "", value: nil)
    
    self.mode = .EditAddressDetails
  }
  
  func locationsMapViewDidTapSaveParkingDetailsButton() {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Save Location On Map Tap", label: "", value: nil)
    
    handleActionForCurrentState(nil)
  }

  func locationsMapViewCanServiceCoordinates(coordinates: CLLocationCoordinate2D) -> Bool {
    return viewModel.areCoordinatesServiceable(coordinates)
  }
  
  /**
   if the address value is nil, then this means that we this VC is used in the 
   Booking process for choosing a location. When the map is dragged then
   no address is selected and the map center indicator view is displayed.
   If there is an address selected then we are viewing its details.
  */
  func locationsMapViewDidSelectAddress(address: Address?) {
    viewModel.address = address
    if let _ = address {
      mode = .ViewAddress
    } else {
      mode = .EditAddressCoordinates
    }
  }
  
  func locationsMapViewShouldDeselectSelectedMarker() -> Bool {
    return functionality != .Default
  }
}


//==============================================================================
// MARK: - ViewTapDelegate
//==============================================================================
extension AddressProfileViewController: ViewTapDelegate {
  public func didTapToCancel() {
  }
}
