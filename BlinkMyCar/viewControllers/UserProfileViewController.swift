//
//  UserProfileViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/27/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class UserProfileViewController: UIViewController {
  
  @IBOutlet weak var submitButton: BMCOrangeButton!
  @IBOutlet weak var firstNameLabel: BMCFormLabel!
  @IBOutlet weak var lastNameLabel: BMCFormLabel!
  @IBOutlet weak var emailLabel: BMCFormLabel!
  @IBOutlet weak var phoneLabel: BMCFormLabel!
  @IBOutlet weak var phoneErrorLabel: BMCErrorLabel!
  @IBOutlet weak var emailErrorLabel: BMCErrorLabel!
   
  
  @IBOutlet weak var firstNameTextField: BMCTextField!
  @IBOutlet weak var lastNameTextField: BMCTextField!
  @IBOutlet weak var phoneCodeNumberNameTextField: BMCTextField!
  @IBOutlet weak var phoneNumberNameTextField: BMCTextField!
  
  @IBOutlet weak var emailValidationTextField: ValidationTextField!
  
  @IBOutlet weak var emailErrorLabelDistanceFromLabelConstraint: NSLayoutConstraint!
  @IBOutlet weak var emailContainerHeightConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var phoneNumberErrorLabelDistanceFromLabelConstraint: NSLayoutConstraint!
  @IBOutlet weak var phoneNumberContainerHeightConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var scrollViewConstraintToSuperViewBottom: NSLayoutConstraint!
  
  var viewModel: UserProfileViewModel = UserProfileViewModel()
  let errorPadding: CGFloat = 15.0
  let textFieldHeight: CGFloat = 44.0
  let itemContainerHeight: CGFloat = 96.0
  
  @IBOutlet weak var scrollView: UIScrollView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Profile")
    self.submitButton.setTitle(viewModel.submitButtonTitle, forState: .Normal)
    self.setupFields()
    
    
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(UserProfileViewController.keyboardWillShow(_:)),
      name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(UserProfileViewController.keyboardWillHide(_:)),
      name: UIKeyboardWillHideNotification, object: nil)
  
    self.view.addTapGesture(self)
  }
  
  func setupFields() {
    emailValidationTextField.keyboardType = UIKeyboardType.EmailAddress
    phoneNumberNameTextField.keyboardType = UIKeyboardType.NumberPad
    
    phoneCodeNumberNameTextField.enable(false)
    phoneCodeNumberNameTextField.text = "+961"
    phoneCodeNumberNameTextField.textAlignment = NSTextAlignment.Center
  
    phoneErrorLabel.text = viewModel.textFieldPhoneErrorMessage
    emailErrorLabel.text = viewModel.textFieldEmailErrorMessage
    
    phoneNumberNameTextField.delegate = self
    firstNameTextField.delegate = self
    lastNameTextField.delegate = self
    emailValidationTextField.validationTextFieldDelegate = self
    
    firstNameTextField.returnKeyType = UIReturnKeyType.Next
    lastNameTextField.returnKeyType = UIReturnKeyType.Next
    emailValidationTextField.returnKeyType = UIReturnKeyType.Next
    phoneNumberNameTextField.returnKeyType = UIReturnKeyType.Done
    
    firstNameLabel.text = self.viewModel.labelFirstNameText
    lastNameLabel.text = self.viewModel.labelLastNameText
    emailLabel.text = self.viewModel.labelEmailText
    phoneLabel.text = self.viewModel.labelPhoneText

    self.fillUserForm()
  }
  
  func fillUserForm() {
    firstNameTextField.text = self.viewModel.firstNameValue
    lastNameTextField.text = self.viewModel.lastNameValue
    emailLabel.setRequired(true)
    phoneLabel.setRequired(true)
    emailValidationTextField.text = self.viewModel.emailValue
    phoneNumberNameTextField.text = self.viewModel.phoneNumberValue
  }
  
  func collectDataFromForm() {
    self.viewModel.firstNameValue = firstNameTextField.text ?? ""
    self.viewModel.lastNameValue = lastNameTextField.text ?? ""
    self.viewModel.emailValue = emailValidationTextField.text ?? ""
    self.viewModel.phoneNumberValue = phoneNumberNameTextField.text ?? ""
  }
  
  func didTapBackButton() {
    self.dismissKeyboard()
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func didTapSubmitButton(sender: BMCButton) {
    submitAction()
  }
  
  func submitAction() {
    if self.viewModel.validateEmail(emailValidationTextField.text) &&
      self.viewModel.validatePhoneNumber(phoneNumberNameTextField.text) {
        self.collectDataFromForm()
    
        SwiftLoader.show(animated: true)
        self.viewModel.updateProfile { (success: Bool) -> Void in
          SwiftLoader.hide()
          if success {
            self.didTapBackButton()
          }
          else {
            self.showFailToUpdateUserProfileAlert()
          }
        }
    }
    else {
      self.showInvalidFormDataAlert()
    }
  }
  
  func didTapDoneButton() {
    dismissKeyboard()
  }
  
  func showFailToUpdateUserProfileAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToUpdateUserProfileAlertTitle,
      message: viewModel.alertViewFailToUpdateUserProfileAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewFailToUpdateUserProfileAlertActionButtonText,
      style: UIAlertActionStyle.Destructive, handler: nil)
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func showInvalidFormDataAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewInvalidFormDataAlertTitle,
      message: viewModel.alertViewInvalidFormDataAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewInvalidFormDataAlertActionButtonText,
      style: UIAlertActionStyle.Destructive, handler: nil)
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}
//==========================================================================
// MARK: - K E Y B O A R D  N O T I F I C A T I O N S
//==========================================================================
extension UserProfileViewController {
  
  private func dismissKeyboard() {
    emailValidationTextField.textField.resignFirstResponder()
    firstNameTextField.resignFirstResponder()
    lastNameTextField.resignFirstResponder()
    phoneNumberNameTextField.resignFirstResponder()
    phoneCodeNumberNameTextField.resignFirstResponder()
  }
  
  func keyboardWillShow(notification: NSNotification) {
    if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let frame = value.CGRectValue()
      scrollViewConstraintToSuperViewBottom.constant = frame.height
     
    }
    
    layoutForVisibleKeyboard()
  }
  
  func keyboardWillHide(notification: NSNotification) {
    scrollViewConstraintToSuperViewBottom.constant = 0
    layoutForHiddenKeyboard()
  }
  
  func layoutForVisibleKeyboard() {

    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func layoutForHiddenKeyboard() {
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func scrollToView(textField: UITextField) {
    _ = scrollView.contentOffset
    
    var rc: CGRect = textField.bounds
    rc = textField.convertRect(rc, toView:scrollView)
    var pt: CGPoint = rc.origin;
    pt.x = 0
    pt.y -= rc.height
    scrollView.setContentOffset(pt, animated: true)
  }
}
extension UserProfileViewController: ValidationTextFieldDelegate {
  
  func validationTextFieldDidBeginEditing(textField: UITextField) {
       self.scrollToView(textField)
  }
  
  func validationTextFieldDidEndEditing(textField: UITextField) {
    if self.viewModel.validateEmail(emailValidationTextField.text) {
      hideEmailErrorLabel()
    }else{
      showEmailErrorLabel()
    }
  }
  
  func validationTextFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case emailValidationTextField.textField:
      self.viewModel.emailValue = emailValidationTextField.textField.text ?? ""
      phoneNumberNameTextField.becomeFirstResponder()
    default:
      break
    }
    return true
  }
  
  func validationTextFieldShouldLayoutIfNeeded(animationBlock: (()->Void) ) {
    UIView.animateWithDuration(0.5,
      animations: {() -> Void in
        self.view.layoutIfNeeded()
        animationBlock()
    })
  }
}

extension UserProfileViewController: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch(textField) {
    case firstNameTextField:
      self.viewModel.firstNameValue = firstNameTextField.text ?? ""
      lastNameTextField.becomeFirstResponder()
    case lastNameTextField:
      self.viewModel.lastNameValue = lastNameTextField.text ?? ""
      emailValidationTextField.textField.becomeFirstResponder()
    default:
      self.viewModel.phoneNumberValue = phoneNumberNameTextField.text ?? ""
      break
    }
    return true
  }

  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    self.addKeyboardToolBarDone(textField)
    self.scrollToView(textField)
    if textField.text == " " {
      textField.text = ""
    }
    return true
  }
  
  func textFieldDidEndEditing(textField: UITextField) {
    if phoneNumberNameTextField == textField {
      if (viewModel.validatePhoneNumber(phoneNumberNameTextField.text)) {
        hidePhoneNumberErrorLabel()
      } else {
        showPhoneNumberErrorLabel()
      }
    }
  }
  
  func addKeyboardToolBarDone(textField: UITextField) {
    // Create a button bar for the number pad
    let keyboardDoneButtonView = UIToolbar()
    keyboardDoneButtonView.sizeToFit()
    
    // Setup the buttons to be put in the system.
    let item = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(UserProfileViewController.didTapDoneButton) )
    let spacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
    let toolbarButtons = [spacer,item]
    
    //Put the buttons into the ToolBar and display the tool bar
    keyboardDoneButtonView.setItems(toolbarButtons, animated: false)
    textField.inputAccessoryView = keyboardDoneButtonView
  }

}

//MARK: E R R O R  A N I M A T I O N
extension UserProfileViewController {
  private func showPhoneNumberErrorLabel() {
    self.phoneErrorLabel.alpha = 1.0
    phoneNumberErrorLabelDistanceFromLabelConstraint.constant = textFieldHeight + errorPadding
    phoneNumberContainerHeightConstraint.constant = itemContainerHeight + errorPadding
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  private func hidePhoneNumberErrorLabel() {
    phoneNumberErrorLabelDistanceFromLabelConstraint.constant = textFieldHeight - errorPadding
    phoneNumberContainerHeightConstraint.constant = itemContainerHeight
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.phoneErrorLabel.alpha = 0.0
      self.view.layoutIfNeeded()
    })
  }
  
  private func showEmailErrorLabel() {
    self.emailErrorLabel.alpha = 1.0
    emailErrorLabelDistanceFromLabelConstraint.constant = textFieldHeight + errorPadding
    emailContainerHeightConstraint.constant = itemContainerHeight + errorPadding
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  private func hideEmailErrorLabel() {
    emailErrorLabelDistanceFromLabelConstraint.constant = textFieldHeight - errorPadding
    emailContainerHeightConstraint.constant = itemContainerHeight
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.emailErrorLabel.alpha = 0.0
      self.view.layoutIfNeeded()
    })
  }
}

//======================================================
// MARK: - ViewTapDelegate
//======================================================
extension UserProfileViewController: ViewTapDelegate {
  func didTapToCancel() {
    dismissKeyboard()
  }
}
