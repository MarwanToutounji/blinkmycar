//
//  VehicleInfoTableViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/25/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

protocol VehicleInfoTableViewControllerDelegate {
  func didSelectRow(tableView:VehicleInfoTableViewController, selectedObject: AnyObject)
}

class VehicleInfoTableViewController: UIViewController {

  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!

  var delegate: VehicleInfoTableViewControllerDelegate?

  private var searchController = UISearchController()

  var dataSource = [String]() {
    didSet {
      if let _ = tableView {
        tableView.reloadData()
      }
    }
  }

  private var filteredData = [String]() {
    didSet {
      self.tableView.reloadData()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    tableView.delegate = self
    tableView.dataSource = self

    searchController = ({
      let controller = UISearchController(searchResultsController: nil)
      controller.searchResultsUpdater = self
      controller.hidesNavigationBarDuringPresentation = false
      controller.dimsBackgroundDuringPresentation = false
      controller.searchBar.searchBarStyle = UISearchBarStyle.Minimal
      controller.searchBar.sizeToFit()
      self.tableView.tableHeaderView = controller.searchBar
      return controller
    })()

    topLabel.text = descriptionLabel
  }

  var descriptionLabel: String? {
    didSet{
      if topLabel != nil {
        topLabel.text = descriptionLabel
      }
    }
  }
}

extension VehicleInfoTableViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    if self.searchController.active {
      return (self.filteredData.count > 0) ? 1 : 0
    } else {
      return (dataSource.count > 0) ? 1 : 0
    }
  }

  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if self.searchController.active {
      return self.filteredData.count
    } else {
      return dataSource.count
    }
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(VehicleInfoTableViewCell.identifier)
      as! VehicleInfoTableViewCell

    var data: String
    if self.searchController.active {
      data = self.filteredData[indexPath.row]
    } else {
      data = dataSource[indexPath.row]
    }

    cell.info = data
    return cell
  }
}

extension VehicleInfoTableViewController: UITableViewDelegate {
  func tableView(tableView: UITableView,
    didSelectRowAtIndexPath indexPath: NSIndexPath) {
      var data: String
      if self.searchController.active {
        data = self.filteredData[indexPath.row]
      } else {
        data = dataSource[indexPath.row]
      }
      delegate?.didSelectRow(self, selectedObject: data)

      searchController.active = false
  }
}

extension VehicleInfoTableViewController: UISearchBarDelegate, UISearchResultsUpdating {

  func filterContentForSearchText(searchText: String) {
    // Filter the array using the filter method
    if searchText.isEmpty {
      self.filteredData = self.dataSource
    } else {
      self.filteredData = self.dataSource.filter({( data: String) -> Bool in
        let stringMatch = data.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
        return stringMatch != nil
      })
    }
  }

  func updateSearchResultsForSearchController(searchController: UISearchController) {
    filterContentForSearchText(searchController.searchBar.text ?? "")
  }
}
