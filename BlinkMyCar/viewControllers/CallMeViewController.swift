//
//  CallMeViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class CallMeViewController: UIViewController {

  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var okButton: BMCGreenButton!
  @IBOutlet weak var cancelButton: BMCRedButton!

  var viewModel: CallMeViewModel?

  override func viewDidLoad() {
    super.viewDidLoad()
    setupContent()
  }

  func setupContent() {
    self.title = viewModel!.viewControllerTitle
    self.okButton.setTitle(self.viewModel!.okButtonTitle, forState: .Normal)
    self.cancelButton.setTitle(self.viewModel!.cancelButtonTitle, forState: .Normal)
    self.headerLabel.text = self.viewModel!.headerLabelText
  }

  @IBAction func okButtonTouchUpInside(sender: UIButton) {
    SwiftLoader.show(animated: true)
    viewModel?.sendCallMeRequest({ (error) -> Void in
      var title = ""
      var message = ""
      var isError = false
      if let _ = error {
        title = self.viewModel!.failAlertTitle
        message = self.viewModel!.failAlertMessage
        isError = true
      } else {
        title = self.viewModel!.successAlertTitle
        message = self.viewModel!.successAlertMessage
      }
      let alertController = UIAlertController(
        title: title,
        message: message,
        preferredStyle: UIAlertControllerStyle.Alert)

      let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        if !isError {
          self.dismissViewControllerAnimated(true, completion: nil)
        }
      }

      alertController.addAction(okAction)

      SwiftLoader.hide()
      self.presentViewController(alertController, animated: true, completion: nil)
    })
  }

  @IBAction func cancelButtonTouchUpInside(sender: UIButton) {
    navigationController?.popViewControllerAnimated(true)
  }

}
