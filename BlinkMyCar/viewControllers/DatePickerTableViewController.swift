//
//  DatePickerTableViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/22/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import SwiftLoader

class DatePickerTableViewCell: UITableViewCell {
  @IBOutlet weak var dateLabel: UILabel!

  var dateLabelText: String? {
    get {
      return dateLabel.text
    }

    set {
      dateLabel.text = newValue
    }
  }

  private var _isEnabled = true
  var isEnabled: Bool {
    get {
      return _isEnabled
    }
    set {
      _isEnabled = newValue
      if _isEnabled {
        dateLabel.textColor = UIColor.darkGray()
      } else {
        dateLabel.textColor = UIColor.lightGray()
      }
    }
  }
}

protocol DatePickerTableViewDelegate {
  func datePickerDidSelectServiceDate(serviceDate: ServiceDate?)
  func datePickerCancelLoadingDates()
}

class DatePickerTableViewController: UITableViewController {

  var viewModel = DatePickerViewModel()

  var selectedIndexPath: NSIndexPath?

  var datePickerDelegate: DatePickerTableViewDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.tableFooterView = UIView(frame: CGRectZero)
    
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(DatePickerTableViewController.stopTimer(_:)),
      name: BMCNotificationNames.shouldStopBookingFlowTimer.rawValue,
      object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(
      self, selector: #selector(DatePickerTableViewController.displayBookingTimeOutAlertView),
      name: BMCNotificationNames.DisplayBookingProcessTimeOutAlert.rawValue, object: nil)
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  var mustShowTimeOutAlert: Bool = false
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    if mustShowTimeOutAlert {
      displayBookingTimeOutAlertView()
    }
  }
  
  func reloadData() {
    SwiftLoader.show(animated: true)
    viewModel.reloadDateAndTimes { (success, error) in
      SwiftLoader.hide()
      
      if !success {
        self.displayFailToRetrieveDatesAlertView()
        self.viewModel.stopTimer()
        return
      }
      
      self.viewModel.startTimer(
        self,
        selector: #selector(DatePickerTableViewController.timerTimedOut))
      
      self.tableView.reloadData()
    }
  }
  
  //===============================================
  // MARK: - Actions
  //===============================================
  
  func timerTimedOut() {
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.BookingProcessTimeOut.rawValue, object: nil)
  }
  
  func stopTimer(sender: AnyObject?) {
    viewModel.stopTimer()
  }

  //===============================================
  // MARK: - Table view data source
  //===============================================

  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(
      "DatePickerTableViewCell") as! DatePickerTableViewCell
    cell.dateLabelText = viewModel.dateLabelForRow(indexPath.row)
    cell.isEnabled = viewModel.dateEnabledForRow(indexPath.row)

    if viewModel.shouldSelectRow(indexPath.row) {
      selectedIndexPath = indexPath
      tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
    }

    return cell
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let dateForRow = viewModel.dateForRow(indexPath.row)
    let shouldRemainSelected = viewModel.dateEnabledForRow(indexPath.row)
    selectedIndexPath = shouldRemainSelected ? indexPath : nil
    viewModel.selectedDate = shouldRemainSelected ? dateForRow : nil
    datePickerDelegate?.datePickerDidSelectServiceDate(shouldRemainSelected ? dateForRow : nil)
    if !shouldRemainSelected {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  
  //===============================================
  // MARK: - Alerts
  //===============================================
  
  func displayFailToRetrieveDatesAlertView() {
    let alertController = UIAlertController(
      title: viewModel.failToRetrieveDatesAlertTitle,
      message: viewModel.failToRetrieveDatesAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(
      title: viewModel.failToRetrieveDatesCancelActionButtonTitle,
      style: UIAlertActionStyle.Cancel) { (let action: UIAlertAction) -> Void in
        self.datePickerDelegate?.datePickerCancelLoadingDates()
    }
    
    let retryAction = UIAlertAction(title: viewModel.failToRetrieveDatesRetryActionButtonTitle, style: UIAlertActionStyle.Default) { (let action: UIAlertAction) -> Void in
      self.reloadData()
    }
    
    alertController.addAction(cancelAction)
    alertController.addAction(retryAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayBookingTimeOutAlertView() {
    mustShowTimeOutAlert = true
    
    let alertController = UIAlertController(
      title: viewModel.alertViewBookingTimeOutAlertTitle,
      message: viewModel.alertViewBookingTimeOutAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(
      title: viewModel.alertViewBookingTimeOutAlertActionButtonText,
      style: UIAlertActionStyle.Default) { (let action: UIAlertAction) -> Void in
        self.mustShowTimeOutAlert = false
        self.reloadData()
    }
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}
