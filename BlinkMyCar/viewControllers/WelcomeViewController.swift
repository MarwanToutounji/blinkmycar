//
//  KWDHomeViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/11/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

  @IBOutlet weak var buttonSignIn: UIButton!
  @IBOutlet weak var buttonRegister: UIButton!
  @IBOutlet weak var labelWelcome: UILabel!

  private var viewModel: WelcomeViewModel

  required init?(coder aDecoder: NSCoder) {
    viewModel = WelcomeViewModel()
    super.init(coder: aDecoder)
  }


  override func viewDidLoad() {
    super.viewDidLoad()
    self.applyTheme()
    self.setupContent()
    
    self.title = "Welcome Screen"
    self.sendScreenView("Welcome Screen")

    NSNotificationCenter.defaultCenter().removeObserver(self)
    
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(WelcomeViewController.showSignInVCInsteadOfRegistrationVC(_:)),
      name: BMCNotificationNames.FailToSignInAfterRegistration.rawValue,
      object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(WelcomeViewController.showSignInVCInsteadOfRegistrationVC(_:)),
      name: BMCNotificationNames.DidTapLoginFromRegisterVC.rawValue,
      object: nil)

    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(WelcomeViewController.showRegistrationVCInsteadOfSignInVC(_:)),
      name: BMCNotificationNames.DidTapRegisterFromSignInVC.rawValue,
      object: nil)

    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(WelcomeViewController.goToHomeVC(_:)),
      name: BMCNotificationNames.LoginSuccess.rawValue,
      object: nil)
    
    (UIApplication.sharedApplication().delegate as! AppDelegate).disablePushNotificationAlerts()
  }

  deinit {
    self.dismissViewControllerAnimated(false, completion: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    viewModel = WelcomeViewModel()
  }
  
  func applyTheme() {
    self.view.backgroundColor = UIColor.applicationMainColor()
  }

  func setupContent() {
    labelWelcome.text = viewModel.welcomeLabelText
  }
  
  //=========================================================
  // MARK - A C T I O N S
  //=========================================================

  @IBAction func didClickSignIn(sender: AnyObject) {
    self.trackEvent(AnalyticsCategories.WelcomeScreen.rawValue, action: "Signin Tap", label: "", value: nil)
    presentSignInVC(nil)
  }

  @IBAction func didClickRegister(sender: AnyObject) {
    self.trackEvent(AnalyticsCategories.WelcomeScreen.rawValue, action: "Register Tap", label: "", value: nil)
    presentAccountInfoVC()
  }

  func goToHomeVC(notification: NSNotification) {
    buttonRegister.alpha = 0
    buttonSignIn.alpha = 0
    self.dismissViewControllerAnimated(true, completion: { () -> Void in
      NSNotificationCenter.defaultCenter().postNotificationName(
        BMCNotificationNames.ShouldShowHomeVC.rawValue, object: nil)
    })
  }

  func showSignInVCInsteadOfRegistrationVC(notification: NSNotification) {
    self.dismissViewControllerAnimated(true,
      completion: { () -> Void in
        let userEmail: String? = (notification.userInfo?[BMCNotificationDataKeys.BMCUserEmailKey.rawValue] ?? "") as? String
        self.presentSignInVC(userEmail)
    })
  }

  func showRegistrationVCInsteadOfSignInVC(notification: NSNotification) {
    self.dismissViewControllerAnimated(true,
      completion: { () -> Void in
        self.presentRegistrationVC()
    })
  }

  //=========================================================
  // MARK - H E L P E R S
  //=========================================================

  private func presentSignInVC(userEmail: String?) {
    let viewController = storyboard!.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.SignInViewController.rawValue) as! SignInViewController
    
    viewController.viewModel = SignInViewModel()
    viewController.viewModel.email = userEmail ?? nil
    
    let navVC = UINavigationController(rootViewController: viewController)
    self.presentViewController(navVC, animated: true, completion: nil)
  }

  private func presentRegistrationVC() {
    let viewController = storyboard!.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.RegisterViewController.rawValue) 
    let navVC = UINavigationController(rootViewController: viewController)
    self.presentViewController(navVC, animated: true, completion: nil)
  }

  private func presentAccountInfoVC() {
    let accountInfoVC = storyboard!.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.RegisterViewController.rawValue) 
    let navigationViewController = UINavigationController(rootViewController: accountInfoVC)
    self.presentViewController(navigationViewController, animated: true, completion: nil)
  }
}
