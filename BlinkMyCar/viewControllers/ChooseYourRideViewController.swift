//
//  ChoseYourRideViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/19/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class ChooseYourRideViewController: UIViewController {
  @IBOutlet weak var carView: ImageTitleView!
  @IBOutlet weak var bikeView: ImageTitleView!

  var viewModel: ChooseYourRideViewModel = ChooseYourRideViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.applyTheme()
    
    self.navigationItem.hidesBackButton = true
    self.title = viewModel.chooseARideTitle
    self.sendScreenView("Choose Type")
    
    carView.image = UIImage(named: "iconCar")
    carView.text = viewModel.carTitle
    bikeView.image = UIImage(named: "iconMotorcycle")
    bikeView.text = viewModel.bikeTitle
    
//    if viewModel.shouldDisplayFreeWashAlert {
//      displayFreeWashAlert()
//    }
  }
  
  func applyTheme() {
    self.view.backgroundColor = UIColor.applicationMainColor()
  }
  
  func displayFreeWashAlert() {
    let alertViewController = UIAlertController(
      title: viewModel.freeWashAlertViewTitle,
      message: viewModel.freeWashAlertViewMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    let actionButton = UIAlertAction(
      title: viewModel.freeWashAlertViewActionButtonTitle,
      style: UIAlertActionStyle.Default,
      handler: nil)
    alertViewController.addAction(actionButton)
    
    // Style the Message
    let attributedString = NSMutableAttributedString(string: viewModel.freeWashAlertViewMessage)
    attributedString.addAttribute(NSFontAttributeName,
      value: UIFont.boldBMCFont(size: 16),
      range: (viewModel.freeWashAlertViewMessage as NSString).rangeOfString("FREE"))
    alertViewController.setValue(attributedString, forKey: "attributedMessage")
    
    presentViewController(
      alertViewController,
      animated: true,
      completion: nil)
  }
  
  // MARK - A C T I O N S
  //----------------------
  @IBAction func tapCarView(sender: UITapGestureRecognizer) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Vehicle Type Tap", label: "Car", value: nil)
    registerANewVehicle(.Car)
    carView.label.textColor = UIColor.whiteColor()
  }

  @IBAction func tapBikeView(sender: UITapGestureRecognizer) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Vehicle Type Tap", label: "Bike", value: nil)
    registerANewVehicle(.Motorcycle)
    bikeView.label.textColor = UIColor.whiteColor()
  }

  private func registerANewVehicle(vehicleType: Make.VehicleType) {
    // TODO: Implement this action
    let newVehicleVC = storyboard!.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.CreateRideViewController.rawValue) as! CreateRideViewController
    newVehicleVC.viewModel.analyticsCategory = viewModel.analyticsCategory
    newVehicleVC.vehicleType = vehicleType
    self.navigationController?.pushViewController(newVehicleVC, animated: true)
  }
}

extension ChooseYourRideViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
    let view = gestureRecognizer.view as? ImageTitleView
    view?.label.textColor = UIColor.orange()
    return true
  }
}
