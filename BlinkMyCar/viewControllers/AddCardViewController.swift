//
//  AddCardViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 4/12/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import Stripe
import SwiftLoader

protocol AddCardViewControllerDelegate: NSObjectProtocol {
  func didAddCard(token: STPToken)
  func didCancelCardAddition()
}

class AddCardViewController: UIViewController {
  let STPInvalidRequestError: Int = 1

  var paymentTextField: STPPaymentCardTextField!
  var delegate: AddCardViewControllerDelegate?
  var viewModel: AddCardViewModel = AddCardViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()

    self.view.backgroundColor = UIColor.whiteColor()
    self.title = self.viewModel.addCardViewControllerTitle
    self.sendScreenView("Add Payment")
    if self.respondsToSelector(Selector("setEdgesForExtendedLayout:")) {
      self.edgesForExtendedLayout = .None
    }
    // Setup save button
    let title: String = self.viewModel.addCardButtonTitle
    let saveButton: UIBarButtonItem = UIBarButtonItem(title: title, style: .Done, target: self, action: #selector(AddCardViewController.save(_:)))
    let cancelButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(AddCardViewController.cancel(_:)))
    saveButton.enabled = false
    self.navigationItem.leftBarButtonItem = cancelButton
    self.navigationItem.rightBarButtonItem = saveButton

    // Setup payment view
    let paymentTextField: STPPaymentCardTextField = STPPaymentCardTextField()
    paymentTextField.delegate = self

    // Apply Theming
    paymentTextField.cursorColor = UIColor.applicationMainColor()
    paymentTextField.backgroundColor = UIColor.lightWhite()
    paymentTextField.borderColor = UIColor.lightWhite()
    paymentTextField.font = UIFont.lightBMCFont(size: 20)
    paymentTextField.textColor = UIColor.darkGray()
    paymentTextField.textErrorColor = UIColor.orange()
    //

    self.paymentTextField = paymentTextField
    self.view!.addSubview(paymentTextField)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    displayDisclaimer()
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    let padding: CGFloat = 15
    let width: CGFloat = CGRectGetWidth(self.view.frame) - (padding * 2)
    self.paymentTextField.frame = CGRectMake(padding, padding, width, 44)
  }

  func displayFailToAddCardAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToAddCardTitle,
      message: viewModel.alertViewFailToAddCardMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewFailToAddCardActionButtonTitle,
      style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayDisclaimer() {
    let alertController = UIAlertController(
      title: viewModel.alertViewDisclaimerActionButtonTitle,
      message: viewModel.alertViewDisclaimerActionButtonMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(
      title: viewModel.alertViewDisclaimerActionButtonActionButtonTitle,
      style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

//MARK: A C T I O N S
extension AddCardViewController {

  func cancel(sender: AnyObject) {
    self.view.endEditing(true)
    
    delegate?.didCancelCardAddition()
    
    self.dismissViewControllerAnimated(true, completion: nil)
  }

  func save(sender: AnyObject) {
    self.view.endEditing(true)
    
    if !self.paymentTextField.valid {
      return
    }
    if (Stripe.defaultPublishableKey() == nil) {
      self.errorDeletingAlert(viewModel.alertViewErrorAddingAlertStipeSetupMessage)
      return
    }

    SwiftLoader.show(animated: true)
    STPAPIClient.sharedClient().createTokenWithCard(self.paymentTextField.cardParams) { (token: STPToken?, error: NSError?) -> Void in
      SwiftLoader.hide()
      if let token = token where error == nil {
        //TOKEN FETCHED FROM STRIPE
        //NOW Add to Customer throught BMC-API
        self.addCard(token)
      } else {
        self.errorDeletingAlert(self.viewModel.alertViewErrorAddingAlertMessage)
      }
    }
  }

  func errorDeletingAlert(message: String) {
    let alertController = UIAlertController(
      title: viewModel.alertViewErrorAddingCard,
      message: message,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewErrorAddingButtonTitle, style: UIAlertActionStyle.Default, handler: nil)

    alertController.addAction(okAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func addCard(token: STPToken){
    SwiftLoader.show(animated: true)
    self.viewModel.addCardRequest(token) { (success, error) -> Void in
      SwiftLoader.hide()
      if !success {
        self.trackEvent(self.viewModel.analyticsCategory.rawValue, action: "Failure Add Card", label: "", value: nil)
        self.displayFailToAddCardAlert()
        return
      }
      self.trackEvent(self.viewModel.analyticsCategory.rawValue, action: "Successful Add Card", label: "", value: nil)
      
      self.delegate?.didAddCard(token)
      NSNotificationCenter.defaultCenter().postNotificationName(
        BMCNotificationNames.DidAddNewCard.rawValue, object: nil)
      
      self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
  }
}

//MARK: STPPaymentCardTextField D e l e g a t e
extension AddCardViewController: STPPaymentCardTextFieldDelegate {
  func paymentCardTextFieldDidChange(textField: STPPaymentCardTextField) {
    self.navigationItem.rightBarButtonItem!.enabled = textField.valid;
  }
}
