//
//  RateBookingViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/28/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SwiftLoader

protocol RateBookingViewControllerDelegate {
  func rateBookingVCDidSubmitRating(rateBookingVC: RateBookingViewController, booking: Booking, success: Bool, error: NSError?)
  func rateBookingVCDidCancelRating(rateBookingVC: RateBookingViewController, booking: Booking)
}

class RateBookingViewController: UIViewController {
  
  //================================================================
  // Mark: - UI VARIABLE
  //================================================================
  
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var scrollViewContentView: UIView!
  @IBOutlet weak var vehicleInfoView: VehicleBasicInformationView!
  @IBOutlet weak var bookingDetailsView: UIView!
  @IBOutlet weak var dateLabel: BMCRatingDateTimeLabel!
  @IBOutlet weak var servicesLabel: BMCServiceTitleLabel!
  @IBOutlet weak var triangularView: UIView!
  @IBOutlet weak var ratingViewContainer: UIView!
  @IBOutlet weak var ratingTitle: BMCSecondaryTitleLabel!
  @IBOutlet weak var ratingControl: HCSStarRatingView!
  @IBOutlet weak var actionButtonsContainer: UIView!
  @IBOutlet weak var cancelButton: BMCOrangeButton!
  @IBOutlet weak var submitButton: BMCOrangeButton!
  
  @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
  
  //================================================================
  // Mark: VARIABLES
  //================================================================
  
  var viewModel: RateBookingViewModel!
  var delegate: RateBookingViewControllerDelegate?
  
  //================================================================
  // Mark: - LIFECYCLE
  //================================================================
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.sendScreenView("Rating")
    
    self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    
    contentView.backgroundColor = UIColor.veryLightGray()
    bookingDetailsView.backgroundColor = UIColor.veryLightGray()
    ratingViewContainer.backgroundColor = UIColor.whiteColor()
    actionButtonsContainer.backgroundColor = UIColor.orange()
    ratingControl.tintColor = UIColor.lightGray()
    servicesLabel.textColor = UIColor.darkGray()
    
    // Give curve to main View border
    contentView.makeCurvedEdges()
    
    // Triangulate View
    triangularView.triangularView(color: UIColor.darkPurple())
    
    // Init Labels
    ratingTitle.text = viewModel.ratingControlTitle
    
    // Init Vehicle Info View
    vehicleInfoView.selectionBackgroundColor = UIColor.darkPurple()
    vehicleInfoView.selectedState = true
    vehicleInfoView.compactView(compactSize)
    vehicleInfoView.vehicle = viewModel.vehicle
    
    // Init Date
    dateLabel.text = viewModel.bookingDate
    
    // Init Services Details
    servicesLabel.text = viewModel.bookingServices
    
    // For the labels sizes to take their final values
    self.view.layoutIfNeeded()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    // Set the scrollView height constraint Dynamically
    var scrollViewHeight = scrollViewContentHeight
    if scrollViewHeight > scrollViewMaximumHeight {
      scrollViewHeight = scrollViewMaximumHeight
    }
    scrollViewHeightConstraint.constant = scrollViewHeight
    self.view.layoutIfNeeded()
  }
  
  //================================================================
  // Mark: ACTIONS
  //================================================================
  
  @IBAction func didTapSubmitButton(sender: UIButton) {
    viewModel.selectedRating = Int(ratingControl.value)
    if isValidForSubmit() {
      SwiftLoader.show(animated: true)
      viewModel.rateService { (success, error) -> Void in
        SwiftLoader.hide()
        if !success {
          let alertController = UIAlertController(
            title: self.viewModel.alertViewFailToSubmitRatingTitle,
            message: self.viewModel.alertViewFailToSubmitRatingMessage,
            preferredStyle: UIAlertControllerStyle.Alert)
          let okAction = UIAlertAction(title: self.viewModel.alertViewFailToSubmitRatingActionButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
          alertController.addAction(okAction)
          self.presentViewController(alertController, animated: true, completion: nil)
          return
        }
        
        if self.delegate != nil {
          self.delegate?.rateBookingVCDidSubmitRating(self, booking: self.viewModel.booking, success: true, error: nil)
        } else {
          self.dismissViewControllerAnimated(true, completion: nil)
        }
      }
    }
  }
  
  @IBAction func didTapCancelButton(sender: UIButton) {
    if delegate != nil {
      self.delegate?.rateBookingVCDidCancelRating(self, booking: self.viewModel.booking)
    } else {
      self.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  //================================================================
  // Mark: HELPERS
  //================================================================
  
  func isValidForSubmit() -> Bool {
    if let selectedRating = viewModel.selectedRating
      where selectedRating > 0{
      return true
    }
    
    displayMustRateAlertView()
    
    return false
  }
  
  var scrollViewMaximumHeight: CGFloat {
    let verticalPadding: CGFloat = 40 // 20 from top and 20 from bottom
    let maximumContentViewHeight = self.view.frame.height - verticalPadding
    
    return  maximumContentViewHeight - actionButtonsContainer.frame.height - ratingViewContainer.frame.height
  }
  
  var scrollViewContentHeight: CGFloat {
    return scrollViewContentView.frame.height
  }
  
  //================================================================
  // Mark: ALERTS
  //================================================================
  
  private func displayMustRateAlertView() {
    let alertController = UIAlertController(
      title: viewModel.alertViewMustRateTitle,
      message: viewModel.alertViewMustRateMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let negativeAction = UIAlertAction(
      title: viewModel.alertViewMustRateActionButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    
    alertController.addAction(negativeAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}



//================================================================
// Mark: - PRESENTING THE VC
//================================================================

extension RateBookingViewController {
  class func showFromViewController(
    viewController: UIViewController,
    rateBookingViewModel: RateBookingViewModel,
    delegate: RateBookingViewControllerDelegate!) {
    
    guard let rateBookingVC = viewController.storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.RateBookingViewController.rawValue) as? RateBookingViewController else {
      return
    }
    
    rateBookingVC.viewModel = rateBookingViewModel
    rateBookingVC.delegate = delegate
    rateBookingVC.modalPresentationStyle = .OverCurrentContext
    rateBookingVC.modalTransitionStyle = .CrossDissolve
    viewController.presentViewController(rateBookingVC, animated: true, completion: nil)
    
    return
  }
}
