//
//  CustomerServiceViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/28/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import ORStackView
import SwiftLoader

class CustomerServiceViewController: UIViewController {
  private static var sideMargin: CGFloat = 60
  
  private var labelRequestType: BMCFormLabel!
  private var labelDescription: BMCFormLabel!
  private var textFieldDescription: BMCTextView!
  private var textFieldRequestType: PickerTextField!
  private var buttonSave: BMCOrangeButton!
  private var headerView: CustomerServiceHeaderView!
  
  @IBOutlet weak var scrollStackView: ORStackScrollView!
  
  @IBOutlet weak var constraintStackViewBottomToSuperviewBottom: NSLayoutConstraint!
  
  var viewModel: CustomerServiceViewModel = CustomerServiceViewModel()
  var headerViewInitialVisibilityStatus: Bool = false
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    initLayout()
    setupNavigationBarButtons()
    updateHeaderViewVisibility()
    
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(CustomerServiceViewController.keyboardWillShow(_:)),
      name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(CustomerServiceViewController.keyboardWillHide(_:)),
      name: UIKeyboardWillHideNotification, object: nil)
    
    self.view.addTapGesture(self)
  }
  
  private func updateHeaderViewVisibility() {
    headerViewInitialVisibilityStatus = self.viewModel.canCallNow()
    if headerViewInitialVisibilityStatus == false {
        self.removeHeaderViewToStackView()
        UIView.animateWithDuration(0.1, animations: { () -> Void in
          self.view.layoutIfNeeded()
        })
    }
  }
  
  private func setupNavigationBarButtons() {
    navigationItem.leftBarButtonItem = UINavigationBar.closeBarButton(self, selector: "didTapBackButton")
  }
  
  private func initLayout() {
    //add header
    headerView = UIView.loadFromNib(CustomerServiceHeaderView.nibName,
      owner: self) as! CustomerServiceHeaderView
    labelRequestType = BMCFormLabel()
    labelDescription = BMCFormLabel()
    textFieldDescription = BMCTextView()
    textFieldRequestType = PickerTextField()
    buttonSave = BMCOrangeButton()
    
    //Set delegates
    textFieldDescription.delegate = self
    textFieldRequestType.delegate = self
    headerView.delegate = self
    
    //Init Picker
    textFieldRequestType.viewModel = self.viewModel.requestTypeViewModel!
    textFieldRequestType.viewModel.setSelectedComponent(self.viewModel.data[0])
    
    //Set views height constriant
    textFieldDescription.constrainHeight("100")
    textFieldRequestType.constrainHeight("44")
    buttonSave.constrainHeight("44")
    
    //Set localized text Values
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Customer Support")
    labelRequestType.text = viewModel.requestTypeLabelText
    labelDescription.text = viewModel.descriptionLabelText
    buttonSave.setTitle(viewModel.saveButtonTitle, forState: .Normal)
    headerView.buttonCall.setTitle(viewModel.callNowButtonTitle, forState: .Normal)
    headerView.labelGiveUsACall.text = viewModel.giveUsACallLabelText
    
    //Non-Static values
    headerView.labelPhoneNumber.text = viewModel.phoneNumber
    headerView.labelCallingPeriod.text = viewModel.callingPeriod
    
    //set Keyboard return type
    textFieldDescription.returnKeyType = UIReturnKeyType.Default
    
    //Add Target
    buttonSave.userInteractionEnabled = true
    buttonSave.addTarget(self, action: #selector(CustomerServiceViewController.didTapSaveButton),
      forControlEvents: UIControlEvents.TouchUpInside)

    
    //Add Views to scroll stack view
    scrollStackView.stackView.addSubview(headerView, withPrecedingMargin: 0,
      sideMargin: 0)
    scrollStackView.stackView.addSubview(labelRequestType, withPrecedingMargin: 20,
      sideMargin: CustomerServiceViewController.sideMargin)
    scrollStackView.stackView.addSubview(textFieldRequestType, withPrecedingMargin: 10,
      sideMargin: CustomerServiceViewController.sideMargin)
    scrollStackView.stackView.addSubview(labelDescription, withPrecedingMargin: 20,
      sideMargin: CustomerServiceViewController.sideMargin)
    scrollStackView.stackView.addSubview(textFieldDescription, withPrecedingMargin: 10,
      sideMargin: CustomerServiceViewController.sideMargin)
    scrollStackView.stackView.addSubview(buttonSave, withPrecedingMargin: 30,
      sideMargin: CustomerServiceViewController.sideMargin)
    scrollStackView.stackView.addSubview(UIView(), withPrecedingMargin: 30,
      sideMargin: 0)
  }
  
  func didTapSaveButton() {
    viewModel.selectedSupportType = self.viewModel.valueForRequestType(textFieldRequestType.text!)
    self.viewModel.description = textFieldDescription.text
    if let desc = viewModel.description
      where desc != "" {
      SwiftLoader.show(animated: true)
      viewModel.sendCustomerRequest { (error) -> Void in
        var title = ""
        var message = ""
        var isError = false
        if let _ = error {
          title = self.viewModel.failAlertTitle
          message = self.viewModel.failAlertMessage
          isError = true
        } else {
          title = self.viewModel.successAlertTitle
          message = self.viewModel.successAlertMessage
        }
        let alertController = UIAlertController(
          title: title,
          message: message,
          preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
          if !isError {
            self.dismissViewControllerAnimated(true, completion: nil)
          }
        }
        
        alertController.addAction(okAction)
        
        SwiftLoader.hide()
        self.presentViewController(alertController, animated: true, completion: nil)
      }
    } else {
      submitEmptyDataErrorAlert()
    }
  }
  
  func didTapBackButton() {
    textFieldDescription.resignFirstResponder()
    textFieldRequestType.resignFirstResponder()
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func callUsNow() {
    let telephone = viewModel.phoneNumber
    if !telephone.isEmpty {
      if let url = NSURL(string: "tel://\(telephone)") {
        let alertController = UIAlertController(
          title: viewModel.callAlertTitle,
          message: viewModel.callAlertMessage,
          preferredStyle: UIAlertControllerStyle.Alert)

        let cancelAction = UIAlertAction(title: viewModel.callAlertNoButton,
          style: UIAlertActionStyle.Cancel, handler: nil)
        let okAction = UIAlertAction(title: viewModel.callAlertYesButton,
          style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            UIApplication.sharedApplication().openURL(url)
        }

        alertController.addAction(okAction)
        alertController.addAction(cancelAction)

        self.presentViewController(alertController, animated: true, completion: nil)
      }
    }
  }
}

//==========================================================================
// MARK: - N O T I F I C A T I O N S
//==========================================================================
extension CustomerServiceViewController {
  
  func keyboardWillShow(notification: NSNotification) {
    if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let frame = value.CGRectValue()
      layoutForVisibleKeyboard(frame.height)
    }
  }
  
  func keyboardWillHide(notification: NSNotification) {
    layoutForHiddenKeyboard()
  }
  
  func layoutForVisibleKeyboard(keyboardHeight: CGFloat) {
      self.constraintStackViewBottomToSuperviewBottom.constant = keyboardHeight
      UIView.animateWithDuration(0.5, animations: { () -> Void in
        self.removeViewsFromStackView()
        self.view.layoutIfNeeded()
      })
  }
  
  func layoutForHiddenKeyboard() {
      self.constraintStackViewBottomToSuperviewBottom.constant = 0
      UIView.animateWithDuration(0.5, animations: { () -> Void in
        self.addViewsFromStackView()
        self.view.layoutIfNeeded()
      })
  }
  
  private func removeViewsFromStackView() {
    self.removeHeaderViewToStackView()
    self.scrollStackView.stackView.removeSubview(self.buttonSave)
  }
  
  private func addViewsFromStackView() {
    self.addHeaderViewToStackView()
    self.scrollStackView.stackView.insertSubview(
      self.buttonSave, atIndex: 5, withPrecedingMargin: 30,
      sideMargin: CustomerServiceViewController.sideMargin)
  }
  
  private func addHeaderViewToStackView() {
    if let _ = self.headerView.superview {
      return
    }
    //Do not add the header view incase it was removed on view did load
    if headerViewInitialVisibilityStatus {
      self.scrollStackView.stackView.insertSubview(
        self.headerView, atIndex: 0, withPrecedingMargin: 0,
        sideMargin: 0)
    }
  }
  
  private func removeHeaderViewToStackView() {
    if let _ = self.headerView.superview {
      self.scrollStackView.stackView.removeSubview(self.headerView)
    }
  }
}

//======================================================
// MARK: - H E L P E R S
//======================================================
extension CustomerServiceViewController {
  func dismissKeyboard() {
    textFieldRequestType.resignFirstResponder()
    textFieldDescription.resignFirstResponder()
  }
}

//======================================================
// MARK: - UITextFieldDelegate
//======================================================
extension CustomerServiceViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if textField.text == " " {
      textField.text = ""
    }
    return true
  }
  
  func textFieldDidEndEditing(textField: UITextField) {
    self.dismissKeyboard()
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    self.dismissKeyboard()
    return true
  }
}
//======================================================
// MARK: - UITextViewDelegate
//======================================================
extension CustomerServiceViewController: UITextViewDelegate {
  func textViewShouldBeginEditing(textField: UITextView) -> Bool {
    self.addKeyboardToolBarDone(textField)
    if textField.text == " " {
      textField.text = ""
    }
    return true
  }
  
  func textViewDidEndEditing(textField: UITextView) {
    self.dismissKeyboard()
  }
  
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    return true
  }
  
  func textViewShouldReturn(textField: UITextField) -> Bool {
    self.dismissKeyboard()
    return true
  }
  
  func addKeyboardToolBarDone(textField: UITextView) {
    // Create a button bar for the number pad
    let keyboardDoneButtonView = UIToolbar()
    keyboardDoneButtonView.sizeToFit()
    
    // Setup the buttons to be put in the system.
    let item = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(CustomerServiceViewController.endEditingNow) )
    let spacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
    let toolbarButtons = [spacer,item]
    
    //Put the buttons into the ToolBar and display the tool bar
    keyboardDoneButtonView.setItems(toolbarButtons, animated: false)
    textField.inputAccessoryView = keyboardDoneButtonView
  }
  
  func endEditingNow() {
    self.dismissKeyboard()
  }
}
//======================================================
// MARK: - CustomerServiceHeaderViewDelegate
//======================================================
extension CustomerServiceViewController: CustomerServiceHeaderViewDelegate {
  func didTapCallButton() {
    if self.viewModel.canCallNow() {
        self.callUsNow()
    }
  }
}

//======================================================
// MARK: - ViewTapDelegate
//======================================================
extension CustomerServiceViewController: ViewTapDelegate {
  func didTapToCancel() {
    dismissKeyboard()
  }
}

//MARK: A L E R T S
extension CustomerServiceViewController {
  func submitEmptyDataErrorAlert() {
    let alertController = UIAlertController(
      title: viewModel.cantSubmitAlertTitle,
      message: viewModel.cantSubmitAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.cantSubmitAlertOkButton ,
      style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

