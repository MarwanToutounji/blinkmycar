//
//  PaymentListingViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 4/12/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import Stripe
import SwiftLoader

protocol PaymentListingViewControllerDelegate {
  func paymentListingDidSelectPaymentTool(viewController: PaymentListingViewController)
  func paymentListingDidAddANewPaymentTool(viewController: PaymentListingViewController)
  func paymentListingDidCancelAddingANewPaymentTool(viewController: PaymentListingViewController)
}

class PaymentListingViewController: UIViewController, Dismissable {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var addCardButtonFooterView: UIView!
  @IBOutlet weak var addCardButtonLabel: BMCAddressHeaderLabel!

  var viewModel: PaymentViewModel = PaymentViewModel()
  
  var delegate: PaymentListingViewControllerDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
    initView()
    applyTheme()
    reloadData()
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  func initView() {
    title = viewModel.paymentListingViewControllerTitle
    self.sendScreenView("Payment")
    
    // To remove the back wording
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    
    // Add the X button on the left
    if navigationItem.leftBarButtonItem == nil {
      navigationItem.leftBarButtonItem = UINavigationBar.closeBarButton(self, selector: "didTapCancel")
    }
    
    // Add Notifications
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(PaymentListingViewController.reloadData),
      name: BMCNotificationNames.DidAddNewCard.rawValue,
      object: nil)
  }
  
  func applyTheme() {
    addCardButtonFooterView.backgroundColor = UIColor.orange()
    addCardButtonLabel.text = viewModel.addPaymentButtonTitle 
  }
  
  func reloadData() {
    SwiftLoader.show(animated: true)
    viewModel.reloadData { (success, error) -> Void in
      SwiftLoader.hide()
      
      self.tableView.reloadData()
      if !success {
        self.displayErrorLoadingPayments()
      }
    }
  }
  
  func dismiss(sender: AnyObject?) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func deleteConfirmationAlert(indexPath: NSIndexPath) {
    let alertController = UIAlertController(
      title: viewModel.alertViewDeleteConfirmationTitle,
      message: viewModel.alertViewDeleteConfirmationMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.alertViewDeleteConfirmationNegativeButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(title: viewModel.alertViewDeleteConfirmationPostiveButtonTitle, style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
      self.deleteAction(indexPath)
    }
    
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayErrorLoadingPayments() {
    let alertController = UIAlertController(
      title: viewModel.alertViewErrorLoadingPaymentsTitle,
      message: viewModel.alertViewErrorLoadingPaymentsMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.alertViewErrorLoadingPaymentsNegativeActionButtonTitle, style: UIAlertActionStyle.Default) {
      (_) -> Void in
      self.didTapCancel()
    }
    let retryAction = UIAlertAction(title: viewModel.alertViewErrorLoadingPaymentsPositiveActionButtonTitle, style: UIAlertActionStyle.Default) {
      (_) -> Void in
      self.reloadData()
    }
    
    alertController.addAction(cancelAction)
    alertController.addAction(retryAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func cantDeleteLastCardAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewCantDeleteLastCardTitle,
      message: viewModel.alertViewCantDeleteLastCardMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewCantDeleteLastCardButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func cantDeleteDefaultCardAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewCantDeleteLastCardTitle,
      message: viewModel.alertViewCantDeleteDefaultCardMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewCantDeleteLastCardButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayFailToChangeDefaultCardAlert() {
    let alertController = UIAlertController(
      title: viewModel.titleForFailToChangeDefaultCardAlert,
      message: viewModel.messageForFailToChangeDefaultCardAlert,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(
      title: viewModel.actionButtonTitleForFailToChangeDefaultCardAlert,
      style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  
  func errorDeletingAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewErrorDeletingTitle,
      message: viewModel.alertViewErrorDeletingMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewErrorDeletingButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}


//==============================================================================
// MARK: D E L E G A T E - UITableViewDataSource, UITableViewDelegate
//==============================================================================

extension PaymentListingViewController: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSections()
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsForSection(section)
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: PaymentListingTableViewCell = tableView.dequeueReusableCellWithIdentifier(
      PaymentListingTableViewCell.identifier,
      forIndexPath: indexPath) as! PaymentListingTableViewCell
    let tool = viewModel.paymentToolForIndexPath(indexPath)
    if let tool = tool {
      cell.descriptionLabel.text = tool.displayedText
      if tool is CashPayment {
        cell.cashToolImageView.image = tool.image
      } else {
        cell.paymentToolImageView.image = tool.image
      }
      let isDefault = viewModel.isPaymentToolAtIndexPathDefaultTool(indexPath)
      cell.defaultLabel.underlinedString((isDefault) ? viewModel.defualtLabelText : "")
      cell.checkBox.setCheckState((isDefault ? .Checked : .Unchecked), animated: true)
    }
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if tableView.cellForRowAtIndexPath(indexPath) is PaymentListingTableViewCell {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
      
      if let paymentTool = viewModel.paymentToolForIndexPath(indexPath) where !viewModel.isPaymentToolAtIndexPathDefaultTool(indexPath) {
        // Unselect Current Payment Toll
        
        SwiftLoader.show(animated: true)
        self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Payment Selection", label: "\(paymentTool.method.name.capitalizedString)", value: nil)
        viewModel.setAsDefaultPaymentToolAtIndexPath(
          indexPath,
          completionBlock: { (success, error) in
            SwiftLoader.hide()
            
            if success {
              self.tableView.reloadData()
              self.delegate?.paymentListingDidSelectPaymentTool(self)
            } else {
              self.displayFailToChangeDefaultCardAlert()
            }
        })
      }
    }
  }
  
  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return viewModel.canDeletePaymentToolAtIndexPath(indexPath)
  }
  
  func tableView(tableView: UITableView,
    commitEditingStyle editingStyle: UITableViewCellEditingStyle,
    forRowAtIndexPath indexPath: NSIndexPath) {
      if editingStyle != UITableViewCellEditingStyle.Delete {
        return
      }
    
      if viewModel.canCommitDeletingPaymentToolAtIndexPath(indexPath){
        // Can delete Credit Card
        self.deleteConfirmationAlert(indexPath)
      } else {
        //Cant delete default
        self.cantDeleteDefaultCardAlert()
      }
      
      tableView.setEditing(false, animated: true)
  }
  
}


//==============================================================================
//MARK: A C T I O N S
//==============================================================================

extension PaymentListingViewController {

  @IBAction func didTapAddButton(sender: UITapGestureRecognizer) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Add Payment Tap", label: "", value: nil)
    
    let vc = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.AddCardViewController.rawValue)
      as! AddCardViewController
    vc.viewModel.analyticsCategory = viewModel.analyticsCategory
    vc.delegate = self
    self.presentViewController(
      UINavigationController(rootViewController: vc), animated: true,
      completion: nil)
  }
  
  func setPaymentToolAtIndexPathAsDefault(indexPath: NSIndexPath) {
      SwiftLoader.show(animated: true)
    viewModel.setAsDefaultPaymentToolAtIndexPath(
      indexPath) { (success, error) in
        SwiftLoader.hide()
        
        if success {
          self.tableView.reloadData()
        }
    }
  }
  
  func deleteAction(indexPath: NSIndexPath) {
    SwiftLoader.show(animated: true)
    
    viewModel.deleteCardForIndexPath(
      indexPath) { (success, error) in
        SwiftLoader.hide()
        if !success {
          self.errorDeletingAlert()
          return
        }
        
        self.tableView.beginUpdates()
        self.tableView.deleteRowsAtIndexPaths(
          [indexPath],
          withRowAnimation: UITableViewRowAnimation.Automatic)
        self.tableView.endUpdates()
    }
  }

  func didTapCancel() {
    stopEditing()
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }

  func didTapDoneButton() {
    stopEditing()
  }

  func stopEditing() {
    if tableView.editing {
      tableView.beginUpdates()
      tableView.editing = false
      navigationItem.rightBarButtonItem = UIBarButtonItem(
        barButtonSystemItem: UIBarButtonSystemItem.Edit,
        target: self,
        action: #selector(PaymentListingViewController.goToEditMode))
      tableView.endUpdates()
    }
  }

  func goToEditMode() {
    if !tableView.editing {
      let doneButton = UIBarButtonItem(barButtonSystemItem: .Done,
        target: self, action: #selector(PaymentListingViewController.didTapDoneButton))
      self.navigationItem.rightBarButtonItem = doneButton
      tableView.beginUpdates()
      tableView.editing = true
      tableView.endUpdates()
    }
  }
}


//==============================================================================
//MARK: D E L E G A T E - AddCardViewControllerDelegate
//==============================================================================

extension PaymentListingViewController: AddCardViewControllerDelegate {
  func didAddCard(token: STPToken) {
    viewModel.reloadData { (success, error) in
      self.delegate?.paymentListingDidAddANewPaymentTool(self)
      return
    }
  }
  
  func didCancelCardAddition() {
    self.delegate?.paymentListingDidCancelAddingANewPaymentTool(self)
  }
}
