//
//  SignInViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/8/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import ORStackView
import SwiftLoader

class SignInViewController: UIViewController, ValidationTextFieldDelegate {
  private static var sideMargin: CGFloat = 60

  private var labelEmail: BMCFormLabel!
  private var labelPassword: BMCFormLabel!
  private var labelInvalidCredentialsError: BMCErrorLabel!
  private var textFieldEmail: ValidationTextField!
  private var textFieldPassword: ValidationTextField!
  private var buttonSignIn: BMCOrangeButton!
  private var buttonForgotPassword: BMCLinkButton!

  @IBOutlet weak var scrollStackView: ORStackScrollView!
  @IBOutlet weak var registrationLabel: BMCLabel!
  @IBOutlet weak var registrationButton: BMCLinkButton!

  @IBOutlet weak var constraintStackViewBottomToRegistrationLabelTop: NSLayoutConstraint!
  @IBOutlet weak var constraintStackViewBottomToSuperviewBottom: NSLayoutConstraint!

  var viewModel: SignInViewModel

  init() {
    viewModel = SignInViewModel()
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    viewModel = SignInViewModel()
    super.init(coder: aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Signin")
    
    registrationButton.textColor = UIColor.lightGray()
    
    initBarItems()
    initLayout()

    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(SignInViewController.keyboardWillShow(_:)),
      name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(SignInViewController.keyboardWillHide(_:)),
      name: UIKeyboardWillHideNotification, object: nil)
    
    self.view.addTapGesture(self)
  }

  func initBarItems() {
    let cancelButton = UIBarButtonItem(barButtonSystemItem: .Cancel,
      target: self,
      action: #selector(SignInViewController.didTapOnCancelButton(_:)))
    self.navigationItem.leftBarButtonItem = cancelButton
  }

  private func initLayout() {
    labelEmail = BMCFormLabel()
    labelPassword = BMCFormLabel()

    textFieldEmail = UIView.loadFromNib(ValidationTextField.nibName,
      owner: self) as! ValidationTextField
    textFieldPassword = UIView.loadFromNib(ValidationTextField.nibName,
      owner: self) as! ValidationTextField

    textFieldEmail.text = viewModel.email ?? " "
    
    buttonSignIn = BMCOrangeButton()
    buttonSignIn.addTarget(self,
      action: #selector(SignInViewController.didTapSignInButton(_:)),
      forControlEvents: UIControlEvents.TouchUpInside)
    
    buttonForgotPassword = BMCLinkButton()
    buttonForgotPassword.textColor = UIColor.lightGray()
    buttonForgotPassword.addTarget(self,
      action: #selector(SignInViewController.didTapForgotPasswordButton(_:)),
      forControlEvents: UIControlEvents.TouchUpInside)
    buttonForgotPassword.setTitle(viewModel.buttonForgotPasswordTitle, forState: .Normal)
    buttonForgotPassword.constrainHeight("44")

    textFieldEmail.keyboardType = UIKeyboardType.EmailAddress

    labelEmail.text = viewModel.labelEmailText
    labelPassword.text = viewModel.labelPasswordText
    textFieldEmail.errorMessage = viewModel.textFieldEmailErrorMessage
    textFieldPassword.errorMessage = viewModel.textFieldPasswordErrorMessage
    textFieldPassword.secureTextEntry = true
    buttonSignIn.setTitle(viewModel.buttonSignInTitle, forState: .Normal)

    registrationLabel.text = viewModel.registerLabelText
    registrationButton.setTitle(viewModel.registerButtonText, forState: .Normal)

    labelInvalidCredentialsError = BMCErrorLabel()
    labelInvalidCredentialsError.text = viewModel.invalidCredentialsErrorMessage
    labelInvalidCredentialsError.textAlignment = NSTextAlignment.Center

    scrollStackView.stackView.addSubview(labelEmail, withPrecedingMargin: 30,
      sideMargin: SignInViewController.sideMargin)
    scrollStackView.stackView.addSubview(textFieldEmail, withPrecedingMargin: 10,
      sideMargin: SignInViewController.sideMargin)
    scrollStackView.stackView.addSubview(labelPassword, withPrecedingMargin: 20,
      sideMargin: SignInViewController.sideMargin)
    scrollStackView.stackView.addSubview(textFieldPassword, withPrecedingMargin: 10,
      sideMargin: SignInViewController.sideMargin)
    scrollStackView.stackView.addSubview(buttonSignIn, withPrecedingMargin: 30,
      sideMargin: SignInViewController.sideMargin)
    scrollStackView.stackView.addSubview(buttonForgotPassword, withPrecedingMargin: 10,
      sideMargin: SignInViewController.sideMargin)
    scrollStackView.stackView.addSubview(UIView(), withPrecedingMargin: 30,
      sideMargin: 0)

    buttonSignIn.constrainHeight("44")

    textFieldEmail.validationTextFieldDelegate = self
    textFieldPassword.validationTextFieldDelegate = self
    textFieldEmail.validationMethod = viewModel.validateEmail
    textFieldPassword.validationMethod = viewModel.validatePassword
    textFieldEmail.returnKeyType = UIReturnKeyType.Next
    textFieldPassword.returnKeyType = UIReturnKeyType.Done
  }

  //=================================================================
  // MARK: - N O T I F I C A T I O N S
  //=================================================================

  func keyboardWillShow(notification: NSNotification) {
    if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let frame = value.CGRectValue()
      constraintStackViewBottomToSuperviewBottom.constant = -frame.height
    }

    layoutForVisibleKeyboard()
  }

  func keyboardWillHide(notification: NSNotification) {
    layoutForHiddenKeyboard()
  }

  func layoutForVisibleKeyboard() {
    self.view.removeConstraint(constraintStackViewBottomToRegistrationLabelTop)
    self.view.addConstraint(constraintStackViewBottomToSuperviewBottom)
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
      if DeviceType.IS_IPHONE_5_OR_LESS {
        self.scrollStackView.stackView.removeSubview(self.buttonSignIn)
      }
    })
  }

  func layoutForHiddenKeyboard() {
    self.view.removeConstraint(constraintStackViewBottomToSuperviewBottom)
    self.view.addConstraint(constraintStackViewBottomToRegistrationLabelTop)
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
      if DeviceType.IS_IPHONE_5_OR_LESS {
        self.scrollStackView.stackView.insertSubview(self.buttonSignIn, atIndex: 4,
          withPrecedingMargin: 30, sideMargin: SignInViewController.sideMargin)
      }
    })
  }

  //==========================================================================
  // MARK: - V A L I D A T I O N   T E X T   F I E L D   D E L E G A T E
  //==========================================================================

  func validationTextFieldDidBeginEditing(textField: UITextField) {
    removeSignInAlertView()
  }

  func validationTextFieldDidEndEditing(textField: UITextField) {
  }

  func validationTextFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case textFieldEmail.textField:
      textFieldPassword.becomeFirstResponder()
    case textFieldPassword.textField:
      textField.resignFirstResponder()
    default:
      break
    }
    return true
  }

  func validationTextFieldShouldLayoutIfNeeded(animationBlock: (() -> Void)) {
    UIView.animateWithDuration(0.5,
      animations: {() -> Void in
        self.view.layoutIfNeeded()
        animationBlock()
    })
  }

  //=================================================================
  // MARK: A C T I O N S
  //=================================================================

  func didTapOnCancelButton(sender: AnyObject?) {
    dismissKeyboard()
    self.parentViewController?.dismissViewControllerAnimated(true,
      completion: nil)
  }

  func didTapSignInButton(sender: AnyObject?) {
    // Handle SignIn
    dismissKeyboard()

    if validateFields() {
      SwiftLoader.show(animated: true)
      viewModel.loginUser(textFieldEmail.text!, password: textFieldPassword.text!,
        completionBlock: { (error: NSError?) -> Void in
          SwiftLoader.hide()
          if let _ = error {
            self.showSignInErrorAlert()
            return
          }
          self.trackEvent(AnalyticsCategories.Signin.rawValue, action: "Successful Signin", label: "", value: nil)
          NSNotificationCenter.defaultCenter().postNotificationName(
            BMCNotificationNames.LoginSuccess.rawValue, object: nil)
      })
    } else {
      showFieldsInvalidationAlert()
    }
  }

  func didTapForgotPasswordButton(sender: AnyObject?) {
    self.trackEvent(AnalyticsCategories.Signin.rawValue, action: "Forgot Password Tap", label: "", value: nil)
    dismissKeyboard()
    //TODO: remove static content and handle diff between dev and prod urls
    if let forgotPasswordURL = NSURL(string: "https://users.blinkmycar.com/user/forgot-password") {
      openUrl(forgotPasswordURL)
    }
  }
  
  @IBAction func didTapRegisterButton(sender: AnyObject?) {
    self.trackEvent(AnalyticsCategories.Signin.rawValue, action: "Register Tap", label: "", value: nil)
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidTapRegisterFromSignInVC.rawValue,
      object: nil)
  }

  //=================================================================
  // MARK: - H E L P E R S
  //=================================================================

  func openUrl(url: NSURL) {
    UIApplication.sharedApplication().openURL(url)
  }
  
  func dismissKeyboard() {
    textFieldEmail.resignFirstResponder()
    textFieldPassword.resignFirstResponder()
  }

  func validateFields() -> Bool {
    let isEmailValid = textFieldEmail.validate()
    let isPasswordValid = textFieldPassword.validate()
    if isEmailValid
      && isPasswordValid{
        return true
    }
    return false
  }

  func showFieldsInvalidationAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewTitle,
      message: viewModel.alertViewInvalidEmailAndOrPassword,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewDismissActionButtonText,
      style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)

    // Tint the action buttons
    alertController.view.tintColor = UIColor.applicationMainColor()

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func showSignInErrorAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewTitle,
      message: viewModel.alertViewServerErrorMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewDismissActionButtonText,
      style: UIAlertActionStyle.Default, handler: nil)
    alertController.addAction(okAction)

    // Tint the action buttons
    alertController.view.tintColor = UIColor.applicationMainColor()

    self.presentViewController(alertController, animated: true, completion: nil)

    addSignInAlertView()
  }

  private func addSignInAlertView() {
    self.scrollStackView.stackView.insertSubview(self.labelInvalidCredentialsError, atIndex: 4,
      withPrecedingMargin: 35, sideMargin: SignInViewController.sideMargin)
  }
  
  private func removeSignInAlertView() {
    self.scrollStackView.stackView.removeSubview(self.labelInvalidCredentialsError)
  }
}

//======================================================
// MARK: - ViewTapDelegate
//======================================================
extension SignInViewController: ViewTapDelegate {
  func didTapToCancel() {
    dismissKeyboard()
  }
}
