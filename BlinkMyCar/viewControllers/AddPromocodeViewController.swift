//
//  AddPromocodeViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/30/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import SwiftLoader

protocol AddPromocodeViewControllerDelegate {
  /**
   NOTE: !!!!!!
   Sending a completion block that triggers an asyncronous call to get the total amount to be charged from the server can lead to zombie object in case this view controller or the delegate are dead for a sudden reason (a phone call, needs memory), or if the UX doesn't block the user during this operation. For now this is implemented this way for experimentation. the completion block can be replaced by notification calls later on ;)
   */
  
  func addPromocodeDidTapApplyButton(viewController: AddPromocodeViewController, promocode: String, completionBlock: (success: Bool, error: NSError?) -> Void)
  func addPromocodeDidTapCancel(viewController: AddPromocodeViewController)
  func addPromocodeRemovePromocode()
}

class AddPromocodeViewController: UIViewController {
  
  @IBOutlet weak var label: BMCLabelForSmallSectionView!
  @IBOutlet weak var textField: BMCAddressTextField!
  @IBOutlet weak var actionButton: BMCLinkButton!
  
  var delegate: AddPromocodeViewControllerDelegate?
  
  var viewModel: AddPromocodeViewModel = AddPromocodeViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.viewControllerTitle
    self.sendScreenView("Add Promo Code")
    
    label.text = viewModel.labelText
    actionButton.setTitle(viewModel.actionButtonTitle, forState: .Normal)
    
    textField.delegate = self
    
    textField.text = viewModel.preSelectedPromocode
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    textField.becomeFirstResponder()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    textField.resignFirstResponder()
  }
  
  @IBAction func actionButtonTapped(sender: AnyObject) {
    textField.resignFirstResponder()
    if let promocode = textField.text?.trim() where !promocode.isEmpty {
      SwiftLoader.show(animated: true)
      delegate?.addPromocodeDidTapApplyButton(self, promocode: promocode) { (success, error) in
        SwiftLoader.hide()
        if let error = error where !success {
          self.displayAlertView(message: error.userInfo["error"] as? String)
          return
        }
        
        if success {
          self.dismissViewControllerAnimated(true, completion: nil)
        }
      }
    } else {
      delegate?.addPromocodeRemovePromocode()
      self.dismissViewControllerAnimated(true, completion: nil)
//      displayEmptyPromocodeAlert()
    }
  }
  
  func displayEmptyPromocodeAlert() {
    let alertController = UIAlertController(
      title: viewModel.emptyPromocodeAlertViewTitle,
      message: viewModel.emptyPromocodeAlertViewMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(
      title: viewModel.emptyPromocodeAlertViewActionButtonTitle,
      style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayAlertView(title: String? = nil, message: String? = nil) {
    let title: String = title ?? "Ooops"
    let message: String = message ?? ""
    
    let alertController = UIAlertController(
      title: NSLocalizedString(title, value: title, comment: "Promocode error title"),
      message: NSLocalizedString(message, value: message, comment: "Promocode error message"),
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(
      title: viewModel.alertMessageOkActionButtonTitle,
      style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

extension AddPromocodeViewController: Dismissable {
  func dismiss(sender: AnyObject?) {
    self.dismissViewControllerAnimated(true, completion: nil)
    delegate?.addPromocodeDidTapCancel(self)
  }
}

extension AddPromocodeViewController: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}


