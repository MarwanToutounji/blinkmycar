//
//  WebViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/14/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class WebViewController: UIViewController {
  @IBOutlet weak var webView: UIWebView!
  
  var viewModel: WebViewModel = WebViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()

    title = viewModel.title
    self.sendScreenView(title ?? "Web View")
    
    let requestURL = NSURL(string: viewModel.url ?? "")
    let request = NSURLRequest(URL: requestURL!)
    webView.loadRequest(request)
    
    navigationItem.leftBarButtonItem = UINavigationBar.closeBarButton(self, selector: "didTapCancelButton")
  }
  
  func didTapCancelButton() {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
}
