//
//  VehicleListingViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/6/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class VehicleListingViewController: UIViewController {
  private var viewModel = VehiclesMenuViewModel()
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var addVehicleButton: BMCOrangeButton!

  private var isEditingTable: Bool = false
  
  private var areAllTableViewCellsInEditMode: Bool = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    initNavigationItems()
    applyContent()
    
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(VehicleListingViewController.reloadView),
      name: BMCNotificationNames.DidUpdateVehicleListing.rawValue,
      object: nil)
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  func applyContent() {
    addVehicleButton.setTitle(viewModel.addVehicleButtonText, forState: UIControlState.Normal)
  }
  
  func initNavigationItems() {
    title = viewModel.vehicleListingViewControllerTitle
    self.sendScreenView("My Vehicles")
    
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if viewModel.vehicles.count > 0 {
      //Disable temporary
      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: self, action: #selector(VehicleListingViewController.goToEditMode))
    } else {
      navigationItem.rightBarButtonItem = nil
    }
  }
  
  func reloadView() {
    if !isEditingTable {
      self.viewModel.loadData()
      tableView.reloadData()
    }
  }
}

//MARK: A C T I O N S
extension VehicleListingViewController {
  
  @IBAction func addVehicleTouchUpInside(sender: AnyObject) {
    createRideProcess()
  }
  
  func createRideProcess() {
    let chooseRideViewController = self.storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.ChooseYourRideViewController.rawValue) as! ChooseYourRideViewController
    
    let navigationController = UINavigationController(rootViewController: chooseRideViewController)
    chooseRideViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: #selector(VehicleListingViewController.cancelModelViewController))
    self.presentViewController(navigationController, animated: true, completion: nil)
  }
  
  func cancelModelViewController() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func didTapCancel() {
      stopEditing()
      self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func didTapDoneButton() {
    stopEditing()
  }
  
  func stopEditing() {
    areAllTableViewCellsInEditMode = false
    tableView.beginUpdates()
    tableView.editing = false
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: self, action: #selector(VehicleListingViewController.goToEditMode))
    tableView.endUpdates()
  }
  
  func goToEditMode() {
    areAllTableViewCellsInEditMode = true
    let doneButton = UIBarButtonItem(barButtonSystemItem: .Done,
                                     target: self, action: #selector(VehicleListingViewController.didTapDoneButton))
    self.navigationItem.rightBarButtonItem = doneButton
    tableView.beginUpdates()
    tableView.editing = true
    tableView.endUpdates()
  }
  
  func deleteConfirmationAlert(indexPath: NSIndexPath) {
    let alertController = UIAlertController(
      title: viewModel.alertViewDeleteConfirmationTitle,
      message: viewModel.alertViewDeleteConfirmationMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.alertViewDeleteConfirmationNegativeButtonTitle,
      style: UIAlertActionStyle.Cancel, handler: nil)
    let okAction = UIAlertAction(title: viewModel.alertViewDeleteConfirmationPostiveButtonTitle, style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
      self.deleteAction(indexPath)
    }
    
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func cantDeleteLastVehicleAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewCantDeleteLastVehicleTitle,
      message: viewModel.alertViewCantDeleteLastVehicleMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewCantDeleteLastVehicleButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func cantDeleteVehicleWithBookingsAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewCantDeleteTitle,
      message: viewModel.alertViewCantDeleteMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewCantDeleteButtonTitle, style: UIAlertActionStyle.Default, handler: nil)

    alertController.addAction(okAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func errorDeletingAlert(message: String) {
    let alertController = UIAlertController(
      title: viewModel.alertViewErrorDeletingTitle,
      message: message,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewErrorDeletingButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func deleteAction(indexPath: NSIndexPath) {
    SwiftLoader.show(animated: true)
    isEditingTable = true
    viewModel.deleteVehicleForRow(indexPath.row, completionBlock:
      { (success: Bool, error: NSError?) -> Void in
        SwiftLoader.hide()
        if !success {
          var errorMsg: String = self.viewModel.deleteVehicleErrorMessage
          if let error = error {
            errorMsg = error.userInfo["error"] as! String
          }
          self.isEditingTable = false
          self.errorDeletingAlert(errorMsg)
          return
        }

        
        self.tableView.beginUpdates()
        self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        self.isEditingTable = false
        self.tableView.endUpdates()
    })
  }
}

extension VehicleListingViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows()
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: CarListingTableViewCell = tableView.dequeueReusableCellWithIdentifier(CarListingTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! CarListingTableViewCell
    cell.vehicleInfoView.vehicle = viewModel.cellForRow(indexPath)
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let vehicle = viewModel.cellForRow(indexPath)
    _ = tableView.cellForRowAtIndexPath(indexPath) as? CarListingTableViewCell
    
    let vc = storyboard?.instantiateViewControllerWithIdentifier(
    ViewControllerStoryboardIdentifier.VehicleProfileViewController.rawValue)
    as! VehicleProfileViewController
    vc.viewModel.vehicle = vehicle
    self.navigationController?.pushViewController(vc, animated: true)
    
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
  
  func tableView(tableView: UITableView,
    commitEditingStyle editingStyle: UITableViewCellEditingStyle,
    forRowAtIndexPath indexPath: NSIndexPath) {
      if editingStyle != UITableViewCellEditingStyle.Delete {
        return
      }
      if viewModel.canDeleteVehicleAtIndex(indexPath.row) {
        self.deleteConfirmationAlert(indexPath)
      }
      else {
        if areAllTableViewCellsInEditMode {
          tableView.setEditing(false, animated: true)
          dispatch_async(dispatch_get_main_queue(), { 
            tableView.setEditing(true, animated: true)
          })
        } else {
          tableView.setEditing(false, animated: true)
        }
        
        if viewModel.numberOfRows() <= 1 {
          // Trying to delete the last vehicle
          self.cantDeleteLastVehicleAlert()
        } else {
          // Trying to delete a vehicle that has unfinished bookings
          self.cantDeleteVehicleWithBookingsAlert()
        }
      }
  }

}
