//
//  WashInProgressViewController.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class WashInProgressViewController: UIViewController {

  @IBOutlet weak var waterAnimationView: WaterFillView!
  @IBOutlet weak var waterAmountLabel: BMCBoldTitleLabel!
  @IBOutlet weak var descriptionLabel: BMCAnimationInfoLabel!


  @IBOutlet weak var washerActionDescriptionLabel: UILabel!
  @IBOutlet weak var carNameLabel: UILabel!

  public var viewModel = WashInProgressViewModel()

  public override func viewDidLoad() {
    super.viewDidLoad()
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Wash In Progress View")

    waterAnimationView.delegate = self

    waterAnimationView.waterColor = UIColor.bmcGreen()
    waterAnimationView.startDate = viewModel.washIsCompleted ? NSDate().timeIntervalSince1970 : viewModel.booking.washStartDate().timeIntervalSince1970
    waterAnimationView.slotDuration = viewModel.washIsCompleted ? 0.0005 : CGFloat(GeneralSettings().bookingSlotDuration)
    waterAnimationView.amplitude = 15.0
    waterAnimationView.frequency = 0.025
    waterAnimationView.verticalSpeed = 0.25
    waterAnimationView.horizontalSpeed = viewModel.washIsCompleted ? 0.2 : 0.1
    waterAnimationView.startingFrom = 0.5
    waterAnimationView.startAnimation()

    waterAmountLabel.textColor = UIColor.lightGray()
    waterAmountLabel.font = UIFont.boldBMCFont(size: 42)

    descriptionLabel.text = viewModel.descriptionText

    washerActionDescriptionLabel.text = viewModel.washerActionDescription
    carNameLabel.text = viewModel.vehicleDisplayInformation
  }

  private func displayTheRatingVC() {
//    let rateVC = storyboard!.instantiateViewControllerWithIdentifier(
//      ViewControllerStoryboardIdentifier.RatingViewController.rawValue) as! RatingViewController
//    rateVC.viewModel.booking = viewModel.booking
//    let navigationVC = UINavigationController(rootViewController: rateVC)
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1500 * NSEC_PER_MSEC)), dispatch_get_main_queue(), { () -> Void in
//      self.presentViewController(navigationVC, animated: true, completion: nil)
//    })
  }
}

extension WashInProgressViewController: WaterFillViewDelegate {
  public func waterFillViewDidUpdate(percentage: CGFloat) {
    waterAmountLabel.text = "\(Int(percentage * 250))"

    if percentage == 1 && viewModel.washIsCompleted {
      if viewModel.washIsCompleted {
        displayTheRatingVC()
      } else {
        PersistenceManager.sharedManager.loadBookings({
          (success: Bool, error: NSError?) -> Void in
          if success {
            if let newBooking = Booking.bookingById(self.viewModel.booking.bookingId!)
            where newBooking.status == BookingStatus.Completed {
              self.displayTheRatingVC()
            }
          }
        })
      }
    } else if percentage >= 0.55 {
      waterAmountLabel.textColor = UIColor.whiteColor()
      descriptionLabel.textColor = UIColor.whiteColor()
    }
  }
}
