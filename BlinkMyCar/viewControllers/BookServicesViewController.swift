//
//  BookServicesViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/10/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

class BookServicesViewController: UIViewController {
  
  @IBOutlet weak var stepsIndicatorView: StepsIndicatorView!
  @IBOutlet weak var sliderView: SliderView!
  
  private var viewControllers = [UIViewController]()
  
  var viewModel: BookServicesViewModel = BookServicesViewModel(bookingFlowViewModel: BookingFlowViewModel())
  
  var didAppearOnceInLifecycle = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Needed for a stable search behavior in the addresses View
    self.edgesForExtendedLayout = UIRectEdge.None
    
    // Init Stepper View
    stepsIndicatorView.steps = viewModel.bookingProcessStepsStrings()
    
    // Init View Controllers
    viewControllers = viewModel.bookingProcessViewControllers()
    for subVC in viewControllers {
      if let subVC = subVC as? AddressesTableViewController {
        subVC.viewModel.analyticsCategory = AnalyticsCategories.BookingProcess
        subVC.addressesTableViewDelegate = self
      }
      if let subVC = subVC as? DatePickerTableViewController {
        subVC.datePickerDelegate = self
      }
      if let subVC = subVC as? TimePickerViewController {
        subVC.delegate = self
      }
    }
    sliderView.setViewsControllersViews(viewControllers, parentViewController: self)
    
    // Set Selection For Step
    let indexOfCurrentStep = viewModel.indexForCurrentBookingStep
    stepsIndicatorView.selectIndicatorForIndex(indexOfCurrentStep)
    sliderView.selectedIndex = indexOfCurrentStep
    
    // Set Title
    self.title = viewModel.viewControllerTitle
    self.sendScreenView(viewModel.analyticsScreenName)
    
    // Back Button
    diplayBackButtonIfNeeded()
    
    // Register Notifications
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(BookServicesViewController.goToDateSelectionStep),
      name: BMCNotificationNames.ShouldReselectDate.rawValue,
      object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(BookServicesViewController.goToAddressSelectionStep),
      name: BMCNotificationNames.ShouldReselectAddress.rawValue,
      object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(BookServicesViewController.handleInvalidBookingDates),
      name: BMCNotificationNames.BookingProcessTimeOut.rawValue,
      object: nil)
    
    if viewModel.bookingFlowViewModel.shouldDisplayCancelBookingFlowButton ?? false {
      let cancelBookingFlow = UIBarButtonItem(
        barButtonSystemItem: UIBarButtonSystemItem.Cancel,
        target: self, action: #selector(self.dismissBookingFlow))
      self.navigationItem.rightBarButtonItem = cancelBookingFlow
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    if viewModel.haveNoAddressesAvailable && !didAppearOnceInLifecycle {
      didAppearOnceInLifecycle = true
      displayAddressProfileViewController()
    }
  }
  
  func next() {
    let info = viewModel.next()
    switch (info.success, info.isLastStep) {
    case (true, false):
      stepsIndicatorView.selectIndicatorForIndex(info.index)
      sliderView.next(true)
      self.title = viewModel.viewControllerTitle
      self.sendScreenView(viewModel.analyticsScreenName)
      
      // Refresh Dates When Reaching The Date Picker Step
      if let vc = viewControllers[info.index] as? DatePickerTableViewController {
        vc.reloadData()
      }
      
      // Set the time for the selected date
      if let vc = viewControllers[info.index] as? TimePickerViewController {
        vc.viewModel.serviceTimes = (viewModel.selectedDate?.times)!
        vc.reloadData()
      }
    case (true, true):
      goToConfirmationViewController()
    default:
      break
    }
    
    diplayBackButtonIfNeeded()
  }
  
  /**
   - Description: 
   If going back t previous step fails then this means that we reached the first 
   Step. The View Controller needs to be popped
   
   - Returns: 
   Boolean indicating if going to previous step was successful or not
  */
  func previous() -> Bool {
    let info = viewModel.previous()
    if info.success {
      stepsIndicatorView.selectIndicatorForIndex(info.index)
      sliderView.previous(true)
      self.title = viewModel.viewControllerTitle
      self.sendScreenView(viewModel.analyticsScreenName)
      
      // Useless to keep the timer going when we are not yet there
      if viewModel.currentBookingStep == .PickLocation {
        if let datePickerVC = getDatePickerViewController() {
          datePickerVC.viewModel.stopTimer()
        }
      }
    }
    diplayBackButtonIfNeeded()
    return info.success
  }
  
  func didTapCancelAddANewAddressButton() {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  //=======================================================
  // MARK: - ACTIONS
  //=======================================================
  
  /**
   This method is called as a response for tapping the address cell to edit
   in the Confirmation VC. Knowing this, this vc would be at the third step
   wich is choose a time.
   Hence we have to go back One steps back
   */
  func goToDateSelectionStep() {
    previous()
  }
  
  /**
   This method is called as a response for tapping the address cell to edit
   in the Confirmation VC. Knowing this, this vc would be at the third step
   wich is choose a time.
   Hence we have to go back two steps back
  */
  func goToAddressSelectionStep() {
    previous()
    previous()
  }
  
  func handleInvalidBookingDates() {
    // Pop Other VCs if they were on top
    func popToDatePicker() {
      self.navigationController?.popToViewController(self, animated: true)
      // Go To Pick a Date step
      let numberOfStepsToPop = viewModel.numberOfStepsComparedToCurrentStep(forBookingStep: .ChooseDate)
      for _ in 0..<numberOfStepsToPop {
        previous()
      }
      
      NSNotificationCenter.defaultCenter().postNotificationName(
        BMCNotificationNames.DisplayBookingProcessTimeOutAlert.rawValue, object: nil)
    }
    
    if let navigationController = presentedViewController as? UINavigationController where navigationController.viewControllers.first is AddPromocodeViewController {
      // Dismiss any view controller that is presented modally on top of
      // navigation controller of this VC
      self.presentedViewController!.dismissViewControllerAnimated(true, completion: {
        popToDatePicker()
      })
    } else {
      popToDatePicker()
    }
  }
  
  //=======================================================
  // MARK: - HELPERS
  //=======================================================
  
  func diplayBackButtonIfNeeded() {
    var displayBackButton = false
    if let indexInNavigationController = self.navigationController?.viewControllers.indexOf(self) where indexInNavigationController > 0 {
      displayBackButton = true
    } else if viewModel.indexForCurrentBookingStep > 0 {
      displayBackButton = true
    }
    
    if displayBackButton {
      let backBarButton = UIBarButtonItem(
        image: UIImage(named: "backBarButtonItem"),
        style: .Plain, target: self, action: #selector(BookServicesViewController.didTapBackButton))
      self.navigationItem.leftBarButtonItem = backBarButton
    } else {
      self.navigationItem.leftBarButtonItem = nil
    }
  }
  
  func dismissBookingFlow() {
    self.navigationController?.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func didTapBackButton() {
    if !previous() {
      self.navigationController?.popViewControllerAnimated(true)
    }
  }
  
  func goToConfirmationViewController() {
    let vc = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.BookingConfirmationViewController.rawValue) as! BookingConfirmationViewController
    vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem
    vc.viewModel = viewModel.confirmationViewModel()
    self.navigationController?.pushViewController(vc, animated: true)
  }
  
  func displayAddressProfileViewController(address: Address? = nil, withMode mode: AddressProfileViewControllerMode? = nil) {
    guard let addressProfileVC = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.AddressProfileViewController.rawValue)
      as? AddressProfileViewController else { return }
    addressProfileVC.delegate = self
    addressProfileVC.viewModel.address = address
    if let mode  = mode {
      addressProfileVC.initialRequiredMode = mode
    }
    let cancelBarButton = UIBarButtonItem(
      title: NSLocalizedString("BarButtonButton.Cancel",
        value: "Cancel",
        comment: "Appears in the left bar button space of a navigationVC"),
      style: .Plain,
      target: self, action: #selector(BookServicesViewController.didTapCancelAddANewAddressButton))
    addressProfileVC.navigationItem.leftBarButtonItem = cancelBarButton
    self.presentViewController(UINavigationController(rootViewController: addressProfileVC), animated: true, completion: nil)
  }
  
  func getDatePickerViewController() -> DatePickerTableViewController? {
    for subVC in viewControllers {
      if let subVC = subVC as? DatePickerTableViewController {
        return subVC
      }
    }
    return nil
  }
}

//=======================================================
// MARK: - ADDRESSES | Delegates
//=======================================================

extension BookServicesViewController: AddressesTableViewControllerDelegate {
  
  func addressesTableViewDidSelectAddress(viewController: AddressesTableViewController, address: Address, withMode mode: AddressProfileViewControllerMode?) {
    if viewController.viewModel.haveValidAddresses {
      let serviceable = viewController.viewModel.addressServiceable(address)
      let hasCoordinates = address.coordinates != nil
      if serviceable && hasCoordinates {
        viewModel.selectedAddress = address
        next()
      } else if !serviceable {
        //If is not serviceable
        displayAddressProfileViewController(address)
      } else {
        // does not have coordinates
        displayAddressProfileViewController(address)
      }
    } else {
      viewController.showCouldNotProcedeNeedToSyncWithServerAlert()
    }
  }
  
  func addressesTableViewDidTapEditActionButton(viewController: AddressesTableViewController, forAddress address: Address) {
    displayAddressProfileViewController(address, withMode: AddressProfileViewControllerMode.EditAddressDetails)
  }
  
  func addressesTableViewDidTapAddAddresses(viewController: AddressesTableViewController) {
    displayAddressProfileViewController()
  }
}

extension BookServicesViewController: AddressProfileViewControllerDelegate {
  func didAddAddressSuccessfully(address: Address) {
    self.dismissViewControllerAnimated(true) {
      self.viewModel.selectedAddress = address
      self.next()
    }
  }
  
  func didEditAddressSuccessfully(address: Address) {
    self.dismissViewControllerAnimated(true) { 
      self.viewModel.selectedAddress = address
      self.next()
    }
  }
}

//=======================================================
// MARK: - DATE PICKER | Delegate
//=======================================================

extension BookServicesViewController: DatePickerTableViewDelegate {
  func datePickerDidSelectServiceDate(serviceDate: ServiceDate?) {
    self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Date Tap", label: "", value: nil)
    viewModel.selectedDate = serviceDate
    next()
  }
  
  func datePickerCancelLoadingDates() {
    previous()
  }
}

//=======================================================
// MARK: - TIME PICKER | Delegate
//=======================================================

extension BookServicesViewController: TimePickerViewControllerDelegate {
  func timePickerDidSelectServiceTime(serviceTime: ServiceTime?) {
    self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Time Tap", label: "", value: nil)
    viewModel.selectedTime = serviceTime
    next()
  }
}

//=======================================================
// MARK: - Dismissable
//=======================================================

extension BookServicesViewController: Dismissable {
  func dismiss(sender: AnyObject?) {
    dismissBookingFlow()
  }
}


