//
//  ServicesViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 5/27/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import SwiftLoader

enum ServiceCellType {
  case CheckBox
  case RadioButton
  case NotSpecified
}

protocol ServicesViewControllerDataSource {
  func servicesViewControllerActionButtonTitle() -> String
}

protocol ServicesViewControllerDelegate {
  func servicesViewControllerDidTapActionButton(viewController: ServicesViewController, bookingFlowViewModel: BookingFlowViewModel)
}

protocol ServicesViewControllerNavigationDelegate {
  func servicesViewControllerShouldTransferRightBarButton() -> Bool
}

class ServicesViewController: UIViewController, ReloadableViewController, Dismissable {
  @IBOutlet var tableView: UITableView!
  @IBOutlet weak var carProfileView: VehicleBasicInformationView!
  @IBOutlet weak var priceLabelContainerView: UIView!
  @IBOutlet weak var priceInfoLabel: BMCPriceInfoLabel!
  @IBOutlet weak var actionButtonContainerView: UIView!
  @IBOutlet weak var actionButton: BMCOrangeButton!
  
  @IBOutlet weak var priceInfoViewTrailingToSuperviewLeading: NSLayoutConstraint!
  @IBOutlet weak var priceInfoViewLeadingToSuperviewLeading: NSLayoutConstraint!
  
  var refreshController: UIRefreshControl?
  
  var viewModel: ServicesViewModel = ServicesViewModel()
  
  var delegate: ServicesViewControllerDelegate?
  var dataSource: ServicesViewControllerDataSource?
  var navigationDelegate: ServicesViewControllerNavigationDelegate?
  
  func needsReload() -> Bool {
    return !viewModel.hasServices || (viewModel.currentVehicle!.id != Vehicle.getCurrentVehicle()!.id)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.title
    self.sendScreenView("Services Listing")
    
    self.view.clipsToBounds = true
    
    // Init UI
    self.view.backgroundColor = UIColor.lighterMidGray()
    tableView.tableFooterView = UIView(frame: CGRectZero)
    tableView.backgroundColor = UIColor.clearColor()
    
    priceLabelContainerView.backgroundColor = UIColor.orange()
    priceInfoLabel.attributedText("$0", discountedPrice: nil, note: nil)
    
    // Register Cells
    tableView.registerNib(
      ServiceCheckBoxTableViewCell.nib,
      forCellReuseIdentifier: ServiceCheckBoxTableViewCell.identifier)
    tableView.registerNib(
      ServiceRadioButtonsTableViewCell.nib,
      forCellReuseIdentifier: ServiceRadioButtonsTableViewCell.identifier)
    // register section header view
    tableView.registerNib(
      TableSectionView.nib,
      forHeaderFooterViewReuseIdentifier: TableSectionView.identifier)
    
    // Implement the pull table view to refresh
    refreshController = UIRefreshControl()
    refreshController!.addTarget(self, action: #selector(self.handlePullToRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
    tableView.addSubview(refreshController!)
    
    // Set Properties for dynamic cell height
    tableView.estimatedRowHeight = 50
    tableView.rowHeight = UITableViewAutomaticDimension
    // Set Properties for dynamic section header height
    tableView.sectionHeaderHeight = UITableViewAutomaticDimension
    tableView.estimatedSectionHeaderHeight = 50
    
    // Init Vehicle Info View
    carProfileView.selectionBackgroundColor = UIColor.darkPurple()
    carProfileView.selectedState = true
    carProfileView.compactView(compactSize)
    carProfileView.vehicle = viewModel.currentVehicle!
    
    // Initialize Action Button
    let actionButtonTitle = dataSource?.servicesViewControllerActionButtonTitle()
      ?? viewModel.actionButtonTitle
    actionButton.setTitle(actionButtonTitle, forState: .Normal)
    actionButton.addTarget(
      self,
      action: #selector(ServicesViewController.didTapActionButton),
      forControlEvents: .TouchUpInside)
    
    // Notification
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(ServicesViewController.reloadServices(_:)),
      name: BMCNotificationNames.DidUpdateSavedServices.rawValue,
      object: nil)
    
    reloadServices()
    
    if viewModel.bookingFlowViewModel?.shouldDisplayCancelBookingFlowButton ?? false {
      let cancelBookingFlow = UIBarButtonItem(
        barButtonSystemItem: UIBarButtonSystemItem.Cancel,
        target: self, action: #selector(self.dismissBookingFlow))
      self.navigationItem.rightBarButtonItem = cancelBookingFlow
    }
  }
  
  func dismissBookingFlow() {
    self.navigationController?.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    refreshViewForSelectedServices(false)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    if !PersistenceManager.sharedManager.isSyncing {
      self.displayBookingToBeRatedIfAvailable(nil)
    }
  }
  
  func reloadServices(sender: AnyObject? = nil) {
    let reloadView = {
      if self.viewModel.hasServices {
        // TODO: Display table view
        self.tableView.reloadData()
      } else {
        // TODO: Hide table view
      }
    }
    
    if viewModel.canReloadServices(sender) {
      SwiftLoader.show(animated: true)
      self.refreshViewForSelectedServices(false)
      viewModel.reloadServices() {
        (success, error) -> Void in
        if !self.viewModel.isSyncing {
          SwiftLoader.hide()
        }
        reloadView()
        
        if !success {
          // TODO: Display Alert Message
        }
      }
    } else {
      if viewModel.isSyncing {
        SwiftLoader.show(animated: true)
      }
    }
  }
  
  func refreshViewForSelectedServices(animated: Bool = true) {
    self.view.removeConstraint(priceInfoViewLeadingToSuperviewLeading)
    self.view.removeConstraint(priceInfoViewTrailingToSuperviewLeading)
    
    if viewModel.hasSelectedServices {
      // Show the Price Information Section
      let priceInfo = viewModel.priceInformation
      priceInfoLabel.attributedText(priceInfo.actualPrice, discountedPrice:
        priceInfo.discountedPrice, note: priceInfo.note)
      self.view.addConstraint(priceInfoViewLeadingToSuperviewLeading)
      
      // Enable the Action Button
      actionButton.enable(true)
      actionButtonContainerView.backgroundColor = actionButton.enabledColor
    } else {
      // Hide the Price Information Section
      self.view.addConstraint(priceInfoViewTrailingToSuperviewLeading)
      
      // Disable the Action Button
      actionButton.enable(false)
      actionButtonContainerView.backgroundColor = actionButton.disabledColor
    }
    
    if animated {
      UIView.animateWithDuration(0.44) {
        self.view.layoutIfNeeded()
      }
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  //==================================================
  // MARK: Actions
  //==================================================
  
  func handlePullToRefresh(refreshControl: UIRefreshControl) {
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.RefreshApplication.rawValue, object: nil)
    SwiftLoader.refreshIfNeeded()
    refreshControl.endRefreshing()
  }
  
  func didTapActionButton() {
    self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Pick A Location Tap", label: "", value: nil)
    
    if delegate != nil {
      delegate!.servicesViewControllerDidTapActionButton(
        self, bookingFlowViewModel: viewModel.getBookingFlowViewModelForNextStep())
    } else {
      if viewModel.isAlreadyInTheBookingFlow() {
        goToBookServicesViewController()
      } else {
        BookingFlowViewController.startBookingFlowFromViewController(
          self, selectedServices: viewModel.getBookingFlowViewModelForNextStep().selectedServices!)
      }
    }
  }
  
  func dismiss(sender: AnyObject?) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func goToBookServicesViewController() {
    let vc = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.BookServicesViewController.rawValue) as! BookServicesViewController
    vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem
    vc.viewModel.bookingFlowViewModel = viewModel.getBookingFlowViewModelForNextStep()
    self.navigationController?.pushViewController(vc, animated: true)
  }
}

//=========================================================
// MARK: - Table View | DELEGATE && DATA SOURCE
//=========================================================

extension ServicesViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSection(section)
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableHeaderFooterViewWithIdentifier(
      TableSectionView.identifier) as! TableSectionView
    let sectionInfo = viewModel.sectionInfoForIndex(section)
    cell.setTitle(sectionInfo.title, imageName: sectionInfo.imageName)
    return cell
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cellInformation = viewModel.cellInformationForIndexPath(indexPath)
    
    let lastCell = viewModel.isLastCellInLastSection(indexPath)
    
    if let rowData = cellInformation.rowData as? [Service] {
      let cell = tableView.dequeueReusableCellWithIdentifier(ServiceRadioButtonsTableViewCell.identifier)
        as! ServiceRadioButtonsTableViewCell
      cell.delegate = self
      cell.services = rowData
      cell.selectService(cellInformation.selectedService)
      if lastCell {
        cell.hideSeparator()
      } else {
        cell.showSeparator()
      }
      return cell
    } else if let rowData = cellInformation.rowData as? Service {
      let cell = tableView.dequeueReusableCellWithIdentifier(ServiceCheckBoxTableViewCell.identifier)
        as! ServiceCheckBoxTableViewCell
      cell.delegate = self
      if rowData.parentId != nil {
        cell.isAddOn = true
        cell.serviceNameLabel.addOnAttributedText(
          rowData.name,
          servicePrice: "\(rowData.price.displayAsUsDollarsPrice)",
          separator: " ")
      } else {
        cell.isAddOn = false
        cell.serviceNameLabel.mainServiceAttributedText(
          rowData.name,
          servicePrice: "\(rowData.price.displayAsUsDollarsPrice)",
          separator: " ")
      }
      
      cell.serviceAvailability = rowData.availability
      
      cell.hasDetails = !(rowData.description.trim() ?? "").isEmpty
      let selected = cellInformation.isServiceSelected ?? false
      cell.setSelected(selected, animated: false)
      if selected {
        tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .None)
      }
      
      // If this is the last cell hide the separator
      if lastCell {
        cell.hideSeparator()
      } else {
        cell.showSeparator()
      }
      
      return cell
    }
    
    let cell = UITableViewCell()
    cell.textLabel?.text = "Service Not Recognized"
    return cell
    
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    guard viewModel.canSelectRowAtIndexPath(indexPath) else {
      tableView.deselectRowAtIndexPath(indexPath, animated: false)
      return
    }
    
    let cellInformation = viewModel.cellInformationForIndexPath(indexPath)
    
    switch typeOfCellForIndexPath(indexPath) {
    case .CheckBox:
      let addOnsAffected = viewModel.didSelectService((cellInformation.rowData as! Service), withIndexPath: indexPath, withStatus: true)
      refreshViewForSelectedServices()
      if addOnsAffected.addOnsAffected {
        tableView.beginUpdates()
        tableView.insertRowsAtIndexPaths(addOnsAffected.addOnsIndexPaths!,
                                         withRowAnimation: .Automatic)
        tableView.endUpdates()
        tableView.scrollToRowAtIndexPath((addOnsAffected.addOnsIndexPaths?.last)!, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
      }
      tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .None)
      tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .None, animated: true)
    default:
      return
    }
  }
  
  func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    let cellInformation = viewModel.cellInformationForIndexPath(indexPath)
    
    switch typeOfCellForIndexPath(indexPath) {
    case .CheckBox:
      let addOnsAffected = viewModel.didSelectService((cellInformation.rowData as! Service), withIndexPath: indexPath, withStatus: false)
      refreshViewForSelectedServices()
      if addOnsAffected.addOnsAffected {
        tableView.beginUpdates()
        tableView.deleteRowsAtIndexPaths(addOnsAffected.addOnsIndexPaths!, withRowAnimation: .Automatic)
        tableView.endUpdates()
      }
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    default:
      return
    }
  }
  
  private func typeOfCellForIndexPath(indexPath: NSIndexPath) -> ServiceCellType {
    let cellInformation = viewModel.cellInformationForIndexPath(indexPath)
    
    switch cellInformation.rowData {
    case is [Service]:
      return .RadioButton
    case is Service:
      return .CheckBox
    default:
      return .NotSpecified
    }
  }
}

//=========================================================
// MARK: - Rate View Delegate
//=========================================================

extension ServicesViewController: RateBookingViewControllerDelegate {
  func rateBookingVCDidSubmitRating(
    rateBookingVC: RateBookingViewController,
    booking: Booking,
    success: Bool, error: NSError?) {
    rateBookingVC.dismissViewControllerAnimated(true) {
      self.displayBookingToBeRatedIfAvailable(booking)
    }
    
  }
  
  func rateBookingVCDidCancelRating(
    rateBookingVC: RateBookingViewController,
    booking: Booking) {
    rateBookingVC.dismissViewControllerAnimated(true) {
      self.displayBookingToBeRatedIfAvailable(booking)
    }
  }
  
  private func displayBookingToBeRatedIfAvailable(ignoredBooking: Booking?) {
    if let ignoredBooking = ignoredBooking {
      viewModel.popBookingThatNeedsRating(ignoredBooking)
    }
    
    if let bookingToBeRated = viewModel.bookingToBeRated{
      RateBookingViewController.showFromViewController(
        self,
        rateBookingViewModel: RateBookingViewModel(booking: bookingToBeRated),
        delegate: self)
    }
  }
}

//=========================================================
// MARK: - Service Radio Buttons Cell | DELEGATE
//=========================================================

extension ServicesViewController: ServiceRadioButtonsTableViewCellDelegate {
  func radioButtonCellDidSelectService(cell: ServiceRadioButtonsTableViewCell, selectedService: Service?, unselectedService: Service?) {
    let indexPath = tableView.indexPathForCell(cell)
    if let unselectedService = unselectedService {
      let addOnsAffected = viewModel.didSelectService(unselectedService, withIndexPath: indexPath!, withStatus: false)
      refreshViewForSelectedServices()
      if addOnsAffected.addOnsAffected {
        tableView.beginUpdates()
        tableView.deleteRowsAtIndexPaths(addOnsAffected.addOnsIndexPaths!, withRowAnimation: .Automatic)
        tableView.endUpdates()
      }
    }
    if let selectedService = selectedService {
      let addOnsAffected = viewModel.didSelectService(selectedService, withIndexPath: indexPath!, withStatus: true)
      refreshViewForSelectedServices()
      if addOnsAffected.addOnsAffected {
        tableView.beginUpdates()
        tableView.insertRowsAtIndexPaths(addOnsAffected.addOnsIndexPaths!,
                                         withRowAnimation: .Automatic)
        tableView.endUpdates()
        tableView.scrollToRowAtIndexPath((addOnsAffected.addOnsIndexPaths?.last)!, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
      }
    }
  }
}

//=========================================================
// MARK: - Service Check Box Cell | DELEGATE
//=========================================================

extension ServicesViewController: ServiceCheckBoxTableViewCellDelegate {
  func serviceCheckBoxCellDidTapInfoIcon(cell: ServiceCheckBoxTableViewCell) {
    guard let cellIndexPath = tableView.indexPathForCell(cell) else {
      return
    }
    
    self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Service Description Tap", label: "", value: nil)
    
    let serviceDescription = viewModel.serviceDescriptionForIndexPath(cellIndexPath)
    
    let alertController = UIAlertController(
      title: serviceDescription.title,
      message: serviceDescription.description,
      preferredStyle: .Alert)
    
    let okAction = UIAlertAction(
      title: viewModel.descriptionAlertActionButtonTitle,
      style: .Default, handler: nil)
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}
