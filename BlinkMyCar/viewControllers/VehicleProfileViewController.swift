//
//  VehicleProfileViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/5/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import SwiftLoader
import AVFoundation

class VehicleProfileViewController: UIViewController {
  enum CarProfileViewMode {
    case Edit
    case Normal
  }
  @IBOutlet weak var headerView: UIView!
  @IBOutlet weak var cameraIconImageView: UIImageView!
  @IBOutlet weak var carImageView: UIImageView!
  @IBOutlet weak var carNameTextField: BMCTextFieldWithEditIcon!
  @IBOutlet weak var tableView: UITableView!
  
  var viewModel: VehicleProfileViewModel = VehicleProfileViewModel()
  var mode: CarProfileViewMode = .Normal

  var didFinishLoadingImage = true
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupContent()
    applyTheme()
    initNavigationControllerButtons()
  }
  
  private func initNavigationControllerButtons() {
    self.setupRightNavigationBarButton(edit: true)
  }
  
  func setupRightNavigationBarButton(edit edit: Bool) {
    if edit {
      let editButton = UIBarButtonItem(barButtonSystemItem: .Edit,
        target: self, action: #selector(VehicleProfileViewController.didTapEditButton))
      self.navigationItem.rightBarButtonItem = editButton
    }
    else {
      let doneButton = UIBarButtonItem(barButtonSystemItem: .Done,
        target: self, action: #selector(VehicleProfileViewController.didTapDoneButton))
      self.navigationItem.rightBarButtonItem = doneButton
    }
  }
  
  private func setupContent() {
    self.doneMode()
    self.carNameTextField.delegate = self
    self.title = viewModel.viewControllerTitle
    self.carNameTextField.text = viewModel.vehicle!.name ?? ""
    
    if let selectedImageData = viewModel.uploadImageData {
      //Load selected Image if any
      self.carImageView.image = UIImage(data: selectedImageData)
    }
    else {
      //Load API image or place holder
      let placeholderImageName = viewModel.rideIconName
      carImageView.sd_setImageWithURL(APIProvider.mediaURLForPath(self.viewModel.vehicle!.imagePath!),
        placeholderImage: UIImage(named: placeholderImageName),
        completed: { (image: UIImage!, error: NSError!, cash: SDImageCacheType, url: NSURL!) -> Void in
          if image != nil && error == nil {
            self.carImageView.backgroundColor = UIColor.whiteColor()
          }
        }
      )
    }
  }

  private func applyTheme() {
    carImageView.cicularView()
    carNameTextField.hideStyle()
    headerView.backgroundColor = UIColor.applicationMainColor()
    tableView.separatorColor = UIColor.lightGray()
    tableView.backgroundColor = UIColor.lightWhite()
  }
  
}

//===================================================================
//MARK: A C T IO N S
//===================================================================
extension VehicleProfileViewController {
  func didTapEditButton() {
    editMode()
  }
  
  func didTapDoneButton() {
    doneMode()
    if self.viewModel.canUpdate {
      SwiftLoader.show(animated: true)

      let delay = 50 * Double(NSEC_PER_MSEC)
      _ = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
      while !self.didFinishLoadingImage {
        NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode,
          beforeDate: NSDate(timeIntervalSinceNow: 0.1))
      }

      self.viewModel.updateRide { (success: Bool) -> Void in
        SwiftLoader.hide()

        if !success {
          self.failedToUpdateVehicleProfileAlert()
        }
      }
    }
  }
  
  func editMode() {
    mode = .Edit
    carImageView.userInteractionEnabled = true
    cameraIconImageView.userInteractionEnabled = true
    tableView.reloadData()
    cameraIconImageView.alpha = 1.0
    carNameTextField.showStyle()
    self.setupRightNavigationBarButton(edit: false)
  }
  
  func doneMode() {
    mode = .Normal
    carImageView.userInteractionEnabled = false
    cameraIconImageView.userInteractionEnabled = false
    tableView.reloadData()
    cameraIconImageView.alpha = 0.0
    carNameTextField.hideStyle()
    self.setupRightNavigationBarButton(edit: true)
  }
  
  func brandAction() {
    let newVehicleVC = storyboard!.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.CreateRideViewController.rawValue) as! CreateRideViewController
    newVehicleVC.vehicleType = viewModel.vehicle!.type!
    newVehicleVC.delegate = self
    newVehicleVC.viewModel.viewMode = .Make
    newVehicleVC.viewModel.fullProcess = false

    newVehicleVC.viewModel.imagePath = viewModel.vehicle!.imagePath
    newVehicleVC.viewModel.userSetcarName = viewModel.carNameValue ?? viewModel.vehicle!.name ?? ""
    newVehicleVC.viewModel.uploadImageData = viewModel.uploadImageData
    self.navigationController?.pushViewController(newVehicleVC, animated: true)
  }
  
  func modelAction() {
    let newVehicleVC = storyboard!.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.CreateRideViewController.rawValue) as! CreateRideViewController
    newVehicleVC.vehicleType = viewModel.vehicle!.type!
    newVehicleVC.delegate = self
    newVehicleVC.viewModel.viewMode = .Model
    newVehicleVC.viewModel.fullProcess = false
    
    newVehicleVC.viewModel.imagePath = viewModel.vehicle!.imagePath
    newVehicleVC.viewModel.userSetcarName = viewModel.carNameValue ?? viewModel.vehicle!.name ?? ""
    newVehicleVC.viewModel.uploadImageData = viewModel.uploadImageData
    SwiftLoader.show(animated: true)
    newVehicleVC.viewModel.loadData { (success: Bool) -> Void in
      var make: Make? =  newVehicleVC.viewModel.makeViewModel.makeFor(self.viewModel.vehicle!.make!)
      if make == nil {
        make = self.viewModel.vehicleMake
      }
      newVehicleVC.viewModel.selectedMake = make
      newVehicleVC.viewModel.modelViewModel = RideModelViewModel(make: make!)
      SwiftLoader.hide()
      if success {
        self.navigationController?.pushViewController(newVehicleVC, animated: true)
      }
      else {
//        self.failedToLoadModelAlert()
      }
    }

  }
  
  func colorAction() {
    let newVehicleVC = storyboard!.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.CreateRideViewController.rawValue) as! CreateRideViewController
    newVehicleVC.vehicleType = viewModel.vehicle!.type!
    newVehicleVC.delegate = self
    newVehicleVC.viewModel.viewMode = .Color
    newVehicleVC.viewModel.fullProcess = false
    
    newVehicleVC.viewModel.imagePath = viewModel.vehicle!.imagePath
    newVehicleVC.viewModel.userSetcarName = viewModel.carNameValue ?? viewModel.vehicle!.name ?? ""
    newVehicleVC.viewModel.uploadImageData = viewModel.uploadImageData
    self.navigationController?.pushViewController(newVehicleVC, animated: true)
  }
  
  func plateAction() {
    let newVehicleVC = storyboard!.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.CreateRideViewController.rawValue) as! CreateRideViewController
    newVehicleVC.vehicleType = viewModel.vehicle!.type!
    newVehicleVC.delegate = self
    newVehicleVC.viewModel.viewMode = .PlateNumber
    newVehicleVC.viewModel.fullProcess = false
    
    newVehicleVC.viewModel.imagePath = viewModel.vehicle!.imagePath
    newVehicleVC.viewModel.userSetcarName = viewModel.carNameValue ?? viewModel.vehicle!.name ?? ""
    newVehicleVC.viewModel.plateNumber = viewModel.plateNumberValue ?? viewModel.vehicle!.plateNumber
    newVehicleVC.viewModel.uploadImageData = viewModel.uploadImageData
    self.navigationController?.pushViewController(newVehicleVC, animated: true)
  }
}

//===================================================================
// MARK: A L E R T S
//===================================================================

extension VehicleProfileViewController {
  func failedToLoadModelAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToLoadModelsAlertTitle,
      message: viewModel.alertViewFailToLoadModelsAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.alertViewFailToLoadModelsAlertActionButtonText,
      style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        self.navigationController?.popViewControllerAnimated(true)
    }

    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func failedToUpdateVehicleProfileAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToUpdateVehicleProfileAlertTitle,
      message: viewModel.alertViewFailToUpdateVehicleProfileAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let cancelAction = UIAlertAction(title: viewModel.alertViewFailToUpdateVehicleProfileAlertActionButtonText,
      style: UIAlertActionStyle.Default, handler: nil)

    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }

  func failedToUploadImageAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToUploadImageAlertTitle,
      message: viewModel.alertViewFailToUploadImageAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let cancelAction = UIAlertAction(title: viewModel.alertViewFailToUploadImageAlertActionButtonText,
      style: UIAlertActionStyle.Default, handler: nil)

    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

//===================================================================
// MARK: UITableViewDataSource
//===================================================================
extension VehicleProfileViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows()
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: VehicleProfileTableViewCell = tableView.dequeueReusableCellWithIdentifier(VehicleProfileTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! VehicleProfileTableViewCell
    let item: (title: String, value: String, color: UIColor?) = viewModel.cellForRow(indexPath)
    cell.nameText = item.title
    cell.valueText = item.value
    cell.editIconImage =  (mode == .Edit) ? UIImage(named: "pencilIcon") : nil
    if self.viewModel.isColorRowNumber(indexPath.row) {
      //Hide Color Box
      cell.boxColor = item.color
    }
    else{
      cell.boxColor = nil
    }
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    if mode == .Edit {
      self.resignResponder()
      switch (viewModel.itemAtRow(indexPath.row)) {
      case .Brand:
        brandAction()
      case .Model:
        modelAction()
      case .Color:
        colorAction()
      case .PlateNumber:
        plateAction()
      }
    }
  }
}

extension VehicleProfileViewController: CreateRideViewControllerDelegate {
  func updatedModelAndMake(model: String, make: Make) {
    self.viewModel.modelValue = model
    self.viewModel.brandValue = make.name
    tableView.reloadData()
  }

  func updatedModel(model: String) {
    self.viewModel.modelValue = model
    tableView.reloadData()
  }

  func updatedColor(rideColor: RideColor) {
    self.viewModel.colorValue = rideColor.name
    tableView.reloadData()
  }

  func updatedPlateNumber(number: String) {
    let arry = number.characters.split {$0 == " "}.map { String($0) }
    self.viewModel.plateNumberValue = (arry.count == 0) ? nil : ((arry.count == 2) ? arry[1] : arry[0])
    tableView.reloadData()
  }

  func updatedCarName(name: String?) {
    viewModel.carNameValue = name
    self.carNameTextField.text = viewModel.carNameValue ?? viewModel.vehicle!.name ?? ""
  }

  func updatedImageData(data: NSData?, imageURL: NSURL?) {
    // If the image data is corrupted or is nil then we reload the image
    // in the image View and wait for it till it finishes

    if let data = data where data.length > 0 {
      let carPlaceholderImage = UIImageJPEGRepresentation(UIImage(named: "iconCar")!, 0.8)!
      let motorcyclePlaceholderImage = UIImageJPEGRepresentation(UIImage(named: "iconMotorcycle")!, 0.8)!

      if let image = UIImage(data: data) {
        if !carPlaceholderImage.isEqualToData(data) && !motorcyclePlaceholderImage.isEqualToData(data) {
          self.carImageView.image = image
          self.viewModel.uploadImageData = UIImageJPEGRepresentation(image, 0.8)
          self.didFinishLoadingImage = true
        }
        return
      }
    }

    if let imageURL = imageURL {
      let placeholderImageName = viewModel.rideIconName
      self.didFinishLoadingImage = false
      print("Should Update: \(self.didFinishLoadingImage)")
      self.carImageView.sd_setImageWithURL(imageURL, placeholderImage: UIImage(named: placeholderImageName),
        completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, url: NSURL!) -> Void in
          if image != nil && error == nil {
            self.viewModel.uploadImageData = UIImageJPEGRepresentation(image, 0.8)
            self.didFinishLoadingImage = true
          } else {
            self.didFinishLoadingImage = true
            self.carImageView.backgroundColor = UIColor.clearColor()
            self.failedToUploadImageAlert()
          }

          print("Finish Update Image: \(self.didFinishLoadingImage)")
      })
    }
  }
}

//MARK: UITextFieldDelegate
extension VehicleProfileViewController: UITextFieldDelegate {
  func textFieldDidEndEditing(textField: UITextField) {
    if textField.text != nil && !textField.text!.isEmpty {
      viewModel.carNameValue = textField.text
    } else {
      viewModel.carNameValue = nil
      textField.text = viewModel.vehicle!.name!
    }
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func resignResponder() {
    self.carNameTextField.resignFirstResponder()
  }
}

extension VehicleProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  @IBAction private func changeCarImage() {
    let actionView = UIAlertController(title: nil,
      message: nil,
      preferredStyle: UIAlertControllerStyle.ActionSheet)
    
    let destructiveButton = UIAlertAction(title: "Cancel",
      style: UIAlertActionStyle.Cancel, handler: nil)
    let takePhotoButton = UIAlertAction(title: "Take Photo",
      style: UIAlertActionStyle.Default, handler: takePhotoAction)
    let choosePhotoButton = UIAlertAction(title: "Choose From Library",
      style: UIAlertActionStyle.Default, handler: chooseFromLibraryAction)
    
    actionView.addAction(destructiveButton)
    actionView.addAction(takePhotoButton)
    actionView.addAction(choosePhotoButton)
    
    presentViewController(actionView, animated: true, completion: nil)
  }
  
  private func takePhotoAction(action: UIAlertAction!) {
    let imagePickerController = UIImagePickerController()
    if imagePickerController.hasCameraAccess {
      imagePickerController.delegate = self
      imagePickerController.sourceType = .Camera
      navigationController?.presentViewController(imagePickerController, animated: true, completion: nil)
    } else {
      imagePickerController.showNeedPermissionAlert(self)
    }
  }
  
  private func chooseFromLibraryAction(action: UIAlertAction!) {
    let imagePickerController = UIImagePickerController()
    imagePickerController.delegate = self
    imagePickerController.sourceType = .PhotoLibrary
    navigationController?.presentViewController(imagePickerController, animated: true, completion: nil)
  }

  func imagePickerController(picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [String : AnyObject]) {
      let image = info[UIImagePickerControllerOriginalImage] as? UIImage
      carImageView.image = image
      viewModel.uploadImageData = UIImageJPEGRepresentation(image!, 0.8)
      navigationController?.dismissViewControllerAnimated(true, completion: nil)
  }
}
