//
//  CreateRideViewController.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import CSStickyHeaderFlowLayout
import SDWebImage
import SwiftLoader
import AVFoundation

public protocol CreateRideViewControllerDelegate: NSObjectProtocol {
  func updatedModelAndMake(model: String, make: Make)
  func updatedModel(model: String)
  func updatedColor(rideColor: RideColor)
  func updatedPlateNumber(number: String)
  func updatedCarName(name: String?)
  func updatedImageData(data: NSData?, imageURL: NSURL?)
}

public class CreateRideViewController: UIViewController {

  @IBOutlet weak var noDataView: VehicleNoDataView!
  @IBOutlet weak var plateNumberScrollView: UIScrollView!
  @IBOutlet weak var plateNumberView: CreateRidePlateNumberView!
  @IBOutlet weak var headerView: CreateRideHeaderView!
  @IBOutlet weak var searchView: UIView!
  @IBOutlet weak var searchTextField: BMCSearchTextField!
  @IBOutlet weak var searchViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var collectionViewFlowLayout: CSStickyHeaderFlowLayout!

  @IBOutlet weak var plateNumberViewTopToParentTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var plateNumberViewTopToHeaderBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var plateNumberViewBottomToParentBottomConstraint: NSLayoutConstraint!

  @IBOutlet weak var headerViewLabelBottomToViewBottomConstraint: NSLayoutConstraint!
  private var isKeyboardVisible = false
  
  lazy var viewModel:CreateRideViewModel = {
    return CreateRideViewModel(rideType: self.vehicleType)
  }()

  private var maxTopConstant:CGFloat = 214.0
  private let minTopConstant:CGFloat = 0.0
  
  public var vehicleType: Make.VehicleType = .Car
  public var delegate: CreateRideViewControllerDelegate?
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    collectionView?.registerNib(ColorsCollectionViewCell.nib,
      forCellWithReuseIdentifier: ColorsCollectionViewCell.reusableIdentifier)
    
    title = viewModel.title
    self.sendScreenView(viewModel.analyticsScreenName)
    
    headerView.titleLabel.text = viewModel.question
    searchTextField.placeholder = viewModel.searchTextPlaceHolder
    headerView.carNameTextField.text = viewModel.carName

    if let selectedImageData = viewModel.uploadImageData {
      //Load selected Image if any
      headerView.carImageView.image = UIImage(data: selectedImageData)
      headerView.carImageView.backgroundColor = UIColor.whiteColor()
    }
    else {
      //Load API image or place holder
      let placeholderImageName = viewModel.rideIconName
      headerView.carImageView.sd_setImageWithURL(APIProvider.mediaURLForPath(self.viewModel.imagePath ?? ""), placeholderImage: UIImage(named: placeholderImageName),
        completed:{ (image: UIImage!, error: NSError!, cash: SDImageCacheType, url: NSURL!) -> Void in
          if image != nil && error == nil {
            self.headerView.carImageView.backgroundColor = UIColor.whiteColor()
          }
        }
      )
    }
    refreshHeader()
    refreshCollectionView()

    plateNumberView.saveButton.setTitle(viewModel.saveButtonTitle, forState: UIControlState.Normal)
    (self.plateNumberView.skipButton as? BMCLinkButton)?.textColor = UIColor.gray()
    self.plateNumberView.skipButton.setTitle(viewModel.skipButtonTitle, forState: UIControlState.Normal)
    plateNumberView.plateNumberLabel.text = viewModel.plateNumberLabel
    plateNumberView.plateNumberTextField.placeholder = viewModel.plateNumberPlaceHolder
    plateNumberView.delegate = self
    plateNumberScrollView.delegate = self
    
    collectionViewFlowLayout.itemSize = CGSizeMake(self.view.frame.size.width, CreateRideMakeModelCollectionViewCell.cellHeight)
    collectionViewFlowLayout.minimumLineSpacing = 0
    collectionViewFlowLayout.sectionInset = UIEdgeInsetsZero
    collectionViewFlowLayout.disableStickyHeaders = false



    searchView.backgroundColor = UIColor.lightWhite()
    view.backgroundColor = searchView.backgroundColor
    searchTextField.addTarget(self, action: #selector(CreateRideViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)

    noDataView.alpha = 0
    noDataView.delegate = self
    noDataView.state = viewModel.noDataViewState

    if self.viewModel.viewMode != .Model {
      SwiftLoader.show(animated: true)
      viewModel.loadData { (success: Bool) -> Void in
        if success {
          self.collectionView.reloadData()
        }
        else {
          self.failedToLoadModelAlert()
        }
        SwiftLoader.hide()
      }
    }
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CreateRideViewController.keyboardWillShow(_:)), name:UIKeyboardDidShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CreateRideViewController.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil)

    if !self.viewModel.fullProcess {
      //If partial process: prefill plate number fields and update UI - hide / show UI elements
      self.plateNumberView.skipButton.alpha = 0.0
      let plateInfo = self.viewModel.plateNumberFormatted
      self.plateNumberView.plateNumberTextField.text = plateInfo ?? nil
      //Add Cancel button instead of the back
      navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: #selector(CreateRideViewController.didTapCancel))
    } else {
      //Default: Full process
      let backButton: UIButton = UIButton(type: UIButtonType.Custom)
      backButton.frame = CGRectMake(0, 0, 20, 30)
      backButton.setImage(UIImage(named: "backBarButtonItem"), forState: UIControlState.Normal)
      backButton.addTarget(self, action: #selector(CreateRideViewController.goBack), forControlEvents: UIControlEvents.TouchUpInside)
      let leftBarButton = UIBarButtonItem(customView: backButton)
      navigationItem.leftBarButtonItem = leftBarButton
    }
    
    headerView.addTapGesture(self)
    plateNumberView.addTapGesture(self)
    searchView.addTapGesture(self)
  }

  public override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    maxTopConstant = self.headerView.frame.height
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  func keyboardWillShow(notification: NSNotification) {
    isKeyboardVisible = true
    var frame = CGRectZero
    if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      frame = value.CGRectValue()
    }

    if plateNumberView.isFirstResponder() {
      self.view.removeConstraint(plateNumberViewTopToHeaderBottomConstraint)
      self.view.addConstraint(plateNumberViewTopToParentTopConstraint)
      self.plateNumberViewBottomToParentBottomConstraint.constant = -frame.height
      self.view.needsUpdateConstraints()
      UIView.animateWithDuration(0.4, animations: { () -> Void in
        self.view.layoutIfNeeded()
      })
    } else {
      collectionViewBottomConstraint.constant = frame.size.height
    }
  }

  func keyboardWillHide(notification: NSNotification) {
    isKeyboardVisible = false
    if plateNumberView.isFirstResponder() {
      resetSearchViewPosition(true, completion: nil)
      self.view.removeConstraint(plateNumberViewTopToParentTopConstraint)
      self.view.addConstraint(plateNumberViewTopToHeaderBottomConstraint)
      self.plateNumberViewBottomToParentBottomConstraint.constant = -10
      self.view.needsUpdateConstraints()
      UIView.animateWithDuration(0.4, animations: { () -> Void in
        self.view.layoutIfNeeded()
      })
    } else {
      collectionViewBottomConstraint.constant = 0
    }
  }
  
  func didTapCancel() {
    headerView.resignFirstResponder()
    plateNumberView.resignFirstResponder()
    //Just make sure that partial processing is active
    if !self.viewModel.fullProcess {
      navigationController?.popViewControllerAnimated(true)
    }
  }
  
  func goBack() {
    //For full proces only
    headerView.resignFirstResponder()
    plateNumberView.resignFirstResponder()
    searchTextField.resignFirstResponder()
    title = viewModel.title
    self.sendScreenView(viewModel.analyticsScreenName)
    switch (self.viewModel.viewMode) {
    case .Make:
      navigationController?.popViewControllerAnimated(true)
    case .Model:
      headerView.carImageView.image = UIImage(named: viewModel.rideIconName)
      headerView.carImageView.backgroundColor = UIColor.clearColor()
      noDataView.state = .Make
      searchTextField.showSearchIcon()
      fallthrough
    case .Color:
      refreshHeader()
      viewModel.goBack()
      refreshHeader()
      refreshCollectionView()
    case .PlateNumber:
      viewModel.goBack()
      refreshHeader()
      refreshCollectionView()
    }
  }

  // This method is needed since we're moving the views based on the scroll
  // position. When there isn't enough items to scroll, this method adds enough
  // padding in order to always scroll. This padding is used when setting
  // the section insets.
  private func bottomPadding() -> CGFloat {
    if isKeyboardVisible {
      return 0
    }
    var itemsHeight: CGFloat = 0.0
    var extraPadding: CGFloat = 0.0
    if viewModel.viewMode == .Color {
      let perRow = view.frame.size.width / ColorsCollectionViewCell.cellWidth - 1
      let rows = CGFloat(viewModel.numberOfRows(0)) / perRow + 1
      itemsHeight = rows * ColorsCollectionViewCell.cellHeight
      extraPadding = 50
    } else {
      for i in 0 ..< viewModel.numberOfSections() {
        itemsHeight += CGFloat(viewModel.numberOfRows(i)) * CreateRideMakeModelCollectionViewCell.cellHeight
      }
      extraPadding = 10
    }
    let availableHeight = view.frame.size.height - searchView.frame.size.height
    let padding = availableHeight - itemsHeight
    return max(0, padding + extraPadding)
  }

  private func scrollSearchViewToTop() {
    searchViewTopConstraint.constant = minTopConstant
    UIView.animateWithDuration(0.4, animations: { () -> Void in
      self.headerView.fade(0)
      self.view.layoutIfNeeded()
    })
  }

  private func resetSearchViewPosition(shouldResetSearchViewInPosition: Bool = true, completion: ((Bool) -> Void)?) {
    if shouldResetSearchViewInPosition {
      searchViewTopConstraint.constant = maxTopConstant
    }

    if viewModel.viewMode == .Color || viewModel.viewMode == .PlateNumber {
      searchViewHeightConstraint.constant = 0
      searchView.alpha = 0
    } else {
      searchViewHeightConstraint.constant = 60
      searchView.alpha = 1
    }
    
    UIView.animateWithDuration(0.4, delay: 0, options: UIViewAnimationOptions.BeginFromCurrentState, animations: { () -> Void in
      self.headerView.fade(1)
      if self.viewModel.viewMode == .Color || self.viewModel.viewMode == .PlateNumber {
        self.searchView.alpha = 0
      } else {
        self.searchView.alpha = 1
      }
      self.view.layoutIfNeeded()
      }, completion: { (done) -> Void in
        completion?(done)
        self.collectionView.contentOffset = CGPointMake(0, 0)
    })
  }

  private func refreshHeader() {
    let isPlateNumberMode: Bool = self.viewModel.viewMode == .PlateNumber
    let fromPlateNumberMode: Bool = self.viewModel.prevViewMode == .PlateNumber
    
    headerViewLabelBottomToViewBottomConstraint.constant = isPlateNumberMode ? 10 : 0
    
    var completionBlock: (() -> Void)? = nil
    
    //Handes Back (.PlateNumber -> .Color ): Title animation jump issue
    if fromPlateNumberMode {
      completionBlock = { () -> Void in
        self.headerView.titleLabel.text = self.viewModel.question
      }
    } else {
      self.headerView.titleLabel.text = self.viewModel.question
    }

    if isPlateNumberMode {
      showPlateNumberView(true, completionBlock: completionBlock)
    } else {
      hidePlateNumberView(true, completionBlock: completionBlock)
    }

    headerView.carNameTextField.text = viewModel.carName
    searchTextField.text = ""
    self.title = self.viewModel.title
    self.sendScreenView(viewModel.analyticsScreenName)
    self.view.layoutIfNeeded()
  }

  public func showPlateNumberView(animated: Bool, completionBlock: (() -> Void)?) {
    self.plateNumberView.alpha = 1
    if plateNumberScrollView.alpha != 1.0 && animated {
      UIView.animateWithDuration(0.4, animations: { () -> Void in
        self.plateNumberView.update(show: true, animated: true, completionBlock: completionBlock)
        self.plateNumberScrollView.alpha = 1.0
        self.plateNumberScrollView.layoutIfNeeded()
      })
    }
  }

  public func hidePlateNumberView(animated: Bool, completionBlock: (() -> Void)?) {
    self.plateNumberView.alpha = 1
    if plateNumberScrollView.alpha != 0.0 && animated {
      UIView.animateWithDuration(0.4, animations: { () -> Void in
        self.plateNumberView.update(show: false, animated: true, completionBlock: completionBlock)
        self.plateNumberScrollView.alpha = 0.0
        self.plateNumberScrollView.layoutIfNeeded()
      })
    }
  }

  private func refreshCollectionView(shouldResetSearchViewPosition: Bool = true) {
    let block: () -> Void = { () -> Void in
      let transition = CATransition()
      transition.duration = 0.4
      transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
      transition.type = kCATransitionFade
      self.collectionView.layer.addAnimation(transition, forKey: nil)

      self.collectionView.reloadData()
    }

    resetSearchViewPosition(shouldResetSearchViewPosition, completion: { (done) -> Void in
      block()
    })
  }

  var didFinishLoadingImage = true

  func saveRide() {
    SwiftLoader.show(animated: true)
    if viewModel.uploadImageData == nil {
      didFinishLoadingImage = false
      headerView.carImageView.sd_setImageWithURL(APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath), placeholderImage: UIImage(named: viewModel.rideIconName),
        completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, url: NSURL!) -> Void in
          if let _ = error {
            self.didFinishLoadingImage = true
            return
          }
          self.headerView.carImageView.backgroundColor = UIColor.whiteColor()
          self.viewModel.uploadImageData = UIImageJPEGRepresentation(image!, 0.8)
          self.didFinishLoadingImage = true
      })

      // Wait for the image to be loaded before proceeding
      let delay = 50 * Double(NSEC_PER_MSEC)
      _ = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
      while !self.didFinishLoadingImage {
        NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode,
          beforeDate: NSDate(timeIntervalSinceNow: 0.1))
      }
    }

    viewModel.createRide { (success) -> Void in
      SwiftLoader.hide()
      if success {
        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.ShouldShowHomeVC.rawValue,
          object: nil)
      } else {
        self.showFailToAddRideAlert()
      }
    }
  }

  func showFailToAddRideAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToAddRideAlertTitle,
      message: viewModel.alertViewFailToAddRideAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewFailToAddRideAlertActionButtonText,
      style: UIAlertActionStyle.Destructive, handler: nil)
    alertController.addAction(okAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

extension CreateRideViewController: UICollectionViewDataSource {
  public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    let numberOfSections = viewModel.numberOfSections()
    if viewModel.isDataLoaded {
      noDataView.alpha = numberOfSections > 0 ? 0 : 1
    }
    return numberOfSections
  }

  public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.numberOfRows(section)
  }

  public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    if viewModel.viewMode == .Color {
      let cell: ColorsCollectionViewCell? = collectionView.dequeueReusableCellWithReuseIdentifier(ColorsCollectionViewCell.reusableIdentifier,forIndexPath: indexPath) as? ColorsCollectionViewCell
      let color = viewModel.itemForIndexPath(indexPath) as? RideColor
      cell?.color = color?.value
      cell?.colorNameLabel.text = color?.name
      return cell!
    } else {
      let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CreateRideMakeModelCollectionViewCell.cellReuseIdentifier, forIndexPath: indexPath) as? CreateRideMakeModelCollectionViewCell
      cell?.textLabel.text = viewModel.itemForIndexPath(indexPath) as? String
      if viewModel.viewMode == .Make {
        cell?.separatorView.hidden = viewModel.isLastItem(indexPath)
      } else {
        cell?.separatorView.hidden = false
      }
      return cell!
    }
  }

  public func collectionView(collectionView: UICollectionView,
    viewForSupplementaryElementOfKind kind: String,
    atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
      if kind == UICollectionElementKindSectionHeader {
        let cell = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: CreateRideMakeModelCollectionViewSectionHeaderCell.reuseIdentifier, forIndexPath: indexPath) as? CreateRideMakeModelCollectionViewSectionHeaderCell
        cell?.titleLabel.text = viewModel.sectionTitle(indexPath.section)
        return cell!
      }
      return UICollectionReusableView()
  }
}

extension CreateRideViewController: UICollectionViewDelegate {

  public func scrollViewDidScroll(scrollView: UIScrollView) {
    var constant = searchViewTopConstraint.constant - scrollView.contentOffset.y

    if scrollView.contentOffset.y > 0 {
      constant = max(minTopConstant, constant)
      if constant > 0 {
        scrollView.contentOffset.y = 0
      }
    } else {
      constant = min(maxTopConstant, constant)
    }

    searchViewTopConstraint.constant = constant
    let percentage = constant / (maxTopConstant - minTopConstant)
    headerView.fade(percentage)
  }

  public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    let prevMode = self.viewModel.viewMode
    let shouldContinueProcess: Bool = viewModel.selectItemAtIndexPath(indexPath)
    if self.viewModel.fullProcess {
      if prevMode == .Make && viewModel.uploadImageData == nil {
        headerView.carImageView.sd_setImageWithURL(APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath), placeholderImage: UIImage(named: viewModel.rideIconName),
          completed: { (image: UIImage!, error: NSError!, cash: SDImageCacheType, url: NSURL!) -> Void in
            if image != nil && error == nil {
              self.headerView.carImageView.backgroundColor = UIColor.whiteColor()
            }
          }
        )
      }
      self.continueProcess(shouldContinueProcess)
    } else {
      //Partial Process
      if prevMode == .Make {
        headerView.carImageView.sd_setImageWithURL(APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath), placeholderImage: UIImage(named: viewModel.rideIconName),
          completed: { (image: UIImage!, error: NSError!, cash: SDImageCacheType, url: NSURL!) -> Void in
            if image != nil && error == nil {
              self.headerView.carImageView.backgroundColor = UIColor.whiteColor()
            }
          }
        )

        //Goto Brand
        self.continueProcess(shouldContinueProcess)
      } else {
        //Delegate value back
        self.handleDelegations(prevMode)
      }
    }
  }
  
  func continueProcess(shouldContinueProcess: Bool, shouldResetSearchViewPosition: Bool = true) {
    // Analytics
    sendAnalyticsEvents()
    
    if shouldContinueProcess {
      if shouldResetSearchViewPosition {
        headerView.resignFirstResponder()
        searchView.resignFirstResponder()
        searchTextField.resignFirstResponder()
      }
      refreshHeader()
      noDataView.state = viewModel.noDataViewState
      refreshCollectionView(shouldResetSearchViewPosition)
    }
  }
  
  func sendAnalyticsEvents() {
    switch viewModel.prevViewMode {
    case .Make:
      self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Brand Tap", label: "", value: nil)
    case .Model:
      self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Model Tap", label: "", value: nil)
    case .Color:
      self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Color Tap", label: "", value: nil)
    case .PlateNumber:
      // Basically it shal never fall to here
      self.trackEvent(viewModel.analyticsCategory.rawValue, action: "PlateNumber Tap", label: "", value: nil)
      
    }
  }
}

//====================================================================
//MARK: P A R T I A L  P R O C E S S  C A L L  D E L E G A T I O N
//====================================================================
extension CreateRideViewController {
  func handleDelegations(mode: CreateRideViewMode) {
    switch (mode) {
    case .Model:
      delegateModel()
    case .Color:
      delegateColor()
    case .PlateNumber:
      delegatePlateNumber()
    default: break
    }
  }
  
  func delegateModel() {
    if self.viewModel.selectedMake == nil {
      self.delegate?.updatedModel(self.viewModel.selectedModel!)
    }
    else {
      self.delegate?.updatedModelAndMake(self.viewModel.selectedModel!, make: self.viewModel.selectedMake!)
    }
    self.delegate?.updatedCarName(viewModel.userSetcarName ?? nil)

    viewModel.uploadImageData = UIImageJPEGRepresentation(headerView.carImageView.image!, 0.8)
    self.delegate?.updatedImageData(viewModel.uploadImageData,
      imageURL: APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath))
    //done - go back
    navigationController?.popViewControllerAnimated(true)
  }
  
  func delegateColor() {
    self.delegate?.updatedColor(self.viewModel.selectedColor!)
    self.delegate?.updatedCarName(viewModel.userSetcarName ?? nil)
    self.delegate?.updatedImageData((viewModel.uploadImageData ?? nil),
      imageURL: (viewModel.selectedMake?.imagePath != nil) ? APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath) : nil)
    //done - go back
    navigationController?.popViewControllerAnimated(true)
  }
  
  func delegatePlateNumber() {
    self.delegate?.updatedPlateNumber(self.viewModel.plateNumber ?? "")
    self.delegate?.updatedCarName(viewModel.userSetcarName ?? nil)
    self.delegate?.updatedImageData((viewModel.uploadImageData ?? nil),
      imageURL: (viewModel.selectedMake?.imagePath != nil) ? APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath) : nil)
    //done - go back
    navigationController?.popViewControllerAnimated(true)
  }
}

extension CreateRideViewController: UICollectionViewDelegateFlowLayout {
  public func collectionView(collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
      if (viewModel.viewMode == .Color) {
        return CGSizeMake(ColorsCollectionViewCell.cellWidth, ColorsCollectionViewCell.cellHeight)
      }
      return CGSizeMake(self.view.frame.size.width, CreateRideMakeModelCollectionViewCell.cellHeight)
  }

  public func collectionView(collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    insetForSectionAtIndex section: Int) -> UIEdgeInsets
  {
    if viewModel.viewMode == .Color {
      return UIEdgeInsetsMake(20, 20, bottomPadding(), 20)
    }
    else {
      return UIEdgeInsetsMake(0, 0, bottomPadding(), 0)
    }
  }

  public func collectionView(collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    referenceSizeForHeaderInSection section: Int) -> CGSize {
      if (self.viewModel.viewMode != .Make) {
        return CGSizeZero
      }
      return CGSizeMake(self.view.frame.size.width,
        CreateRideMakeModelCollectionViewSectionHeaderCell.cellHeight)
  }
}

//======================================================
// MARK: T E X T   F I E L D   D E L E G A T E
//======================================================
extension CreateRideViewController: UITextFieldDelegate {
  public func textFieldDidBeginEditing(textField: UITextField) {
    if textField == searchTextField {
      scrollSearchViewToTop()
    }
  }

  public func textFieldDidEndEditing(textField: UITextField) {
    if textField == searchTextField {
      resetSearchViewPosition(completion: nil)
    } else {
      if textField.text != nil && !textField.text!.isEmpty {
        viewModel.userSetcarName = textField.text
      } else {
        viewModel.userSetcarName = nil
        textField.text = viewModel.carName
      }
    }
  }

  public func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }

  func textFieldDidChange(sender: UITextField) {
    viewModel.search(sender.text ?? "")
    collectionView.reloadData()
    noDataView.addedItemName = sender.text ?? ""
  }
}

extension CreateRideViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  @IBAction private func changeCarImage() {
    let actionView = UIAlertController(title: nil,
      message: nil,
      preferredStyle: UIAlertControllerStyle.ActionSheet)

    let destructiveButton = UIAlertAction(title: "Cancel",
      style: UIAlertActionStyle.Cancel, handler: nil)
    let takePhotoButton = UIAlertAction(title: "Take Photo",
      style: UIAlertActionStyle.Default, handler: takePhotoAction)
    let choosePhotoButton = UIAlertAction(title: "Choose From Library",
      style: UIAlertActionStyle.Default, handler: chooseFromLibraryAction)

    actionView.addAction(destructiveButton)
    actionView.addAction(takePhotoButton)
    actionView.addAction(choosePhotoButton)

    presentViewController(actionView, animated: true, completion: nil)
  }

  private func takePhotoAction(action: UIAlertAction!) {
    let imagePickerController = UIImagePickerController()
    if imagePickerController.hasCameraAccess {
      imagePickerController.delegate = self
      imagePickerController.sourceType = .Camera
      navigationController?.presentViewController(imagePickerController, animated: true, completion: nil)
    } else {
      imagePickerController.showNeedPermissionAlert(self)
    }
  }

  private func chooseFromLibraryAction(action: UIAlertAction!) {
    let imagePickerController = UIImagePickerController()
    imagePickerController.delegate = self
    imagePickerController.sourceType = .PhotoLibrary
    navigationController?.presentViewController(imagePickerController, animated: true, completion: nil)
  }

  public func imagePickerController(picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [String : AnyObject]) {
      let image = info[UIImagePickerControllerOriginalImage] as? UIImage
      headerView.carImageView.image = image
      headerView.carImageView.backgroundColor = UIColor.whiteColor()
      viewModel.uploadImageData = UIImageJPEGRepresentation(image!, 0.8)
      navigationController?.dismissViewControllerAnimated(true, completion: nil)
  }
}

//======================================================
//MARK: - CreateRidePlateNumberViewDelegate
//======================================================
extension CreateRideViewController: CreateRidePlateNumberViewDelegate {
  func plateNumberTextFieldDidEndEditing(textField: UITextField) {
    self.viewModel.plateNumber = textField.text
  }
  
  func saveAction() {
    if self.viewModel.fullProcess {
      self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Save Plate Number Tap", label: "", value: nil)
      self.saveRide()
    }
    else {
      self.delegatePlateNumber()
    }
    return
  }

  func skipAction() {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Skip Plate Number Tap", label: "", value: nil)
    
    //nullify plate values
    plateNumberView.plateNumberTextField.text = nil
    self.viewModel.plateNumber = nil
    //call save
    self.saveRide()
  }
}

//======================================================
// MARK: - ViewTapDelegate
//======================================================
extension CreateRideViewController: ViewTapDelegate {
  public func didTapToCancel() {
    headerView.resignFirstResponder()
    plateNumberView.resignFirstResponder()
    searchView.resignFirstResponder()
    searchTextField.resignFirstResponder()
  }
}

//======================================================
// MARK: A L E R T S
//======================================================
extension CreateRideViewController {
  func failedToLoadModelAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToLoadModelsAlertTitle,
      message: viewModel.alertViewFailToLoadModelsAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.alertViewFailToLoadModelsAlertActionButtonText,
      style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        self.navigationController?.popViewControllerAnimated(true)
    }

    alertController.addAction(cancelAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

//======================================================
// MARK:  N O   D A T A   V I E W   D E L E G A T E
//======================================================
extension CreateRideViewController: VehicleNoDataViewDelegate {
  public func didTapAddButton(text: String) {
    if text.isEmpty {
      displayCantAddEmptyValueAlert()
      return
    }

    let prevMode = self.viewModel.viewMode
    let shouldContinueProcess: Bool = viewModel.selectItemAtIndexPath(nil, value: text)
    if self.viewModel.fullProcess {
      if prevMode == .Make {
        if viewModel.uploadImageData == nil {
          headerView.carImageView.sd_setImageWithURL(APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath), placeholderImage: UIImage(named: viewModel.rideIconName),
            completed: { (image: UIImage!, error: NSError!, cash: SDImageCacheType, url: NSURL!) -> Void in
              if image != nil && error == nil {
                self.headerView.carImageView.backgroundColor = UIColor.whiteColor()
              } else {
                self.headerView.carImageView.backgroundColor = UIColor.clearColor()
              }
            }
          )
        }

        noDataView.state = .Model
        searchTextField.hideSearchIcon()

        self.continueProcess(shouldContinueProcess, shouldResetSearchViewPosition: false)
      } else {
        self.continueProcess(shouldContinueProcess)
      }
    } else {
      //Partial Process
      if prevMode == .Make {
        headerView.carImageView.sd_setImageWithURL(APIProvider.mediaURLForPath(viewModel.selectedMake!.imagePath), placeholderImage: UIImage(named: viewModel.rideIconName),
          completed: { (image: UIImage!, error: NSError!, cash: SDImageCacheType, url: NSURL!) -> Void in
            if image != nil && error == nil {
              self.headerView.carImageView.backgroundColor = UIColor.whiteColor()
            }
          }
        )

        noDataView.state = .Model
        searchTextField.hideSearchIcon()

        //Goto Brand
        self.continueProcess(shouldContinueProcess)
      } else {
        //Delegate value back
        self.handleDelegations(prevMode)
      }
    }
  }

  func displayCantAddEmptyValueAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewEmptyValueAlertTitle,
      message: viewModel.alertViewEmptyValueAlertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: viewModel.alertViewEmptyValueAlertActionButtonText,
      style: UIAlertActionStyle.Destructive, handler: nil)
    alertController.addAction(okAction)

    self.presentViewController(alertController, animated: true, completion: nil)
  }
}
