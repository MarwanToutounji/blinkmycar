//
//  VehiclesMenuViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import ORStackView

protocol VehiclesMenuViewControllerDelegate {
  func vehicleMenuViewControllerDidSelectVehicle(vehicle: Vehicle?)
  func vehicleMenuViewControllerDidTapOnAddVehicle()
}

class VehiclesMenuViewController: UIViewController, VehicleListingViewDelegate {

  @IBOutlet weak var scrollStackView: ORStackScrollView!
  var vehiclesListingView: VehicleListingView!

  private var viewModel = VehiclesMenuViewModel()

  var delegate: VehiclesMenuViewControllerDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = viewModel.switchVehicleViewControllerTitle
    self.sendScreenView("Change Vehicle")
    setupView()

    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(VehiclesMenuViewController.reloadVehiclesList),
      name: BMCNotificationNames.DidChangeCurrentlySelectedVehicle.rawValue,
      object: nil)
    NSNotificationCenter.defaultCenter().addObserver(
        self,
        selector: #selector(VehiclesMenuViewController.reloadVehiclesList),
        name: BMCNotificationNames.DidUpdateVehicleListing.rawValue,
        object: nil)
  }

  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  func setupView() {

    // Add the Car listing
    vehiclesListingView = NSBundle.mainBundle().loadNibNamed("VehicleListingView",
      owner: nil, options: nil)[0] as! VehicleListingView
    vehiclesListingView.delegate = self
    vehiclesListingView.vehicles = viewModel.vehicles
    vehiclesListingView.selectedVehicle = viewModel.selectedVehicle
    scrollStackView.stackView.addSubview(vehiclesListingView,
      withPrecedingMargin: 0, sideMargin: 0)

    // Add the Add Vehicle button
    let button = BMCOrangeButton()
    button.setTitle(viewModel.addVehicleButtonText, forState: UIControlState.Normal)
    button.addTarget(self, action: #selector(VehiclesMenuViewController.didTapOnAddVehicleButton(_:)),
      forControlEvents: UIControlEvents.TouchUpInside)
    button.constrainHeight("44")
    scrollStackView.stackView.addSubview(button, withPrecedingMargin: 30, sideMargin: 30)

    // Add Empty Space to the bottom
    let spaceView = UIView(frame: CGRectZero)
    scrollStackView.stackView.addSubview(spaceView, withPrecedingMargin: 30)
  }

  func reloadVehiclesList() {
    viewModel.reloadVehicles()

    vehiclesListingView.vehicles = viewModel.vehicles
    vehiclesListingView.selectedVehicle = viewModel.selectedVehicle
  }

  //=======================================================================
  // MARK: A C T I O N S
  //=======================================================================

  func didTapOnAddVehicleButton(sender: AnyObject?) {
    delegate?.vehicleMenuViewControllerDidTapOnAddVehicle()
  }

  //=======================================================================
  // MARK: V E H I C L E   L I S T I N G   V I E W   D E L E G A T E
  //=======================================================================

  func vehicleListingViewDidSelectVehicle(vehicle: Vehicle?) {
    self.trackEvent(viewModel.analyticsCategory.rawValue, action: "Change Vehicle Tap", label: "", value: nil)
    
    let delay = 250 * Double(NSEC_PER_MSEC)
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
    dispatch_after(time, dispatch_get_main_queue()) {
      self.delegate?.vehicleMenuViewControllerDidSelectVehicle(vehicle)
    }
  }

  //=======================================================================
  // MARK: H E L P E R S
  //=======================================================================

  private func scrollToTop() {
    scrollStackView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
  }
}
