//
//  BookingHistoryViewController.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/6/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

class BookingHistoryViewController: UIViewController {
  var viewModel: BookingHistoryViewModel = BookingHistoryViewModel()
  
  private var isPageRefreshing = false
  
  @IBOutlet weak var tableView: UITableView!
  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.title
    self.sendScreenView("History")
    
    // Dynamic table view cell height properties
    tableView.estimatedRowHeight = 50
    tableView.rowHeight = UITableViewAutomaticDimension
    
    // Register table view cell
    tableView.registerClass(
      BookingHistoryTableViewCell.self,
      forCellReuseIdentifier: BookingHistoryTableViewCell.identifier)
    
    SwiftLoader.show(animated: true)
    viewModel.loadBookingHistory { (success) -> Void in
      SwiftLoader.hide()
      if success {
       self.tableView.reloadData()
      }
      self.handleNoDataView()
    }
  }
  
  func didTapCancel() {
    self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func handleNoDataView() {
    if self.viewModel.noData {
      let headerView: UIView = UIView(frame: CGRectMake(0, 0,tableView.frame.width, 200))
      headerView.constrainHeight("200")
      headerView.constrainWidth("\(tableView.frame.width)")
      
      //create a lable size to fit the Table View
      let messageLbl: UILabel = BMCListItemLabel()
      //set the message
      messageLbl.text = viewModel.bookingHistoryNoRecordsMessage
      //center the text
      messageLbl.textAlignment = NSTextAlignment.Center
      messageLbl.numberOfLines = 0
      headerView.addSubview(messageLbl)
      
      messageLbl.alignCenterXWithView(headerView, predicate: "0")
      messageLbl.alignCenterYWithView(headerView, predicate: "15")
      
      tableView.tableHeaderView = headerView
    }
    else {
      tableView.tableHeaderView = nil
    }
    
  }

}

extension BookingHistoryViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows()
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: BookingHistoryTableViewCell = tableView.dequeueReusableCellWithIdentifier(
      BookingHistoryTableViewCell.identifier, forIndexPath: indexPath) as! BookingHistoryTableViewCell
    cell.setBooking(viewModel.bookingForRow(indexPath.row))
    cell.selectionStyle = UITableViewCellSelectionStyle.None
    cell.userInteractionEnabled = false

    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
}

//=================================================================
// MARK: - Scroll View Delegate
//=================================================================

extension BookingHistoryViewController {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height))
    {
      if !self.isPageRefreshing {
        if self.viewModel.hasNextPage() {
          self.isPageRefreshing = true
          SwiftLoader.show(animated: true)
          //MAKE NEXT PAGE API CALL AND REFRESH VIEW
          self.viewModel.fetchNextPage({ (success, error) -> Void in
            SwiftLoader.hide()
            if success {
              self.tableView.reloadData()
            }
            self.isPageRefreshing = false
          })
        }
      }
    }
  }
}
