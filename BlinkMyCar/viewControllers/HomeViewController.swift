//
//  HomeViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/6/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import DrawerController
import SwiftLoader

class HomeViewController: DrawerController {

  private var viewModel = HomeViewModel()

  private var vehicleMenuButton: UIButton!

  private var viewDidAppearAtLeastOnce = false

  override func viewDidLoad() {
    super.viewDidLoad()

    self.animationVelocity = 500

    self.rightDrawerViewController = rightMenuVC
    self.leftDrawerViewController = leftMenuVC
    self.centerViewController = centerNavigationVC

    self.maximumLeftDrawerWidth = self.view.frame.width - 50
    self.maximumRightDrawerWidth = self.view.frame.width - 50

    self.openDrawerGestureModeMask = OpenDrawerGestureMode.All
    self.closeDrawerGestureModeMask = CloseDrawerGestureMode.All

    NSNotificationCenter.defaultCenter().removeObserver(self)

    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(HomeViewController.reloadCenter),
      name: BMCNotificationNames.ShouldRefreshApplicationRootVC.rawValue, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(HomeViewController.reloadCenter),
      name: BMCNotificationNames.DidUpdateBookings.rawValue, object: nil)

    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(HomeViewController.reloadCenter),
      name: BMCNotificationNames.DidChangeCurrentlySelectedVehicle.rawValue,
      object: nil)

    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(HomeViewController.refreshApplication),
      name: BMCNotificationNames.RefreshApplication.rawValue,
      object: nil)
    
    // Register for push notifications
    (UIApplication.sharedApplication().delegate as! AppDelegate).registerForNotifications()
  }

  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)

    if !viewDidAppearAtLeastOnce {
      viewDidAppearAtLeastOnce = true
      refreshApplication()
    }

    takeControlOverShowHomeVCNotification()
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  //=================================================================
  // MARK: - Center VC related Methods
  //=================================================================

  /// This method triggers a sync operation. When the sync is done the
  /// center vc is reloaded
  func refreshApplication() {
    SwiftLoader.show(animated: true)
    PersistenceManager.sharedManager.syncData { (errors: [NSError]?) -> Void in
      if let errors = errors
        where errors.count > 0 {
          // TODO: Display alert maybe ?
      }

      SwiftLoader.hide()
      self.reloadCenter()
    }
  }

  
  func reloadCenter() {
    var needsReload = true
    
    let navigationVC = centerNavigationVC
    var vcs = navigationVC.viewControllers

    
    let rootVC = updatedRootVCForCurrentState()

    if let currentRootVC = vcs.first
      where currentRootVC.dynamicType == rootVC.dynamicType {
      if let currentRootVC = currentRootVC as? ReloadableViewController
        where !currentRootVC.needsReload() {
         needsReload = false
      }
    }
    
    if needsReload {
      rootVC.navigationItem.rightBarButtonItem = rightBarButtonItem
      rootVC.navigationItem.leftBarButtonItem = leftBarButtonItem
      
      if vcs.count > 0 {
        vcs.removeAtIndex(0)
      }
      vcs.insert(rootVC, atIndex: 0)
      
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        navigationVC.setViewControllers(vcs, animated: false)
      })
    }
  }

  func updatedRootVCForCurrentState() -> UIViewController {
    var rootVC: UIViewController?

    if viewModel.currentVehicleHasSavedBookings {
      rootVC = storyboard?.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.UpcomingBookingsListingViewController.rawValue)
        as! UpcomingBookingsListingViewController
    } else {
      if viewModel.areBookingsForCurrentVehicleSynced { // Synced && Doesn't have bookings
        rootVC = storyboard?.instantiateViewControllerWithIdentifier(
          ViewControllerStoryboardIdentifier.ServicesViewController.rawValue)
          as! ServicesViewController
      } else {
        rootVC = storyboard?.instantiateViewControllerWithIdentifier(
          ViewControllerStoryboardIdentifier.UpcomingBookingsListingViewController.rawValue)
          as! UpcomingBookingsListingViewController
      }
    }

    return rootVC!
  }

  /// If the Navigation Controller that is holding the root VC is already loaded 
  /// then return the previousely instanciated instance
  var centerNavigationVC: UINavigationController {
    if let viewController = self.centerViewController as? UINavigationController {
      return viewController
    }

    // TODO: MARWAN - Change the root VC that is loaded at first is always the Upcoming VC
    let homeVC = storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.ServicesViewController.rawValue) as! ServicesViewController
    let navVC = UINavigationController(rootViewController: homeVC)
    // Add the Navigation Bar buttons
    homeVC.navigationItem.rightBarButtonItem = rightBarButtonItem
    homeVC.navigationItem.leftBarButtonItem = leftBarButtonItem
    return navVC
  }
  
  //=================================================================
  // MARK: - Drawer Related Methods
  //=================================================================

  var leftMenuVC: UIViewController {
    let leftMenuVC = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.DetailsMenuTableViewController.rawValue)
      as! DetailsMenuTableViewController
    leftMenuVC.detailsMenuDelegate = self
    let navVC = UINavigationController(rootViewController: leftMenuVC)
    return navVC
  }


  var rightMenuVC: UIViewController {
    let rightMenuVC = storyboard?.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.VehiclesMenuViewController.rawValue)
      as! VehiclesMenuViewController
    rightMenuVC.delegate = self
    let navVC = UINavigationController(rootViewController: rightMenuVC)
    return navVC
  }

  var rightBarButtonItem: UIBarButtonItem {
    // If the centerViewController was instanciated and is alive and is a navigation controller
    // then this means that the bar buttons can be fetched from its root VC
    if let centerVC = self.centerViewController as? UINavigationController,
      let rightBarButton = centerVC.viewControllers.first?.navigationItem.rightBarButtonItem {
        return rightBarButton
    }

    vehicleMenuButton = UIButton(type: UIButtonType.Custom)
    vehicleMenuButton.setImage(
      UIImage(named: "carMenuIcon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal),
      forState: UIControlState.Normal)
    vehicleMenuButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
    vehicleMenuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    vehicleMenuButton.addTarget(self, action: #selector(HomeViewController.didTapRightBarButtonItem(_:)),
      forControlEvents: UIControlEvents.TouchUpInside)
    let barButton = UIBarButtonItem(customView: vehicleMenuButton)
    self.navigationItem.leftBarButtonItem = barButton

    return barButton
  }

  var leftBarButtonItem: UIBarButtonItem {
    return UIBarButtonItem(title: viewModel.leftMenuBarButtonText,
      style: UIBarButtonItemStyle.Plain,
      target: self,
      action: #selector(HomeViewController.didTapLeftBarButtonItem(_:)))
  }

  //=================================================================
  // MARK: A C T I O N S
  //=================================================================

  func didTapRightBarButtonItem(sender: AnyObject?) {
    self.toggleRightDrawerSideAnimated(true,
      completion: nil)
  }

  func didTapLeftBarButtonItem(sender: AnyObject?) {
    self.trackEvent(AnalyticsCategories.LeftMenu.rawValue, action: "Menu Tap", label: "", value: nil)
    self.toggleLeftDrawerSideAnimated(true, completion: nil)
  }

  func cancelModelViewController() {
    dismissViewControllerAnimated(true, completion: nil)
  }

  /**
   When triggering a 'ShouldShowHomeVC' notification, if the home VC is visible then
   he is in charge of dismissing any view controller that is presented modally
   and is covering the HomeVC
   Otherwise the App Delegate respond to this notification and show the actual HomeVC
   */
  private func takeControlOverShowHomeVCNotification() {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    NSNotificationCenter.defaultCenter().removeObserver(
      appDelegate, name: BMCNotificationNames.ShouldShowHomeVC.rawValue, object: nil)

    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(HomeViewController.makeThisVCVisible),
      name: BMCNotificationNames.ShouldShowHomeVC.rawValue,
      object: nil)
  }

  func makeThisVCVisible() {
    self.centerNavigationVC.dismissViewControllerAnimated(true, completion: nil)
  }
}

//=========================================================================
// MARK: - Details Menu Table View Controller (left menu) | DELEGATE
//=========================================================================

extension HomeViewController: DetailsMenuTableViewControllerDelegate {
  func detailsMenuDidSelectMenuItem(menuItem: MenuItem) {
    // TODO: Do Action Here

    self.closeDrawerAnimated(true, completion: nil)

    switch menuItem {
    case .Account:
      let vc = storyboard?.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.AccountSettingsViewController.rawValue)
        as! AccountSettingsViewController
      self.centerViewController?.presentViewController(
        UINavigationController(rootViewController: vc), animated: true,
        completion: nil)
    case .CustomerSupport:
      let vc = storyboard?.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.CustomerServiceViewController.rawValue)
        as! CustomerServiceViewController
      self.centerViewController?.presentViewController(
        UINavigationController(rootViewController: vc), animated: true,
        completion: nil)
    case .Payment:
      let vc = storyboard?.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.PaymentListingViewController.rawValue)
        as! PaymentListingViewController
      self.centerViewController?.presentViewController(
        UINavigationController(rootViewController: vc), animated: true,
        completion: nil)
    case .About:
      let vc = storyboard?.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.AboutUsViewController.rawValue)
        as! AboutUsViewController
      self.centerViewController?.presentViewController(
        UINavigationController(rootViewController: vc), animated: true,
        completion: nil)
    }
  }
}

//=========================================================================
// MARK: - Vehicles Menu View Controller (right menu) | DELEGATE
//=========================================================================

extension HomeViewController: VehiclesMenuViewControllerDelegate {
  func vehicleMenuViewControllerDidSelectVehicle(vehicle: Vehicle?) {
    if let vehicle = vehicle {
      Vehicle.saveCurrentVehicle(vehicle)
    }
    self.closeDrawerAnimated(true, completion: nil)
  }

  func vehicleMenuViewControllerDidTapOnAddVehicle() {
    toggleRightDrawerSideAnimated(true, completion: { (done) -> Void in
      let chooseRideViewController = self.storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.ChooseYourRideViewController.rawValue) as! ChooseYourRideViewController
      chooseRideViewController.viewModel.analyticsCategory = AnalyticsCategories.InappChooseVehicle
      
      let navigationController = UINavigationController(rootViewController: chooseRideViewController)
      chooseRideViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: #selector(HomeViewController.cancelModelViewController))
      self.presentViewController(navigationController, animated: true, completion: nil)
    })
  }
}
