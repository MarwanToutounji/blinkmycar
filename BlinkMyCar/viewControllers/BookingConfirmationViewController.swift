//
//  BookingConfirmationViewController.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/27/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import SwiftLoader

class BookingConfirmationViewController: UIViewController {
  @IBOutlet weak var carProfileView: VehicleBasicInformationView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var priceInfoLabel: BMCPriceInfoLabel!
  @IBOutlet weak var priceActivityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var actionButton: BMCOrangeButton!
  
  var viewModel:BookingConfirmationViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Confirm")
    
    // Register cells in table view
    tableView.registerNib(
      ConfirmationMinimalTableViewCell.nib,
      forCellReuseIdentifier: ConfirmationMinimalTableViewCell.identifier)
    tableView.registerNib(
      DefaultPaymentToolTableViewCell.nib,
      forCellReuseIdentifier: DefaultPaymentToolTableViewCell.identifier)
    tableView.registerNib(
      SmallTableSectionView.nib,
      forHeaderFooterViewReuseIdentifier: SmallTableSectionView.identifier)
    tableView.registerClass(
      ServiceConfirmationTableViewCell.self,
      forCellReuseIdentifier: ServiceConfirmationTableViewCell.identifier)
    tableView.registerNib(
      PromocodeBookingConfirmationTableViewCell.nib,
      forCellReuseIdentifier: PromocodeBookingConfirmationTableViewCell.identifier)
    
    // Set Properties for dynamic cell height
    tableView.estimatedRowHeight = 30
    tableView.rowHeight = UITableViewAutomaticDimension
    // Set Properties for dynamic section header height
    tableView.sectionHeaderHeight = UITableViewAutomaticDimension
    tableView.estimatedSectionHeaderHeight = 30
    
    // Init UI
    self.view.backgroundColor = UIColor.lighterMidGray()
    tableView.tableFooterView = UIView(frame: CGRectZero)
    tableView.backgroundColor = UIColor.clearColor()
  
    priceInfoLabel.attributedText("$0", discountedPrice: nil, note: nil)
    
    // Init Vehicle Info View
    carProfileView.selectionBackgroundColor = UIColor.darkPurple()
    carProfileView.selectedState = true
    carProfileView.compactView(compactSize)
    carProfileView.vehicle = viewModel.bookingFlowViewModel.currentVehicle!
    
    // Initialize Action Button
    actionButton.setTitle(viewModel.actionButtonTitle, forState: .Normal)
    actionButton.addTarget(
      self,
      action: #selector(BookingConfirmationViewController.didTapActionButton),
      forControlEvents: .TouchUpInside)
  
    if viewModel.bookingFlowViewModel.shouldDisplayCancelBookingFlowButton {
      let cancelBookingFlow = UIBarButtonItem(
        barButtonSystemItem: UIBarButtonSystemItem.Cancel,
        target: self, action: #selector(self.dismissBookingFlow))
      self.navigationItem.rightBarButtonItem = cancelBookingFlow
    }
  }
  
  func dismissBookingFlow() {
    self.navigationController?.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  //=========================================================
  // MARK: - ACTION
  //=========================================================
  
  private var proceedWithBookingCallAfterPaymentSelection = false
  
  func didTapActionButton() {
    self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Confirm Booking Tap", label: "", value: nil)
    
    SwiftLoader.show(animated: true)
    self.performPaymentMethodCheck { (success) in
      SwiftLoader.hide()
      
      if success {
        self.addUpdateBooking()
      } else {
        // If the user doesn't have a payment tool. then success == flase
        // The payment listing VC is displayed to select/add a payment tool
        // Upon finishing that operation
        // The booking call should proceed directly
        // UNCOMMENT THIS CODE IF AFTER CHOOSING A PAYMENT METHOD THE 
        // BOOKING CONFIRMATION PROCESS CONTINUES
//        self.proceedWithBookingCallAfterPaymentSelection = true
      }
    }
  }
  
  func addUpdateBooking() {
    SwiftLoader.show(animated: true)
    self.viewModel.addUpdateBooking { (success: Bool, isNew: Bool, error: NSError?) -> Void in
      SwiftLoader.hide()
      
      if success {
        // Stop Booking Flow Timer
        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.shouldStopBookingFlowTimer.rawValue,
          object: nil)
        
        // Display additional info view
        self.displayAdditionalInfrormationView()
      } else {
        if isNew {  // Is Creating a new Booking
          self.failToCreateBookingAlert(error)
        } else {    // Is Updating existing booking
          self.failToUpdateBookingAlert(error)
        }
      }
    }
  }
  
  func getTotalPriceForSelectedServices(completion: ((success: Bool, error: NSError?) -> Void)? = nil) {
    func hidePriceLabel() {
      priceActivityIndicator.startAnimating()
      priceInfoLabel.hidden = true
    }
    
    func showPriceLabel() {
      priceActivityIndicator.stopAnimating()
      priceInfoLabel.hidden = false
    }
    
    hidePriceLabel()
    let priceInfo = viewModel.getPriceForSelectedServices {
      (success, price, discountedPrice, note, error) in
      if success {
        self.priceInfoLabel.attributedText(price, discountedPrice: discountedPrice, note: note)
      }
      showPriceLabel()
      completion?(success: success, error: error)
    }
    
    priceInfoLabel.attributedText(priceInfo.price, discountedPrice: priceInfo.discountedPrice, note: priceInfo.note)
  }
  
  func handleCancelServiceSelection() {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func handleCancelPaymentToolSelection() {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func navigateAway() {
    self.dismissViewControllerAnimated(true, completion: nil)
    self.navigationController?.popToRootViewControllerAnimated(true)
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidUpdateBookings.rawValue,
      object: nil)
  }
  
  //=========================================================
  // MARK: - ALERTS
  //=========================================================
  
  private func displayFailToCheckForPaymentMethodAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFailToCheckForPaymentMethodTitle,
      message: viewModel.alertViewFailToCheckForPaymentMethodMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewFailToCheckForPaymentMethodButtonTitle, style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func displayAdditionalInfrormationView() {
    AdditionalInstructionsViewController.showFromViewController(
      self,
      viewModel: viewModel.additionalInformationViewModel,
      delegate: self)
  }
  
  func displayBookingSuccessMessage() {
    let alertController = UIAlertController(
      title: viewModel.bookingSuccessAlertViewTitle,
      message: viewModel.bookingSuccessAlertViewMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(
      title: viewModel.bookingSuccessAlertViewPostiveButtonTitle,
      style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
        self.navigateAway()
    }
    
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func failToCreateBookingAlert(error: NSError? = nil) {
    let alertTitle = viewModel.failToCreateBookingAlertTitle
    var alertMessage = viewModel.failToCreateBookingAlertMessage
    
    if let error = error, message = error.userInfo["error"] as? String {
      alertMessage = message
    }
    
    let alertController = UIAlertController(
      title: alertTitle,
      message: alertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.failToCreateBookingAlertOkButtonTitle,
                                     style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func failToUpdateBookingAlert(error: NSError? = nil) {
    let alertTitle = viewModel.failToUpdateBookingAlertTitle
    var alertMessage = viewModel.failToUpdateBookingAlertMessage
    
    if let error = error, message = error.userInfo["error"] as? String {
      alertMessage = message
    }
    
    let alertController = UIAlertController(
      title: alertTitle,
      message: alertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let cancelAction = UIAlertAction(title: viewModel.failToUpdateBookingAlertOkButtonTitle,
                                     style: UIAlertActionStyle.Default, handler: nil)
    
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  //=========================================================
  // MARK: - HELPERS
  //=========================================================
  
  private func displayServicesVCWithSelectedServices() {
    if let vc = storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.ServicesViewController.rawValue) as? ServicesViewController {
      vc.viewModel = viewModel.servicesViewModel
      vc.delegate = self
      vc.dataSource = self
      let navigationVontroller = UINavigationController(rootViewController: vc)
      self.presentViewController(navigationVontroller, animated: true, completion: nil)
    }
  }
  
  private func displayPaymentToolsSelectionList() {
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.PaymentListingViewController.rawValue)
      as! PaymentListingViewController
    vc.delegate = self
    let cancelBarButton = UIBarButtonItem(
      title: "Cancel",
      style: UIBarButtonItemStyle.Plain,
      target: vc,
      action: #selector(PaymentListingViewController.dismiss))
    vc.navigationItem.leftBarButtonItem = cancelBarButton
    self.presentViewController(
      UINavigationController(rootViewController: vc), animated: true,
      completion: nil)
  }
  
  private func displayAddPromocodeViewController() {
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.AddPromocodeViewController.rawValue)
      as! AddPromocodeViewController
    vc.delegate = self
    vc.viewModel.preSelectedPromocode = viewModel.promocode
    let cancelBarButton = UIBarButtonItem(
      title: "Cancel",
      style: UIBarButtonItemStyle.Plain,
      target: vc,
      action: #selector(PaymentListingViewController.dismiss))
    vc.navigationItem.leftBarButtonItem = cancelBarButton
    self.presentViewController(
      UINavigationController(rootViewController: vc), animated: true,
      completion: nil)
  }
  
  func performPaymentMethodCheck(completion: (success: Bool)-> Void) {
    viewModel.hasDefaultPaymentMethod { (success, error) -> Void in
      if success {
        completion(success: true)
        return
      }
      
      if let error = error {
        switch error.code {
        case BlinkMyCarErrorCodes.MissingDefaultPaymentMethod.rawValue:
          SwiftLoader.hide()
          self.displayPaymentToolsSelectionList()
          completion(success: false)
          return
        case BlinkMyCarErrorCodes.RetrieveCardsFailed.rawValue:
          break
        default:
          break
        }
      }
      
      // If this line of code is reached then the current
      // state is on these options
      // - We couldn't Retrieve Cards to check for default
      // - An unknown error occured
      self.displayFailToCheckForPaymentMethodAlert()
      completion(success: false)
    }
  }
}


//=========================================================
// MARK: - TABLE VIEW | Data Source
//=========================================================

extension BookingConfirmationViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // Calculate price for selectedServices
    getTotalPriceForSelectedServices()
    
    return viewModel.numberOfSections
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSection(section)
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cellInfo = viewModel.cellInformationForIndexPath(indexPath)
    
    let lastCell = viewModel.isLastCellInLastSection(indexPath)
    
    switch cellInfo.section {
    case .DateAndCarLocation:
      let cell = tableView.dequeueReusableCellWithIdentifier(ConfirmationMinimalTableViewCell.identifier) as! ConfirmationMinimalTableViewCell
      cell.label.text = (cellInfo.info as? String) ?? ""
      return cell
    case .Services:
      let cell = tableView.dequeueReusableCellWithIdentifier(ServiceConfirmationTableViewCell.identifier) as! ServiceConfirmationTableViewCell
      cell.setServiceBundle(cellInfo.info as! ServiceBundle)
      if lastCell {
        cell.hideSeparator()
      } else {
        cell.showSeparator()
      }
      return cell
    case .PaymentTool:
      let cell = tableView.dequeueReusableCellWithIdentifier(DefaultPaymentToolTableViewCell.identifier) as! DefaultPaymentToolTableViewCell
      cell.paymentTool = cellInfo.info as? PaymentTool
      if lastCell {
        cell.hideSeparator()
      } else {
        cell.showSeparator()
      }
      return cell
    case .Promocode:
      let cell = tableView.dequeueReusableCellWithIdentifier(PromocodeBookingConfirmationTableViewCell.identifier) as! PromocodeBookingConfirmationTableViewCell
      cell.promocodeLabel.text = cellInfo.info as? String
      return cell
    case .Other:
      break
    }
    
    return UITableViewCell()
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerSectionView = tableView.dequeueReusableHeaderFooterViewWithIdentifier(
      SmallTableSectionView.identifier) as! SmallTableSectionView
    headerSectionView.setTitle(viewModel.sectionTitle(section))
    return headerSectionView
  }
}


//=========================================================
// MARK: - TABLE VIEW | Delegate
//=========================================================

extension BookingConfirmationViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    
    switch viewModel.sectionForCellIndexPath(indexPath) {
    case .DateAndCarLocation:
      switch indexPath.row {
      case 0:
        self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Edit Button Tap", label: "Date", value: nil)
        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.ShouldReselectDate.rawValue,
          object: nil)
        self.navigationController?.popViewControllerAnimated(true)
      case 1:
        self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Edit Button Tap", label: "Location", value: nil)
        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.ShouldReselectAddress.rawValue,
          object: nil)
        self.navigationController?.popViewControllerAnimated(true)
      default:
        break
      }
    case .Services:
      self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Edit Button Tap", label: "Service", value: nil)
      displayServicesVCWithSelectedServices()
    case .PaymentTool:
      self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Payment Tap", label: "", value: nil)
      proceedWithBookingCallAfterPaymentSelection = false
      displayPaymentToolsSelectionList()
    case .Promocode:
      self.trackEvent(AnalyticsCategories.BookingProcess.rawValue, action: "Promocode Tap", label: "", value: nil)
      displayAddPromocodeViewController()
    case .Other:
      break
    }
  }
}

//=========================================================
// MARK: - PAYMENT
//=========================================================

extension BookingConfirmationViewController: PaymentListingViewControllerDelegate {
  
  // PaymentListingViewController is displayed in two cases
  // Case 1: The user tappes on the default payment tool cell to change it
  // upon selecting the default payment tool the user is promted back to
  // this view controller.
  // Case 2: The user has no default payment method yet. Upon pressing
  // confirm booking button. the payment listing vc is displayed.
  // upon selecting/addding the default payment tool the user is promted back to
  // this view controller. And add/edit booking API is triggered
  
  func paymentListingDidSelectPaymentTool(viewController: PaymentListingViewController) {
    viewModel.refreshDefaultPaymentTool()
    self.tableView.reloadData()
    self.dismissViewControllerAnimated(true, completion: nil)
    
    if proceedWithBookingCallAfterPaymentSelection {
      proceedWithBookingCallAfterPaymentSelection = false
      addUpdateBooking()
    }
  }
  
  func paymentListingDidAddANewPaymentTool(viewController: PaymentListingViewController) {
    // This means that at this stage there is two VC on top of each others
    // PaymentListingVC && AddCardVC
    // The add card VC dismisses himself after addition
    // So here we have to dismiss the Payment Listing VC
    viewModel.refreshDefaultPaymentTool()
    self.tableView.reloadData()
    self.dismissViewControllerAnimated(true, completion: nil)
    
    if proceedWithBookingCallAfterPaymentSelection {
      proceedWithBookingCallAfterPaymentSelection = false
      addUpdateBooking()
    }
  }
  
  func paymentListingDidCancelAddingANewPaymentTool(viewController: PaymentListingViewController) {
    if proceedWithBookingCallAfterPaymentSelection {
      proceedWithBookingCallAfterPaymentSelection = false
    }
    self.dismissViewControllerAnimated(true, completion: nil)
  }
}

//=========================================================
// MARK: - ADDITIONAL INFORMATION | Delegate
//=========================================================

extension BookingConfirmationViewController: AdditionalInstructionsViewControllerDelegate {
  func additionalInfoVCDidCancelRating(
    additionalInfoVC: AdditionalInstructionsViewController, booking: Booking) {
    additionalInfoVC.dismissViewControllerAnimated(true) {
      self.navigateAway()
    }
  }
  
  func additionalInfoVCDidSubmitRating(
    additionalInfoVC: AdditionalInstructionsViewController, booking: Booking, success: Bool, error: NSError?) {
    additionalInfoVC.dismissViewControllerAnimated(true) {
      self.navigateAway()
    }
  }
}

//=========================================================
// MARK: - SERVICES VC | Delegate
//=========================================================

extension BookingConfirmationViewController: ServicesViewControllerDelegate {
  func servicesViewControllerDidTapActionButton(viewController: ServicesViewController, bookingFlowViewModel: BookingFlowViewModel) {
    viewModel.bookingFlowViewModel = bookingFlowViewModel
    self.tableView.reloadData()
    self.dismissViewControllerAnimated(true, completion: nil)
  }
}

//=========================================================
// MARK: - SERVICES VC | Data Source
//=========================================================

extension BookingConfirmationViewController: ServicesViewControllerDataSource {
  func servicesViewControllerActionButtonTitle() -> String {
    return NSLocalizedString(
      "BookingConfirmationViewController.ServicesViewController.",
      value: "Select",
      comment: "")
  }
}

//=========================================================
// MARK: - Add Promocode | Delegate
//=========================================================

extension BookingConfirmationViewController: AddPromocodeViewControllerDelegate {
  func addPromocodeDidTapApplyButton(viewController: AddPromocodeViewController, promocode: String, completionBlock: (success: Bool, error: NSError?) -> Void) {
    viewModel.promocode = promocode
    self.getTotalPriceForSelectedServices { (success, error) in
      if !success {
        self.viewModel.promocode = nil
      }
      self.tableView.reloadData()
      completionBlock(success: success, error: error)
    }
  }
  
  func addPromocodeDidTapCancel(viewController: AddPromocodeViewController) { }
  
  func addPromocodeRemovePromocode() {
    viewModel.promocode = nil
    self.tableView.reloadData()
  }
}
