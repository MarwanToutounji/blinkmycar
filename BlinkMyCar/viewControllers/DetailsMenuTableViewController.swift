//
//  DetailsMenuTableViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout

protocol DetailsMenuTableViewControllerDelegate {
  func detailsMenuDidSelectMenuItem(menuItem: MenuItem)
}

class DetailsMenuTableViewController: UITableViewController {

  private var viewModel = DetailsMenuViewModel()

  var detailsMenuDelegate: DetailsMenuTableViewControllerDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = viewModel.viewControllerTitle
    self.sendScreenView("Menu")
    
    // TODO: Detect the vehicle in question
    viewModel.vehicle = nil

    tableView.tableFooterView = UIView(frame: CGRectZero)
    
    tableView.backgroundColor = UIColor.veryLightGray()
    
    NSNotificationCenter.defaultCenter().addObserver(
      self,
      selector: #selector(DetailsMenuTableViewController.refresh),
      name: BMCNotificationNames.DidChangeCurrentlySelectedVehicle.rawValue,
      object: nil)
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  func refresh() {
    viewModel.refreshModel()
    self.tableView.reloadData()
  }

  // MARK: - Table view data source

  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSectionInTableView
  }

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsForSection(section)
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView .dequeueReusableCellWithIdentifier(
      DetailsMenuTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! DetailsMenuTableViewCell
    
    let cellInfo = viewModel.cellInformationForIndexPath(indexPath)
    cell.menuItemText = cellInfo.title
    cell.menuItemImage = cellInfo.image
    cell.imageTintColor = cellInfo.imageTintColor
    if viewModel.cellIsLastInLastSection(indexPath) {
      cell.hideSeparator()
    } else {
      cell.showSeparator()
    }
    
    return cell
  }
  
  override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView(frame: CGRect.zero)
    view.backgroundColor = UIColor.veryLightGray()
    return view
  }
  
  override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
  
  override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return viewModel.sectionHeight(section)
  }

  override func tableView(tableView: UITableView,
    didSelectRowAtIndexPath indexPath: NSIndexPath) {
      let menuItem: MenuItem = viewModel.menuItemForIndexPath(indexPath)
      sendAnalytics(menuItem)
      detailsMenuDelegate?.detailsMenuDidSelectMenuItem(menuItem)
  }
  
  func sendAnalytics(menuItem: MenuItem) {
    switch menuItem {
    case .Account:
      self.trackEvent(AnalyticsCategories.LeftMenu.rawValue, action: "My Account Tap", label: "", value: nil)
    case .CustomerSupport:
      self.trackEvent(AnalyticsCategories.LeftMenu.rawValue, action: "Customer Support Tap", label: "", value: nil)
    case .Payment:
      self.trackEvent(AnalyticsCategories.LeftMenu.rawValue, action: "Payment Tap", label: "", value: nil)
    case .About:
      self.trackEvent(AnalyticsCategories.LeftMenu.rawValue, action: "About Us Tap", label: "", value: nil)
    }
  }
}
