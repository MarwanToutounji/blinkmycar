//
//  LocationsMapViewController.swift
//  BlinkMyCar
//
//  Created by Marwan on 3/31/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import GoogleMaps


enum VelocityDirection {
  case Up
  case Down
  
  private static var previousVelocityPoint: CGPoint = CGPointZero
  private static var currentVelocityPoint: CGPoint = CGPointZero
  
  
  static func updateForVelocityPoint(velocity: CGPoint) {
    previousVelocityPoint = currentVelocityPoint
    currentVelocityPoint = velocity
  }
  
  static var direction: VelocityDirection {
    if (previousVelocityPoint.y + currentVelocityPoint.y) > 0{
      return .Down
    } else {
      return .Up
    }
  }
}



protocol LocationsMapViewDelegate {
  func locationsMapViewDidTapCenterIndicator()
  func locationsMapViewCanTransitionToMode(mode: LocationsMapViewMode) -> Bool
  func locationsMapViewDidTapEditParkingDetailsButton()
  func locationsMapViewDidTapSaveParkingDetailsButton()
  func locationsMapViewCanServiceCoordinates(coordinates: CLLocationCoordinate2D) -> Bool
  func locationsMapViewDidSelectAddress(address: Address?)
  func locationsMapViewShouldDeselectSelectedMarker() -> Bool
}

private class AddressMarker {
  var address: Address?
  var marker: GMSMarker?
  
  init(address: Address? = nil, marker: GMSMarker? = nil) {
    self.address = address
    self.marker = marker
  }
}

public enum LocationsMapViewMode {
  case MapFullScreen
  case MapViewPeek
  case ParkingDetailsPeek
  case ParkingDetailsFullScreen
  case ActiveSearch
}

public class LocationsMapViewController: UIViewController {
  
  // MARK: View Model Declaration
  //-----------------------------
  let viewModel: LocationsMapViewModel = LocationsMapViewModel()
  
  // MARK: View Controller Basic Components
  //---------------------------------------
  var searchController :UISearchController!
  var mapView: GMSMapView!
  var addressDetailsView: AddressDetailsView!
  var mapOverlayView: UIView!
  var resultsViewController: GMSAutocompleteResultsViewController?
  var mapCenterIndicatorView: MapCenterIndicator = MapCenterIndicator.instance()
  let geocoder = GMSGeocoder()
  var delegate: LocationsMapViewDelegate?
  var locationManager = CLLocationManager()
  var currentLocation: CLLocation?
  
  // MARK: Constraints Properties
  //-----------------------------
  var constraintParkingDetailsViewHeight: NSLayoutConstraint!
  var constraintParkingDetailsViewBottomToSuperBottom: NSLayoutConstraint!
  var constraintParkingDetailsViewTopToSuperTop: NSLayoutConstraint!
  var constraintParkingDetailsViewTopToSuperBottom: NSLayoutConstraint!
  var constraintMapOverlayViewToSuperTop: NSLayoutConstraint!
  var constraintMapOverlayViewToParkingDetailsTop: NSLayoutConstraint!
  
  // MARK: Values
  //-------------
  var keyboard: (isDisplayed: Bool, height: CGFloat) = (false, 0)
  let mapMinDisplayHeight: CGFloat = 190
  let zoom: Float = 16
  let cityLevelZoom: Float = 13
  let streetLevelZoom: Float = 17
  let animationDuration: NSTimeInterval = 0.4
  var canSelectMarkers = true
  var beirutCoordinates = CLLocationCoordinate2D(latitude: 33.8886, longitude: 35.4955)
  var lastKnownCoordinates: CLLocationCoordinate2D? = nil
  var displayMapCenterIndicator = true {
    didSet {
      mapCenterIndicatorView.hidden = !displayMapCenterIndicator
    }
  }
  var shouldAutoCenterMapOnCurrentLocation = false
  let kwdTimer = KWDTimer()

  var shouldReverseGeocodingBeActive: Bool {
    return currentMode != .ParkingDetailsFullScreen && !addressDetailsView.fieldsAreLocked
  }
  
  var didAppear = false
  
  private var selectedAddressMarker: AddressMarker? {
    willSet {
      if selectedAddressMarker != nil {
        let icon = UIImage(named: "markerIconOrange")?.imageWithRenderingMode(.AlwaysTemplate)
        selectedAddressMarker?.marker?.icon = icon
      }
    }
    
    didSet {
      endEditing()
      if selectedAddressMarker != nil {
        let icon = UIImage(named: "markerIcon")?.imageWithRenderingMode(.AlwaysTemplate)
        selectedAddressMarker?.marker?.icon = icon
        centerMapOnSelectedMarkerPosition()
      }
      fillParkingDetailsForSelectedAddressMarker()
    }
  }
  private var addressMarkers = [AddressMarker]()
  
  var originalMode: LocationsMapViewMode = .MapFullScreen {
    didSet {
      currentMode = originalMode
    }
  }
  
  var currentMode: LocationsMapViewMode = .MapFullScreen {
    didSet {
      // Discussion: the refresh flag is implemented for the following case
      // Given that the current view is in MapViewPeek mode.
      // If the user drags the Parking Details (P.D) view up the mode will refresh
      // itself to reset the P.D to it 'MapViewPeek'.
      // if the same constraints are not altered edited or changed, the PD view
      // Doesn't snap to the expected position.
      // That's why we force removing the constraints we want to install
      refreshViewForMode(currentMode, animated: true && didAppear, refresh: (oldValue == currentMode))
    }
  }

  public func updateMapCenterIndicator(isServiceable: Bool) {
    mapCenterIndicatorView.state = !isServiceable ? .NotValid : mapCenterIndicatorView.state
  }


  //*************************
  // MARK: - Lifecycle
  //*************************
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    view.clipsToBounds = true
    
    
    // ====== Influences the search bar behavior ======
    
    self.extendedLayoutIncludesOpaqueBars = false
    self.edgesForExtendedLayout = UIRectEdge.None
    definesPresentationContext = true
    
    
    // ====== Add Maps to view ======
    
    let camera = GMSCameraPosition.cameraWithLatitude(
      beirutCoordinates.latitude,
      longitude: beirutCoordinates.longitude,
      zoom: cityLevelZoom)
    mapView = GMSMapView.mapWithFrame(self.view.frame, camera: camera)
    mapView.settings.myLocationButton = true
    mapView.delegate = self
    self.view.addSubview(mapView)
    
    
    // ====== Add search bar to view ======
    
    resultsViewController = GMSAutocompleteResultsViewController()
    resultsViewController?.extendedLayoutIncludesOpaqueBars = false
    resultsViewController?.edgesForExtendedLayout = UIRectEdge.None
    resultsViewController?.definesPresentationContext = true
    resultsViewController?.delegate = self
    for view in resultsViewController!.view.subviews {
      if view is UITableView {
        (view as! UITableView).contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
      }
    }
    
    searchController = UISearchController(searchResultsController: resultsViewController)
    searchController.searchResultsUpdater = resultsViewController
    searchController.dimsBackgroundDuringPresentation = false
    searchController.hidesNavigationBarDuringPresentation = false
    searchController.delegate = self
    searchController.searchBar.sizeToFit()
    self.view.addSubview(searchController.searchBar)
    
    
    // ====== Add The map center indicator view ======
    addMapCenterIndicatorView()
    
    
    // ====== Add the map overlay view ======
    
    mapOverlayView = UIView(frame: CGRectZero)
    mapOverlayView.backgroundColor = UIColor.clearColor()
    let mapOverlayTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(LocationsMapViewController.handleMapOverlayRecognizer(_:)))
    let mapOverlayPanRecognizer = UIPanGestureRecognizer(target: self, action: #selector(LocationsMapViewController.handleMapOverlayRecognizer(_:)))
    mapOverlayView.addGestureRecognizer(mapOverlayTapRecognizer)
    mapOverlayView.addGestureRecognizer(mapOverlayPanRecognizer)
    self.view.addSubview(mapOverlayView)
    
    
    // ====== Add parking details view ======
    
    addressDetailsView = AddressDetailsView(frame: CGRectZero)
    let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(LocationsMapViewController.handlePan(_:)))
    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LocationsMapViewController.handleParkingDetailsTap(_:)))
    addressDetailsView.addGestureRecognizer(panGestureRecognizer)
    addressDetailsView.addGestureRecognizer(tapGestureRecognizer)
    addressDetailsView.delegate = self
    self.view.addSubview(addressDetailsView)
    
    
    // ====== Initialize Constraints ======
    
    initConstraints()
    
    
    // ====== Fill Address Info If Available ======
    
    fillParkingDetailsForSelectedAddressMarker()

    // ====== Configure location manager ======
    
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
    // A minimum distance a device must move before update event generated
    locationManager.distanceFilter = 500
    // Request permission to use location service
    locationManager.requestWhenInUseAuthorization()
    // Request permission to use location service when the app is run
    locationManager.requestAlwaysAuthorization()
    // Start the update of user's location
    locationManager.startUpdatingLocation()
  }
  
  override public func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    didAppear = true
  }
  
  override public func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    if currentMode != .ParkingDetailsPeek {
      /**
       In case there was no selected address marker then the map should center 
       On the current position
       */
      let lat = selectedAddressMarker?.address?.coordinates?.latitude ?? currentLocation?.coordinate.latitude ?? beirutCoordinates.latitude
      let lng = selectedAddressMarker?.address?.coordinates?.longitude ?? currentLocation?.coordinate.longitude ?? beirutCoordinates.longitude
      let cameraPosition = GMSCameraPosition.cameraWithLatitude(lat,
        longitude: lng, zoom: zoom)
      mapView.animateToCameraPosition(cameraPosition)
    }
    calibrateMapCenterIndicatorView(false)
    

    transitionToMode(originalMode)
  }
  
  func goToCurrentPosition() {
    shouldAutoCenterMapOnCurrentLocation = false
    centerMapOnSelectedMarkerPosition()
  }
  
  private func initConstraints() {
    // MAO VIEW
    mapView.alignTop("0", leading: "0", bottom: "0", trailing: "0", toView: self.view)
    
    // PARKINF DETAILS VIEW
    constraintParkingDetailsViewTopToSuperBottom = addressDetailsView.constrainTopSpaceToView(self.view,
      predicate: "0").first as! NSLayoutConstraint
    self.view.removeConstraint(constraintParkingDetailsViewTopToSuperBottom)
    addressDetailsView.alignLeading("0", trailing: "0", toView: self.view)
    constraintParkingDetailsViewHeight = addressDetailsView.constrainHeightToView(self.view,
      predicate: "0").first as! NSLayoutConstraint
    self.view.removeConstraint(constraintParkingDetailsViewHeight)
    constraintParkingDetailsViewBottomToSuperBottom = addressDetailsView.alignBottomEdgeWithView(self.view,
      predicate: "0").first as! NSLayoutConstraint
    constraintParkingDetailsViewTopToSuperTop = addressDetailsView.alignTopEdgeWithView(self.view,
      predicate: "0").first as! NSLayoutConstraint
    
    // MAP OVERLAY VIEW
    mapOverlayView.constrainHeightToView(self.view, predicate: "0")
    mapOverlayView.constrainWidthToView(self.view, predicate: "0")
    mapOverlayView.alignLeadingEdgeWithView(self.view, predicate: "0")
    constraintMapOverlayViewToSuperTop = mapOverlayView.alignTopEdgeWithView(self.view, predicate: "0").first as! NSLayoutConstraint
    constraintMapOverlayViewToParkingDetailsTop = mapOverlayView.alignTopEdgeWithView(addressDetailsView, predicate: "0").first as! NSLayoutConstraint
    self.view.removeConstraint(constraintMapOverlayViewToSuperTop)
  }
  
  //********************************
  // MARK: - Public API
  //********************************
  
  func getCurrentAddress() -> Address? {
    let addressDetails = addressDetailsView.getCurrentAddressDetails()
    let coordinates = selectedAddressMarker?.marker?.position
    return Address.addressFromAddressAndCoordinates(addressDetails, coordinates: coordinates)
  }
  
  var displayMapCenterIndicatorCollapsed = true {
    didSet {
      mapCenterIndicatorView.state = displayMapCenterIndicatorCollapsed ? .Collapsed : .Normal
    }
  }
  
  /**
   This method is called from the delegate, Upon tapping on the map center
   indicator view. The selectedAddressMarker is edited to take the current 
   map center position and place a marker
   */
  func setMarkerForSelectedAddressMarker() {
    let cameraTarget = mapView.camera.target
    
    if selectedAddressMarker == nil {
      selectedAddressMarker = AddressMarker()
    }
    let marker = GMSMarker(position: cameraTarget)
    let icon = UIImage(named: "markerIcon")?.imageWithRenderingMode(.AlwaysTemplate)
    marker.icon = icon
    selectedAddressMarker?.marker = marker
    centerMapOnSelectedMarkerPosition()
  }
  
  func displayMarkers(serviceable: Bool? = nil, hideSelectedMarker: Bool = false) {
    addressMarkers.forEach({
      $0.marker?.map = mapView
    })
    selectedAddressMarker?.marker?.map = hideSelectedMarker ? nil : mapView
    centerMapOnSelectedMarkerPosition(serviceable)
  }
  
  func hideMarkers() {
    addressMarkers.forEach({
      $0.marker?.map = nil
    })
  }
  
  func updateLocateYourselfVisibility(visible: Bool) {
    mapView.settings.myLocationButton = visible
  }
  
  //********************************
  // MARK: - Triggered Methods
  //********************************
  
  func handlePan(recognizer:UIPanGestureRecognizer) {
    // Dismiss keyboard is available
    endEditing()
    
    let velocity = recognizer.translationInView(self.view)
    VelocityDirection.updateForVelocityPoint(velocity)
    let translation = recognizer.translationInView(self.view)
    if let view = recognizer.view {
      view.center = CGPoint(x:view.center.x,
        y:view.center.y + translation.y)
      view.frame.size.height += (-translation.y)
    }
    if recognizer.state == UIGestureRecognizerState.Ended {
      switch VelocityDirection.direction {
      case .Up:
        if let view = recognizer.view where view.frame.height >= ((2/3) *  UIScreen.mainScreen().bounds.height) {
          transitionToMode(.ParkingDetailsFullScreen)
        } else {
          transitionToMode(.MapViewPeek)
        }
      case .Down:
        transitionToMode(.ParkingDetailsPeek)
      }
    }
    
    recognizer.setTranslation(CGPointZero, inView: self.view)
  }
  
  func handleParkingDetailsTap(recognizer: UITapGestureRecognizer) {
    endEditing()
    
    switch currentMode {
    case .MapViewPeek:
      endEditing()
    case .MapFullScreen:
      transitionToMode(.ParkingDetailsPeek)
    case .ParkingDetailsPeek:
      transitionToMode(.MapViewPeek)
    default:
      // This case should never be reachable
      return
    }
  }
  
  func handleMapOverlayRecognizer(recognizer:UIGestureRecognizer) {
    transitionToMode(.ParkingDetailsPeek)
  }
  
  //******************************
  // MARK: - Helpers methods
  //******************************
  
  func addConstraintIfNeeded(constraint: NSLayoutConstraint) {
    guard let _ = self.view.constraints.indexOf(constraint) else {
      self.view.addConstraint(constraint)
      return
    }
  }
  
  func centerMapOnSelectedMarkerPosition(serviceable: Bool? = nil) {
    if let marker = selectedAddressMarker?.marker {
      goToCoordinates(marker.position, serviceable: serviceable)
    } else if let currentLocation = currentLocation {
      goToCoordinates(currentLocation.coordinate)
    }
  }
  
  func endEditing() {
    self.view.endEditing(true)
  }
  
  var parkingDetailsEditable: Bool {
    get {
      return addressDetailsView.fieldsAreLocked
    }
    
    set {
      return addressDetailsView.fieldsAreLocked = !newValue
    }
  }
  
  func fillParkingDetailsForSelectedAddressMarker() {
    if addressDetailsView != nil {
      addressDetailsView.fillFieldsForAddress(selectedAddressMarker?.address)
      // Discussion: At this point Whatever was the state of the parking details
      // view, it should get back to Read only mode
      // Maybe locking the fields should be put in fillFieldsForAddress
      if selectedAddressMarker != nil {
        addressDetailsView.fieldsAreLocked = true
      }
    }
  }
  
  func setAddresses(addresses: [Address]?, withSelectedAddress selectedAddress: Address?) {
    guard let allAddresses = addresses else {
      return
    }
    
    for address in allAddresses {
      let addressMarker = AddressMarker(address: address, marker: GMSMarker.markerFromAddress(address))
      addressMarkers.append(addressMarker)
      
      if let selectedAddress = selectedAddress
        where selectedAddress == address {
          selectedAddressMarker = addressMarker
      }
    }
  }
  
  private func transitionToMode(mode: LocationsMapViewMode) {
    if let delegate = delegate where !delegate.locationsMapViewCanTransitionToMode(mode) {
      return
    }
    
    currentMode = mode
  }
  
  private func refreshViewForMode(display: LocationsMapViewMode,
    animated: Bool = false, refresh: Bool = false) {
      snapParkingDetailsViewToPositionForViewMode(display, animated: animated, refresh: refresh)
      refreshMapVisibleViewForViewMode(display, animated: animated)
  }
  
  public func displaySearchBar(display: Bool, animated: Bool = false) {
    
      var searchBarFrame = searchController.searchBar.frame
    
      if display {
        searchBarFrame.origin.y = 0
      } else {
        searchBarFrame.origin.y = -searchBarFrame.height
      }
      
      let animationBlock = { () -> Void in
        self.searchController.searchBar.frame = searchBarFrame
      }
      
      if animated {
        UIView.animateWithDuration(0.44,
          animations: animationBlock
        )
      } else {
        animationBlock()
      }
  }
  
  private func refreshMapVisibleViewForViewMode(display: LocationsMapViewMode,
    animated: Bool = false) {
      let currentPadding = mapView.padding
      var top: CGFloat = currentPadding.top
      var bottom: CGFloat = currentPadding.bottom
      
      switch display {
      case .ParkingDetailsPeek:
        top = 0
        bottom = addressDetailsView.headerViewHeight
      case .ParkingDetailsFullScreen:
        top = 0
        bottom = 0
      case .MapViewPeek:
        top = 0
        bottom = mapView.frame.height - mapMinDisplayHeight
      case .ActiveSearch:
        top = 0
        bottom = 0
      case .MapFullScreen:
        top = 0
        bottom = 0
      }
      
      
      let animationBlock = { () -> Void in
        self.mapView.padding = UIEdgeInsets(top: top, left: currentPadding.left,
          bottom: bottom, right: currentPadding.right)
        self.calibrateMapCenterIndicatorView()
      }
      
      if animated {
        UIView.animateWithDuration(0.44,
          animations: animationBlock
        )
      } else {
        animationBlock()
      }
  }
  
  private func snapParkingDetailsViewToPositionForViewMode(display: LocationsMapViewMode,
    animated: Bool = false, refresh: Bool = false) {
      
      let bottomConstraintConstant: CGFloat = keyboard.isDisplayed ? -keyboard.height : 0
      
      switch display {
      case .ParkingDetailsPeek:
        self.view.removeConstraint(constraintParkingDetailsViewTopToSuperBottom)
        self.view.removeConstraint(constraintParkingDetailsViewBottomToSuperBottom)
        self.view.removeConstraint(constraintMapOverlayViewToSuperTop)
        if refresh {
          self.view.removeConstraint(constraintParkingDetailsViewHeight)
          self.view.removeConstraint(constraintParkingDetailsViewTopToSuperTop)
          self.view.removeConstraint(constraintMapOverlayViewToParkingDetailsTop)
        }
        addConstraintIfNeeded(constraintParkingDetailsViewHeight)
        addConstraintIfNeeded(constraintParkingDetailsViewTopToSuperTop)
        addConstraintIfNeeded(constraintMapOverlayViewToParkingDetailsTop)
        let topConstraintConstant = self.view.frame.height - addressDetailsView.headerViewHeight
        constraintParkingDetailsViewTopToSuperTop.constant = topConstraintConstant
      case .ParkingDetailsFullScreen:
        self.view.removeConstraint(constraintParkingDetailsViewTopToSuperBottom)
        self.view.removeConstraint(constraintParkingDetailsViewHeight)
        self.view.removeConstraint(constraintMapOverlayViewToParkingDetailsTop)
        if refresh {
          self.view.removeConstraint(constraintParkingDetailsViewBottomToSuperBottom)
          self.view.removeConstraint(constraintMapOverlayViewToSuperTop)
          self.view.removeConstraint(constraintParkingDetailsViewTopToSuperTop)
        }
        addConstraintIfNeeded(constraintParkingDetailsViewBottomToSuperBottom)
        addConstraintIfNeeded(constraintMapOverlayViewToSuperTop)
        addConstraintIfNeeded(constraintParkingDetailsViewTopToSuperTop)
        let topConstraintConstant: CGFloat = 0
        constraintParkingDetailsViewTopToSuperTop.constant = topConstraintConstant
        constraintParkingDetailsViewBottomToSuperBottom.constant = bottomConstraintConstant
      case .MapViewPeek:
        self.view.removeConstraint(constraintParkingDetailsViewTopToSuperBottom)
        self.view.removeConstraint(constraintParkingDetailsViewHeight)
        self.view.removeConstraint(constraintMapOverlayViewToParkingDetailsTop)
        if refresh {
          self.view.removeConstraint(constraintParkingDetailsViewTopToSuperTop)
          self.view.removeConstraint(constraintParkingDetailsViewBottomToSuperBottom)
          self.view.removeConstraint(constraintMapOverlayViewToSuperTop)
        }
        addConstraintIfNeeded(constraintParkingDetailsViewTopToSuperTop)
        addConstraintIfNeeded(constraintParkingDetailsViewBottomToSuperBottom)
        addConstraintIfNeeded(constraintMapOverlayViewToSuperTop)
        let topConstraintConstant: CGFloat = mapMinDisplayHeight
        constraintParkingDetailsViewTopToSuperTop.constant = topConstraintConstant
        constraintParkingDetailsViewBottomToSuperBottom.constant = bottomConstraintConstant
      case .ActiveSearch:
        fallthrough
      case .MapFullScreen:
        self.view.removeConstraint(constraintParkingDetailsViewTopToSuperTop)
        self.view.removeConstraint(constraintParkingDetailsViewBottomToSuperBottom)
        self.view.removeConstraint(constraintMapOverlayViewToSuperTop)
        if refresh {
          self.view.removeConstraint(constraintParkingDetailsViewHeight)
          self.view.removeConstraint(constraintMapOverlayViewToParkingDetailsTop)
          self.view.removeConstraint(constraintParkingDetailsViewTopToSuperBottom)
        }
        addConstraintIfNeeded(constraintParkingDetailsViewHeight)
        addConstraintIfNeeded(constraintMapOverlayViewToParkingDetailsTop)
        addConstraintIfNeeded(constraintParkingDetailsViewTopToSuperBottom)
      }
      
      if animated {
        UIView.animateWithDuration(0.44,
          animations: { () -> Void in
            self.view.layoutIfNeeded()
          }
        )
      } else {
        self.view.layoutIfNeeded()
      }
      
  }
  
  private func reverseGeocodeCoordinateIfNeeded(position: GMSCameraPosition) {
    if shouldReverseGeocodingBeActive {
      let handler = { (response : GMSReverseGeocodeResponse?, error: NSError?) -> Void in
        guard error == nil else { return }
        
        if let result = response?.firstResult()
          where self.shouldReverseGeocodingBeActive {
            self.addressDetailsView.fillGeocodedFieldsForAddress(result.country ?? "", city: result.administrativeArea ?? "", street: result.thoroughfare)
            return
        }
      }
      geocoder.reverseGeocodeCoordinate(position.target, completionHandler: handler)
    }
  }
  
  
  
  private func goToCoordinates(clLocation: CLLocationCoordinate2D, serviceable: Bool? = nil) {
    if let _ = mapView {
      let zoomLevel = (serviceable ?? true) ? ((mapView.camera.zoom > streetLevelZoom) ? mapView.camera.zoom : streetLevelZoom) : cityLevelZoom
      let cameraPosition = GMSCameraPosition.cameraWithLatitude(clLocation.latitude,
                                                                longitude: clLocation.longitude, zoom: zoomLevel)
      mapView.animateToCameraPosition(cameraPosition)
    }
  }

  func mapVisibleViewFrame() -> CGRect {
    let padding = self.mapView.padding
    let visibleFrameRelativeToMapView = CGRect(x: padding.left, y: padding.top,
      width: self.mapView.frame.width - (padding.left + padding.right),
      height: self.mapView.frame.height - (padding.top + padding.bottom))
    return visibleFrameRelativeToMapView
  }
  
  func mapVisibleViewCenter() -> CGPoint {
    let visibleFrame = mapVisibleViewFrame()
    return CGPoint(x: visibleFrame.origin.x + (visibleFrame.width/2),
      y: visibleFrame.origin.y + (visibleFrame.height/2))
  }
  
  func displayFillRequiredFieldsAlert() {
    let alertController = UIAlertController(
      title: viewModel.alertViewFillRequiredFieldsTitle,
      message: viewModel.alertViewFillRequiredFieldsMessage,
      preferredStyle: UIAlertControllerStyle.Alert)
    
    let okAction = UIAlertAction(title: viewModel.alertViewFillRequiredFieldsActionButtonTitle,
      style: UIAlertActionStyle.Default, handler: nil)
    alertController.addAction(okAction)
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  func areFieldsValid() -> Bool {
    return addressDetailsView.areFieldsValid()
  }
}


//****************************************
//MARK: - AddressDetailsViewDelegate
//****************************************

extension LocationsMapViewController: AddressDetailsViewDelegate {
  func addressDetailsViewDidTapEditButton() {
    delegate?.locationsMapViewDidTapEditParkingDetailsButton()
  }
  
  func addressDetailsViewDidTapSaveButton() {
    self.endEditing()
    
    if !addressDetailsView.areFieldsValid() {
      displayFillRequiredFieldsAlert()
      return
    }
    
    delegate?.locationsMapViewDidTapSaveParkingDetailsButton()
  }
}


//****************************************
//MARK: - MapCenterIndicatorDelegate
//****************************************

extension LocationsMapViewController: MapCenterIndicatorDelegate {
  func mapCenterIndicatorDidReceiveTap() {
    delegate?.locationsMapViewDidTapCenterIndicator()
  }
}


//********************************
//MARK: - GMSMapViewDelegate
//********************************

extension LocationsMapViewController: GMSMapViewDelegate {
  public func mapView(mapView: GMSMapView, willMove gesture: Bool) {
    mapCenterIndicatorView.state = .Collapsed
    transitionToMode(.MapFullScreen)
    
  }
  
  public func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
    let shouldDeselectSelectedMarker = delegate?.locationsMapViewShouldDeselectSelectedMarker() ?? false
    if shouldDeselectSelectedMarker && canSelectMarkers {
      selectedAddressMarker = nil
      delegate?.locationsMapViewDidSelectAddress(nil)
      goToCoordinates(coordinate)
    }
  }
  
  public func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
    lastKnownCoordinates = position.target
    reverseGeocodeCoordinateIfNeeded(position)
    displayMapCenterIndicatorCollapsed = (originalMode == .MapViewPeek) ? true : false

    if !(delegate?.locationsMapViewCanServiceCoordinates(position.target) ?? false) {
      mapCenterIndicatorView.state = .NotValid
    }
    
    transitionToMode(originalMode)
  }
  
  public func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
    if selectedAddressMarker?.marker == marker {
      centerMapOnSelectedMarkerPosition()
      if currentMode == .ParkingDetailsPeek {
        transitionToMode(.ParkingDetailsFullScreen)
      } else {
        transitionToMode(.ParkingDetailsPeek)
      }
    }
    
    if canSelectMarkers {
      let markersArray = addressMarkers.flatMap({ $0.marker })
      for (index, loopMarker) in markersArray.enumerate() {
        if loopMarker == marker {
          selectedAddressMarker = addressMarkers[index]
          delegate?.locationsMapViewDidSelectAddress(addressMarkers[index].address)
        }
      }
      
    }
  
    return canSelectMarkers
  }

  public func didTapMyLocationButtonForMapView(mapView: GMSMapView) -> Bool {
    let status = CLLocationManager.authorizationStatus()
    if status == CLAuthorizationStatus.Denied || status == CLAuthorizationStatus.Restricted {
      showNeedPermissionAlert(self)
    }

    //Do not consume event
    return false
  }
  
  public func haveLocationPermission() -> Bool {
    let status = CLLocationManager.authorizationStatus()
    if status == CLAuthorizationStatus.Denied || status == CLAuthorizationStatus.Restricted {
      showNeedPermissionAlert(self)
      return false
    } else {
      return true
    }
  }
}


//**********************************************************
//MARK: - GMSAutocompleteResultsViewControllerDelegate
//**********************************************************

extension LocationsMapViewController: GMSAutocompleteResultsViewControllerDelegate {
  public func resultsController(resultsController: GMSAutocompleteResultsViewController,
    didAutocompleteWithPlace place: GMSPlace) {
      // Do something with the selected place.
      searchController.active = false
      goToCoordinates(place.coordinate)
  }
  
  public func resultsController(resultsController: GMSAutocompleteResultsViewController,
    didFailAutocompleteWithError error: NSError) {
      // TODO: handle the error.
      print("Error: ", error.description)
  }
  
  public func didRequestAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
  }
  
  public func didUpdateAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
  }
}


//*****************************************************
//MARK: - UISearchControllerDelegate implementation
//*****************************************************

extension LocationsMapViewController: UISearchControllerDelegate {
  public func willPresentSearchController(searchController: UISearchController) {
    //Note: This allows the search bar to take place of the navigation bar and not disappear
    navigationController?.navigationBar.translucent = true
    transitionToMode(.ActiveSearch)
  }
  
  public func didPresentSearchController(searchController: UISearchController) {
    calibrateMapCenterIndicatorView()
  }
  
  public func willDismissSearchController(searchController: UISearchController) {
    //Note: This allows the search bar to take place of the navigation bar and not disappear
    navigationController?.navigationBar.translucent = false
    transitionToMode(originalMode)
  }
  
  public func didDismissSearchController(searchController: UISearchController) {
    calibrateMapCenterIndicatorView()
  }
}

//*****************************************************
//MARK: - CLLocationManagerDelegate
//*****************************************************

extension LocationsMapViewController: CLLocationManagerDelegate {
  public func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
  {
    if (status == CLAuthorizationStatus.AuthorizedWhenInUse)
    {
      mapView.myLocationEnabled = true
      shouldAutoCenterMapOnCurrentLocation = true
    } else {
      if status == CLAuthorizationStatus.AuthorizedAlways || status == CLAuthorizationStatus.AuthorizedWhenInUse {
        // Do something here
      } else {
        // Display pop up view
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.NotDetermined
          && (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied  || status == CLAuthorizationStatus.Restricted){
            showNeedPermissionAlert(self)
        }
      }
    }
  }
  
  public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    currentLocation = locations.last
    
    if shouldAutoCenterMapOnCurrentLocation {
      kwdTimer.startTimer(self, selector: #selector(LocationsMapViewController.goToCurrentPosition))
    }
  }

  func showNeedPermissionAlert(vc: UIViewController, cancelledCompletionBlock: (()->())? = nil) {
    let alert = UIAlertController(title: NSLocalizedString("CurrentLocation.permissionNeededAlert.title", value: "Location Services Disabled", comment: "Alert title to tell the user to open the settings and enable the Location permission for the app"),
      message: NSLocalizedString("CurrentLocation.permissionNeededAlert.message", value: "Please turn on location services in \nSettings->Privacy->Location Services.", comment: "Alert message to tell the user to open the settings and enable the Location permission for the app"),
      preferredStyle: .Alert)

    alert.addAction(UIAlertAction(title: NSLocalizedString("CurrentLocation.permissionNeededAlert.cancelButtonTitle", value: "OK", comment: "Alert cancel button title"), style: .Default, handler: {action in
      //Do nothing for now
      cancelledCompletionBlock?()
    }))

    alert.addAction(UIAlertAction(title: NSLocalizedString("CurrentLocation.permissionNeededAlert.button", value: "Settings", comment: "Alert Open settings button"), style: .Default, handler: {action in
      UIApplication.sharedApplication().openURL(NSURL(string:UIApplicationOpenSettingsURLString)!)
    }))

    vc.presentViewController(alert, animated:true, completion:nil)
    
  }
}

//*********************************************
//MARK: - Maps middle pin related methods
//*********************************************

extension LocationsMapViewController {
  func addMapCenterIndicatorView() {
    mapCenterIndicatorView.delegate = self
    self.view.addSubview(mapCenterIndicatorView)
  }

  func calibrateMapCenterIndicatorView(animated: Bool = false) {
    let calibrate = {
      let mapVisibleCenter = self.mapVisibleViewCenter()
      self.mapCenterIndicatorView.center = CGPoint(
        x: mapVisibleCenter.x,
        y: mapVisibleCenter.y - self.mapCenterIndicatorView.frame.height/2)
    }
    
    if animated {
      UIView.animateWithDuration(animationDuration,
        animations: { () -> Void in
          calibrate()
      })
    } else {
      calibrate()
    }
  }
}



private extension GMSMarker {
  static func markerFromAddress(address: Address?) -> GMSMarker? {
    if let address = address,
      coordinates = address.coordinates {
      let marker = GMSMarker(position: coordinates)
      let icon = UIImage(named: "markerIconOrange")?.imageWithRenderingMode(.AlwaysTemplate)
      marker.icon = icon
      return marker
    }
    return nil
  }
}



extension Address {
  static func addressFromAddressAndCoordinates(address: Address?,
    coordinates: CLLocationCoordinate2D?) -> Address {
      return Address(addressID: address?.addressID, type: address?.type, name: address?.name, country: address?.country, city: address?.city, area: address?.area, street: address?.street, building: address?.building, userBuilding: address?.userBuilding, userFloorNumber: address?.userFloorNumber, locationType: address?.locationType, isIndoor: address?.isIndoor, floor: address?.floor, spotNumber: address?.spotNumber, comments: address?.comments, landMark: address?.landMark, voiceNote: address?.voiceNote, coordinates: coordinates, createDate: address?.createDate, updateDate: address?.updateDate)
  }
  
  static func addressFromAddressAndType(
    address: Address?,
    addressType: AddressType?) -> Address {
    return Address(addressID: address?.addressID, type: addressType, name: address?.name, country: address?.country, city: address?.city, area: address?.area, street: address?.street, building: address?.building, userBuilding: address?.userBuilding, userFloorNumber: address?.userFloorNumber, locationType: address?.locationType, isIndoor: address?.isIndoor, floor: address?.floor, spotNumber: address?.spotNumber, comments: address?.comments, landMark: address?.landMark, voiceNote: address?.voiceNote, coordinates: address?.coordinates, createDate: address?.createDate, updateDate: address?.updateDate)
  }
}
