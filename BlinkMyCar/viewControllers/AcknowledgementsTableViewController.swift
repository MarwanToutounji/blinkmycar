//
//  LegalTermsTableViewController.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 9/9/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class AcknowledgementTableViewCell: UITableViewCell {
  @IBOutlet weak var labelContent: BMCMenuLabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }

  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }

  var title: String? {
    get {
      return labelContent.text
    }
    set {
      labelContent.text = newValue
    }
  }

  static var reuseIdentifier: String {
    return "AcknowledgementTableViewCell"
  }
}

class AcknowledgementsTableViewController: UITableViewController {

  let viewModel = AcknowledgementsViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.viewControllerTitle

    self.clearsSelectionOnViewWillAppear = true

    navigationItem.leftBarButtonItem = UINavigationBar.closeBarButton(self, selector: "didTapCancelButton")
  }

  //===============================================
  // MARK: - Table view data source
  //===============================================

  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(AcknowledgementTableViewCell.reuseIdentifier, forIndexPath: indexPath) as! AcknowledgementTableViewCell

    let info = viewModel.cellInfoForRow(indexPath.row)
    cell.title = info.libraryName
    cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator

    return cell
  }

  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let vc = storyboard?.instantiateViewControllerWithIdentifier(ViewControllerStoryboardIdentifier.AcknowledgementViewController.rawValue) as! AcknowledgementViewController
    vc.viewModel = viewModel.acknowledgementViewModelForRow(indexPath.row)
    self.navigationController?.pushViewController(vc, animated: true)
  }

  //===============================================
  // MARK: A C T I O N S
  //===============================================

  func didTapCancelButton() {
    self.dismissViewControllerAnimated(true, completion: nil)
  }

}
