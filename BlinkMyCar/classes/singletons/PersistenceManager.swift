//
//  PersistenceManager.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public class PersistenceManager: NSObject {
  static public let sharedManager = PersistenceManager()
  private override init() {
    super.init()

    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(PersistenceManager.stopSynching),
      name: BMCNotificationNames.FailToRefreshToken.rawValue,
      object: nil
    )
  }
  
  lazy var currentUser: User? = {
    return User.getSavedUser()
  }()
  
  public var isSyncing = false

  private var _didLoadVehicles = false
  private var _didLoadAddresses = false
  private var _didLoadBookings = false
  private var _didLoadCards = false
  private var _didLoadCountryInformation = false
  private var _didLoadBookingsForRating = false
  private var _didLoadServices = false

  private var shouldProceed = true

  public var didLoadAddresses: Bool {
    return _didLoadAddresses
  }
  public var didLoadBookings: Bool {
    return _didLoadBookings
  }
  public var didLoadVehicles: Bool {
    return _didLoadVehicles
  }
  public var didLoadCards: Bool {
    return _didLoadCards
  }
  public var didLoadServices: Bool {
    return _didLoadServices
  }
  
  // Disctionary where the key is a vehicle ID
  var upcomingBookingsPages: [String: Page] = [String : Page]()
  
  var bookingsThatNeedRating: [Booking] = [Booking]()
  
  
  func unvalidatePageForVehicleID(vehicleId: String) {
    upcomingBookingsPages.removeValueForKey(vehicleId)
  }

  public func stopSynching() {
    shouldProceed = false
  }

  public func syncData(completionBlock: ((errors: [NSError]?) -> Void)?) {
    updateBookingInfoIfNeeded()

    var errors = [NSError]()

    shouldProceed = true
    isSyncing = true
    
    loadServices { (success, error) -> Void in
      if let error = error
        where !success {
          errors.append(error)
      }

      if !self.shouldProceed {
        self.shouldProceed = true
        self.isSyncing = false
        completionBlock?(errors: (errors.count > 0) ? errors : nil)
        return
      }
      
      self.loadVehicles { (success, error) -> Void in
        if let error = error
          where !success {
            errors.append(error)
        }

        if !self.shouldProceed {
          self.shouldProceed = true
          self.isSyncing = false
          completionBlock?(errors: (errors.count > 0) ? errors : nil)
          return
        }
        
        self.loadBookings { (success: Bool, error: NSError?) -> Void in
          if let error = error
            where !success {
              errors.append(error)
          }

          if !self.shouldProceed {
            self.shouldProceed = true
            self.isSyncing = false
            completionBlock?(errors: (errors.count > 0) ? errors : nil)
            return
          }
          
          self.loadBookingsThatNeedRating({ (success, error) in
            if !self.shouldProceed {
              self.shouldProceed = true
              self.isSyncing = false
              completionBlock?(errors: (errors.count > 0) ? errors : nil)
              return
            }
            
            self.loadAddresses({ (success: Bool, error: NSError?) -> Void in
              if let error = error
                where !success {
                errors.append(error)
              }
              
              if !self.shouldProceed {
                self.shouldProceed = true
                self.isSyncing = false
                completionBlock?(errors: (errors.count > 0) ? errors : nil)
                return
              }
              
              self.loadCards({ (success: Bool, error: NSError?) -> Void in
                if let error = error
                  where !success{
                  errors.append(error)
                }
                
                if !self.shouldProceed {
                  self.shouldProceed = true
                  self.isSyncing = false
                  completionBlock?(errors: (errors.count > 0) ? errors : nil)
                  return
                }
                
                self.loadCountryInformation({ (success: Bool, error: NSError?) -> Void in
                  if let error = error
                    where !success {
                    errors.append(error)
                  }
                  
                  self.shouldProceed = true
                  self.isSyncing = false
                  
                  completionBlock?(errors: (errors.count > 0) ? errors : nil)
                })
              })
            })
          })
        }
      }
    }
  }

  public func updateBookingInfoIfNeeded() {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
      { () -> Void in
        self.updateBookingsVehicleInformation({ () -> Void in
          self.updateBookingsAddressInformation({ () -> Void in
            dispatch_async(dispatch_get_main_queue(), { 
              NSNotificationCenter.defaultCenter().postNotificationName(
                BMCNotificationNames.DidUpdateBookings.rawValue,
                object: nil)
            })
          })
        })
    })
  }
  
  public func loadBookingsForAvailableVehicles(
    vehicleIDs: [String],
    indexOfID: Int,
    fromDate: NSDate = NSDate().yesterday,
    errors: [NSError] = [NSError](),
    completionBlock: (errors: [NSError]) -> Void) {
    
    if indexOfID >= vehicleIDs.count {
      completionBlock(errors: errors)
      return
    }
    
    let vehicleId = vehicleIDs[indexOfID]
    signedRequest(
      BlinkMyCarAPI.Bookings(vehicleId: vehicleId, from: fromDate),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        
        guard statusCode == 200 else {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.GetAllBookingsForVehicleFailed.rawValue,
            userInfo: ["error":"Could not get bookings for vehicle with id \(vehicleIDs[indexOfID])"])
          self.loadBookingsForAvailableVehicles(vehicleIDs, indexOfID: (indexOfID+1),
            fromDate: fromDate, errors: errors + [error], completionBlock: completionBlock)
          return
        }
        
        do {
          if let data = data,
            let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data,
              options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
            var bookings = Booking.bookingsFromJSON(jsonDictionary)
            
            // Get from the response the next links
            self.upcomingBookingsPages[vehicleId] = Page.pageFromJSON(jsonDictionary)
            
            // Sort Bookings by delivery date and time
            bookings = Booking.sortBookings(bookings)
            
            Booking.saveBookings(bookings, forVehicleWithID: vehicleId)
            
            self.loadBookingsForAvailableVehicles(vehicleIDs, indexOfID: (indexOfID+1),
              fromDate: fromDate, errors: errors, completionBlock: completionBlock)
            return
          }
        } catch {}
        
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.GetAllBookingsForVehicleFailed.rawValue,
          userInfo: ["error":"Could not get bookings"])
        self.loadBookingsForAvailableVehicles(vehicleIDs, indexOfID: (indexOfID+1),
          fromDate: fromDate, errors: errors + [error], completionBlock: completionBlock)
    })
  }

  public func loadBookings(completionBlock: (success: Bool, error: NSError?) -> Void) {
    _didLoadBookings = false
    upcomingBookingsPages.removeAll()
    
    let vehicleIDs = Vehicle.getSavedVehicles().map({ $0.id! })
    let fromDate = NSDate().yesterday
    loadBookingsForAvailableVehicles(vehicleIDs, indexOfID: 0, fromDate: fromDate, errors: [NSError]()) {
      errors in
      
      self._didLoadBookings = true
      
      // Remove Saved bookings that are not related to the saved vehicles
      Booking.cleanSavedBookings(vehicleIDs)
      
      completionBlock(success: (errors.count == 0), error: errors.first)
    }
  }

  public func loadVehicles(completion: (success: Bool, error: NSError?) -> Void) {
    signedRequest(BlinkMyCarAPI.AllVehicles, completion: {
      (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
      if statusCode != 200 {
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.RetrieveVehiclesFailed.rawValue,
          userInfo: ["error":"Could not get vehciles"])
        completion(success: false, error: error)
        return
      }

      self._didLoadVehicles = true

      do {
        if let data = data,
          jsonResult: AnyObject = try NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) {
              let dictionary = jsonResult as! [String : AnyObject]
              let vehicles = Vehicle.vehiclesFromJSON(dictionary)

              /*
              ** If the currently saved current vehicle is not delete from the
              ** server side then do nothing, otherwise save the first result from
              ** retrieved list above
              */

              var shouldOverrideCurrentVehicle = true
              if let currentVehicle = Vehicle.getCurrentVehicle(),
                let _ = vehicles.filter({ $0.id! == currentVehicle.id! }).first {
                  shouldOverrideCurrentVehicle = false
              }

              if let vehicle = vehicles.first where shouldOverrideCurrentVehicle {
                Vehicle.saveCurrentVehicle(vehicle)
              }

              Vehicle.saveVehicles(vehicles)


              completion(success: true, error: nil)
              return
        }
      } catch {}


      let error = NSError(domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.RetrieveVehiclesFailed.rawValue,
        userInfo: ["error":"Could not get vehciles"])
      completion(success: false, error: error)
    })
  }

  public func loadAddresses(completion: (success: Bool, error: NSError?) -> Void) {
    _didLoadAddresses = false
    signedRequest(BlinkMyCarAPI.AllAddresses, completion: { (data, statusCode, response, error) -> () in
      if statusCode != 200 {
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.RetrieveAddressesFailed.rawValue,
          userInfo: ["error":"Could not get Addresses"])
        completion(success: false, error: error)
        return
      }

      self._didLoadAddresses = true

      do {
        if let data = data,
          let addressesJSON = try NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject] {
              let addresses = Address.addressesFromJSON(addressesJSON)
              Address.saveAddresses(addresses)

              completion(success: true, error: nil)
              return
        }
      } catch {}

      let error = NSError(domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.RetrieveAddressesFailed.rawValue,
        userInfo: ["error":"Could not get Addresses"])
      completion(success: false, error: error)
    })
  }
  
  public func loadServices(completion: (success: Bool, error: NSError?) -> Void) {
    _didLoadServices = false
    signedRequest(BlinkMyCarAPI.AllServices) {
      (data, statusCode, response, error) -> () in
      if statusCode != 200 {
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.RetrieveServicesFailed.rawValue,
          userInfo: ["error": "Could not get services"])
        completion(success: false, error: error)
        return
      }
      
      self._didLoadServices = true
      
      do {
        if let data = data,
          let servicesJSON = try NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject] {
              let services = Service.smartParseServicesFromJSON(servicesJSON)
              Service.saveServices(services)
              
              completion(success: true, error: nil)
              return
        }
      } catch {}
      
      let error = NSError(domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.RetrieveServicesFailed.rawValue,
        userInfo: ["error": "Could not get services"])
      completion(success: false, error: error)
    }
  }

  public func loadCards(completion: (success: Bool, error: NSError?) -> Void) {
    _didLoadCards = false
    signedRequest(BlinkMyCarAPI.AllCards) {
      (data, statusCode, response, error) -> () in
      if statusCode != 200 {
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.RetrieveCardsFailed.rawValue,
          userInfo: ["error": "Could not get Cards"])
        completion(success: false, error: error)
        return
      }
      
      self._didLoadCards = true
      
      do {
        if let data = data,
          let cardsJSON = try NSJSONSerialization.JSONObjectWithData(
            data,
            options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject] {
          let cards = StripePayment.cardsFromJSON(cardsJSON)
          StripePayment.saveCardsList(cards)
          
          // If there is no default payment method set and the user has a default
          // Card, then set the default payment method as Stripe
          let paymentListingViewModel = PaymentViewModel()
          paymentListingViewModel.hasDefaultPaymentMethod({ (success, error) in
            if let defaultCard = StripePayment.getDefaultCard() where !success {
              PaymentMethod.saveDefaultPaymentTool(defaultCard)
            }
          })
          
          completion(success: true, error: nil)
          return
        }
      } catch {}
      
      // If this line is reached then there is an erro in the data
      let error = NSError(domain: "com.blinkmycar.network",
        code: BlinkMyCarErrorCodes.RetrieveCardsFailed.rawValue,
        userInfo: ["error": "Could not get Cards"])
      completion(success: false, error: error)
    }
  }

  public func loadCountryInformation(completion: (success: Bool, error: NSError?) -> Void) {
    _didLoadCountryInformation = false
    signedRequest(BlinkMyCarAPI.CountryInformation(countryId: "LB"),
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        if statusCode != 200 {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.RetrieveCountryInformationFailed.rawValue, userInfo: nil)
          completion(success: false, error: error)
          return
        }

        self._didLoadCountryInformation = true

        do {
          if let data = data,
            let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data,
              options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
                let country = Country.fromJSON(jsonDictionary) as! Country
                Country.saveCountry(country)
                
                // Set the stripe API key
                (UIApplication.sharedApplication().delegate as! AppDelegate).stripeID = jsonDictionary["publishable_stripe_api_key"] as? String

                completion(success: true, error: nil)
                return
          }
        } catch {}

        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.RetrieveCountryInformationFailed.rawValue, userInfo: nil)
        completion(success: false, error: error)
    })
  }
  
  public func loadBookingsThatNeedRating(completion: (success: Bool, error: NSError?) -> Void) {
    _didLoadBookingsForRating = false
    
    signedRequest(
      BlinkMyCarAPI.BookingsThatNeedRating,
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        
        guard statusCode == 200 else {
          let error = NSError(domain: "com.blinkmycar.network",
            code: BlinkMyCarErrorCodes.GetAllBookingsNeedsRating.rawValue,
            userInfo: ["error":"Could not get bookings that need rating"])
          completion(success: false, error: error)
          return
        }
        
        self._didLoadBookingsForRating = true
        
        do {
          if let data = data,
            let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data,
              options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
            
            self.bookingsThatNeedRating = Booking.bookingsFromJSON(jsonDictionary)
            
            completion(success: true, error: nil)
            return
          }
        } catch {}
        
        let error = NSError(domain: "com.blinkmycar.network",
          code: BlinkMyCarErrorCodes.GetAllBookingsNeedsRating.rawValue,
          userInfo: ["error":"Could not get bookings that need rating"])
        completion(success: false, error: error)
    })
  }

  //=================================================================
  // MARK: - Updating Booking Information
  //=================================================================

  // Addresses
  //------------

  public func updateBookingsAddressInformation(completionBlock: (() -> Void)?) {
    if let addressesIDs = Address.getAddressesIDsForUpdatingRelatedBookings()
      where addressesIDs.count > 0 {
        updateBookingsForAddresses(0, addressesIDs: addressesIDs,
          completionBlock: { (success, error) -> Void in
            completionBlock?()
        })
    }

    completionBlock?()
  }

  public func updateBookingsForAddresses(addressesIDsIndex: Int, addressesIDs: [String],
    completionBlock:((success: Bool, error: NSError?) -> Void)) {
      if addressesIDsIndex >= addressesIDs.count {
        completionBlock(success: true, error: nil)
        return
      }

      if let address = Address.getAddressWithId(addressesIDs[addressesIDsIndex]) {
        let bookingsForAddress = Booking.getSavedBookingsForAddressId(address.addressID!)
        updateBookingsAddressInfo(address, bookingIndex: 0, bookings: bookingsForAddress,
          completionBlock: { (address, success, error) -> Void in
            if !success {
              completionBlock(success: false, error: error)
              return
            }

            Address.deleteAddressIdForUpdatingRelatedBookings(address.addressID!)
            self.updateBookingsForAddresses(addressesIDsIndex + 1, addressesIDs: addressesIDs, completionBlock: completionBlock)
        })
      }
  }

  public func updateBookingsAddressInfo(address: Address, bookingIndex: Int, bookings: [Booking],
    completionBlock: ((address: Address, success: Bool, error: NSError?) -> Void)) {
      if bookingIndex >= bookings.count {
        completionBlock(address: address, success: true, error: nil)
        return
      }

      signedRequest(BlinkMyCarAPI.UpdateBookingAddress(
        bookingId: bookings[bookingIndex].bookingId!,
        name: address.name,
        area: address.area?.name,
        street: address.street,
        userBuilding: address.userBuilding,
        userFloorNumber: address.userFloorNumber,
        comment: address.comments!),
        completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
          if statusCode != 200 {
            let error = NSError(domain: "com.blinkmycar.network",
              code: BlinkMyCarErrorCodes.UpdateBookingAddressFailed.rawValue,
              userInfo: ["error":"Could not Update booking Address"])
            completionBlock(address: address, success: false, error: error)
            return
          }

          do {
            let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!,
              options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject]
            let updatedBooking = Booking.fromJSON(jsonDictionary!) as! Booking

            Booking.replaceBooking(bookings[bookingIndex], with: updatedBooking)
          } catch (let error) {
            print("Error: \(error)")
          }
          self.updateBookingsAddressInfo(address, bookingIndex: (bookingIndex+1),
            bookings: bookings, completionBlock: completionBlock)
      })
  }


  // Vehicles
  //------------

  public func updateBookingsVehicleInformation(completionBlock: (() -> Void)?) {
    if let vehiclesIDs = Vehicle.getVehicleIDsForUpdatingRelatedBookings()
      where vehiclesIDs.count > 0 {
        updateBookingsForVechilces(0, vehiclesIDs: vehiclesIDs, completionBlock:
          { (success, error) -> Void in
            completionBlock?()
        })
    }

    completionBlock?()
  }

  public func updateBookingsForVechilces(vehiclesIDsIndex: Int, vehiclesIDs: [String],
    completionBlock:((success: Bool, error: NSError?) -> Void)) {
      if vehiclesIDsIndex >= vehiclesIDs.count {
        completionBlock(success: true, error: nil)
        return
      }

      if let vehicle = Vehicle.getVehicleWithId(vehiclesIDs[vehiclesIDsIndex]) {
        let bookingsForVehicle = Booking.getSavedBookingsForVehicleId(vehicle.id!)
        updateBookingsVehicleInfo(vehicle, bookingIndex: 0, bookings: bookingsForVehicle,
          completionBlock: { (vehicle, success, error) -> Void in
            if !success {
              completionBlock(success: false, error: error)
              return
            }

            Vehicle.deleteVehicleIdForUpdatingRelatedBookings(vehicle.id!)
            self.updateBookingsForVechilces(vehiclesIDsIndex + 1, vehiclesIDs: vehiclesIDs, completionBlock: completionBlock)
        })
      }
  }

  public func updateBookingsVehicleInfo(vehicle: Vehicle, bookingIndex: Int, bookings: [Booking],
    completionBlock: ((vehicle: Vehicle, success: Bool, error: NSError?) -> Void)) {
      if bookingIndex >= bookings.count {
        completionBlock(vehicle: vehicle, success: true, error: nil)
        return
      }

      signedRequest(BlinkMyCarAPI.UpdateBookingVehicle(
        bookingId: bookings[bookingIndex].bookingId!,
        name: vehicle.name,
        color: vehicle.color,
        make: vehicle.make,
        model: vehicle.model,
        plateNumber: vehicle.plateNumber),
        completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
          print("Status Code: \(statusCode)")
          if statusCode != 200 {
            let error = NSError(domain: "com.blinkmycar.network",
              code: BlinkMyCarErrorCodes.UpdateBookingVehicleInformationFailed.rawValue,
              userInfo: ["error":"Could not Update booking Vehicle Information"])
            completionBlock(vehicle: vehicle, success: false, error: error)
            return
          }
          
          do {
            let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!,
              options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject]
            let updatedBooking = Booking.fromJSON(jsonDictionary!) as! Booking
            
            Booking.replaceBooking(bookings[bookingIndex], with: updatedBooking)
          } catch {}

          self.updateBookingsVehicleInfo(vehicle, bookingIndex: (bookingIndex+1), bookings: bookings, completionBlock: completionBlock)
      })
  }
  
  
  //=================================================================
  // MARK: - RATING
  //=================================================================
  
  var bookingToBeRated: Booking? {
    return bookingsThatNeedRating.first
  }
  
  /// If the booking that needs to be popped was not sent in the parameter
  /// then remove the first item in the array of bookings that need rating
  func popBookingThatNeedsRating(booking: Booking? = nil) {
    if let booking = booking,
      indexOfBooking = bookingsThatNeedRating.indexOf({ $0.bookingId == booking.bookingId }) {
      bookingsThatNeedRating.removeAtIndex(indexOfBooking)
    } else {
      if bookingsThatNeedRating.count > 0 {
        bookingsThatNeedRating.removeFirst()
      }
    }
  }
  
  //=================================================================
  // MARK: - HELPERS
  //=================================================================
  
  func areBookingsForCurrentVehicleUpToDate() -> Bool {
    if let currentVehicleID = Vehicle.getCurrentVehicle()?.id {
      return upcomingBookingsPages.keys.contains(currentVehicleID)
    } else {
      return false
    }
  }
}
