//
//  Overlay.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 4/21/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

public struct Overlay {
  let polygons: [[CLLocationCoordinate2D]]
  //Later we may add other shapes...cricles etc

  public init(polygons: [[CLLocationCoordinate2D]]) {
    self.polygons = polygons
  }
  
}

//MARK: P A R S E R  - H E L P E R S
extension Overlay {
  public static func overlayFromJSON(json: JSON) -> Overlay {
    func isPolygon(shape: String) -> Bool { return shape == "polygons" }

    for (shape, subJson): (String, JSON) in json {
      if isPolygon(shape) {
        return Overlay(polygons: polygonsFromJSON(subJson))
      }
    }

    return Overlay(polygons: [])
  }

  private static func polygonFromJSON(json: JSON) -> [CLLocationCoordinate2D] {
    let areaBoundariesJson: [JSON] = json.arrayValue
    var coordinates: [CLLocationCoordinate2D] = []

    for boundaryJson: JSON in areaBoundariesJson {
      let lat = boundaryJson["lat"].doubleValue
      let lng = boundaryJson["lng"].doubleValue
      let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lng)

      coordinates.append(coordinate)
    }

    return coordinates
  }

  private static func polygonsFromJSON(json: JSON) -> [[CLLocationCoordinate2D]] {
    var coordinatesArray: [[CLLocationCoordinate2D]] = []

    let shapeJson: [JSON]
    if json.type == .String {
      shapeJson = JSON.parse(json.stringValue).arrayValue
    } else { // Type is .Array
      shapeJson = json.arrayValue
    }

    for areasJson: JSON in shapeJson {
      coordinatesArray.append(polygonFromJSON(areasJson))
    }
    
    return coordinatesArray
  }
}

extension Overlay: JSONable {

  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    let serviceAreaJson = JSON.parse(json["service_area"].stringValue)
    return overlayFromJSON(serviceAreaJson)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    let polygons = self.polygons.map {
      $0.map {
        JSON(["lat":$0.latitude, "lng":$0.longitude])
      }
    }

    return [
      "polygons" : String(polygons)
    ]
  }
}
