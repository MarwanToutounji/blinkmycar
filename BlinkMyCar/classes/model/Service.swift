//
//  Service.swift
//  BlinkMyCar
//
//  Created by Marwan on 3/16/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON


public typealias Category = String

public enum ServiceAvailability {
  case inStock
  case outOfStock
  
  var jsonKey: String {
    switch self {
    case .inStock:
      return "in_stock"
    case .outOfStock:
      return "out_of_stock"
    }
  }
  
  /// If the jsonKey doesn't have a match `ServiceAvailability.inStock` is the default value
  init(jsonKey: String?) {
    guard let jsonKey = jsonKey else {
      self = .inStock
      return
    }
    
    switch jsonKey {
    case ServiceAvailability.outOfStock.jsonKey:
      self = .outOfStock
    case ServiceAvailability.inStock.jsonKey:
      fallthrough
    default:
      self = .inStock
    }
  }
}

public class Service: Equatable {
  private(set) var id: String
  private(set) var availability: ServiceAvailability
  private(set) var sku: String
  private(set) var name: String
  private(set) var description: String
  private(set) var price: Float
  private(set) var category: Category
  private(set) var needKeysLocation: Bool
  private(set) var isCore: Bool
  private(set) var vehicleType: Make.VehicleType
  private(set) var addOns: [Service]
  private(set) var parentId: String?
  
  init(id: String, availability: ServiceAvailability, sku: String, name: String, description: String, price: Float, category: Category, needKeysLocation: Bool, isCore: Bool, vehicleType: Make.VehicleType, addOns: [Service], parentId: String?) {
    self.id = id
    self.availability = availability
    self.sku = sku
    self.name = name
    self.description = description
    self.price = price
    self.category = category
    self.needKeysLocation = needKeysLocation
    self.isCore = isCore
    self.vehicleType = vehicleType
    self.addOns = addOns
    self.parentId = parentId
  }
  
  var radioDisplayName: String {
    var displayName = name
    
    if let range = name.rangeOfString(" ") {
      displayName.replaceRange(range, with: "\n")
    }
    
    return displayName
  }
}

extension Service {
  
  public static func areServicesArrayTheSame(firstArray: [Service], secondArray: [Service]) -> Bool {
    if firstArray.count != secondArray.count {
      return false
    }
    
    for service in firstArray {
      if secondArray.filter({ $0.sku == service.sku }).count == 0 {
        return false
      }
    }
    
    return true
  }
  
  /**
   Group by core services by category.
   
   - Returns:
      - **`Service`:** Core service
      - **`[Service]`:** Core services bundled together. The user cannot select
        them simultaniousely
   */
  public static func servicesByCategory(services: [Service]) -> (orderedCategories: [Category], dataSource: [Category : Any]) {
    let coreServices = services.filter({ $0.isCore == true })
    let categories: [Category] = uniqueValues(coreServices.map({ $0.category }))
    
    var orderedCategories = [Category]()
    var groupedServices = [Category : Any]()
    for category in categories {
      orderedCategories.append(category)
      let filteredServices = coreServices.filter({ $0.category == category })
      if category.lowercaseString == "cleaning" {
        groupedServices[category] = [filteredServices] as [Any]
      } else {
        groupedServices[category] = filteredServices
      }
    }
    return (orderedCategories: orderedCategories, dataSource: groupedServices)
  }
}


//=================================================================
// MARK: - Parsing
//=================================================================

extension Service: JSONable {
  public static func servicesMinimalArray(services: [Service]) -> [[String: AnyObject]]{
    var servicesArray = [[String: AnyObject]]()
    for service in services {
      var dict: [String: AnyObject] = [
        "sku" : service.sku,
        "ride_type" : service.vehicleType.rawValue
      ]
      if let parentId = service.parentId,
        let parentIdInt = Int(parentId) {
        dict["parent"] = parentIdInt
      }
      servicesArray.append(dict)
    }
    return servicesArray
  }
  
  public static func servicesArray(services: [Service]) -> [[String: AnyObject]]{
    var servicesArray = [[String: AnyObject]]()
    for service in services {
      servicesArray.append(
        service.toJSONDictionary()
      )
    }
    return servicesArray
  }
  
  public static func servicesArrayFromJSON(jsonArray: [JSON]) -> [Service] {
    var services = [Service]()
    for jsonObject in jsonArray {
      let service = serviceFromJSON(jsonObject)
      services.append(service)
    }
    return services
  }
  
  public static func serviceFromJSON(json: JSON) -> Service {
    let id: String = json["id"].stringValue
    let availbility: ServiceAvailability = ServiceAvailability(jsonKey: json["availability"].stringValue)
    let sku: String = json["sku"].stringValue
    let type = Make.VehicleType(type: json["ride_type"].stringValue)
    let name = json["name"].stringValue
    let description = json["description"].stringValue
    let category = json["category"].stringValue
    let price = json["price"].floatValue
    let needKeysLocation = json["need_keys_location"].boolValue
    let isCore = json["is_core"].boolValue
    let addOns = Service.addOnsFromServicesJSON(json)
    let parentId = json["parent_id"].string
    
    return Service(
      id: id,
      availability: availbility,
      sku: sku,
      name: name,
      description: description,
      price: price,
      category: category,
      needKeysLocation: needKeysLocation,
      isCore: isCore,
      vehicleType: type,
      addOns: addOns,
      parentId: parentId
    )
  }
  
  public static func smartParseServicesFromJSON(json: [String: AnyObject]) -> [Service] {
    let json = JSON(json)
    var services = servicesArrayFromJSON(json["_embedded"]["product"].arrayValue)
    for (index, parentService) in services.enumerate() {
      services[index].checkAndFixAddOns(services, parentService: parentService)
    }
    return services
  }
  
  func checkAndFixAddOns(originalList: [Service], parentService: Service) {
    var completedAddOns = [Service]()
    for addOn in addOns {
      // Get the original addOn as a service to get all its details
      if let originalService = originalList.filter({ $0.id == addOn.id }).first {
        let fixedAddOn = Service(id: originalService.id, availability: originalService.availability,
          sku: originalService.sku, name: originalService.name,
          description: originalService.description, price: addOn.price,
          category: originalService.category,
          needKeysLocation: originalService.needKeysLocation,
          isCore: originalService.isCore, vehicleType: originalService.vehicleType,
          addOns: addOn.addOns, parentId: parentService.id)
        completedAddOns.append(fixedAddOn)
      }
    }
    addOns = completedAddOns
  }
  
  public static func addOnsFromServicesJSON(json: JSON) -> [Service] {
    
    /**
     *  We can be eather be parsing the addOns from:
     *  - The JSON we retrieve from an API call
     *      In this case the add ons are found following this path "_embedded" -> "addons".
     *      The API response only return two valuable information "price" and "addon_id"
     *  - Saved Services in the User Default
     *      add Ons are saved as a normal Service object, so we parse it as we 
     *      are parsing any Service object
    **/
    
    var services = [Service]()
    
    var addOnsArrayValue = json["_embedded"]["addons"].arrayValue
    if addOnsArrayValue.count > 0 {
      for addOnArray in addOnsArrayValue {
        services.append(
          Service(
            id: addOnArray["addon_id"].stringValue,
            availability: ServiceAvailability(jsonKey: nil),
            sku: "",
            name: "",
            description: "",
            price: addOnArray["price"].floatValue,
            category: "",
            needKeysLocation: false,
            isCore: false,
            vehicleType: Make.VehicleType(type: ""),
            addOns: [Service](),
            parentId: nil
          )
        )
      }
    } else {
      addOnsArrayValue = json["addons"].arrayValue
      for addOnArray in addOnsArrayValue {
        services.append(Service.serviceFromJSON(addOnArray))
      }
    }
    
    return services
  }
  
  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let jsonObject = JSON(json)
    return serviceFromJSON(jsonObject)
  }
  
  public func toJSONDictionary() -> [String: AnyObject] {
    var dict: [String: AnyObject] = [
      "id" : id,
      "availability": availability.jsonKey,
      "sku" : sku,
      "ride_type" : vehicleType.rawValue,
      "name" : name,
      "description" : description,
      "category" : category,
      "price" : price,
      "need_keys_location" : needKeysLocation,
      "is_core" : isCore,
      "addons" : Service.servicesArray(addOns)
    ]
    
    if let parentId = parentId {
      dict["parent_id"] = parentId
    }
    
    return dict
  }
}


//=================================================================
// MARK: - Persistence
//=================================================================

extension Service {
  enum DefaultsKeys: String {
    case Services = "AllServices"
  }
  
  static public func getServices() -> [Service] {
    var services = [Service]()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    if let servicesDictionary = defaults.valueForKey(DefaultsKeys.Services.rawValue) as? [[String : AnyObject]] {
      for serviceDictionary in servicesDictionary {
        services.append(fromJSON(serviceDictionary) as! Service)
      }
    }
    
    return services
  }
  
  static public func getServicesForVehicleType(vehicleType: Make.VehicleType) -> [Service] {
    return getServices().filter({ $0.vehicleType == vehicleType })
  }
  
  static public func saveServices(services: [Service]) {
    var servicesArray = [[String : AnyObject]]()
    
    for service in services {
      let jsonDictionary = service.toJSONDictionary()
      servicesArray.append(jsonDictionary)
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(servicesArray,
      forKey: DefaultsKeys.Services.rawValue)
    defaults.synchronize()
    
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidUpdateSavedServices.rawValue,
      object: nil)
  }
}

public func ==(leftValue: Service, rightValue: Service) -> Bool {
  return (leftValue.id == rightValue.id) && (leftValue.parentId == rightValue.parentId)
}

func uniqueValues<T: Equatable>(collection: [T]) -> [T] {
  var uniqueCollection = [T]()
  for item in collection {
    if !uniqueCollection.contains(item) {
      uniqueCollection.append(item)
    }
  }
  return uniqueCollection
}
