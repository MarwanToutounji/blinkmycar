//
//  CashPayment.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/3/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CashPayment: PaymentTool {
  let method: PaymentMethod
  let displayedText: String
  var image: UIImage? {
    return UIImage(named: "moneyIcon")
  }
  
  init() {
    displayedText = NSLocalizedString(
      "CashOptionDisplayName",
      value: "Cash",
      comment: "")
    method = .Cash
  }
}


//=================================================================
// MARK: - Parsing
//=================================================================

extension CashPayment: JSONable {
  static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return cashFromJSON(json)
  }
  
  func toJSONDictionary() -> [String: AnyObject] {
    return [
      "name" : self.displayedText
    ]
  }
  
  // Helper  Methods
  static func cashFromJSON(json: JSON) -> CashPayment {
    return CashPayment()
  }
}