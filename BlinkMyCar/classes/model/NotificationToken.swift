//
//  NotificationToken.swift
//  BlinkMyCar
//
//  Created by Marwan on 3/2/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct NotificationToken {
  public let id: String
  public let token: String
  
  init(id: String, token: String) {
    self.id = id
    self.token = token
  }
}

//~ MARK: JSONable Protocol
extension NotificationToken: JSONable {
  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    let id = json["id"].stringValue
    let token = json["token"].stringValue
    
    return NotificationToken(id: id, token: token)
  }
  
  public func toJSONDictionary() -> [String: AnyObject] {
    return [
      "id" : self.id,
      "token" : self.token
    ]
  }
}

//~ MARK: Persistence
extension NotificationToken {
  enum DefaultsKeys: String {
    case NotificationTokenKey = "NotificationTokenKey"
  }
  
  public static func getSavedToken() -> NotificationToken? {
    if let jsonDictionary = NSUserDefaults.standardUserDefaults().valueForKey(DefaultsKeys.NotificationTokenKey.rawValue) as? [String: AnyObject],
      token = NotificationToken.fromJSON(jsonDictionary) as? NotificationToken {
        return token
    }
    return nil
  }
  
  public static func saveToken(token: NotificationToken) {
    let jsonDictionary = token.toJSONDictionary()
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(jsonDictionary,
      forKey: DefaultsKeys.NotificationTokenKey.rawValue)
    defaults.synchronize()
  }
  
  public static func deleteNotificationToken() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.NotificationTokenKey.rawValue)
    defaults.synchronize()
  }
}

