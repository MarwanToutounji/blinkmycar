//
//  Area.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Area {
  let id: String?
  let name: String?
  let enabled: Bool?
  let serviceable: Bool?

  public init(id: String? = nil, name: String? = nil, enabled: Bool? = nil, serviceable: Bool? = nil) {
    self.id = id
    self.name = name
    self.enabled = enabled
    self.serviceable = serviceable
  }
}

//=================================================================
// MARK: - Equatable
//=================================================================

extension Area: Equatable {}

public func ==(lhs: Area, rhs: Area) -> Bool {
  return lhs.id == rhs.id &&
    lhs.name == rhs.name &&
    lhs.enabled == rhs.enabled &&
    lhs.serviceable == rhs.serviceable
}

//=================================================================
// MARK: - Parsing
//=================================================================

extension Area: JSONable {
  public static func areasFromJSON(json: [String: AnyObject]) -> [Area] {
    var array = [Area]()
    let json = JSON(json)
    let areasJSON = json["_embedded"]["area"]
    for (_, subJson): (String, JSON) in areasJSON {
      let area = areaFromJSON(subJson)
      array.append(area)
    }
    return array
  }

  public static func areaFromJSON(json: JSON) -> Area {
    let id = json["id"].stringValue
    let name = json["name"].stringValue
    let enabled = json["enabled"].boolValue
    let serviceable = json["serviceable"].boolValue

    return Area(id: id, name: name, enabled: enabled, serviceable: serviceable)
  }

  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return areaFromJSON(json)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    return [:]
  }
}