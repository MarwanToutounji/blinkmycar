//
//  JSONable.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/2/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public protocol JSONable {
  static func fromJSON(json: [String: AnyObject]) -> JSONable
  func toJSONDictionary() -> [String: AnyObject]
}
