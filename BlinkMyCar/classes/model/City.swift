//
//  City.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct City {
  let id: String?
  let name: String?
  let enabled: Bool?

  public init(id: String? = nil, name: String? = nil, enabled: Bool? = nil) {
    self.id = id
    self.name = name
    self.enabled = enabled
  }
}

extension City: Equatable {}

public func ==(lhs: City, rhs: City) -> Bool {
  return lhs.id == rhs.id &&
    lhs.name == rhs.name &&
    lhs.enabled == rhs.enabled
}

extension City: JSONable {
  public static func citiesFromJSON(json: [String: AnyObject]) -> [City] {
    var citiesArray = [City]()
    let json = JSON(json)
    let citiesJSON = json["_embedded"]["city"]
    for (_, subJson): (String, JSON) in citiesJSON {
      let city = cityFromJSON(subJson)
      citiesArray.append(city)
    }
    return citiesArray
  }

  public static func cityFromJSON(json: JSON) -> City {
    let id = json["id"].stringValue
    let name = json["name"].stringValue
    let enabled = json["enabled"].boolValue

    return City(id: id, name: name, enabled: enabled)
  }

  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return cityFromJSON(json)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    return [:]
  }
}