//
//  User.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/2/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct User {
  public let id: Int
  public let email: String
  public let firstName: String
  public let lastName: String
  public let mobileNumber: String
  public let profilePicture: String
  
  lazy var hashedId: String = {
    return String.md5(string: "\(self.id)")
  }()

  public init(id: Int, email: String, firstName: String, lastName: String, mobileNumber: String, profilePicture: String) {
    self.id = id
    self.email = email
    self.firstName = firstName
    self.lastName = lastName
    self.mobileNumber = mobileNumber
    self.profilePicture = profilePicture
  }
}

extension User: JSONable {
  public static func fromJSON(json: [String : AnyObject]) -> JSONable {
    let json = JSON(json)

    let id = json["id"].intValue
    let email = json["email"].stringValue
    let firstName = json["first_name"].stringValue
    let lastName = json["last_name"].stringValue
    let mobileNumber = json["mobile_number"].stringValue
    let profilePicture = json["profile_picture"].stringValue

    return User(id: id,
      email: email,
      firstName: firstName,
      lastName: lastName,
      mobileNumber: mobileNumber,
      profilePicture: profilePicture)
  }

  public func toJSONDictionary() -> [String : AnyObject] {
    return [
      "id" : self.id,
      "email" : self.email,
      "first_name" : self.firstName,
      "last_name" : self.lastName,
      "mobile_number" : self.mobileNumber,
      "profile_picture" : self.profilePicture
    ]
  }
}

extension User {
  enum DefaultsKeys: String {
    case SignInUserKey = "SignInUserKey"
  }

  public static func getSavedUser() -> User? {
    if let jsonDictionary = NSUserDefaults.standardUserDefaults().valueForKey(
      DefaultsKeys.SignInUserKey.rawValue) as? [String:AnyObject],
      user = User.fromJSON(jsonDictionary) as? User {
        return user
    }
    return nil
  }

  public static func saveUser(user: User) {
    let jsonDictionary = user.toJSONDictionary()
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(jsonDictionary,
      forKey: DefaultsKeys.SignInUserKey.rawValue)
    defaults.synchronize()
  }

  public static func deleteSavedUser() {
    NSUserDefaults.standardUserDefaults().removeObjectForKey(
      DefaultsKeys.SignInUserKey.rawValue)
  }
}

