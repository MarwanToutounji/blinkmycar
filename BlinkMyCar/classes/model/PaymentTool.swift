//
//  PaymentMethod.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/3/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation


/// Later on we can for example 'Paypal' ...
public enum PaymentMethod {
  case Cash
  case Stripe
  
  var jsonString: String {
    switch self {
    case .Cash:
      return "cash"
    case .Stripe:
      return "cc"
    }
  }
  
  var name: String {
    switch self {
    case .Cash:
      return "Cash"
    default:
      return "Stripe"
    }
  }
  
  init?(jsonString: String) {
    switch jsonString.lowercaseString {
    case PaymentMethod.Cash.jsonString:
      self = .Cash
    case PaymentMethod.Stripe.jsonString:
      self = .Stripe
    default:
      return nil
    }
  }
  
  static var allAvailableMethods: [PaymentMethod] {
    return [
      .Stripe,
      .Cash
    ]
  }
  
  func paymentTools() -> [PaymentTool] {
    switch self {
    case .Cash:
      return [CashPayment()]
    case .Stripe:
      return StripePayment.getCardsList().map({ $0 as PaymentTool })
    }
  }
  
  func defaultPaymentTool() -> PaymentTool? {
    return PaymentMethod.getDefaultPaymentTool()
  }
  
  static public func getPaymentToolsByMethodName() -> [String : [PaymentTool]] {
    var toolsByMethod = [String: [PaymentTool]]()
    
    for method in allAvailableMethods {
      toolsByMethod[method.name] = method.paymentTools()
    }
    
    return toolsByMethod
  }
}

//=================================================================
// MARK: - Persistence
//=================================================================

extension PaymentMethod {
  enum DefaultsKeys: String {
    case DefaultPaymentMethod = "DefaultPaymentMethod"
    case DefaultPaymentTool = "DefaultPaymentTool"
  }
  
  static private func saveDefaultPaymentMethod(method: PaymentMethod) {
    // Save Method
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(
      method.jsonString,
      forKey: DefaultsKeys.DefaultPaymentMethod.rawValue)
    defaults.synchronize()
  }
  
  static func getDefaultPaymentMethod() -> PaymentMethod? {
    let defaults = NSUserDefaults.standardUserDefaults()
    
    if let defaultMethodString = defaults.stringForKey(DefaultsKeys.DefaultPaymentMethod.rawValue) {
      return PaymentMethod(jsonString: defaultMethodString)
    }
    
    return nil
  }
  
  /// Deleting the payment method will also delete the payment tool
  static func deleteDefaultPaymentMethod() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.DefaultPaymentMethod.rawValue)
    defaults.synchronize()
    deleteDefaultPaymentTool()
    StripePayment.deleteCardsList()
  }
  
  /// If the given value is nil, the saved payment tool is removed.
  /// Otherwise, it is replaced with the new Value
  static func saveDefaultPaymentTool(tool: PaymentTool?) {
    guard let tool = tool else {
      self.deleteDefaultPaymentMethod()
      return
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(
      tool.toJSONDictionary(),
      forKey: DefaultsKeys.DefaultPaymentTool.rawValue)
    defaults.synchronize()
    
    saveDefaultPaymentMethod(tool.method)
  }
  
  static func getDefaultPaymentTool() -> PaymentTool?  {
    let defaults = NSUserDefaults.standardUserDefaults()
    
    guard let dictionary = defaults.dictionaryForKey(DefaultsKeys.DefaultPaymentTool.rawValue), let method = getDefaultPaymentMethod() else {
      return nil
    }
    
    switch method {
    case .Cash:
      return CashPayment.fromJSON(dictionary) as? PaymentTool
    case .Stripe:
      return StripePayment.fromJSON(dictionary) as? PaymentTool
    }
  }
  
  static private func deleteDefaultPaymentTool() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.DefaultPaymentTool.rawValue)
    defaults.synchronize()
  }
}


public protocol PaymentTool: JSONable {
  var method: PaymentMethod { get }
  var image: UIImage? { get }
  var displayedText: String { get }
}
