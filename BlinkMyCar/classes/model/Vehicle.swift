//
//  Vehicule.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/19/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Vehicle {
  let id: String?
  let name: String?
  let color: String?
  let imagePath: String?
  let plateNumber: String?
  let make: String?
  let model: String?
  let year: Int?
  let type: Make.VehicleType?
  let createDate: NSDate?
  let updateDate: NSDate?
  
  init(name: String,
    make: String,
    model: String,
    id: String? = nil,
    color: String? = nil,
    imagePath: String? = nil,
    plateNumber: String? = nil,
    year: Int? = nil,
    type: Make.VehicleType? = nil,
    createDate: NSDate? = nil,
    updateDate: NSDate? = nil) {
    self.id = id
    self.name = name
    self.color = color
    self.imagePath = imagePath
    self.plateNumber = plateNumber
    self.make = make
    self.model = model
    self.year = year
    self.type = type
    self.createDate = createDate
    self.updateDate = updateDate
  }

  var displayedModel: String {
    var concatenatedString = "\(make!)"
    if model?.lowercaseString != "other" {
      concatenatedString += " \(model!)"
    }
    return concatenatedString
  }
}


//=================================================================
// MARK: - Parsing
//=================================================================

extension Vehicle: JSONable {

  public static func vehiclesFromJSON(json: [String : AnyObject]) -> [Vehicle] {
    var vehiclesArray = [Vehicle]()
    let json = JSON(json)
    let vehiclesJson = json["_embedded"]["ride"]
    for (_, subJson): (String, JSON) in vehiclesJson {
      let vehicle = vehicleFromJSON(subJson)
      vehiclesArray.append(vehicle)
    }
    return vehiclesArray
  }

  public static func vehicleFromJSON(json: JSON) -> Vehicle {
    let id = String(json["id"].intValue)
    let name = json["name"].stringValue
    let color = json["color"].stringValue
    let make = json["make"].stringValue
    let model = json["model"].stringValue
    let year = json["year"].intValue
    let plateNumber = json["plate_number"].stringValue
    let type = json["type"].stringValue
    let image = json["image"].stringValue
    
    //TODO: Add all the remaining fields from json

    return Vehicle(name: name,
      make: make,
      model: model,
      id: id,
      color: color,
      imagePath: image,
      plateNumber: plateNumber,
      year: year,
      type: Make.VehicleType(type: type))
  }

  public static func fromJSON(json: [String : AnyObject]) -> JSONable {
    let json = JSON(json)
    return vehicleFromJSON(json)
  }

  public func toJSONDictionary() -> [String : AnyObject] {

    return [
      "id" : self.id ?? "",
      "name" : self.name ?? "",
      "color" : self.color ?? "",
      "make" : self.make ?? "",
      "model" : self.model ?? "",
      "year" : self.year ?? 0,
      "plate_number" : self.plateNumber ?? "",
      "type" : (self.type != nil) ? self.type!.rawValue : "",
      "image" : self.imagePath ?? ""
    ]
  }
}

//=================================================================
// MARK: - Persistence
//=================================================================

extension Vehicle {
  enum DefaultsKeys: String {
    case Vehicles = "Vehicles"
    case CurrentVehicle = "CurrentVehicle"
    case VehicleIDsForUpdatingRelatedBookings = "VehicleIDsForUpdatingRelatedBookings"
  }

  public static func getSavedVehicles() -> [Vehicle] {
    var vehicles = [Vehicle]()
    if let array = NSUserDefaults.standardUserDefaults().valueForKey(
      DefaultsKeys.Vehicles.rawValue) as? [[String: AnyObject]]{
        for vehicleDictionary in array {
          let vehicle = fromJSON(vehicleDictionary) as! Vehicle
          vehicles.append(vehicle)
        }
    }
    return vehicles
  }

  public static func saveVehicles(vehicles: [Vehicle]) {
    var vehiclesArray = [[String: AnyObject]]()

    for vehicle in vehicles {
      let jsonDictionary = vehicle.toJSONDictionary()
      vehiclesArray.append(jsonDictionary)
    }

    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(vehiclesArray,
      forKey: DefaultsKeys.Vehicles.rawValue)
    defaults.synchronize()

    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidUpdateVehicleListing.rawValue,
      object: nil)
  }

  public static func saveVehicle(vehicle: Vehicle) {
    var vehicles = getSavedVehicles()
    let index = vehicles.map({ $0.id! }).indexOf((vehicle.id!))
    if let index = index {
      //Replace
      vehicles[index] = vehicle
    } else {
      //Append
      vehicles.append(vehicle)
    }
    saveVehicles(vehicles)
  }

  public static func deleteSavedVehicles() {
    // Delete Bookings
    Booking.deleteBookings()
    
    // Then Delete Vehicles
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.Vehicles.rawValue)
    defaults.synchronize()
  }

  public static func getCurrentVehicle() -> Vehicle? {
    let defaults = NSUserDefaults.standardUserDefaults()
    if let dictionary = defaults.valueForKey(DefaultsKeys.CurrentVehicle.rawValue)
      as? [String: AnyObject] {
        return (Vehicle.fromJSON(dictionary) as? Vehicle)
    }
    return nil
  }

  public static func getVehicleWithId(vehicleId: String) -> Vehicle? {
    let vehicles = getSavedVehicles()
    return vehicles.filter({ $0.id == vehicleId }).first
  }

  public static func saveCurrentVehicle(vehicle: Vehicle) {
    let dictionary = vehicle.toJSONDictionary()

    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(dictionary, forKey: DefaultsKeys.CurrentVehicle.rawValue)
    defaults.synchronize()

    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidChangeCurrentlySelectedVehicle.rawValue,
      object: nil)
  }

  public static func deleteCurrentVehicle() {
    if let id = getCurrentVehicle()?.id {
      Booking.deleteBookings(forVehicleId: id)
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.CurrentVehicle.rawValue)
    defaults.synchronize()
  }
  
  public static func deleteVehicle(vehicleId: String) {
    Booking.deleteBookings(forVehicleId: vehicleId)
    
    var vehicles = [Vehicle]()
    if let array = NSUserDefaults.standardUserDefaults().valueForKey(
      DefaultsKeys.Vehicles.rawValue) as? [[String: AnyObject]]{
        for vehicleDictionary in array {
          let vehicle = fromJSON(vehicleDictionary) as! Vehicle
          //If Id is the same don't add to list
          if vehicle.id! != vehicleId {
            vehicles.append(vehicle)
          }
        }
    }
    //save updated list
    saveVehicles(vehicles)
  }

  public static func saveVehicleIdForUpdatingRelatedBookings(vehicleId: String) {
    var vehicleIDs: [String]! = getVehicleIDsForUpdatingRelatedBookings()
    if vehicleIDs == nil {
      vehicleIDs = [String]()
    }

    if vehicleIDs.contains(vehicleId) {
      return
    }

    vehicleIDs.append(vehicleId)

    saveVehicleIDsForUpdatingRelatedBookings(vehicleIDs)
  }

  private static func saveVehicleIDsForUpdatingRelatedBookings(vehicleIDs: [String]) {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(vehicleIDs, forKey: DefaultsKeys.VehicleIDsForUpdatingRelatedBookings.rawValue)
    defaults.synchronize()
  }

  public static func deleteVehicleIdForUpdatingRelatedBookings(vehicleId: String) {
    if let vehicleIDs = getVehicleIDsForUpdatingRelatedBookings()
     where vehicleIDs.contains(vehicleId) {
      let filteresVehicleIds = vehicleIDs.filter({ $0 != vehicleId })
      saveVehicleIDsForUpdatingRelatedBookings(filteresVehicleIds)
    }
  }

  public static func deleteVehiclesIDsForUpdatingRelatedBookings() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.VehicleIDsForUpdatingRelatedBookings.rawValue)
    defaults.synchronize()
  }

  public static func getVehicleIDsForUpdatingRelatedBookings() -> [String]? {
    if let vehicleIDs = NSUserDefaults.standardUserDefaults().valueForKey(
      DefaultsKeys.VehicleIDsForUpdatingRelatedBookings.rawValue) as? [String] {
        return vehicleIDs
    }
    return nil
  }
}

extension Vehicle: Equatable {}

public func ==(lhs: Vehicle, rhs: Vehicle) -> Bool{
  return (
    lhs.id == rhs.id
      && lhs.name == rhs.name
      && lhs.color == rhs.color
      && lhs.imagePath == rhs.imagePath
      && lhs.plateNumber == rhs.plateNumber
      && lhs.make == rhs.make
      && lhs.model == rhs.model
      && lhs.year == rhs.year
      && lhs.type == rhs.type
      && lhs.createDate == rhs.createDate
      && lhs.updateDate == rhs.updateDate
  )
}
