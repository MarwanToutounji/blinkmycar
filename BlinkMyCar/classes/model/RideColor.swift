//
//  RideColor.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/20/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class RideColor {
  public let name: String
  public let value: UIColor

  public init(name: String, value: UIColor) {
    self.name = name
    self.value = value
  }
}
