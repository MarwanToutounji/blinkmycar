//
//  ServiceTime.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ServiceTime {
  let from: Int?
  let to: Int?
  let enabled: Bool?

  public init(from: Int?, to: Int?, enabled: Bool? = nil) {
    self.from = from
    self.to = to
    self.enabled = enabled
  }

  public func jsonDictionary() -> [String: AnyObject] {
    return [
      "from": from ?? 0,
      "to": to ?? 0
    ]
  }
  
  var startTimeStringValue: String {
    if let from = from {
      return NSDate().dateWithTime(from)?.hourString ?? ""
    }
    return ""
  }

}

extension ServiceTime: Equatable {}

public func ==(lhs: ServiceTime, rhs: ServiceTime) -> Bool {
  return (
    lhs.from == rhs.from &&
    lhs.to == rhs.to
  )
}

extension ServiceTime: JSONable {
  public static func serviceTimesFromJSON(json: JSON) -> [ServiceTime] {
    var array = [ServiceTime]()
    for (_, subJson): (String, JSON) in json {
      let serviceTime = serviceTimeFromJSON(subJson)
      array.append(serviceTime)
    }
    return array
  }

  public static func serviceTimesFromJSON(json: [String: AnyObject]) -> [ServiceTime] {
    var array = [ServiceTime]()
    let json = JSON(json)
    for (_, subJson): (String, JSON) in json {
      let serviceTime = serviceTimeFromJSON(subJson)
      array.append(serviceTime)
    }
    return array
  }

  public static func serviceTimeFromJSON(json: JSON) -> ServiceTime {
    let from = json["from"].intValue
    let to = json["to"].intValue
    let enabled = json["enabled"].boolValue

    return ServiceTime(from: from, to: to, enabled: enabled)
  }

  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return serviceTimeFromJSON(json)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    return [:]
  }
}