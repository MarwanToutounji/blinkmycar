//
//  Address.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/19/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import SwiftyJSON

public enum AddressType {
  case Home
  case Office
  case Other

  init(jsonName: String?) {
    if let jsonName = jsonName {
      switch jsonName.lowercaseString {
      case "home":
        self = Home
      case "office":
        self = Office
      case "other":
        fallthrough
      default:
        self = Other
      }
    } else {
      self = Other
    }
  }
  
  static func addressTypeFromName(name: String?) -> AddressType? {
    guard let name = name else { return nil }
    
    switch name.lowercaseString {
      case "home":
        return .Home
      case "office":
        return .Office
      case "other":
        return .Other
      default:
        return nil
    }
  }

  func addressTypeName() -> String {
    switch self {
    case .Home:
      return "Home"
    case .Office:
      return "Office"
    case .Other:
      return "Other"
    }
  }

  var jsonName: String {
    switch self {
    case .Home:
      return "home"
    case .Office:
      return "office"
    case .Other:
      return "other"
    }
  }

  var icon: UIImage {
    switch self {
    case .Home:
      return UIImage(named: "houseIcon")!
    case .Office:
      return UIImage(named: "officeAddressIcon")!
    case .Other:
      return UIImage(named: "pinIcon")!
    }
  }

  var roundIcon: UIImage {
    switch self {
    case .Home:
      return UIImage(named: "homeRoundIcon")!
    case .Office:
      return UIImage(named: "officeRoundIcon")!
    case .Other:
      return UIImage(named: "otherRoundIcon")!
    }
  }
}

public enum LocationType {
  case Indoor
  case Outdoor
  case Other

  var jsonName: String {
    switch self {
    case Indoor:
      return "indoor"
    case Outdoor:
      return "outdoor"
    case Other:
      return "other"
    }
  }

  init(jsonName: String?) {
    if let jsonName = jsonName {
      switch jsonName {
      case "indoor":
        self = Indoor
      case "outdoor":
        self = Outdoor
      case "other":
        fallthrough
      default:
        self = Other
      }
    } else {
      self = Other
    }
  }
}

public class Address {
  let addressID: String?
  let type: AddressType?
  let name: String?
  let country: Country?
  let city: City?
  let area: Area?
  let street: String?
  let building: String?
  let userBuilding: String?
  let userFloorNumber: String?
  let locationType: LocationType?
  let isIndoor: Bool?
  let floor: String?
  let spotNumber: String?
  let comments: String?
  let landMark: String?
  let voiceNote: NSData?
  let coordinates: CLLocationCoordinate2D?
  let createDate: NSDate?
  let updateDate: NSDate?

  init(addressID: String? = nil, type: AddressType? = nil, name: String? = nil,
    country: Country? = nil, city: City? = nil, area: Area? = nil,
    street: String? = nil, building: String? = nil, userBuilding: String? = nil, userFloorNumber: String? = nil, locationType: LocationType? = nil,
    isIndoor: Bool? = nil, floor: String? = nil, spotNumber: String? = nil,
    comments: String? = nil, landMark: String? = nil, voiceNote: NSData? = nil,
    coordinates: CLLocationCoordinate2D? = nil, createDate: NSDate? = nil,
    updateDate: NSDate? = nil) {
      self.addressID = addressID
      self.type = type
      self.name = name
      self.country = country
      self.city = city
      self.area = area
      self.street = street
      self.building = building
      self.userBuilding = userBuilding
      self.userFloorNumber = userFloorNumber
      self.locationType = locationType
      self.isIndoor = isIndoor
      self.floor = floor
      self.spotNumber = spotNumber
      self.comments = comments
      self.landMark = landMark
      self.voiceNote = voiceNote
      self.coordinates = coordinates
      self.createDate = createDate
      self.updateDate = updateDate
  }
}

extension Address {
  var fullAddress: String? {
    var text = ""
    var separator = ""
    
    if let userBuilding = userBuilding?.trim() where !userBuilding.isEmpty {
      text = "\(userBuilding)"
      separator = ", "
    }
    
    if let comments = comments?.trim() where !comments.isEmpty {
      text += "\(separator)\(comments)"
    }
    
    return text
  }
}

//=================================================================
// MARK: - Equatable
//=================================================================

extension Address: Equatable {}

public func ==(lhs: Address, rhs: Address) -> Bool {
  var voiceNoteEquatability = true
  if let lhsVoiceNote = lhs.voiceNote,
    let rhsVoiceNote = rhs.voiceNote {
      voiceNoteEquatability = lhsVoiceNote.isEqualToData(rhsVoiceNote)
  } else if !(lhs.voiceNote == nil && rhs.voiceNote == nil) {
    voiceNoteEquatability = false
  }

  var createDateEquatability = true
  if let lhsCreateDate = lhs.createDate,
    let rhsCreateDate = rhs.createDate {
      createDateEquatability = lhsCreateDate.sameDate(rhsCreateDate)
  } else if !(lhs.createDate == nil && rhs.createDate == nil) {
    createDateEquatability = false
  }

  var updateDateEquatability = true
  if let lhsUpdateDate = lhs.updateDate,
    let rhsUpdateDate = rhs.updateDate {
      updateDateEquatability = lhsUpdateDate.sameDate(rhsUpdateDate)
  } else if !(lhs.updateDate == nil && rhs.updateDate == nil) {
    updateDateEquatability = false
  }

  return (
    lhs.addressID == rhs.addressID &&
      lhs.name == rhs.name &&
      lhs.country == rhs.country &&
      lhs.city == rhs.city &&
      lhs.area == rhs.area &&
      lhs.street == rhs.street &&
      lhs.locationType == rhs.locationType &&
      lhs.isIndoor == rhs.isIndoor &&
      lhs.building == rhs.building &&
      lhs.userBuilding == rhs.userBuilding &&
      lhs.floor == rhs.floor &&
      lhs.userFloorNumber == rhs.userFloorNumber &&
      lhs.spotNumber == rhs.spotNumber &&
      lhs.comments == rhs.comments &&
      lhs.landMark == rhs.landMark &&
      voiceNoteEquatability &&
      lhs.coordinates?.latitude == rhs.coordinates?.latitude &&
      lhs.coordinates?.longitude == rhs.coordinates?.longitude &&
      createDateEquatability &&
      updateDateEquatability
  )
}

//=================================================================
// MARK: - Persistence
//=================================================================

extension Address {
  enum DefaultsKeys: String {
    case Addresses = "AddressDefaultsKeysAddresses"
    case AddressesIDsForUpdatingRelatedBookings = "AddressesIDsForUpdatingRelatedBookings"
  }

  static public func saveAddresses(addresses: [Address]) {
    var addressesArray = [[String : AnyObject]]()

    for address in addresses {
      let jsonDictionary = address.toJSONDictionary()
      addressesArray.append(jsonDictionary)
    }

    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(addressesArray,
      forKey: DefaultsKeys.Addresses.rawValue)
    defaults.synchronize()
  }

  static public func saveAddress(address: Address) {
    // Delete an address with same id if any
    if let addressId = address.addressID {
      deleteAddressWithId(addressId)
    }

    // Save address
    var addresses = getAddresses()
    addresses.append(address)
    saveAddresses(addresses)
  }

  static public func getAddresses() -> [Address] {
    var addresses = [Address]()

    if let array = NSUserDefaults.standardUserDefaults().valueForKey(
      DefaultsKeys.Addresses.rawValue) as? [[String: AnyObject]]{
        for addressDictionary in array {
          let address = fromJSON(addressDictionary) as! Address
          addresses.append(address)
        }
    }

    addresses.sortInPlace({ ($0.name ?? "").lowercaseString < ($1.name ?? "").lowercaseString })

    return addresses
  }

  static public func deleteAddresses() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.Addresses.rawValue)
    defaults.synchronize()
  }

  static public func deleteAddressWithId(addressId: String) {
    var addresses = getAddresses()
    for (index, address) in addresses.enumerate() {
      if let savedAddressId = address.addressID
        where savedAddressId == addressId {
          addresses.removeAtIndex(index)
      }
    }
    saveAddresses(addresses)
  }

  public static func saveAddressIdForUpdatingRelatedBookings(addressId: String) {
    var addressesIDs: [String]! = getAddressesIDsForUpdatingRelatedBookings()
    if addressesIDs == nil {
      addressesIDs = [String]()
    }

    if addressesIDs.contains(addressId) {
      return
    }

    addressesIDs.append(addressId)

    saveAddressesIDsForUpdatingRelatedBookings(addressesIDs)
  }

  private static func saveAddressesIDsForUpdatingRelatedBookings(addressesIDs: [String]) {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(addressesIDs, forKey: DefaultsKeys.AddressesIDsForUpdatingRelatedBookings.rawValue)
    defaults.synchronize()
  }

  public static func deleteAddressIdForUpdatingRelatedBookings(addressId: String) {
    if let addressesIDs = getAddressesIDsForUpdatingRelatedBookings()
      where addressesIDs.contains(addressId) {
        let filteresAddressesIds = addressesIDs.filter({ $0 != addressId })
        saveAddressesIDsForUpdatingRelatedBookings(filteresAddressesIds)
    }
  }

  public static func deleteAddressesIDsForUpdatingRelatedBookings() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.AddressesIDsForUpdatingRelatedBookings.rawValue)
    defaults.synchronize()
  }

  public static func getAddressesIDsForUpdatingRelatedBookings() -> [String]? {
    if let addressesIDs = NSUserDefaults.standardUserDefaults().valueForKey(
      DefaultsKeys.AddressesIDsForUpdatingRelatedBookings.rawValue) as? [String] {
        return addressesIDs
    }
    return nil
  }


  public static func getAddressWithId(addressID: String) -> Address? {
    let addresses = getAddresses()
    return addresses.filter({ $0.addressID == addressID }).first
  }

  public static func getAddressesWithCoordinates() -> [Address] {
    let addresses = getAddresses()
    return addresses.filter({ $0.coordinates != nil })
  }
}



//=================================================================
// MARK: - Parsing
//=================================================================

extension Address: JSONable {

  static public func addressesFromJSON(json: [String: AnyObject]) -> [Address] {
    var addressesArray = [Address]()
    let json = JSON(json)
    let addressesJSON = json["_embedded"]["address"]
    for (_, subJson): (String, JSON) in addressesJSON {
      let address = addressFromJSON(subJson)
      addressesArray.append(address)
    }
    return addressesArray
  }

  static public func addressFromJSON(json: JSON) -> Address {
    let id = json["id"].stringValue
    let type =  AddressType(jsonName: json["type"].stringValue)
    let name = json["name"].stringValue

    let countryName = json["country"].stringValue
    var country: Country?
    if !countryName.isEmpty {
      country = Country(name: countryName)
    }

    let cityName = json["city"].stringValue
    var city: City?
    if !cityName.isEmpty {
      city = City(name: cityName)
    }

    let areaName = json["area"].stringValue
    var area: Area?
    if !areaName.isEmpty {
      area = Area(name: areaName)
    }

    let street = json["street"].stringValue
    let building = json["building"].stringValue
    let userBuilding = json["user_building"].stringValue
    let userFloorNumber = json["user_floor_number"].stringValue
    _ = LocationType(jsonName: json["location_type"].stringValue)
    let isIndoor = json["is_indoor"].boolValue
    let floor = json["floor"].stringValue
    let spotNumber = json["spot_number"].stringValue
    let comments = json["comment"].stringValue
    let landMark = json["land_mark"].stringValue
    let coordinates = CLLocationCoordinate2D.coodinatesFromString((json["coordinates"].stringValue))
    // TODO: CreateDate
    // TODO: UpdateDate

    return Address(addressID: id, type: type, name: name, country: country, city: city, area: area,
      street: street, building: building, userBuilding: userBuilding, userFloorNumber: userFloorNumber, isIndoor: isIndoor, floor: floor, spotNumber: spotNumber, comments: comments, landMark: landMark, coordinates: coordinates)
  }

  static public func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return addressFromJSON(json)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    let id = self.addressID ?? ""
    let type = (self.type == nil) ? "" : self.type!.addressTypeName()
    let name = self.name ?? ""
    let country = self.country?.name ?? ""
    let city = self.city?.name ?? ""
    let area = self.area?.name ?? ""
    let street = self.street ?? ""
    let building = self.building ?? ""
    let userBuilding = self.userBuilding ?? ""
    let userFloorNumber = self.userFloorNumber ?? ""
    var isIndoor: Bool? = nil
    if self.isIndoor != nil {
      isIndoor = self.isIndoor
    }
    let floor = self.floor ?? ""
    let spotNumber = self.spotNumber ?? ""
    let comments = self.comments ?? ""
    let landMark = self.landMark ?? ""
    let coordinates = self.coordinates?.coordinatesToString() ?? ""
    // TODO: CreateDate
    // TODO: UpdateDate

    return [
      "id" : id,
      "type" : type,
      "name" : name,
      "country" : country,
      "city" : city,
      "area" : area,
      "street" : street,
      "building" : building,
      "user_building" : userBuilding,
      "user_floor_number": userFloorNumber,
      "is_indoor" : isIndoor ?? "",
      "floor" : floor,
      "spot_number" : spotNumber,
      "comment" : comments,
      "landmark" : landMark,
      "coordinates" : coordinates
    ]
  }
}
