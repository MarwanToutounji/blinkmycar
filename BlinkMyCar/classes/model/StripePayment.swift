//
//  StripePayment.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 4/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import Stripe
import SwiftyJSON

struct StripePayment: PaymentTool {
  let cardId: String
  let last4: String
  let brand: STPCardBrand
  let country: String
  let name: String
  let isDefault: Bool
  let method: PaymentMethod
  var displayedText: String {
    return ".... \(last4)"
  }
  var image: UIImage? {
    return STPImageLibrary.brandImageForCardBrand(brand)
  }

  init(cardId: String, last4: String, brand: STPCardBrand, country: String, name: String, isDefault: Bool) {
    self.cardId = cardId
    self.last4 = last4
    self.brand = brand
    self.country = country
    self.name = name
    self.isDefault = isDefault
    method = .Stripe
  }

  static func fromSTPCard(stpCard: STPCard) -> StripePayment {
    return StripePayment(cardId: stpCard.cardId ?? "", last4: stpCard.last4(), brand: stpCard.brand,
      country: stpCard.country ?? "",
      name: (stpCard.allResponseFields["name"] as? String) ?? "",
      isDefault: false
    )
  }

}

//=================================================================
// MARK: - Parsing
//=================================================================

extension StripePayment: JSONable {
  static func cardsFromJSON(json: [String:AnyObject]) -> [StripePayment] {
    var cardsArray = [StripePayment]()
    let json = JSON(json)
    let cardsJSON = json["_embedded"]["card"]
    for (_, subJson): (String, JSON) in cardsJSON {
      let card = cardFromJSON(subJson)
      cardsArray.append(card)
    }
    return cardsArray
  }
  
  static func cardFromJSON(json: JSON) -> StripePayment {
    let cardId = json["id"].stringValue
    let last4 = json["last4"].stringValue
    let country = json["country"].stringValue
    let name = json["name"].stringValue
    let brand = json["brand"].stringValue
    let isDefault = json["is_default"].boolValue
    
    return StripePayment(cardId: cardId, last4: last4,
      brand: STPCardBrand.brandFor(brand),
      country: country,
      name: name,
      isDefault: isDefault
    )
  }
  
   static func fromJSON(json: [String : AnyObject]) -> JSONable {
    let json = JSON(json)
    return cardFromJSON(json)
  }

  func toJSONDictionary() -> [String : AnyObject] {
    return [
      "id" : self.cardId,
      "last4" : self.last4,
      "country" : self.country,
      "name" : self.name,
      "brand" : STPCardBrand.stringFor(self.brand),
      "is_default": self.isDefault
    ]
  }
}


//=================================================================
// MARK: - Persistence
//=================================================================

extension StripePayment {
  enum DefaultsKeys: String {
    case CardsList = "CardsList"
  }
  
  static func saveDefaultCard(card: StripePayment) {
    // Chenage the isDefault flag of the old default card to false because
    // there cannot be two default cards
    var savedCards = getCardsList()
    if let indexOfOldDefaultCard = savedCards.indexOf({ $0.isDefault == true }) {
      let oldDefaultCard = savedCards[indexOfOldDefaultCard]
      let updatedOldDefaultCard = StripePayment(
        cardId: oldDefaultCard.cardId,
        last4: oldDefaultCard.last4,
        brand: oldDefaultCard.brand,
        country: oldDefaultCard.country,
        name: oldDefaultCard.name,
        isDefault: false)
      savedCards.removeAtIndex(indexOfOldDefaultCard)
      savedCards.insert(updatedOldDefaultCard, atIndex: indexOfOldDefaultCard)
    }
    
    // Check if the new default card is already in the list, if so,
    // edit the isDefault flag, otherwise add the card at the top
    // of the list
    if let indexOfNewDefaultCard = savedCards.indexOf({ $0.cardId == card.cardId }) {
      savedCards.removeAtIndex(indexOfNewDefaultCard)
      savedCards.insert(card, atIndex: indexOfNewDefaultCard)
    } else {
      savedCards.insert(card, atIndex: 0)
    }
    
    saveCardsList(savedCards)
  }
  
  static func getDefaultCard() -> StripePayment? {
    return getCardsList().filter({ $0.isDefault }).first
  }
  
  static func getCardsList() -> [StripePayment] {
    var cardsList = [StripePayment]()
    let defaults = NSUserDefaults.standardUserDefaults()
    if let cardsDisctionary = defaults.valueForKey(DefaultsKeys.CardsList.rawValue)
      as? [[String: AnyObject]] {
        for cardDictionary in cardsDisctionary {
          if let card = StripePayment.fromJSON(cardDictionary) as? StripePayment {
            cardsList.append(card)
          }
        }
    }
    return cardsList
  }
  
  static func saveCardsList(cardsList: [StripePayment]) {
    var cardsDictionary = [[String: AnyObject]]()
    for card in cardsList {
      cardsDictionary.append(card.toJSONDictionary())
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(cardsDictionary, forKey: DefaultsKeys.CardsList.rawValue)
    defaults.synchronize()
    
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.DidUpdateCardsListing.rawValue,
      object: nil)
  }
  
  static func saveCardToList(card: StripePayment) {
    if card.isDefault {
      saveDefaultCard(card)
    } else {
      var savedCards = getCardsList()
      
      let index = savedCards.map({ $0.cardId }).indexOf((card.cardId))
      if let index = index {
        //Replace
        savedCards[index] = card
      } else {
        //Append
        savedCards.append(card)
      }
      saveCardsList(savedCards)
    }
  }
  
  static func deleteCardWithIdFromList(cardId: String) {
    var savedCards = getCardsList()
    let index = savedCards.map({ $0.cardId }).indexOf(cardId)
    
    if let index = index {
      savedCards.removeAtIndex(index)
      saveCardsList(savedCards)
    }
  }
  
  static func deleteCardsList() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.CardsList.rawValue)
    defaults.synchronize()
  }
}

extension STPCardBrand {
  static func brandFor(brand: String) -> STPCardBrand {
    switch brand.lowercaseString {
    case "visa": return STPCardBrand.Visa
    case "amex": return STPCardBrand.Amex
    case "mastercard": return STPCardBrand.MasterCard
    case "jcb": return STPCardBrand.JCB
    case "discover": return STPCardBrand.Discover
    case "dinersclub": return STPCardBrand.DinersClub
    default: return STPCardBrand.Unknown
    }
  }

  static func stringFor(brand: STPCardBrand) -> String {
    switch brand {
    case STPCardBrand.Visa: return "visa"
    case STPCardBrand.Amex: return "amex"
    case STPCardBrand.MasterCard: return "mastercard"
    case STPCardBrand.JCB: return "jcb"
    case STPCardBrand.Discover: return "discover"
    case STPCardBrand.DinersClub: return "dinersclub"
    default: return "unknown"
    }
  }
}
