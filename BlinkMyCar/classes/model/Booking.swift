//
//  Booking.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/19/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
import FontAwesome

public enum BookingStatus: String {
  case Pending = "pending"
  case Modified = "modified"
  case Confirmed = "confirmed"
  case Processing = "processing"
  case Done = "done"
  case Completed = "completed"
  case Canceled = "canceled"
  case Rejected = "rejected"
  case RejectedWashStopped = "rejected-wash-stopped"
  case RejectedUnwashable = "rejected-unwashable"
  case RejectedWasherObstruction = "rejected-washer-obstruction"
  case RejectedWasherAvailability = "rejected-washer-availability"
  
  var displayedStatus: String {
    // TODO: localize strings
    
    switch self {
    case .Canceled:
      return "Cancelled"
    case .Rejected:
      return "Rejected"
    case .RejectedUnwashable:
      return "Rejected Unwashable"
    case .RejectedWasherAvailability:
      return "Rejected Washer Availability"
    case .RejectedWasherObstruction:
      return "Rejected Washer Obstruction"
    case .RejectedWashStopped:
      return "Rejected Bad Weather"
    case .Completed, .Done:
      return "Completed"
    case .Confirmed:
      return "Confirmed"
    case .Pending, .Modified:
      return "Pending"
    case .Processing:
      return "In Progress"
    }
  }
  
  var fontAwsomeSymbol: String {
    switch self {
    case .Canceled:
      return NSString.fontAwesomeIconStringForEnum(FAIcon.FABan)
    case .Rejected, .RejectedUnwashable, .RejectedWasherAvailability, .RejectedWasherObstruction, .RejectedWashStopped:
      return NSString.fontAwesomeIconStringForEnum(FAIcon.FABan)
    case .Completed, .Done:
      return NSString.fontAwesomeIconStringForEnum(FAIcon.FACheckCircleO)
    case .Confirmed:
      return NSString.fontAwesomeIconStringForEnum(FAIcon.FACheck)
    case .Pending, .Modified:
      return NSString.fontAwesomeIconStringForEnum(FAIcon.FAClockO)
    case .Processing:
      return NSString.fontAwesomeIconStringForEnum(FAIcon.FARefresh)
    }
  }
  
  var statusColor: UIColor {
    switch self {
    case .Canceled:
      return UIColor.bmcRed()
    case .Rejected, .RejectedUnwashable, .RejectedWasherAvailability, .RejectedWasherObstruction, .RejectedWashStopped:
      return UIColor.bmcRed()
    case .Completed, .Done:
      return UIColor.grayColor()
    case .Confirmed:
      return UIColor.applicationMainColor()
    case .Pending, .Modified:
      return UIColor.orange()
    case .Processing:
      return UIColor.bmcGreen()
    }
  }
}

public enum AddressLocationType: String {
  case Street = "street"
  case Parking = "parking"
}

public struct Booking {
  let bookingId: String?
  let bookingNumber: String?
  let address: Address?
  let vehicle: Vehicle?
  let status: BookingStatus?
  let services: [Service]?
  let deliveryDate: ServiceDate?
  let bookingTime: ServiceTime?
  let startDate: NSDate?
  let washer: String?
  let rating: Int?
  let needsRating: Bool?
  let needsAddressDetails: Bool?
  let needKeysDetails: Bool?
  let addressKeysLocation: String?
  let addressLocationType: AddressLocationType?
  let totalPrice: Float?
  let paymentMethod: PaymentMethod?
  let promocode: String?
  let specialistName: String?
  let specialistNumber: String?
  
  public init(bookingId: String? = nil, bookingNumber: String? = nil, address: Address? = nil,
              vehicle: Vehicle? = nil, status: BookingStatus? = nil, services: [Service]? = nil,
              deliveryDate: ServiceDate? = nil, serviceTime: ServiceTime? = nil,
              startDate: NSDate? = nil, washer: String? = nil, rating: Int? = nil, needsRating: Bool? = nil,
              needsAddressDetails: Bool? = nil, needKeysDetails: Bool? = nil, addressKeysLocation: String? = nil,  addressLocationType: AddressLocationType? = nil, totalPrice: Float? = nil, paymentMethod: PaymentMethod, promocode: String? = nil, specialistName: String?, specialistNumber: String?) {
    self.bookingId = bookingId
    self.bookingNumber = bookingNumber
    self.address = address
    self.vehicle = vehicle
    self.status = status
    self.services = services
    self.deliveryDate = deliveryDate
    self.bookingTime = serviceTime
    self.startDate = startDate
    self.washer = washer
    self.rating = rating
    self.needsRating = needsRating
    self.needsAddressDetails = needsAddressDetails
    self.needKeysDetails = needKeysDetails
    self.addressKeysLocation = addressKeysLocation
    self.addressLocationType = addressLocationType
    self.totalPrice = totalPrice
    self.paymentMethod = paymentMethod
    self.promocode = promocode
    self.specialistName = specialistName
    self.specialistNumber = specialistNumber
  }

  public func washStartDate() -> NSDate {
    if startDate != nil {
      return startDate!
    }
    return deliveryDate!.date!.dateWithTime(bookingTime!.from!, minute: 0, second: 0)!
  }
}

//=================================================================
// MARK: - L O G I C
//=================================================================

extension Booking {

  public static var bookingThatNeedRating: Booking? {
    // The booking are already sorted by delivery date time
    let bookings = Booking.getSavedBookings()
    for booking in bookings {
      if booking.status != nil && booking.status! == .Completed && booking.needsRating != nil && booking.needsRating! {
        return booking
      }
    }
    return nil
  }

  public var deliveryDateTime: (fromDate: NSDate, toDate: NSDate) {
    let fromDate = deliveryDate!.date!.dateWithTime(bookingTime!.from!)!
    let toDate = deliveryDate!.date!.dateWithTime(bookingTime!.to!)!
    return (fromDate: fromDate, toDate: toDate)
  }

  public static func bookingById(bookingId: String) -> Booking? {
    let bookings = Booking.getSavedBookings()
    return bookings.filter({ $0.bookingId == bookingId }).first
  }

  public static func sortBookings(bookings: [Booking]) -> [Booking] {
    var bookings = bookings

    // Sort Bookings by delivery date and time
    bookings.sortInPlace({ (b1: Booking, b2: Booking) -> Bool in
      let fromDate1 = b1.deliveryDateTime.fromDate
      let fromDate2 = b2.deliveryDateTime.fromDate
      let result = fromDate1.compareDateTime(fromDate2)
      switch result {
      case .OrderedAscending:
        return true
      case .OrderedDescending, .OrderedSame:
        return false
      }
    })

    return bookings
  }

  public static func reverseSortBookings(bookings: [Booking]) -> [Booking] {
    var bookings = bookings

    // Sort Bookings by delivery date and time
    bookings.sortInPlace({ (b1: Booking, b2: Booking) -> Bool in
      let fromDate1 = b1.deliveryDateTime.fromDate
      let fromDate2 = b2.deliveryDateTime.fromDate
      let result = fromDate1.compareDateTime(fromDate2)
      switch result {
      case .OrderedAscending:
        return false
      case .OrderedDescending, .OrderedSame:
        return true
      }
    })

    return bookings
  }
}

//=================================================================
// MARK: - Persistence
//=================================================================

extension Booking {
  enum DefaultsKeys: String {
    case Bookings = "Bookings"
  }

  public static func getSavedBookings() -> [Booking] {
    var bookings = [Booking]()
    if let array = NSUserDefaults.standardUserDefaults().valueForKey(
      DefaultsKeys.Bookings.rawValue) as? [[String: AnyObject]]{
        for bookingsDictionary in array {
          let booking = fromJSON(bookingsDictionary) as! Booking
          bookings.append(booking)
        }
    }
    return bookings
  }

  public static func getSavedBookingsForCurrentVehicle() -> [Booking] {
    if let currentVehicleId = Vehicle.getCurrentVehicle()?.id {
      return getSavedBookingsForVehicleId(currentVehicleId)
    }
    return [Booking]()
  }

  public static func getSavedBookingsForVehicleId(vehicleId: String) -> [Booking] {
    var bookings = [Booking]()
    let savedBookings = getSavedBookings()
    bookings = savedBookings.filter({ (booking: Booking) -> Bool in
      return booking.vehicle?.id == vehicleId
    })
    return bookings
  }

  public static func getSavedBookingsForAddressId(addressId: String) -> [Booking] {
    var bookings = [Booking]()
    let savedBookings = getSavedBookings()
    bookings = savedBookings.filter({ (booking: Booking) -> Bool in
      return booking.address?.addressID == addressId
    })
    return bookings
  }

  public static func getBookingForBookingId(bookingId: String) -> Booking? {
    let savedBookings = getSavedBookings()
    let bookings = savedBookings.filter({ (booking: Booking) -> Bool in
      return booking.bookingId == bookingId
    })
    return bookings.first
  }

  public static func saveBookings(bookings: [Booking]) {
    var bookingArray = [[String: AnyObject]]()

    for booking in bookings {
      let jsonDictionary = booking.toJSONDictionary()
      bookingArray.append(jsonDictionary)
    }

    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(bookingArray,
      forKey: DefaultsKeys.Bookings.rawValue)
    defaults.synchronize()
  }
  
  public static func saveBookings(bookings: [Booking], forVehicleWithID: String) {
    // 1 - Remove saved bookings related to vehicle with ID
    // 2 - Join them with bookings related to that vehicle from the sent array
    // 3 - save them
    
    let allBookings = getSavedBookings().filter({ $0.vehicle!.id! != forVehicleWithID }) + bookings.filter({ $0.vehicle!.id! == forVehicleWithID })
    
    var bookingArray = [[String: AnyObject]]()
    
    for booking in allBookings {
      let jsonDictionary = booking.toJSONDictionary()
      bookingArray.append(jsonDictionary)
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setValue(bookingArray,
                      forKey: DefaultsKeys.Bookings.rawValue)
    defaults.synchronize()
  }

  public static func saveBooking(booking: Booking) {
    // Delete a booking with same id if any
    if let bookingId = booking.bookingId {
      deleteBookingWithId(bookingId)
    }

    // Save Booking
    var bookings = getSavedBookings()
    bookings.append(booking)
    bookings = Booking.sortBookings(bookings)
    saveBookings(bookings)
  }
  
  public static func cleanSavedBookings(vehicleIDs: [String]) {
    let allBookings: [Booking] = getSavedBookings()
    var cleanedBookings = [Booking]()
    
    for vehicleId in vehicleIDs {
      cleanedBookings += allBookings.filter({ $0.vehicle!.id! == vehicleId })
    }
    
    saveBookings(cleanedBookings)
  }

  public static func deleteBookings() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.Bookings.rawValue)
    defaults.synchronize()
  }
  
  public static func deleteBookings(forVehicleId id: String) {
    let bookings = getSavedBookings().filter({ $0.vehicle!.id != id })
    saveBookings(bookings)
  }

  public static func deleteBooking(booking: Booking) {
    var bookings = getSavedBookings()
    let index = bookings.map({ $0.bookingId! }).indexOf((booking.bookingId!))
    if let index = index {
      bookings.removeAtIndex(index)
    }
    saveBookings(bookings)
  }

  static public func deleteBookingWithId(bookingId: String) {
    var bookings = getSavedBookings()
    for (index, booking) in bookings.enumerate() {
      if let savedBookingId = booking.bookingId
        where savedBookingId == bookingId {
          bookings.removeAtIndex(index)
      }
    }
    saveBookings(bookings)
  }

  public static func replaceBooking(booking: Booking, with newBooking: Booking) {
    var bookings = getSavedBookings()
    let index = bookings.map({ $0.bookingId! }).indexOf((booking.bookingId!))
    if let index = index {
      bookings.removeAtIndex(index)
    }

    bookings.append(newBooking)

    saveBookings(bookings)
  }
}

//=================================================================
// MARK: - Parsing
//=================================================================

extension Booking: JSONable {
  public static func bookingsFromJSON(json: [String: AnyObject]) -> [Booking] {
    var bookingArray = [Booking]()
    let json = JSON(json)
    let bookingsJSON = json["_embedded"]["booking"]
    for (_, subJson): (String, JSON) in bookingsJSON {
      let booking = bookingFromJSON(subJson)
      bookingArray.append(booking)
    }
    return bookingArray
  }

  public static func bookingFromJSON(json: JSON) -> Booking {
    let bookingId = json["id"].stringValue
    let bookingNumber = json["booking_number"].stringValue
    let address = addressFromBookingJSON(json)
    let vehicle = vehicleFromBookingJSON(json)
    let status = BookingStatus(rawValue: json["status"].stringValue)
    let services = servicesFromBookingJSON(json)
    var deliveryDate: ServiceDate? = nil
    let deliveryDateString = json["delivery_date"].stringValue
    if !deliveryDateString.isEmpty {
      let date = NSDate(timeIntervalSince1970: (deliveryDateString as NSString).doubleValue)
      deliveryDate = ServiceDate(date: date)
    }
    var bookingTime: ServiceTime? = nil
    var startDate: NSDate? = nil
    let startDateString = json["start_date"].stringValue
    if !startDateString.isEmpty {
      startDate = NSDate(timeIntervalSince1970: (startDateString as NSString).doubleValue)
    }
    let bookingTimeStringFrom = json["delivery_time"]["from"].intValue
    let bookingTimeStringTo = json["delivery_time"]["to"].intValue
    bookingTime = ServiceTime(from: bookingTimeStringFrom, to: bookingTimeStringTo)
    let washer = json["washer"].stringValue
    let needsRating = json["needs_rating"].boolValue
    let needsAddressDetails = json["need_address_details"].boolValue
    let needKeysDetails = json["need_keys_details"].boolValue
    let addressKeysLocation = json["address_keys_location"].stringValue
    let addressLocationType = AddressLocationType(rawValue: json["address_location_type"].stringValue)
    let totalPrice = json["total_price"].float
    let paymentMethod = PaymentMethod(jsonString: json["payment_method"].stringValue) ?? .Stripe
    let promocode = json["promocode"].stringValue
    let specialistName = json["specialist_name"].stringValue
    let specialistNumber = json["specialist_number"].stringValue

    return Booking(bookingId: bookingId, bookingNumber: bookingNumber, address: address, vehicle: vehicle, status: status, services: services, deliveryDate: deliveryDate, serviceTime: bookingTime, startDate: startDate, washer: washer, needsRating: needsRating, needsAddressDetails: needsAddressDetails, needKeysDetails: needKeysDetails, addressKeysLocation: addressKeysLocation, addressLocationType: addressLocationType, totalPrice: totalPrice, paymentMethod: paymentMethod, promocode: promocode, specialistName: specialistName, specialistNumber: specialistNumber)
  }
  
  private static func servicesFromBookingJSON(json: JSON) -> [Service] {
    var servicesArrayValue = json["_embedded"]["products"].arrayValue
    /**
    If their are no services fetched check if we can find them under "products"
    Why? Because:
    - This path ["_embedded"]["products"] is the one we can find
    the services in, when the json is parsed from an API response data
    - This path ["products"] is the one we can find
    the services in, when the json is parsed user defaults (locally saved)
    **/
    if servicesArrayValue.count <= 0 {
      servicesArrayValue = json["products"].arrayValue
    }
    
    return Service.servicesArrayFromJSON(servicesArrayValue)
  }

  private static func addressFromBookingJSON(json: JSON) -> Address {
    var id = json["address_id"].stringValue
    if id.isEmpty {
      id = json["_embedded"]["address"]["id"].stringValue
    }
    let name = json["address_name"].stringValue
    let countryName = json["address_country"].stringValue
    var country: Country?
    if !countryName.isEmpty {
      country = Country(name: countryName)
    }
    let cityName = json["address_city"].stringValue
    var city: City?
    if !cityName.isEmpty {
      city = City(name: cityName)
    }
    let areaName = json["address_area"].stringValue
    var area: Area?
    if !areaName.isEmpty {
      area = Area(name: areaName)
    }
    let type = json["address_type"].stringValue
    var addressType: AddressType? = nil
    if !type.isEmpty {
      addressType = AddressType(jsonName: type)
    }
    _ = json["address_parking_type"].stringValue
    let isIndoor = json["address_is_indoor"].boolValue
    let floor = json["address_floor"].stringValue
    let spotNumber = json["address_spot_number"].stringValue
    let userBuilding = json["address_user_building"].stringValue
    let userFloorNumber = json["address_user_floor_number"].stringValue
    let landmark = json["address_landmark"].stringValue
    let comment = json["address_comment"].stringValue
    let street = json["address_street"].stringValue
    let coordinates = CLLocationCoordinate2D.coodinatesFromString(json["address_coordinates"].stringValue)

    return Address(addressID: id, type: addressType, name: name, country: country, city: city,
      area: area, street: street, isIndoor: isIndoor, userBuilding: userBuilding, userFloorNumber: userFloorNumber, floor: floor, spotNumber: spotNumber, comments: comment, landMark: landmark, coordinates: coordinates, createDate: nil, updateDate: nil)
  }

  private static func vehicleFromBookingJSON(json: JSON) -> Vehicle {
    var id = json["ride_id"].stringValue
    if id.isEmpty {
      id  = json["_embedded"]["ride"]["id"].stringValue
    }

    let savedVehicle = Vehicle.getVehicleWithId(id)

    if let savedVehicle = savedVehicle {
      return savedVehicle
    } else {
      let name = json["ride_name"].stringValue
      let make = json["ride_make"].stringValue
      let model = json["ride_model"].stringValue
      let year = json["ride_year"].intValue
      let plateNumber = json["ride_plate_number"].stringValue
      let type = Make.VehicleType(type: json["ride_type"].stringValue)
      let image = json["ride_image"].stringValue

      return Vehicle(name: name, make: make, model: model, id: id, imagePath: image,
        plateNumber: plateNumber, year: year, type: type)
    }
  }
  
  public static func totalPriceFromJSON(jsonDict: [String: AnyObject]) -> (price: Float?, discountedPrice: Float?, hasError: Bool?, errorMessage: String?) {
    let json = JSON(jsonDict)
    let price = json["grand_total"].float
    let discountedPrice = json["base_total"].float
    let hasError = json["has_error"].bool
    let errorMessage = json["error_message"].string
    return (price: price, discountedPrice: discountedPrice, hasError: hasError, errorMessage: errorMessage)
  }

  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return bookingFromJSON(json)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    let bookingId = self.bookingId ?? ""
    let bookingNumber = self.bookingNumber ?? ""
    let addressId = address?.addressID ?? ""
    let addressName = address?.name ?? ""
    let addressCountry = address?.country?.name ?? ""
    let addressCity = address?.city?.name ?? ""
    let addressArea = address?.area?.name ?? ""
    let addressType = address?.type?.jsonName ?? AddressType.Other.jsonName
    let addressIsIndoor = address?.isIndoor ?? false
    let addressUserFloorNumber = address?.userFloorNumber ?? ""
    let addressFloor = address?.floor ?? ""
    let addressSpotNumber = address?.floor ?? ""
    let addressLandmark = address?.landMark ?? ""
    let addressComment = address?.comments ?? ""
    let addressCoordinates = address?.coordinates?.coordinatesToString() ?? ""
    let addressUserBuilding = address?.userBuilding ?? ""
    let addressStreet = address?.street ?? ""
    let rideId = vehicle?.id ?? ""
    let rideName = vehicle?.name ?? ""
    let rideMake = vehicle?.make ?? ""
    let rideModel = vehicle?.model ?? ""
    let rideYear = vehicle?.year ?? -1
    let ridePlateNumber = vehicle?.plateNumber ?? ""
    let rideType = vehicle?.type?.rawValue ?? Make.VehicleType.Car.rawValue
    let rideImage = vehicle?.imagePath ?? ""
    let status = self.status?.rawValue ?? ""
    let services = (self.services != nil) ? Service.servicesArray(self.services!) : [[String: AnyObject]]()
    let deliveryDate = Double(self.deliveryDate?.date?.timeIntervalSince1970 ?? 0)
    let bookingTime = self.bookingTime?.jsonDictionary() ?? [:]
    let startDate = Double(self.startDate?.timeIntervalSince1970 ?? 0)
    let washer = self.washer ?? ""
    let needsRating = self.needsRating ?? false
    let needsAddressDetails = self.needsAddressDetails ?? false
    let needKeysDetails = self.needKeysDetails ?? false
    let addressKeysLocation = self.addressKeysLocation ?? ""
    let addressLocationType = self.addressLocationType?.rawValue ?? AddressLocationType.Street.rawValue
    let promocode = self.promocode ?? ""
    let specialistName = self.specialistName ?? ""
    let specialistNumber = self.specialistNumber ?? ""
    
    var bookingDict: [String : AnyObject] = [
      "id" : bookingId,
      "booking_number" : bookingNumber,
      "address_id": addressId,
      "address_name" : addressName,
      "address_country" : addressCountry,
      "address_city" : addressCity,
      "address_area" : addressArea,
      "address_type" : addressType,
      "address_user_building" : addressUserBuilding,
      "address_user_floor_number" : addressUserFloorNumber,
      "address_is_indoor" : addressIsIndoor,
      "address_floor" : addressFloor,
      "address_spot_number" : addressSpotNumber,
      "address_landmark" : addressLandmark,
      "address_comment" : addressComment,
      "address_coordinates" : addressCoordinates,
      "address_street" : addressStreet,
      "address_location_type" : addressLocationType,
      "ride_id" : rideId,
      "ride_name" : rideName,
      "ride_make" : rideMake,
      "ride_model" : rideModel,
      "ride_year" : rideYear,
      "ride_plate_number" : ridePlateNumber,
      "ride_type" : rideType,
      "ride_image" : rideImage,
      "status" : status,
      "products" : services,
      "delivery_date" : deliveryDate,
      "delivery_time" : bookingTime,
      "start_date" : startDate,
      "washer" : washer,
      "needs_rating" : needsRating,
      "need_address_details" : needsAddressDetails,
      "need_keys_details": needKeysDetails,
      "address_keys_location": addressKeysLocation,
      "promocode": promocode,
      "specialist_name": specialistName,
      "specialist_number": specialistNumber
    ]
    
    if let paymentMethod = paymentMethod {
      bookingDict["payment_method"] = paymentMethod.jsonString
    }
    
    if let totalPrice = totalPrice {
      bookingDict["total_price"] = totalPrice
    }
    
    return bookingDict
  }
}