//
//  ServiceBundle.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/29/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

/**
 This class is used to join selected core services along with their
 add-ons if available. Can be helpfull For listing booked services.
*/
public class ServiceBundle {
  var mainService: Service!
  var addOnsSerices: [Service]?
}
