//
//  Make.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/13/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Make: Equatable {

  public enum VehicleType: String, Equatable {
    case Car = "car"
    case Motorcycle = "bike"

    public init(type: String) {
      switch type {
      case "bike":
        self = Motorcycle
      case "car":
        fallthrough
      default:
        self = Car
      }
    }
  }

  public let name: String
  public let models: [String]
  public let type: VehicleType
  public let imagePath: String

  public init(name: String, models: [String], type: VehicleType, imagePath: String) {
    self.name = name
    self.models = models
    self.type = type
    self.imagePath = imagePath
  }

  public static func arrayMakeFromJSON(json: [String : AnyObject]) -> [Make] {
    var makeArray = [Make]()

    let json = JSON(json)
    let makeJson = json["_embedded"]["make"]
    for (_, subJson): (String, JSON) in makeJson {
      let make = makeFromJSON(subJson)
      makeArray.append(make)
    }

    return makeArray
  }

  public static func makeFromJSON(json: JSON) -> Make {
    let name = json["name"].stringValue
    let jsonModels = json["_embedded"]["model"]
    var models: [String] = []
    for (_, subJSON): (String, JSON) in jsonModels {
      models.append(subJSON["name"].stringValue)
    }
    let typeString = json["type"].stringValue
    let type = VehicleType(type: typeString)
    let imagePath = json["image"].stringValue
    return Make(name: name, models: models, type: type, imagePath: imagePath)
  }
}

extension Make: JSONable {
  public static func fromJSON(json: [String : AnyObject]) -> JSONable {
    let json = JSON(json)
    return makeFromJSON(json)
  }

  public func toJSONDictionary() -> [String : AnyObject] {
    // TODO: Implement when needed
    return [String : AnyObject]()
  }
}

public func ==(lhs: Make, rhs: Make) -> Bool {
  return lhs.name == rhs.name
}
