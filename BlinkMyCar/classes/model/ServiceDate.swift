//
//  ServiceDate.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON


public struct ServiceDate {
  let date: NSDate?
  let daysFromToday: Int?
  let dayOfWeek: Int?
  let times: [ServiceTime]?

  public init(date: NSDate? = nil, daysFromToday: Int? = nil, dayOfWeek: Int? = nil,
    times: [ServiceTime]? = nil) {
    self.date = date
    self.daysFromToday = daysFromToday
    self.dayOfWeek = dayOfWeek
    self.times = times
  }

  var isServiceable: Bool {
    if let times = self.times {
      for time in times {
        if time.enabled! {
          return true
        }
      }
      return false
    }
    return false
  }

  var isDateStillAvailable: Bool {
    var sortedTimes = self.times!
    sortedTimes.sortInPlace({ $0.from < $1.from })

    let nowTimestamp = NSDate().timeIntervalSince1970
    let lastTimeSlotClosingTimeStamp = self.date!.dateWithTime(sortedTimes.last!.from!)!.timeIntervalSince1970

    return nowTimestamp < lastTimeSlotClosingTimeStamp
  }
  
  /// Can be in the future
  var isServiceDateOlderThan24hours: Bool {
    let aDayInSeconds: Double = 24*60*60
    guard let date = self.date else {
      return true
    }
    return !(date.timeIntervalSinceNow > -aDayInSeconds)
  }
}

extension ServiceDate: Equatable {}

public func ==(lhs: ServiceDate, rhs: ServiceDate) -> Bool {
  var sameDate = true
  if let dateLeft = lhs.date,
    let dateRight = rhs.date {
      sameDate = dateLeft.sameDate(dateRight)
  } else if !(lhs.date == nil && rhs.date == nil) {
    sameDate = false
  }

  return sameDate
}

extension ServiceDate: JSONable {

  public static func serviceDatesFromJSON(json: [String: AnyObject]) -> [ServiceDate] {
    var array = [ServiceDate]()
    let json = JSON(json)
    let serviceDatesJSON = json["service_time"]
    for (_, subJson): (String, JSON) in serviceDatesJSON {
      let date = serviceDateFromJSON(subJson)
      array.append(date)
    }
    return array
  }

  public static func serviceDateFromJSON(json: JSON) -> ServiceDate {
    var date: NSDate?
    if !json["date"].stringValue.isEmpty  {
      date = NSDate(timeIntervalSince1970: (json["date"].stringValue as NSString).doubleValue)
    }

    let daysFromToday = json["days_from_today"].intValue
    let dayOfWeek = json["day_of_week"].intValue

    let times = ServiceTime.serviceTimesFromJSON(json["time"])

    return ServiceDate(date: date, daysFromToday: daysFromToday,
      dayOfWeek: dayOfWeek, times: times)
  }

  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return serviceDateFromJSON(json)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    return [:]
  }
}

extension ServiceDate {
  func displayTextForServiceDate() -> String {
    let nowDate = NSDate()
    
    switch date!.compareDate(nowDate) {
    case .OrderedAscending:
      fallthrough
    case .OrderedDescending where ((date!.timeIntervalSinceNow - (3*24*60*60)) > 0) :
      return date!.stringValueToAttributedText
    case .OrderedDescending:
      fallthrough
    case .OrderedSame:
      let dayOfTheWeek = nowDate.dayOfTheWeek
      
      switch dayOfTheWeek {
      case .Monday:
        if date!.dayOfTheWeek == .Monday {
          return "Today, \(date!.stringValueMonthAndDay)"
        } else if date!.dayOfTheWeek == .Tuesday {
          return "Tomorrow, \(date!.stringValueMonthAndDay)"
        }
      case .Tuesday:
        if date!.dayOfTheWeek == .Tuesday {
          return "Today, \(date!.stringValueMonthAndDay)"
        } else if date!.dayOfTheWeek == .Wednesday {
          return "Tomorrow, \(date!.stringValueMonthAndDay)"
        }
      case .Wednesday:
        if date!.dayOfTheWeek == .Wednesday {
          return "Today, \(date!.stringValueMonthAndDay)"
        } else if date!.dayOfTheWeek == .Thursday {
          return "Tomorrow, \(date!.stringValueMonthAndDay)"
        }
      case .Thursday:
        if date!.dayOfTheWeek == .Thursday {
          return "Today, \(date!.stringValueMonthAndDay)"
        } else if date!.dayOfTheWeek == .Friday {
          return "Tomorrow, \(date!.stringValueMonthAndDay)"
        }
      case .Friday:
        if date!.dayOfTheWeek == .Friday {
          return "Today, \(date!.stringValueMonthAndDay)"
        } else if date!.dayOfTheWeek == .Saturday {
          return "Tomorrow, \(date!.stringValueMonthAndDay)"
        }
      case .Saturday:
        if date!.dayOfTheWeek == .Saturday {
          return "Today, \(date!.stringValueMonthAndDay)"
        } else if date!.dayOfTheWeek == .Sunday {
          return "Tomorrow, \(date!.stringValueMonthAndDay)"
        }
      case .Sunday:
        if date!.dayOfTheWeek == .Sunday {
          return "Today, \(date!.stringValueMonthAndDay)"
        } else if date!.dayOfTheWeek == .Monday {
          return "Tomorrow, \(date!.stringValueMonthAndDay)"
        }
      }
    }
    
    return date!.stringValueToAttributedText
  }
}
