//
//  Page.swift
//  BlinkMyCar
//
//  Created by Marwan  on 9/7/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Page {
  var mySelf: String?
  var first: String?
  var last: String?
  var next: String?
  
  init(mySelf: String?, first: String?, last: String?, next: String?) {
    self.mySelf = mySelf
    self.first = first
    self.last = last
    self.next = next
  }
}

extension Page: JSONable {
  public static func pageFromJSON(json: JSON) -> Page {
    let links = json["_links"]
    let mySelf = links["self"]["href"].stringValue
    let first = links["first"]["href"].stringValue
    let last = links["last"]["href"].stringValue
    let next = links["next"]["href"].stringValue
    return Page(mySelf: mySelf, first: first, last: last, next: next)
  }
  
  public static func pageFromJSON(json: [String: AnyObject]) -> Page {
    return fromJSON(json) as! Page
  }
  
  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return pageFromJSON(json)
  }
  
  public func toJSONDictionary() -> [String: AnyObject] {
    return [
      "self" : ["href" : mySelf ?? ""],
      "first" : ["href" : first ?? ""],
      "last" : ["href" : last ?? ""],
      "next" : ["href" : next ?? ""]
    ]
  }
}
