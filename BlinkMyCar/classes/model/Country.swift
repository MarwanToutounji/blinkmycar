//
//  Country.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Country {
  let id: String?
  let name: String?
  let enabled: Bool?
  let serviceable: Bool?
  let hourInterval: Int?
  let contactInfo: String?
  let telephone: String?
  let overlay: Overlay?

  public init(id: String? = nil, name: String? = nil, enabled: Bool? = nil,
    serviceable: Bool? = nil, hourInterval: Int? = nil, contactInfo: String? = nil,
    telephone: String? = nil, overlay: Overlay? = nil) {
      self.id = id
      self.name = name
      self.enabled = enabled
      self.serviceable = serviceable
      self.hourInterval = hourInterval
      self.contactInfo = contactInfo
      self.telephone = telephone
      self.overlay = overlay
  }
}

//=================================================================
// MARK: - Equatable
//=================================================================

extension Country: Equatable {}

public func ==(lhs: Country, rhs: Country) -> Bool {
  return (
    lhs.id == rhs.id &&
    lhs.name == rhs.name &&
    lhs.enabled == rhs.enabled &&
    lhs.serviceable == rhs.serviceable &&
    lhs.hourInterval == rhs.hourInterval &&
    lhs.contactInfo == rhs.contactInfo &&
    lhs.telephone == rhs.telephone
  )
}

//=================================================================
// MARK: - Persistence
//=================================================================

extension Country {
  enum DefaultsKeys: String {
    case Country = "CountryDefaultsKeysCountry"
  }

  static public func getCountry() -> Country? {
    let defaults = NSUserDefaults.standardUserDefaults()
    if let object = defaults.valueForKey(DefaultsKeys.Country.rawValue) as? [String:AnyObject] {
      let country = Country.fromJSON(object) as? Country
      return country
    }
    return nil
  }

  static public func saveCountry(country: Country) {
    let defaults = NSUserDefaults.standardUserDefaults()
    let dictionary  = country.toJSONDictionary()
    defaults.setValue(dictionary, forKey: DefaultsKeys.Country.rawValue)
    defaults.synchronize()


    // Save the duration curently as an hour
    var generalSettings = GeneralSettings()
    generalSettings.bookingSlotDuration = Float(1.5)
  }

  static public func deleteCountry() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.removeObjectForKey(DefaultsKeys.Country.rawValue)
    defaults.synchronize()
  }
}

//=================================================================
// MARK: - Parsing
//=================================================================

extension Country: JSONable {
  public static func countriesFromJSON(json: [String: AnyObject]) -> [Country] {
    var countriesArray = [Country]()
    let json = JSON(json)
    let countriesJSON = json["_embedded"]["country"]
    for (_, subJson): (String, JSON) in countriesJSON {
      let country = countryFromJSON(subJson)
      countriesArray.append(country)
    }
    return countriesArray
  }

  public static func countryFromJSON(json: JSON) -> Country {
    let id = json["id"].stringValue
    let name = json["name"].stringValue
    let enabled = json["enabled"].boolValue
    let serviceable = json["serviceable"].boolValue
    let hourInterval = json["hour_interval"].intValue
    let contactInfo = json["contact_info"].stringValue
    let telephone = json["telephone"].stringValue
    let serviceAreaJson = json["service_area"]

    var overlay: Overlay? = nil
    if let serviceAreaString = serviceAreaJson.string {
      //Is String, so it is being loaded from the API
      overlay = Overlay.overlayFromJSON(JSON.parse(serviceAreaString))
    } else {
      //Is Object, so it is being loaded from the user defaults
      overlay = Overlay.overlayFromJSON(serviceAreaJson)
    }

    return Country(id: id, name: name, enabled: enabled, serviceable: serviceable,
      hourInterval: hourInterval, contactInfo: contactInfo, telephone: telephone, overlay: overlay)
  }

  public static func fromJSON(json: [String: AnyObject]) -> JSONable {
    let json = JSON(json)
    return countryFromJSON(json)
  }

  public func toJSONDictionary() -> [String: AnyObject] {
    let id = self.id ?? ""
    let name = self.name ?? ""
    let enabled = self.name ?? ""
    let serviceable = self.serviceable ?? false
    let hourInterval = self.hourInterval ?? 1
    let contactInfo = self.contactInfo ?? ""
    let telephone = self.telephone ?? ""
    let overlay = self.overlay?.toJSONDictionary() ?? [:]

    return [
      "id" : id,
      "name" : name,
      "enabled" : enabled,
      "serviceable" : serviceable,
      "hour_interval" : hourInterval,
      "contact_info" : contactInfo,
      "telephone" : telephone,
      "service_area" : overlay
    ]
  }
}
