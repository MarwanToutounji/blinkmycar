//
//  AccessToken.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/2/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import SwiftyJSON

private extension NSDate {
  var isInPast: Bool {
    let now = NSDate()
    return self.compare(now) == NSComparisonResult.OrderedAscending
  }
}

public struct AccessToken {
  enum DefaultsKeys: String {
    case AccessTokenKey = "AccessTokenKey"
    case AccessTokenExpiry = "AccessTokenExpiry"
    case RefreshTokenKey = "RefreshTokenKey"
    case AccessTokenIsUpdating = "IsUpdating"
  }

  // MARK: - Initializers

  public let defaults: NSUserDefaults

  public init() {
    self.defaults = NSUserDefaults.standardUserDefaults()
  }

  public mutating func readFromDictionary(let dictionary: NSDictionary) {
    let json = JSON(dictionary)
    token = json["access_token"].stringValue
    refresh = json["refresh_token"].stringValue
    let expiresIn = json["expires_in"].doubleValue as NSTimeInterval
    expiry = NSDate(timeIntervalSinceNow: expiresIn)
  }

  // MARK: - Properties

  public var token: String? {
    get {
      let key = defaults.stringForKey(DefaultsKeys.AccessTokenKey.rawValue)
      return key
    }
    set(newToken) {
      defaults.setObject(newToken, forKey: DefaultsKeys.AccessTokenKey.rawValue)
    }
  }

  public var expiry: NSDate? {
    get {
      return defaults.objectForKey(DefaultsKeys.AccessTokenExpiry.rawValue) as? NSDate
    }
    set(newExpiry) {
      defaults.setObject(newExpiry, forKey: DefaultsKeys.AccessTokenExpiry.rawValue)
    }
  }

  public var refresh: String? {
    get {
      let key = defaults.stringForKey(DefaultsKeys.RefreshTokenKey.rawValue)
      return key
    }
    set(newToken) {
      defaults.setObject(newToken, forKey: DefaultsKeys.RefreshTokenKey.rawValue)
    }
  }
  
  public var isUpdating: Bool? {
    get {
      let key = defaults.boolForKey(DefaultsKeys.AccessTokenIsUpdating.rawValue)
      return key
    }
    set(newToken) {
      defaults.setObject(newToken, forKey: DefaultsKeys.AccessTokenIsUpdating.rawValue)
    }
  }

  public var expired: Bool {
    if let expiry = expiry {
      return expiry.isInPast
    }
    return true
  }

  public var isValid: Bool {
    if let token = token, refresh = refresh {
      return (token.characters.count > 0) && (refresh.characters.count > 0) && !expired
    }

    return false
  }

  public var hasToken: Bool {
    if let token = token, refresh = refresh {
      return (token.characters.count > 0) && (refresh.characters.count > 0)
    }

    return false
  }

  public var updating: Bool {
    if let updating = isUpdating {
      return updating
    }
    return false
  }
  
  public func deleteToken() {
    defaults.removeObjectForKey(DefaultsKeys.AccessTokenKey.rawValue)
    defaults.removeObjectForKey(DefaultsKeys.AccessTokenExpiry.rawValue)
    defaults.removeObjectForKey(DefaultsKeys.RefreshTokenKey.rawValue)
  }
}
