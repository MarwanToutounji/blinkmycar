//
//  KWDBlinkMyCarAPI.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/10/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import Moya

enum KWDBlinkMyCarAPI {
  case OAuth(username: String, password: String)
}

extension KWDBlinkMyCarAPI : MoyaPath {
  var path: String {
    switch self {
    case .OAuth(_,_):
      return "/oauth2"
    }
  }
}

extension KWDBlinkMyCarAPI: MoyaTarget {
  var baseURL: NSURL {
    return NSURL(string: "http://172.16.54.43")!
  }

  var method: Moya.Method {
    switch self {
    case .OAuth(_,_):
      return .GET
    }
  }

  var parameters: [String: AnyObject] {
    switch self {
    case .OAuth(let username, let password):
      return [
        "client_id": APIKeys.sharedKeys.key,
        "client_secret": APIKeys.sharedKeys.secret,
        "username": username,
        "password":  password,
        "grant_type": "password"
      ]
    }
  }

  var sampleData: NSData {
    return stubbedResponse(self)
  }
}

//==================================================================
// MARK: HELPERS
//==================================================================

public func url(route: MoyaTarget) -> String {
  return route.baseURL.URLByAppendingPathComponent(route.path).absoluteString!
}

private func stubbedResponse(openData: KWDBlinkMyCarAPI) -> NSData! {
  @objc class TestClass { }

  var filename: String = ""
  var type: String = ""
  switch openData {
  case .OAuth(_,_):
    filename = "oauth"
    type = "json"
  }

  let bundle = NSBundle(forClass: TestClass.self)
  let path = bundle.pathForResource(filename, ofType: type)
  return NSData(contentsOfFile: path!)
}