//
//  APIKeys.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 6/12/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import Keys

private let minimumKeyLength = 5

/**
 *  This is a wrapper class around the generated class BlinkMyCarKeys.
 *  It also provides a way to identify if we're in a test environment and we
 *  we need to return stubbed responses by checking the length of the configured
 *  key and secret.
 */
public struct APIKeys {
  let key: String
  let secret: String
  let googleMapsKey: String

  private struct SharedKeys {
    static var instance = APIKeys()
  }

  public static var sharedKeys: APIKeys {
    get {
      return SharedKeys.instance
    }

    set (newSharedKeys) {
      SharedKeys.instance = newSharedKeys
    }
  }

  // MARK: Methods

  public var stubResponses: Bool {
    return key.characters.count < minimumKeyLength || secret.characters.count < minimumKeyLength
  }

  // MARK: Initializers

  public init(key: String, secret: String, googleMapsKey: String) {
    self.key = key
    self.secret = secret
    self.googleMapsKey = googleMapsKey
  }

  public init(keys: BlinkmycarKeys) {
    self.init(key: keys.blinkMyCarAPIClientKey() ?? "",
      secret: keys.blinkMyCarAPIClientSecret() ?? "",
      googleMapsKey: keys.blinkMyCarGoogleMapsKey())
  }

  public init() {
    self.init(keys: BlinkmycarKeys())
  }
}
