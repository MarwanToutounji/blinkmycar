//
//  KWDBlinkMyCarAPI.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/10/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import Moya
import Alamofire
import Keys
import SwiftyJSON

/// Closure to be executed when a request has completed.
public typealias APICompletion = (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> ()
public typealias APIOperation = (token: BlinkMyCarAPI, completion: APICompletion)

var operationQueue: [APIOperation] = [APIOperation]()

var userAgentValue: String {
  let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as! String
  let buildNumber = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as! String
  let device = UIDevice.currentDevice()
  return "BlinkMyCar/\(version)+\(buildNumber) (iOS/\(device.systemVersion); \(device.type); \(device.device.rawValue))"
}

public enum BlinkMyCarAPI {
  case OAuth(username: String, password: String)
  case RefreshToken
  case AddVehicle(color: String?, make: String?, model: String?, name: String?, plateNumber: String?, type: String?, year: Double?)
  case UpdateVehicle(id: String, color: String?, make: String?, model: String?, name: String?, plateNumber: String?, type: String?, year: Double?)
  case AllVehicles
  case Vehicle(id: String)
  case DeleteVehicle(id: String)
  case AllAddresses
  case Address(id: String)
  case AddAddress(area: String?, city: String?, comment: String?, coordinates: String?, country: String?,  street: String?, building: String?, userBuilding: String?, userFloorNumber: String?, floor: String?, isIndoor: Bool?, landmark: String?, locationType: String?, name: String?, spotNumber: String?, type: String?)
  case UpdateAddress(id: String, area: String?, city: String?, comment: String?, coordinates: String?, country: String?, street: String?, building: String?, userBuilding: String?, userFloorNumber: String?, floor: String?, isIndoor: Bool?, landmark: String?, locationType: String?, name: String?, spotNumber: String?, type: String?)
  case DeleteAddress(id: String)
  case Register(firstName: String, lastName: String, email: String, password: String, phoneNumber: String)
  case Profile
  case AllBookings
  case BookingsThatNeedRating
  case Bookings(vehicleId: String, from: NSDate)
  case Booking(bookingId: String)
  case CreateBooking(addressId: String, deliveryDate: NSDate, deliveryTime: ServiceTime, rideId: String, service: [Service], paymentMethod: PaymentMethod, promocode: String?)
  case UpdateBooking(bookingId: String, addressId: String?, deliveryDate: NSDate?, deliveryTime: ServiceTime?, rideId: String?, service: [Service]?, paymentMethod: PaymentMethod?, promocode: String?)
  case BookingAdditionalInformation(bookingId: String, keysInfo: String)
  case UpdateBookingAddress(bookingId: String, name: String?, area: String?, street: String?, userBuilding: String?, userFloorNumber: String?, comment: String?)
  case UpdateBookingAddressDetails(bookingId: String, isIndoor: Bool, floor: String, spotNumber: String, comment: String, street: String, addressLocationType: String)
  case UpdateBookingVehicle(bookingId: String, name: String?, color: String?, make: String?, model: String?, plateNumber: String?)
  case CancelBooking(bookingId: String)
  case UpdateProfile(firstName: String, lastName: String, email: String, phoneNumber: String)
  case CarMakesAndModels
  case Country
  case CountryInformation(countryId: String)
  case City(countryId: String)
  case Area(countryId: String, cityId: String)
  case ServiceDateTime(countryId: String)
  case CustomerSupport(type: String, body: String, bookingId: String?)
  case Rating(bookingId: String, rating: Int, comment: String?)
  case RegisterPushNotification(deviceNotificationToken: String)
  case UnregisterPushNotification(serverTokenID: String)
  case AddCard(token: String, isDefault: Bool)
  case AllCards
  case DeleteCard(cardId: String)
  case SetCardAsDefault(cardId: String)
  case AllServices
  case getTotal(addressId: String, deliveryDate: NSDate, deliveryTime: ServiceTime, rideId: String, service: [Service], promocode: String?)
  case Next(fullURL: NSURL)
}

// Extension for upload info
extension BlinkMyCarAPI {
  public var uploadAttributeName: String {
    switch self {
    case Address:
      return "voice_note"
    case Booking:
      return "address_voice_note"
    case Vehicle:
      return "image"
    default:
      return ""
    }
  }

  public var uploadFileName: String {
    switch self {
    case Address:
      return "voice_note.m4a"
    case Booking:
      return "address_voice_note.m4a"
    case Vehicle:
      return "vehicle_image.jpg"
    default:
      return ""
    }
  }

  public var uploadMimeType: String {
    switch self {
    case Address, Booking:
      return "audio/mp4"
    case Vehicle:
      return "image/jpg"
    default:
      return ""
    }
  }
}

extension BlinkMyCarAPI: TargetType {
  public var baseURL: NSURL {
    switch self {
    case .Next(let fullURL):
      return fullURL
    default:
      var urlString: String = "https://api.blinkmycar.com"
      
      let envURL = NSProcessInfo.processInfo().environment["SERVER_URL"]
      urlString = envURL ?? BlinkmycarKeys().blinkMyCarServerBaseURL()
      
      return NSURL(string: urlString)!
    }
  }

  public var path: String {
    switch self {
    case .OAuth, .RefreshToken:
      return "/oauth"
    case .AddVehicle, .AllVehicles:
      return "/v1/ride"
    case .UpdateVehicle(let id,_,_,_,_,_,_,_):
      return "/v1/ride/\(id)"
    case .Vehicle(let id):
      return "/v1/ride/\(id)"
    case .DeleteVehicle(let id):
      return "/v1/ride/\(id)"
    case .AllAddresses, .AddAddress:
      return "/v1/user/me/address"
    case .Address(let id):
      return "/v1/user/me/address/\(id)"
    case .DeleteAddress(let id):
      return "/v1/user/me/address/\(id)"
    case .UpdateAddress(let id,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_):
      return "/v1/user/me/address/\(id)"
    case .Register:
      return "/v1/user"
    case .Profile, .UpdateProfile:
      return "/v1/user/me"
    case .AllBookings, .BookingsThatNeedRating, .CreateBooking, .Bookings:
      return "/v1/booking"
    case .CancelBooking(let bookingId):
      return "/v1/booking/\(bookingId)"
    case .BookingAdditionalInformation(let bookingId,_):
      return "/v1/booking/\(bookingId)"
    case .UpdateBooking(let bookingId,_,_,_,_,_,_,_):
      return "/v1/booking/\(bookingId)"
    case .UpdateBookingAddress(let bookingId,_,_,_,_,_,_):
      return "/v1/booking/\(bookingId)"
    case .UpdateBookingAddressDetails(let bookingId,_,_,_,_,_,_):
      return "/v1/booking/\(bookingId)"
    case .UpdateBookingVehicle(let bookingId,_,_,_,_,_):
      return "/v1/booking/\(bookingId)"
    case .Booking(let bookingId):
      return "/v1/booking/\(bookingId)"
    case .getTotal:
      return "/v1/booking/getTotal"
    case .CarMakesAndModels:
      return "/v1/make.json"
    case .Country:
      return "/v1/country"
    case .CountryInformation(let countryId):
      return "/v1/country/\(countryId)"
    case .City(let countryId):
      return "/v1/country/\(countryId)/city"
    case .Area(let countryId, let cityId):
      return "/v1/country/\(countryId)/city/\(cityId)/area"
    case .ServiceDateTime(let countryId):
      return "/v1/country/\(countryId)"
    case .CustomerSupport:
      return "/v1/inquiry"
    case .Rating(let bookingId, _, _):
      return "/v1/booking/\(bookingId)/rating"
    case .RegisterPushNotification:
      return "/v1/device-token"
    case .UnregisterPushNotification(let serverTokenID):
      return "/v1/device-token/\(serverTokenID)"
    case .AllCards, .AddCard:
      return "/v1/stripe/card"
    case .DeleteCard(let cardId):
      return "/v1/stripe/card/\(cardId)"
    case .SetCardAsDefault(let cardId):
      return "/v1/stripe/card/\(cardId)"
    case .AllServices:
      return "/v1/product"
    case .Next:
      return ""
    }
  }

  public var method: Moya.Method {
    switch self {
    case .OAuth, .RefreshToken, .Register, .AddVehicle, .AddAddress, .CreateBooking, .CustomerSupport, .Rating, .RegisterPushNotification, .AddCard, .getTotal:
      return .POST
    case .Profile, .CarMakesAndModels, .AllVehicles, .Vehicle, .Address, .AllAddresses, .AllBookings, .BookingsThatNeedRating, .Bookings, .Booking, .Country, .CountryInformation, .City, .ServiceDateTime, .Area, .AllCards, .AllServices, .Next:
      return .GET
    case .DeleteVehicle, .DeleteAddress, .UnregisterPushNotification, .DeleteCard:
      return .DELETE
    case .UpdateVehicle, .UpdateAddress, .CancelBooking, .BookingAdditionalInformation, .UpdateBooking, .UpdateBookingAddress, .UpdateBookingAddressDetails, .UpdateBookingVehicle, .UpdateProfile, .SetCardAsDefault:
      return .PATCH
    }
  }

  public var parameters: [String: AnyObject]? {
    switch self {
    case .OAuth(let username, let password):
      return [
        "client_id": APIKeys.sharedKeys.key,
        "client_secret": APIKeys.sharedKeys.secret,
        "username": username,
        "password":  password,
        "grant_type": "password",
        "scope": "default"
      ]
    case .RefreshToken:
      return [
        "client_id": APIKeys.sharedKeys.key,
        "client_secret": APIKeys.sharedKeys.secret,
        "refresh_token": AccessToken().refresh!,
        "grant_type": "refresh_token",
        "scope": "default"
      ]
    case .AddVehicle(let color, let make, let model, let name, let plateNumber, let type, let year):
      var params = [String: AnyObject]()
      if let color = color {
        params["color"] = color
      }
      if let make = make {
        params["make"] = make
      }
      if let model = model {
        params["model"] = model
      }
      if let name = name {
        params["name"] = name
      }
      if let plateNumber = plateNumber {
        params["plate_number"] = plateNumber
      }
      if let type = type {
        params["type"] = type
      }
      if let year = year {
        params["year"] = year
      }
      return params
    case .UpdateVehicle(_, let color, let make, let model, let name, let plateNumber, let type, let year):
      var params = [String: AnyObject]()
      if let color = color {
        params["color"] = color
      }
      if let make = make {
        params["make"] = make
      }
      if let model = model {
        params["model"] = model
      }
      if let name = name {
        params["name"] = name
      }
      if let plateNumber = plateNumber {
        params["plate_number"] = plateNumber
      }
      if let type = type {
        params["type"] = type
      }
      if let year = year {
        params["year"] = year
      }
      return params
    case .AddAddress(let area, let city, let comment, let coordinates, let country, let street, let building, let userBuilding, let userFloorNumber, let floor, let isIndoor, let landmark, let locationType, let name, let spotNumber, let type):
      var params = [String: AnyObject]()
      if let area = area {
        params["area"] = area
      }
      if let city = city {
        params["city"] = city
      }
      if let comment = comment {
        params["comment"] = comment
      }
      if let coordinates = coordinates {
        params["coordinates"] = coordinates
      }
      if let country = country {
        params["country"] = country
      }
      if let floor = floor {
        params["floor"] = floor
      }
      if let isIndoor = isIndoor {
        params["is_indoor"] = isIndoor
      }
      if let landmark = landmark {
        params["landmark"] = landmark
      }
      if let locationType = locationType {
        params["location_type"] = locationType
      }
      if let name = name {
        params["name"] = name
      }
      if let spotNumber = spotNumber {
        params["spot_number"] = spotNumber
      }
      if let street = street {
        params["street"] = street
      }
      if let building = building {
        params["building"] = building
      }
      if let userBuilding = userBuilding {
        params["user_building"] = userBuilding
      }
      if let userFloorNumber = userFloorNumber {
        params["user_floor_number"] = userFloorNumber
      }
      if let type = type {
        params["type"] = type
      }
      return params
    case .UpdateAddress(_, let area, let city, let comment, let coordinates, let country, let street, let building, let userBuilding, let userFloorNumber, let floor, let isIndoor, let landmark, let locationType, let name, let spotNumber, let type):
      var params = [String: AnyObject]()
      if let area = area {
        params["area"] = area
      }
      if let city = city {
        params["city"] = city
      }
      if let comment = comment {
        params["comment"] = comment
      }
      if let coordinates = coordinates {
        params["coordinates"] = coordinates
      }
      if let country = country {
        params["country"] = country
      }
      if let floor = floor {
        params["floor"] = floor
      }
      if let isIndoor = isIndoor {
        params["is_indoor"] = isIndoor
      }
      if let landmark = landmark {
        params["landmark"] = landmark
      }
      if let locationType = locationType {
        params["location_type"] = locationType
      }
      if let name = name {
        params["name"] = name
      }
      if let spotNumber = spotNumber {
        params["spot_number"] = spotNumber
      }
      if let street = street {
        params["street"] = street
      }
      if let building = building {
        params["building"] = building
      }
      if let userBuilding = userBuilding {
        params["user_building"] = userBuilding
      }
      if let userFloorNumber = userFloorNumber {
        params["user_floor_number"] = userFloorNumber
      }
      if let type = type {
        params["type"] = type
      }
      return params
    case .Register(let firstName, let lastName, let email, let password, let phoneNumber):
      return [
        "mobile_number": phoneNumber,
        "email": email,
        "password": password,
        "first_name": firstName,
        "last_name": lastName,
        "client_id": APIKeys.sharedKeys.key,
        "client_secret": APIKeys.sharedKeys.secret,
      ]
    case .CreateBooking(let addressId, let deliveryDate, let deliveryTime, let rideId, let services, let paymentMethod, let promocode):
      var params: [String: AnyObject] = [
        "address": addressId,
        "delivery_date": deliveryDate.timeIntervalSince1970,
        "delivery_time": ["from":deliveryTime.from!, "to": deliveryTime.to!],
        "ride": rideId,
        "products" : Service.servicesMinimalArray(services),
        "payment_method": paymentMethod.jsonString
      ]
      
      if let promocode = promocode {
        params["promocode"] = promocode
      }
      
      return params
    case .UpdateBooking(_, let addressId, let deliveryDate, let deliveryTime, let rideId, let services, let paymentMethod, let promocode):
      var params = [String: AnyObject]()
      if let addressId = addressId {
        params["address"] = addressId
      }
      if let deliveryDate = deliveryDate {
        params["delivery_date"] = deliveryDate.timeIntervalSince1970
      }
      if let deliveryTime = deliveryTime {
        params["delivery_time"] = ["from":deliveryTime.from!, "to": deliveryTime.to!]
      }
      if let rideId = rideId {
        params["ride"] = rideId
      }
      if let services = services where services.count > 0 {
        params["products"] = Service.servicesMinimalArray(services)
      }
      if let paymentMethod = paymentMethod {
        params["payment_method"] = paymentMethod.jsonString
      }
      if let promocode = promocode {
        params["promocode"] = promocode
      }
      return params
    case .BookingAdditionalInformation(_, let keysInfo):
      var params = [String: AnyObject]()
      params["address_keys_location"] = keysInfo
      return params
    case .UpdateBookingAddress(_, let name, let area, let street, let userBuilding, let userFloorNumber, let comment):
      var params = [String: AnyObject]()
      if let name = name {
        params["address_name"] = name
      }
      if let area = area {
        params["address_area"] = area
      }
      if let street = street {
        params["address_street"] = street
      }
      if let userBuilding = userBuilding {
        params["address_user_building"] = userBuilding
      }
      if let userFloorNumber = userFloorNumber {
        params["address_user_floor_number"] = userFloorNumber
      }
      if let comment = comment {
        params["address_comment"] = comment
      }
      return params
    case .UpdateBookingAddressDetails(_, let isIndoor, let floor, let spotNumber, let comment, let street, let addressLocationType):
      var params = [String: AnyObject]()
      params["address_is_indoor"] = isIndoor
      params["address_floor"] = floor
      params["address_spot_number"] = spotNumber
      params["address_comment"] = comment
      params["address_street"] = street
      params["address_location_type"] = addressLocationType
      return params
    case .UpdateBookingVehicle(_, let name, let color, let make, let model, let plateNumber):
      var params = [String: AnyObject]()
      if let name = name {
        params["ride_name"] = name
      }
      if let color = color {
        params["ride_color"] = color
      }
      if let make = make {
        params["ride_make"] = make
      }
      if let model = model {
        params["ride_model"] = model
      }
      if let plateNumber = plateNumber {
        params["ride_plate_number"] = plateNumber
      }
      return params
    case .getTotal(let addressId, let deliveryDate, let deliveryTime, let rideId, let services, let promocode):
      var params: [String: AnyObject] = [
        "address": addressId,
        "delivery_date": deliveryDate.timeIntervalSince1970,
        "delivery_time": ["from":deliveryTime.from!, "to": deliveryTime.to!],
        "ride": rideId,
        "products" : Service.servicesMinimalArray(services)
      ]
      
      if let promocode = promocode {
        params["promocode"] = promocode
      }
      
      return params
    case .SetCardAsDefault:
      let params = [
        "is_default": true
      ]
      return params
    case .CancelBooking:
      return [
        "status" : "canceled"
      ]
    case .BookingsThatNeedRating:
      return [
        "page" : 1,
        "page_size" : 50,
        "status[]": BookingStatus.Completed.rawValue,
        "needs_rating": "true"
      ]
    case .Bookings(let vehicleId, let from):
      return [
        "page" : 1,
        "page_size" : 25,
        "ride": vehicleId,
        "from": from.yearMonthDayDashedFormat
      ]
    case .Profile, .CarMakesAndModels, .AllBookings, .AllVehicles, .Vehicle, .DeleteVehicle, .Address,
       .AllAddresses, .DeleteAddress, .Booking, .Country, .CountryInformation, .City, .ServiceDateTime,
       .Area, .UnregisterPushNotification, .AllCards, .DeleteCard, .AllServices, .Next:
      return [:]
    case .UpdateProfile(let firstName, let lastName, let email, let phoneNumber):
      return [
        "mobile_number": phoneNumber,
        "email": email,
        "first_name": firstName,
        "last_name": lastName
      ]
    case .CustomerSupport(let type, let body, let bookingId):
      return [
        "type": type.lowercaseString,
        "body": body,
        "booking_id": bookingId ?? "",
        "country_id": "LB"
      ]
    case .Rating(_, let rating, let comment):
      return [
        "rating": rating,
        "comment": comment ?? ""
      ]
    case .RegisterPushNotification(let deviceToken):
      return [
        "token" : deviceToken,
        "device_type" : "apple",
        "uuid" : UIDevice.currentDevice().identifierForVendor!.UUIDString
      ]
    case .AddCard(let token, let isDefault):
      return [
        "token": token,
        "is_default": isDefault
      ]
    }
  }

  public var sampleData: NSData {
    return stubbedResponse(self)
  }
}

//==================================================================
// MARK: PROVIDER
//==================================================================

public struct APIProvider {
  private static var endpointsClosure = { (target: BlinkMyCarAPI) -> Endpoint<BlinkMyCarAPI> in
    var endpoint: Endpoint<BlinkMyCarAPI> = Endpoint<BlinkMyCarAPI>(
      URL: url(target),
      sampleResponseClosure: { () -> EndpointSampleResponse in
        return EndpointSampleResponse.NetworkResponse(200, target.sampleData)
      },
      method: target.method,
      parameters: target.parameters,
      parameterEncoding: (target.method == .GET) ? Moya.ParameterEncoding.URL : Moya.ParameterEncoding.JSON,
      httpHeaderFields: nil)

    var headerParameters = [String : String]();
    switch target{
    case .OAuth:
      break
    case .Register:
      break
    case .CarMakesAndModels:
      break
    default:
      let token = AccessToken()
      if token.isValid {
        headerParameters["Authorization"] = "Bearer \(token.token!)"
      }
      break
    }

    headerParameters["Content-Type"] = "application/json";
    headerParameters["Accept"] = "application/json"
    headerParameters["User-Agent"] = userAgentValue

    if headerParameters.count > 0 {
      endpoint = endpoint.endpointByAddingHTTPHeaderFields(headerParameters)
    }

    return endpoint
  }

  private static func timeoutForHTTPMethod(method: Moya.Method) -> NSTimeInterval {
    switch method {
    case .GET:
      return 8
    case .POST:
      return 10
    case .DELETE:
      return 5
    case .PATCH:
      return 10
    default:
      return 10
    }
  }

  private static func endpointResolver() -> ((endpoint: Endpoint<BlinkMyCarAPI>) -> (NSURLRequest)) {
    return { (endpoint: Endpoint<BlinkMyCarAPI>) -> (NSURLRequest) in
      let request: NSMutableURLRequest = endpoint.urlRequest.mutableCopy() as! NSMutableURLRequest
      request.HTTPShouldHandleCookies = false
      request.timeoutInterval = self.timeoutForHTTPMethod(endpoint.method)
      if endpoint.method == .GET {
        // This is needed otherwise the GET requests will always fail.
        request.HTTPBody = nil
      }
      return request
    }
  }

  private static var requestClosure = { (endpoint: Endpoint<BlinkMyCarAPI>, done: NSURLRequest -> Void) -> Void in
    let request: NSMutableURLRequest = endpoint.urlRequest.mutableCopy() as! NSMutableURLRequest
    request.HTTPShouldHandleCookies = false
    request.timeoutInterval = APIProvider.timeoutForHTTPMethod(endpoint.method)
    if endpoint.method == .GET {
      // This is needed otherwise the GET requests will always fail.
      request.HTTPBody = nil
    }
    done(request)
  }

  public static func mediaURLForPath(path: String) -> NSURL {
    // We just need any instance of BlinkMyCarAPI
    return BlinkMyCarAPI.AllAddresses.baseURL.URLByAppendingPathComponent(path.stringByReplacingOccurrencesOfString("/public/", withString: "/", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil))
  }
  
  private static var alamofireManager: Moya.Manager {
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    configuration.requestCachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
    configuration.HTTPAdditionalHeaders = Alamofire.Manager.defaultHTTPHeaders
    
    let serverTrustPolicies: [String: ServerTrustPolicy] = [
      "bmc.api.keeward.me": .DisableEvaluation,
      "bmc.api-d-01.keeward.me": .DisableEvaluation
    ]
    
    let manager = Alamofire.Manager(configuration: configuration, serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    // Prevent redirecting a request:
    manager.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
      return nil
    }
    
    manager.startRequestsImmediately = false
    return manager
  }

  public static func DefaultProvider() -> MoyaProvider<BlinkMyCarAPI> {
    return MoyaProvider(endpointClosure:endpointsClosure,
      requestClosure:requestClosure,
      stubClosure: { (target: BlinkMyCarAPI) -> StubBehavior in
        return StubBehavior.Never
      },
      manager: alamofireManager
    )
  }

  public static func StubbedProvider() -> MoyaProvider<BlinkMyCarAPI> {
    return MoyaProvider(endpointClosure:endpointsClosure,
      requestClosure:requestClosure,
      stubClosure: { (target: BlinkMyCarAPI) -> StubBehavior in
        return StubBehavior.Delayed(seconds: 3)
      },
      manager: alamofireManager
    )
  }

  private struct SharedProvider{
    static var instance = SharedProvider.stubResponses() ? APIProvider.StubbedProvider() : APIProvider.DefaultProvider()

    private static func stubResponses() -> Bool {
      let env = NSProcessInfo.processInfo().environment
      if let mode = env["STUB_RESPONSES"] {
        return mode == "YES"
      } else {
        return false
      }
    }
  }

  public static var sharedProvider: MoyaProvider<BlinkMyCarAPI> {
    get {
    return SharedProvider.instance
    }

    set (newSharedProvider) {
      SharedProvider.instance = newSharedProvider
    }
  }
}

//==================================================================
// MARK: HELPERS
//==================================================================

private func refreshAccessToken(completion: (success:Bool) -> Void) -> Cancellable? {
  var accessToken = AccessToken()
  accessToken.isUpdating = true
  
  guard accessToken.refresh != nil else {
    NSNotificationCenter.defaultCenter()
      .postNotificationName(BMCNotificationNames.FailToRefreshToken.rawValue, object: nil)
    completion(success: false)
    return nil
  }
  
  return APIProvider.sharedProvider.request(BlinkMyCarAPI.RefreshToken,
    completion: { (result) -> () in
      var accessToken = AccessToken()
      accessToken.isUpdating = false

      switch result {
      case let .Success(response):
        if case response.statusCode = 400 {
          NSNotificationCenter.defaultCenter()
            .postNotificationName(BMCNotificationNames.FailToRefreshToken.rawValue, object: nil)
          completion(success: false)
          return
        }

        do {
          if let accessTokenDictionary = try NSJSONSerialization.JSONObjectWithData(response.data,
            options: NSJSONReadingOptions.AllowFragments) as? NSDictionary {
              accessToken.readFromDictionary(accessTokenDictionary)
              completion(success: true)
          } else {
            completion(success: false)
          }
        } catch {
          completion(success: false)
        }
      case let .Failure(error):
        print("Error Refreshing token: \(error)")
        completion(success: false)
        break
      }
  })
}

private func createRequest(token: BlinkMyCarAPI, completion: APICompletion) -> ( () -> Cancellable ) {
  return { return APIProvider.sharedProvider.request(token, completion: { result -> () in
    switch result {
    case .Success(let response):
      if response.statusCode == 301 {
        let redirectURL = (response.response as! NSHTTPURLResponse).allHeaderFields["Location"] as! String
        
        NSNotificationCenter.defaultCenter().postNotificationName(
          BMCNotificationNames.AppNeedsUpdate.rawValue,
          object: redirectURL)
        // Do not complete the request
        return
      }
      completion(data: response.data, statusCode: response.statusCode, response: response.response, error: nil)
    case .Failure:
      completion(data: nil, statusCode: nil, response: nil, error: nil)
    }
  })
  }
}

private func executePendingOperations(success: Bool) {
  let opQueue = operationQueue
  operationQueue.removeAll(keepCapacity: false)
  if success {
    for op in opQueue {
      signedRequest(op.token, completion: op.completion)
    }
  } else {
    for op in opQueue {
      op.completion(data: nil,
        statusCode: 500,
        response: nil,
        error: NSError(domain: "com.waynak.network",
          code: -1,
          userInfo: ["error": "Could not refresh access token."]))
    }
  }
}


public func signedRequest(token: BlinkMyCarAPI, completion: APICompletion) -> Cancellable? {
  let apiCall = createRequest(token, completion: completion)
  
  let accessToken = AccessToken()
  
  if (accessToken.updating) {
    operationQueue.append((token: token, completion: completion))
    return nil
  }
  
  if !accessToken.isValid {
    return refreshAccessToken({ (success) -> Void in
      if success {
        apiCall()
      } else {
        completion(data: nil,
          statusCode: 500,
          response: nil,
          error: NSError(domain: "com.blinkmycar.network",
            code: -1,
            userInfo: ["error": "Could not refresh access token."]))
      }
      
      executePendingOperations(success)
    })
  }

  return apiCall()
}

private func createMultipartRequest(urlString: String,
  data: NSData,
  fileName: String,
  fieldName: String,
  mimeType: String) -> NSURLRequest {
    let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW";
    let contentType = "multipart/form-data; boundary=" + boundaryConstant
    let boundaryStart = "--\(boundaryConstant)\r\n"
    let boundaryEnd = "--\(boundaryConstant)--\r\n"
    let contentDispositionString = "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n"
    let contentTypeString = "Content-Type: \(mimeType)\r\n\r\n"

    // Prepare the HTTPBody for the request.
    let requestBodyData : NSMutableData = NSMutableData()
    requestBodyData.appendData(boundaryStart.dataUsingEncoding(NSUTF8StringEncoding)!)
    requestBodyData.appendData(contentDispositionString.dataUsingEncoding(NSUTF8StringEncoding)!)
    requestBodyData.appendData(contentTypeString.dataUsingEncoding(NSUTF8StringEncoding)!)
    requestBodyData.appendData(data)
    requestBodyData.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    requestBodyData.appendData(boundaryEnd.dataUsingEncoding(NSUTF8StringEncoding)!)

    let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
    mutableURLRequest.setValue("Bearer \(AccessToken().token!)", forHTTPHeaderField: "Authorization")
    mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
    mutableURLRequest.setValue(userAgentValue, forHTTPHeaderField: "User-Agent")
    mutableURLRequest.HTTPBody = requestBodyData
    mutableURLRequest.HTTPMethod = "PATCH"
    return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0
}

public func uploadRequest(api:BlinkMyCarAPI,
  data:NSData,
  progress: (percentage: CGFloat, json: AnyObject?, error: NSError?) -> Void) {
    let apiCall: () -> Void = {
      let urlRequest = createMultipartRequest(api.baseURL.URLByAppendingPathComponent(api.path).absoluteString,
        data: data, fileName: api.uploadFileName, fieldName: api.uploadAttributeName, mimeType: api.uploadMimeType)
      
      Alamofire.request(urlRequest).progress({ (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) -> Void in
        let percentage = CGFloat(totalBytesWritten) / CGFloat(totalBytesExpectedToWrite)
        if percentage != 1 {
          progress(percentage: percentage, json: nil,
            error: nil)
        }
      }).response(completionHandler: { (request, response, JSON, error) -> Void in
        if let error = error {
          progress(percentage: 0, json: nil, error: error)
          return
        }
        
        if response!.statusCode != 200 {
          progress(percentage: 0,
            json: nil,
            error: NSError(domain: "com.blinkmycar.network",
              code: BlinkMyCarErrorCodes.UploadFailed.rawValue,
              userInfo: ["httpStatusCode": response!.statusCode]))
          return
        }
        
        progress(percentage: 1, json: JSON, error: nil)
      })
    }
    
    if !AccessToken().isValid {
      refreshAccessToken({ (success) -> Void in
        if success {
          apiCall()
        } else {
          progress(percentage: 0,
            json: nil,
            error: NSError(domain: "com.blinkmycar.network",
              code: -1,
              userInfo: ["error": "Could not refresh access token."]))
        }
      })
    } else {
      apiCall()
    }
}


public func url(route: TargetType) -> String {
  guard !route.path.isEmpty else {
    return route.baseURL.absoluteString
  }
  
  return route.baseURL.URLByAppendingPathComponent(route.path).absoluteString
}

private func stubbedResponse(blinkMyCarAPI: BlinkMyCarAPI) -> NSData! {
  var filename: String = ""
  switch blinkMyCarAPI {
  case .OAuth, .RefreshToken:
    filename = "oauth"
  case .AllVehicles:
    filename = "allVehicles"
  case .Vehicle, .AddVehicle, .UpdateVehicle:
    filename = "vehicle"
  case .Register:
    filename = "register"
  case .Profile:
    filename = "profile"
  case .CarMakesAndModels:
    filename = "make"
  case .AllAddresses:
    filename = "allAddresses"
  case .Address, .AddAddress, .UpdateAddress:
    filename = "address"
  case .AllBookings, .Bookings:
    filename = "allBookings"
  case .CreateBooking, .CancelBooking, .Booking, .UpdateBooking, .UpdateBookingAddress, .UpdateBookingAddressDetails, .UpdateBookingVehicle:
    filename = "booking"
  case .Country:
    filename = "countries"
  case .CountryInformation:
    filename = "country"
  case .City:
    filename = "cities"
  case .Area:
    filename = "areas"
  case .ServiceDateTime:
    filename = "serviceDatesAndTimes"
  case .DeleteVehicle, .DeleteAddress:
    fallthrough
  default:
    filename = ""
  }

  let bundle = NSBundle.mainBundle()
  let path = "\(bundle.resourcePath!)/\(filename).json"
  return NSData(contentsOfFile: path)
}

//==================================================================
// MARK: PARSING
//==================================================================

extension BlinkMyCarAPI {
  public static func retrieveErrorFromData(data: NSData?) -> NSError? {
    do {
      if let data = data,
        let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject],
        let statusCode = jsonDictionary["status"] as? Int,
        let validationMessageDictionary = jsonDictionary["validation_messages"] as? [String:AnyObject],
        let validationKey = validationMessageDictionary.keys.first,
        let errorDictionary = validationMessageDictionary[validationKey] as? [String:String],
        let errorMessageKey = errorDictionary.keys.first,
        let errorMessage = errorDictionary[errorMessageKey] where !errorMessage.isEmpty
      {
        let error = NSError(domain: "com.blinkmycar.network", code: statusCode, userInfo: ["error":errorMessage])
        return error
      }
    } catch {}
    
    return nil
  }
}
