//
//  String+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/18/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

extension String {
  func isValidEmail() -> Bool {
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    let predicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
    return predicate.evaluateWithObject(self)
  }

  func isValidURL() -> Bool {
    let urlRegex = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
    let predicate = NSPredicate(format: "SELF MATCHES %@", urlRegex)
    return predicate.evaluateWithObject(self)
  }

  func localizedWithComment(comment: String) -> String {
    return NSLocalizedString(self,
      tableName: nil,
      bundle: NSBundle.mainBundle(),
      comment: comment)
  }

  func trim() -> String?  {
    return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
  }
  
  static func timeStringForTimeInterval(timeInterval timeInterval: Double) -> String {
    let ti: Int = Int(timeInterval)
    let seconds: Int = ti % 60
    let minutes: Int = (ti / 60) % 60
    let hours: Int = (ti / 3600)
    
    let sec: String = (seconds > 9) ? "\(seconds)" : "0\(seconds)"
    let min: String = (minutes > 9) ? "\(minutes)" : "0\(minutes)"
    let hr: String = (hours > 9) ? "\(hours)" : "0\(hours)"
    
    if (hours > 0) {
      return "\(hr):\(min):\(sec)"
    }
    else {
      return "\(min):\(sec)"
    }
  }
  
  func boolValue() -> Bool? {
    let selfValue = self.lowercaseString
    switch selfValue {
    case "true", "yes", "1":
      return true
    case "false", "no", "0":
      return false
    default:
      return nil
    }
  }
}
