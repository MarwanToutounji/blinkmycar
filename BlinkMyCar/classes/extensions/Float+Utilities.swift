//
//  Float+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan  on 5/12/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

extension Float {
  var displayAsUsDollarsPrice: String {
    // TODO: replace this with NSNumberFormatterStyle.CurrencyStyle
    return "$\(self.cleanValue)"
  }
  
  var cleanValue: String {
    return self % 1 == 0 ? String(format: "%.0f", self) : String(format: "%.1f", self)
  }
}
