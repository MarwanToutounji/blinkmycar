//
//  CLLocationCoodinate2D.swift
//  BlinkMyCar
//
//  Created by Marwan  on 4/11/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
  static func coodinatesFromString(coordinatesString: String?) -> CLLocationCoordinate2D? {
    if let coordinatesString = coordinatesString {
      let coordinatesArray = coordinatesString.characters.split {$0 == ","}.map { String($0) }
      if coordinatesArray.count == 2 {
        return CLLocationCoordinate2D(
          latitude: (coordinatesArray[0] as NSString).doubleValue,
          longitude: (coordinatesArray[1] as NSString).doubleValue
        )
      }
    }
    return nil
  }
  
  func coordinatesToString() -> String {
    return "\(latitude),\(longitude)"
  }
}
