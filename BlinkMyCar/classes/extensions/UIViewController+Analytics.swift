//
//  UIViewController+Analytics.swift
//  BlinkMyCar
//
//  Created by Joseph on 7/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

extension UIViewController {
  internal func sendScreenView(screenName: String) {
    let tracker = GAI.sharedInstance().defaultTracker
    tracker.set(kGAIScreenName, value: screenName)
    
    let builder = GAIDictionaryBuilder.createScreenView()
    tracker.send(builder.build() as [NSObject : AnyObject])
  }
  
  public func trackEvent(category: String, action: String, label: String, value: NSNumber?) {
    let tracker = GAI.sharedInstance().defaultTracker
    if let userId = PersistenceManager.sharedManager.currentUser?.hashedId {
      // TODO: Remove this print later on
      print("Hashed User ID: \(userId)")
      tracker.set(kGAIUserId, value: userId)
    }
    let trackDictionary = GAIDictionaryBuilder.createEventWithCategory(category, action: action, label: label, value: value)
    tracker.send(trackDictionary.build() as [NSObject : AnyObject])
  }
}
