//
//  UIImagePickerController+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import AVFoundation

extension UIImagePickerController {
  var hasCameraAccess: Bool {
    let mediaAccessType = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
    switch mediaAccessType {
    case .Denied: //camera access is blocked
      fallthrough
    case .Restricted: // camera access is restricted
      return false
    case .Authorized: //camera access is allowed
      fallthrough
    case .NotDetermined: // camera access has never been requested
      fallthrough
    default:
      return true
    }
  }
  
  func showNeedPermissionAlert(vc: UIViewController, cancelledCompletionBlock: (()-> Void)? = nil) {
    let alert = UIAlertController(
      title: noAccessPermissionForCameraAlertTitle,
      message: noAccessPermissionForCameraAlertMessage,
      preferredStyle: .Alert)
    
    alert.addAction(
      UIAlertAction(
        title: noAccessPermissionForCameraAlertNeutralActionButtonTitle,
        style: .Default,
        handler: { action in
          //Do nothing for now
          cancelledCompletionBlock?()
      }))
    
    alert.addAction(
      UIAlertAction(
        title: noAccessPermissionForCameraAlertSettingsActionButtonTitle,
        style: .Default,
        handler: { action in
          UIApplication.sharedApplication().openURL(NSURL(string:UIApplicationOpenSettingsURLString)!)
      }))
    
    vc.presentViewController(alert, animated:true, completion:nil)
  }
  
  public var noAccessPermissionForCameraAlertTitle: String {
    return NSLocalizedString(
      "UIImagePickerController.permissionNeededAlert.title",
      value: "Camera Access Disabled",
      comment: "Alert title to tell the user to open the settings and enable the camera permission for the app")
  }
  
  public var noAccessPermissionForCameraAlertMessage: String {
    return  NSLocalizedString(
      "UIImagePickerController.permissionNeededAlert.message",
      value: "Please grand access to camera in \nSettings->BlinkMyCar->Camera.",
      comment: "Alert message to tell the user to open the settings and enable the camera permission for the app")
  }
  
  public var noAccessPermissionForCameraAlertNeutralActionButtonTitle: String {
    return NSLocalizedString(
      "UIImagePickerController.permissionNeededAlert.cancelButtonTitle",
      value: "OK",
      comment: "Alert cancel button title")
  }
  
  public var noAccessPermissionForCameraAlertSettingsActionButtonTitle: String {
    return NSLocalizedString(
      "UIImagePickerController.permissionNeededAlert.settingsButton",
      value: "Settings",
      comment: "Alert Open settings button")
  }
}