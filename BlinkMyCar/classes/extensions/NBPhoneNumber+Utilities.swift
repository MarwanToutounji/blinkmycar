//
//  NBPhoneNumber+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 2/16/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import libPhoneNumber_iOS

extension NBPhoneNumber {
  public static func getFormattedPhoneNumber(phoneNumber: String?) -> String? {
    let phoneUtil = NBPhoneNumberUtil()
    if let phoneNumber = phoneNumber {
      do {
        let number:NBPhoneNumber = try phoneUtil.parse(phoneNumber,
          defaultRegion:"LB")
        let formattedPhoneNumber = try phoneUtil.format(number,
          numberFormat: NBEPhoneNumberFormat.INTERNATIONAL)
        return formattedPhoneNumber
      } catch let error {
        print("Error: \(error)")
        return ""
      }
    }
    return ""
  }

  public static func getFormattedPhoneNumberLocal(phoneNumber: String?) -> String {
    let phoneUtil = NBPhoneNumberUtil()
    if let phoneNumber = phoneNumber {
      do {
        let number:NBPhoneNumber = try phoneUtil.parse(phoneNumber,
          defaultRegion:"LB")
        let formattedPhoneNumber = try phoneUtil.format(number,
          numberFormat: NBEPhoneNumberFormat.NATIONAL)
        return formattedPhoneNumber
      } catch let error {
        print("Error: \(error)")
        return ""
      }
    }
    return ""
  }
}