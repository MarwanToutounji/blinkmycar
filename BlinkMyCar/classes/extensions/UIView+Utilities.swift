//
//  UIView+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/23/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
  func afterUsingCoderLoadNibIfNeeded(nibName: String) -> AnyObject? {
    let isJustAPlaceholder = self.subviews.count == 0
    if isJustAPlaceholder {
      let view = NSBundle.mainBundle().loadNibNamed(nibName,
        owner: nil, options: nil)[0] as! UIView

      view.autoresizingMask = self.autoresizingMask
      view.translatesAutoresizingMaskIntoConstraints = self.translatesAutoresizingMaskIntoConstraints

      for constraint in self.constraints {
        var firstItem = constraint.firstItem as? NSObject
        if firstItem == self {
          firstItem = view
        }

        var secondItem = constraint.secondItem as? NSObject
        if secondItem == self {
          secondItem = view
        }

        let newConstraint = NSLayoutConstraint(
          item: firstItem!,
          attribute: constraint.firstAttribute,
          relatedBy: constraint.relation,
          toItem: secondItem,
          attribute: constraint.secondAttribute,
          multiplier: constraint.multiplier,
          constant: constraint.constant)

        view.addConstraint(newConstraint)
      }

      self.removeConstraints(self.constraints)

      return view
    }
    
    return self
  }

  func cicularView() {
    self.layer.masksToBounds = false
    makeCurvedEdges(self.frame.size.height/2)
    addBorder(color: UIColor.whiteColor())
  }
  
  func makeRounded() {
    makeCurvedEdges(self.frame.size.height/2)
  }
  
  func makeCurvedEdges(cornerRadius: CGFloat = 10) {
    self.layer.cornerRadius = cornerRadius
    self.clipsToBounds = true
  }
  
  func addBorder(color color: UIColor, width: CGFloat = 1) {
    self.layer.borderColor = color.CGColor
    self.layer.borderWidth = width
  }

  func triangularView(color color: UIColor) {
    backgroundColor = UIColor.clearColor()
    clipsToBounds = false

    let mask = CAShapeLayer()
    mask.frame = self.layer.bounds

    let width = self.layer.frame.size.width
    let height = self.layer.frame.size.height

    let path = CGPathCreateMutable()

    //draw triangle
    CGPathMoveToPoint(path, nil, 0, 0)
    CGPathAddLineToPoint(path, nil, width, 0)
    CGPathAddLineToPoint(path, nil, width/2, height)
    //Remove edginess
    CGPathAddLineToPoint(path, nil, width/2 + 1, height - 1)
    CGPathAddLineToPoint(path, nil, width/2 - 1, height - 1)


    let shape = CAShapeLayer()
    shape.frame = self.bounds
    shape.path = path
    shape.lineWidth = 0.0
    shape.cornerRadius = 9.0
    shape.fillColor = color.CGColor

    self.layer.insertSublayer(shape, atIndex: 0)
  }
  
  public func addTapGesture(viewController: ViewTapDelegate){
    let tapGesture = UITapGestureRecognizer(target: viewController, action: Selector("didTapToCancel"))
    tapGesture.numberOfTapsRequired = 1
    self.userInteractionEnabled = true
    self.addGestureRecognizer(tapGesture)
  }

  class func loadFromNib(nibName: String, owner: AnyObject!) -> UIView {
    return NSBundle.mainBundle().loadNibNamed(
      nibName,
      owner: owner,
      options: nil)[0] as! UIView
  }
  
  func clearSubviews() {
    for view in subviews {
      view.removeFromSuperview()
    }
  }
}

public protocol ViewTapDelegate : NSObjectProtocol {
  func didTapToCancel()
}
