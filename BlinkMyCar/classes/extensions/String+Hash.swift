//
//  String+Hash.swift
//  BlinkMyCar
//
//  Created by Marwan  on 10/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

extension String {
  static func md5(string string: String) -> String {
    var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
    if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
      CC_MD5(data.bytes, CC_LONG(data.length), &digest)
    }
    
    var digestHex = ""
    for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
      digestHex += String(format: "%02x", digest[index])
    }
    
    return digestHex
  }
}
