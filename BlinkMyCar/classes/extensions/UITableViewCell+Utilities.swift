//
//  UITableViewCell+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/12/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

extension UITableViewCell {
  func hideSeparator() {
    // Remove separator inset
    if self.respondsToSelector(Selector("setSeparatorInset:")) {
      self.separatorInset = UIEdgeInsetsZero
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if self.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
      self.preservesSuperviewLayoutMargins = false
    }
    
    // Explictly set your cell's layout margins
    if self.respondsToSelector(Selector("setLayoutMargins:")) {
      self.layoutMargins = UIEdgeInsetsZero
    }
  }
  
  func showSeparator() {
    // Remove separator inset
    if self.respondsToSelector(Selector("setSeparatorInset:")) {
      self.separatorInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if self.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
      self.preservesSuperviewLayoutMargins = true
    }
    
    // Explictly set your cell's layout margins
    if self.respondsToSelector(Selector("setLayoutMargins:")) {
      self.layoutMargins = UIEdgeInsetsZero
    }
  }
}
