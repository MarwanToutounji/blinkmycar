//
//  UIViewController+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan on 3/28/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

@objc protocol Dismissable: class {
  func dismiss(sender: AnyObject?)
}

protocol ReloadableViewController {
  func needsReload() -> Bool
}

extension UIViewController {
  public static func getVisiblePresentedViewController(viewController: UIViewController) -> UIViewController {
    guard let vc = viewController.presentedViewController else {
      return viewController
    }
    return getVisiblePresentedViewController(vc)
  }
  
  public func addViewController(viewController: UIViewController,
    toView: UIView) -> UIView {
      // call before adding child view controller's view as subview
      self.addChildViewController(viewController)
      
      let view: UIView = viewController.view
      toView.addSubview(view)
      
      // call after adding child view controller's view as subview
      viewController.didMoveToParentViewController(parentViewController)
      
      return view
  }
  
  public var compactSize: Bool {
    get {
      return DeviceType.IS_IPHONE_5_OR_LESS
    }
  }
}
