//
//  NSDate+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/31/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

extension NSDate {
  var stringValueToAttributedText: String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "EEEE',' MMM dd"
    return dateFormatter.stringFromDate(self)
  }

  var stringValueMonthAndDay: String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "MMM dd"
    return dateFormatter.stringFromDate(self)
  }

  var dayOfTheWeek: DayOfTheWeek {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "e"
    let dayNumber = (dateFormatter.stringFromDate(self) as NSString).integerValue
    return DayOfTheWeek(rawValue: dayNumber)!
  }

  var hour: Int {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "k"
    let hourNumber = (dateFormatter.stringFromDate(self) as NSString).integerValue
    return hourNumber
  }
  
  var hourString: String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "h:mm a"
    return dateFormatter.stringFromDate(self)
  }
  
  var yearMonthDayDashedFormat: String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy'-'MM'-'dd"
    return dateFormatter.stringFromDate(self)
  }

  func sameDate(date:NSDate) -> Bool {
    let calendar = NSCalendar.currentCalendar()
    let comps: NSCalendarUnit = [NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year]

    let date1Component = calendar.components(comps, fromDate: self)
    let date2Component = calendar.components(comps, fromDate: date)

    let date1 = calendar.dateFromComponents(date1Component)
    let date2 = calendar.dateFromComponents(date2Component)

    let result = date1!.compare(date2!)
    switch result {
    case .OrderedSame:
      return true
    case .OrderedAscending, .OrderedDescending:
      return false
    }
  }

  func compareDateTime(date:NSDate) -> NSComparisonResult {
    let calendar = NSCalendar.currentCalendar()
    let comps: NSCalendarUnit = [NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second]

    let date1Component = calendar.components(comps, fromDate: self)
    let date2Component = calendar.components(comps, fromDate: date)

    let date1 = calendar.dateFromComponents(date1Component)
    let date2 = calendar.dateFromComponents(date2Component)

    let result = date1!.compare(date2!)
    return result
  }
  
  /// Discard the time
  func compareDate(date:NSDate) -> NSComparisonResult {
    let calendar = NSCalendar.currentCalendar()
    let comps: NSCalendarUnit = [NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year]
    
    let date1Component = calendar.components(comps, fromDate: self)
    let date2Component = calendar.components(comps, fromDate: date)
    
    let date1 = calendar.dateFromComponents(date1Component)
    let date2 = calendar.dateFromComponents(date2Component)
    
    let result = date1!.compare(date2!)
    return result
  }

  func dateWithTime(hour: Int = 0, minute: Int = 0, second: Int = 0) -> NSDate? {
    let calendar = NSCalendar.currentCalendar()
    let comps: NSCalendarUnit = [NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second]
    let dateComponent = calendar.components(comps, fromDate: self)
    dateComponent.hour = hour
    dateComponent.minute = minute
    dateComponent.second = second
    let date = calendar.dateFromComponents(dateComponent)
    return date
  }

  var nextDay: NSDate {
    let numberOfDays: Double = 1
    return self.dateByAddingTimeInterval(60*60*24*numberOfDays)
  }
  
  var yesterday: NSDate {
    let aDayInSeconds: Double = 24*60*60
    return NSDate(timeIntervalSince1970: self.timeIntervalSince1970-aDayInSeconds)
  }
}

enum DayOfTheWeek: Int {
  case Sunday = 1
  case Monday = 2
  case Tuesday = 3
  case Wednesday = 4
  case Thursday = 5
  case Friday = 6
  case Saturday = 7
}