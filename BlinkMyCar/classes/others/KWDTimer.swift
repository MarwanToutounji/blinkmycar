//
//  KWDTimer.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/14/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

class KWDTimer {
  var timer: NSTimer?
  let timeOutTime: NSTimeInterval = 0.6
  
  func startTimer(target: AnyObject, selector: Selector) {
    stopTimer()
    
    timer = NSTimer(
      timeInterval: timeOutTime,
      target: target,
      selector: selector,
      userInfo: nil,
      repeats: false)
    NSRunLoop.currentRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
  }
  
  func stopTimer() {
    if timer != nil {
      timer!.invalidate()
      timer = nil
    }
  }
}
