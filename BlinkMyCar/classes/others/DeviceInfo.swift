//
//  DeviceInfo.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/14/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

struct ScreenSize
{
  static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
  static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
  static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
  static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
  static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
  static let IS_IPHONE_5_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
  static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
  static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
  static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

extension DeviceType {
  static var deviceType: String {
    let userInterfaceIdiom: UIUserInterfaceIdiom = UIDevice.currentDevice().userInterfaceIdiom
    switch userInterfaceIdiom {
    case .Phone:
      return "iPhone"
    case .Pad:
      return "iPad"
    case .Unspecified:
      fallthrough
    default:
      return "Unspecified"
    }
  }

}


public extension UIDevice {
  var type: String {
    switch userInterfaceIdiom {
    case .Phone:
      return "iPhone"
    case .Pad:
      return "iPad"
    case .Unspecified:
      fallthrough
    default:
      return "Unspecified"
    }
  }
}

// Source: http://www.jianshu.com/p/d66575b17f78
public enum Model : String {
  case simulator = "simulator/sandbox",
  iPod1          = "iPod1",
  iPod2          = "iPod2",
  iPod3          = "iPod3",
  iPod4          = "iPod4",
  iPod5          = "iPod5",
  iPad2          = "iPad2",
  iPad3          = "iPad3",
  iPad4          = "iPad4",
  iPhone4        = "iPhone4",
  iPhone4S       = "iPhone4S",
  iPhone5        = "iPhone5",
  iPhone5S       = "iPhone5S",
  iPhone5C       = "iPhone5C",
  iPadMini1      = "iPadMini1",
  iPadMini2      = "iPadMini2",
  iPadMini3      = "iPadMini3",
  iPadAir1       = "iPadAir1",
  iPadAir2       = "iPadAir2",
  iPhone6        = "iPhone6",
  iPhone6plus    = "iPhone6Plus",
  iPhone6S       = "iPhone6S",
  iPhone6Splus   = "iPhone6SPlus",
  unrecognized   = "?unrecognized?"
}

public extension UIDevice {
  public var device: Model {
    var systemInfo = utsname()
    uname(&systemInfo)
    let modelCode = withUnsafeMutablePointer(&systemInfo.machine) {
      ptr in String.fromCString(UnsafePointer<CChar>(ptr))
    }
    var modelMap : [ String : Model ] = [
      "i386"      : .simulator,
      "x86_64"    : .simulator,
      "iPod1,1"   : .iPod1,
      "iPod2,1"   : .iPod2,
      "iPod3,1"   : .iPod3,
      "iPod4,1"   : .iPod4,
      "iPod5,1"   : .iPod5,
      "iPad2,1"   : .iPad2,
      "iPad2,2"   : .iPad2,
      "iPad2,3"   : .iPad2,
      "iPad2,4"   : .iPad2,
      "iPad2,5"   : .iPadMini1,
      "iPad2,6"   : .iPadMini1,
      "iPad2,7"   : .iPadMini1,
      "iPhone3,1" : .iPhone4,
      "iPhone3,2" : .iPhone4,
      "iPhone3,3" : .iPhone4,
      "iPhone4,1" : .iPhone4S,
      "iPhone5,1" : .iPhone5,
      "iPhone5,2" : .iPhone5,
      "iPhone5,3" : .iPhone5C,
      "iPhone5,4" : .iPhone5C,
      "iPad3,1"   : .iPad3,
      "iPad3,2"   : .iPad3,
      "iPad3,3"   : .iPad3,
      "iPad3,4"   : .iPad4,
      "iPad3,5"   : .iPad4,
      "iPad3,6"   : .iPad4,
      "iPhone6,1" : .iPhone5S,
      "iPhone6,2" : .iPhone5S,
      "iPad4,1"   : .iPadAir1,
      "iPad4,2"   : .iPadAir2,
      "iPad4,4"   : .iPadMini2,
      "iPad4,5"   : .iPadMini2,
      "iPad4,6"   : .iPadMini2,
      "iPad4,7"   : .iPadMini3,
      "iPad4,8"   : .iPadMini3,
      "iPad4,9"   : .iPadMini3,
      "iPhone7,1" : .iPhone6plus,
      "iPhone7,2" : .iPhone6,
      "iPhone8,1" : .iPhone6S,
      "iPhone8,2" : .iPhone6Splus
    ]

    if let model = modelMap[String.fromCString(modelCode!)!] {
      return model
    }
    return Model.unrecognized
  }
}
