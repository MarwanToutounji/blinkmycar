//
//  GeneralSettings.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 8/7/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public struct GeneralSettings {
  private enum SettingsKeys: String {
    case SendUsageDataKey = "SendUsageData"
    case BookingSlotDuration = "BookingSlotDuration"
  }

  private let defaults = NSUserDefaults.standardUserDefaults()

  public var sendUsageData: Bool {
    get {
      if let savedValue: AnyObject = defaults.objectForKey(SettingsKeys.SendUsageDataKey.rawValue) {
        return savedValue.boolValue
      }
      defaults.setObject(true, forKey: SettingsKeys.SendUsageDataKey.rawValue)
      defaults.synchronize()
      return true
    }
    set(newValue) {
      defaults.setObject(newValue, forKey: SettingsKeys.SendUsageDataKey.rawValue)
      defaults.synchronize()

      NSNotificationCenter.defaultCenter().postNotificationName(BMCNotificationNames.DidChangeSendUsageData.rawValue, object: nil)
    }
  }

  public var bookingSlotDuration: Float {
    get {
      return defaults.floatForKey(SettingsKeys.BookingSlotDuration.rawValue)
    }
    set(newValue) {
      defaults.setObject(newValue, forKey: SettingsKeys.BookingSlotDuration.rawValue)
      defaults.synchronize()
    }
  }

  var telephoneNumber: String {
    let country = Country.getCountry()
    if let country = country {
      return country.telephone ?? ""
    }
    return ""
  }
}
