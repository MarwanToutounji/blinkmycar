//
//  VehicleProfileTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/5/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class VehicleProfileTableViewCell: UITableViewCell {
  @IBOutlet weak var editIconImageView: UIImageView!
  @IBOutlet weak var colorView: UIView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  
  @IBOutlet weak var colorBoxViewWidthConstriant: NSLayoutConstraint!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  var nameText: String? {
    get {
      return nameLabel.text
    }
    
    set {
      nameLabel.text = newValue
    }
  }
  
  
  var valueText: String? {
    get {
      return valueLabel.text
    }
    
    set {
      valueLabel.text = newValue
    }
  }
  
  var editIconImage: UIImage? {
    get {
      return editIconImageView.image
    }
    
    set {
      editIconImageView.alpha = (newValue == nil) ? 0.0 : 1.0
      editIconImageView.image = newValue
    }
  }

  var boxColor: UIColor? {
    get {
      return colorView.backgroundColor
    }
    
    set {
      hideColorBox(hide: (newValue == nil))
      colorView.backgroundColor = newValue
    }
  }
  
  private func hideColorBox(hide hide: Bool) {
    if hide {
      colorBoxViewWidthConstriant.constant = 0
    }
    else {
      colorBoxViewWidthConstriant.constant = 15
    }
    
    UIView.animateWithDuration(0.5,
      animations: { () -> Void in
        self.layoutIfNeeded()
    })
  }
  
  class var reusableIdentifier: String {
    return "VehicleProfileTableViewCell"
  }
}
