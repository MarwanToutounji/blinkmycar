//
//  DefaultPaymentToolTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/8/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public class DefaultPaymentToolTableViewCell: UITableViewCell {
  
  @IBOutlet weak var paymentToolImage: UIImageView!
  @IBOutlet weak var paymentDescriptionLabel: BMCSmallFormLabel!
  @IBOutlet weak var changeLabel: BMCLightNoteLabel!
  
  var paymentTool: PaymentTool? = nil {
    didSet {
      paymentToolImage.image = paymentTool?.image
      paymentDescriptionLabel.text = paymentTool?.displayedText
    }
  }
  
  public class var nib: UINib {
    return  UINib(nibName: "DefaultPaymentToolTableViewCell" , bundle: nil)
  }
  
  class var identifier: String {
    return "DefaultPaymentToolTableViewCell"
  }
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    changeLabel.text = NSLocalizedString(
      "DefaultPaymentToolTableViewCell.changeLabel",
      value: "Change",
      comment: "Label that indicates that if the cell is pressed the user can change the default tool")
  }
  
  override public func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
