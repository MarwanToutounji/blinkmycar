//
//  AddressDetailsViewViewModel.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/25/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

public class AddressDetailsViewViewModel {
  var country: String?
  var city: String?
  var street: String?
  
  var buildingFieldPlaceholder: String {
    return NSLocalizedString(
      "AddressDetailsView.buildingFieldPlaceholder",
      value: "e.g. Sodeco Square",
      comment: "Building Field placeholder text")
  }
  
  var parkingFloorFieldPlaceholder: String {
    return NSLocalizedString(
      "AddressDetailsView.parkingFloorFieldPlaceholder",
      value: "e.g. 8th",
      comment: "Parking Floor Field placeholder text")
  }
  
  var carParkingDetailsFieldPlaceholder: String {
    return NSLocalizedString(
      "AddressDetailsView.carParkingDetailsFieldPlaceholder",
      value: "e.g. -2 or outdoor parking facing building",
      comment: "Car Parking Details Field placeholder text")
  }
}