//
//  AddressDetailsView.swift
//  BlinkMyCar
//
//  Created by Marwan on 4/6/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import ORStackView
import FontAwesome

enum AddressDetailsViewMode {
  case Read
  case Write
}

protocol AddressDetailsViewDelegate {
  func addressDetailsViewDidTapEditButton()
  func addressDetailsViewDidTapSaveButton()
}

public class AddressDetailsView: UIView {
  private enum ActionButton {
    case Edit
    case Save
  }
  // TODO: Initialize the Strings
  
  var headerViewHeight: CGFloat {
    return headerView.frame.height
  }
  
  private let sideMargin: CGFloat = 30
  private let precedingLabelViewMargin: CGFloat = 17
  private let precedingFieldViewMargin: CGFloat = 5

  private let textFieldHeight: String = "45"
  private let textViewHeight: String = "75"
  
  var headerView: UIView!
  var headerInfoContainerView: UIView!
  var parkingDetailsView: ORStackScrollView!
  
  var addressTypeLabel: BMCAddressTitleLabel!
  var fullAddressLabel: BMCAddressSubtitleLabel!
  var headerSeparatorView: UIView!
  var buildingLabel: BMCAddressFormLabel!
  var parkingFloorLabel: BMCAddressFormLabel!
  var detailsLabel: BMCAddressFormLabel!
  var buildingField: BMCAddressTextView!
  var parkingFloorField: BMCAddressTextField!
  var detailsField: BMCAddressTextField!
  var buildingAndParkingView: UIView!
  var editButton: BMCLinkButton!
  var bottomPadding: UIView = UIView()

  var delegate: AddressDetailsViewDelegate?
  
  var viewModel: AddressDetailsViewViewModel = AddressDetailsViewViewModel()

  public var fieldsAreLocked = false {
    didSet {
      refreshFieldsEditState()
    }
  }
  
  public var fieldsAreEmpty: Bool {
    return (buildingField.valueForText()).isEmpty && (parkingFloorField.text ?? "").isEmpty && (detailsField.text ?? "").isEmpty
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initView()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initView()
  }
  
  private func initView() {
    initializeSubviews()
    initializeConstraints()
    addFormFieldsToView()
  }

  private func initializeSubviews() {
    self.backgroundColor = UIColor.whiteColor()
    func initializeHeaderView() {
      headerView = UIView(frame: CGRectZero)
      headerInfoContainerView = UIView(frame: CGRectZero)
      addressTypeLabel = BMCAddressTitleLabel(frame: CGRectZero)
      addressTypeLabel.text = "Other"
      fullAddressLabel = BMCAddressSubtitleLabel(frame: CGRectZero)
      fullAddressLabel.text = ""
      editButton = BMCLinkButton(type: UIButtonType.Custom)
      editButton.addTarget(self,
        action: #selector(AddressDetailsView.didTapEditButton(_:)),
        forControlEvents: UIControlEvents.TouchUpInside)
      configureButtonFor(.Edit)

      headerSeparatorView = UIView(frame: CGRectZero)
      headerSeparatorView.backgroundColor = UIColor.lightWhite()

      headerInfoContainerView.addSubview(addressTypeLabel)
      headerInfoContainerView.addSubview(fullAddressLabel)
      headerView.addSubview(headerInfoContainerView)
      headerView.addSubview(editButton)
      headerView.addSubview(headerSeparatorView)

      self.addSubview(headerView)
    }
    
    func initializeParkingDetailsView() {
      parkingDetailsView = ORStackScrollView()
      parkingDetailsView.frame = self.frame
      parkingDetailsView.translatesAutoresizingMaskIntoConstraints = false
      self.translatesAutoresizingMaskIntoConstraints = false
      
      //~ Init Labels
      buildingLabel = BMCAddressFormLabel(frame: CGRectZero)
      buildingLabel.text = "Building & Details"
      parkingFloorLabel = BMCAddressFormLabel(frame: CGRectZero)
      parkingFloorLabel.text = "Your Floor"
      detailsLabel = BMCAddressFormLabel(frame: CGRectZero)
      detailsLabel.text = "Car Parking"
      
      //~ Initialize Text Fields
      buildingField = BMCAddressTextView(frame: CGRectZero)
      buildingField.constrainHeight(textViewHeight)
      parkingFloorField = BMCAddressTextField(frame: CGRectZero)
      parkingFloorField.constrainHeight(textFieldHeight)
      detailsField = BMCAddressTextField(frame: CGRectZero)
      detailsField.constrainHeight(textFieldHeight)

      //set placeholders
      buildingField.placeholderText = viewModel.buildingFieldPlaceholder
      parkingFloorField.placeholder = viewModel.parkingFloorFieldPlaceholder
      detailsField.placeholder = viewModel.carParkingDetailsFieldPlaceholder

      //Setup Side-by-Side Parking And Building Views
      buildingAndParkingView = UIView(frame: CGRectZero)
      let screenWidth: CGFloat = UIScreen.mainScreen().bounds.width
      let buildingAndParkingViewWidth: CGFloat = screenWidth-(sideMargin)
      buildingAndParkingView.constrainWidth("\(buildingAndParkingViewWidth)")
      buildingAndParkingView.addSubview(buildingField)
      buildingAndParkingView.addSubview(buildingLabel)
      buildingAndParkingView.addSubview(parkingFloorField)
      buildingAndParkingView.addSubview(parkingFloorLabel)
      setupParkingAndBuildingLabelConstraints()

      //~ Input next field
      buildingField.returnKeyType = .Next
      parkingFloorField.returnKeyType = .Next
      detailsField.returnKeyType = .Done

      //~ Input field delegates
      buildingField.delegateForTextViewWithPlaceholder = self
      parkingFloorField.delegate = self
      detailsField.delegate = self

      self.addSubview(parkingDetailsView)
    }
    
    initializeHeaderView()
    initializeParkingDetailsView()
  }
  
  private func initializeConstraints() {
    self.translatesAutoresizingMaskIntoConstraints = false
    removeConstraints(constraints)
    
    headerView.alignTop("0", leading: "0", toView: self)
    headerView.alignTrailingEdgeWithView(self, predicate: "0")
    headerView.alignBottomEdgeWithView(editButton, predicate: ">=10@1000")
    headerView.alignBottomEdgeWithView(headerInfoContainerView, predicate: "0@750")

    headerInfoContainerView.alignTop("0", leading: "\(sideMargin/2)", toView: headerView)
    headerInfoContainerView.alignBottomEdgeWithView(fullAddressLabel, predicate: "15")
    headerInfoContainerView.constrainTrailingSpaceToView(editButton, predicate: "-10")
    
    let fullAddressTopToAddressTypeSpace: CGFloat = 5
    addressTypeLabel.alignTop("14", leading: "0", toView: headerInfoContainerView)
    addressTypeLabel.alignTrailingEdgeWithView(headerInfoContainerView, predicate: "0")
    fullAddressLabel.alignLeading("0", trailing: "0", toView: addressTypeLabel)
    fullAddressLabel.constrainTopSpaceToView(addressTypeLabel, predicate: "\(fullAddressTopToAddressTypeSpace)")

    editButton.constrainWidth("60")
    editButton.alignTopEdgeWithView(headerView, predicate: "10")
    editButton.constrainHeight("30")
    editButton.alignTrailingEdgeWithView(headerView, predicate: "-10")

    headerSeparatorView.constrainHeight("1")
    headerSeparatorView.alignLeading("0", trailing: "0", toView: headerView)
    headerSeparatorView.alignBottomEdgeWithView(headerView, predicate: "0")
    
    parkingDetailsView.constrainTopSpaceToView(headerView, predicate: "0")
    parkingDetailsView.alignLeading("0", trailing: "0", toView: self)
    parkingDetailsView.alignBottomEdgeWithView(self, predicate: "0")
  }

  private func setupParkingAndBuildingLabelConstraints() {
    parkingFloorLabel.constrainWidth("110")
    parkingFloorField.constrainWidth("110")
    
    //Align Horizontally relative to parent
    buildingLabel.alignLeadingEdgeWithView(buildingAndParkingView, predicate: "0")
    buildingField.alignLeadingEdgeWithView(buildingAndParkingView, predicate: "0")

    parkingFloorLabel.alignTrailingEdgeWithView(buildingAndParkingView, predicate: "0")
    parkingFloorField.alignTrailingEdgeWithView(buildingAndParkingView, predicate: "0")

    //Align Vertically relative to siblings
    buildingLabel.constrainTrailingSpaceToView(parkingFloorLabel, predicate: "\(-sideMargin/2)")
    buildingField.constrainTrailingSpaceToView(parkingFloorField, predicate: "\(-sideMargin/2)")

    //Align Vetically relative to parent
    buildingLabel.alignTopEdgeWithView(buildingAndParkingView, predicate: "0")
    buildingField.alignBottomEdgeWithView(buildingAndParkingView, predicate: "0")

    parkingFloorLabel.alignTopEdgeWithView(buildingAndParkingView, predicate: "0")

    //Align Vertically relative to siblings
    buildingLabel.constrainBottomSpaceToView(buildingField, predicate: "\(-precedingFieldViewMargin)")
    parkingFloorLabel.constrainBottomSpaceToView(parkingFloorField, predicate: "\(-precedingFieldViewMargin)")

  }

  //===============================================
  // MARK: Public Methods
  //===============================================
  func getCurrentAddressDetails() -> Address? {
    let addressType = AddressType.addressTypeFromName(addressTypeLabel.text)
    let street = viewModel.street ?? ""
    let userBuilding = buildingField.valueForText()

    var addressName = (!street.isEmpty && !userBuilding.isEmpty) ? "\(userBuilding), \(street)" : (street.isEmpty ? userBuilding : street)
    if !addressName.isEmpty {
      addressName.replaceRange(addressName.startIndex...addressName.startIndex, with: String(addressName[addressName.startIndex]).capitalizedString)
    }
    let floor = parkingFloorField.text
    let comments = detailsField.text
    let country = Country(id: viewModel.country, name: viewModel.country, enabled: nil, serviceable: nil, hourInterval: nil, contactInfo: nil, telephone: nil)
    let city = City(id: viewModel.city, name: viewModel.city, enabled: nil)
    return Address(addressID: nil, type: addressType, name: addressName, country: country, city: city, area: nil, street: street, userBuilding: userBuilding, userFloorNumber: floor, locationType: nil, isIndoor: nil, floor: floor, spotNumber: nil, comments: comments, landMark: nil, coordinates: nil, createDate: nil, updateDate: nil)
  }
  
  //===============================================
  // MARK: Actions
  //===============================================
  func didTapEditButton(sender: AnyObject?) {
    fieldsAreLocked ? delegate?.addressDetailsViewDidTapEditButton() : delegate?.addressDetailsViewDidTapSaveButton()
  }
  
  //================================
  // MARK: HELPERS
  //================================
  
  func areFieldsValid() -> Bool {
    return !((buildingField.valueForText().trim()?.isEmpty ?? true) || (detailsField.text?.trim() ?? "").isEmpty)
  }
  
  func addFormFieldsToView() {
    parkingDetailsView.stackView.addSubview(buildingAndParkingView,
      withPrecedingMargin: precedingLabelViewMargin, sideMargin: sideMargin)
    parkingDetailsView.stackView.addSubview(detailsLabel,
      withPrecedingMargin: precedingLabelViewMargin, sideMargin: sideMargin)
    parkingDetailsView.stackView.addSubview(detailsField,
      withPrecedingMargin: precedingFieldViewMargin, sideMargin: sideMargin)
  }

  func fillFieldsForAddress(address: Address?) {
    viewModel.street = address?.street
    viewModel.country = address?.country?.id
    viewModel.city = address?.city?.id

    parkingFloorField.text = address?.userFloorNumber
    if !(address?.userBuilding?.isEmpty ?? true) {
      //Do not set an empty value to field - will cause problem with the placeholder
      buildingField.text = address?.userBuilding
    } else {
      buildingField.text = address?.userBuilding
      buildingField.placeholderText = viewModel.buildingFieldPlaceholder
    }
    if !(address?.comments?.isEmpty ?? true) {
      //Do not set an empty value to field - will cause problem with the placeholder
      detailsField.text = address?.comments
    } else {
      detailsField.text = address?.comments
      detailsField.placeholder = viewModel.carParkingDetailsFieldPlaceholder
    }
    addressTypeLabel.text = address?.type?.addressTypeName()
    fullAddressLabel.text = address?.fullAddress
  }

  func fillGeocodedFieldsForAddress(country: String?, city: String?, street: String?,details: String? = nil) {
    if let country = country where !country.isEmpty {
      viewModel.country = country
    }
    if let city = city where !city.isEmpty {
      viewModel.city = city
    }
    if let street = street where !street.isEmpty {
      viewModel.street = street
    }

    if let details = details where (!details.isEmpty && (detailsField.text?.characters.count > 0)) {
      detailsField.text = details
    }
  }

  func refreshFieldsEditState() {
    buildingField.enable(!fieldsAreLocked)
    detailsField.enable(!fieldsAreLocked)
    parkingFloorField.enable(!fieldsAreLocked)

    refreshHeaderViewForState()
    fieldsAreLocked ? refreshButtonForEditDisplay() : refreshButtonForSaveDisplay()
  }

  func refreshHeaderViewForState() {
    // TODO:
  }
  
  func refreshButtonForEditDisplay() {
    configureButtonFor(ActionButton.Edit)
  }
  
  func refreshButtonForSaveDisplay() {
    configureButtonFor(ActionButton.Save)
  }

  private func configureButtonFor(mode: ActionButton) {
    editButton.fontText = mode == .Edit ? UIFont.lightBMCFont(size: 18) : UIFont.boldBMCFont(size: 18)
    editButton.textColor = mode == .Edit ? UIColor.applicationSecondaryColor() : UIColor.orange()
    editButton.setTitle(mode == .Edit ? "Edit" : "Save", forState: .Normal)
  }
}

extension AddressDetailsView: UITextFieldDelegate {
  public func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch textField {
    case parkingFloorField: detailsField.becomeFirstResponder()
    case detailsField: detailsField.resignFirstResponder()
    default: break
    }
    return true
  }
}

extension AddressDetailsView: TextViewWithPlaceholderDelegate {
  func textView(textView: UITextView,
    shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
      if text == "\n" {
        textView.resignFirstResponder()
      }
      return true
  }
  func textViewShouldBeginEditing(textView: UITextView) -> Bool { return true }
  public func textViewDidBeginEditing(textView: UITextView) {
    parkingDetailsView.scrollRectToVisible(textView.frame, animated: true)
  }
  func textViewDidEndEditing(textView: UITextView) {
    switch textView {
    case buildingField:
      parkingFloorField.becomeFirstResponder()
    default:
      break
    }
  }
  func textViewShouldReturn(textField: UITextField) -> Bool {
    return true
  }
}

