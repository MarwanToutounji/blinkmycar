//
//  ColorsCollectionViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/26/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

public class ColorsCollectionViewCell: UICollectionViewCell {

  @IBOutlet var colorView: UIView!
  @IBOutlet var colorNameLabel: UILabel!

  public override func awakeFromNib() {
    super.awakeFromNib()

    // Initialization code
    let selectedView = UIView()
    selectedView.backgroundColor = UIColor.clearColor()
    selectedView.layer.cornerRadius = 5
    selectedView.layer.borderWidth = 2
    selectedView.layer.borderColor = UIColor.darkGray().CGColor
    selectedBackgroundView = selectedView

    colorView.layer.cornerRadius = 5
  }

  //============================================================
  // MARK: Class functions
  //============================================================
  public static let nib = UINib(nibName: "ColorsCollectionViewCell" , bundle: nil)

  public static let reusableIdentifier = "ColorsCollectionViewCell"

  public static let cellWidth: CGFloat = 75.0
  public static let cellHeight: CGFloat = 100.0

  //============================================================
  // MARK: PUBLIC METHODS
  //============================================================

  public var color : UIColor? {
    get {
      return colorView.backgroundColor
    }
    set {
      if newValue == UIColor.whiteColor() {
        colorView.layer.borderWidth = 1
        colorView.layer.borderColor = UIColor.darkGray().CGColor
      }
      colorView.backgroundColor = newValue
    }
  }
}
