//
//  TimePickerView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/23/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import ORStackView

class TimeHeaderView: UIView {

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var label: UILabel!

  var text: String? {
    get {
      return label.text
    }

    set {
      label.text = newValue
    }
  }

  var image: UIImage? {
    get {
      return imageView.image
    }

    set {
      imageView.image = newValue
    }
  }
}


protocol TimeViewDelegate {
  func timeViewDidSelectTimeView(timeView: TimeView)
}
class TimeView: UIView {
  private var _time: ServiceTime?
  private var isSelected = false

  var delegate: TimeViewDelegate?

  @IBOutlet weak var label: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    userInteractionEnabled = true
    label.userInteractionEnabled = true
  }

  var time: ServiceTime? {
    get {
      return _time
    }

    set {
      _time = newValue
      label.text = _time?.startTimeStringValue
      if let timeIsEnabled = _time?.enabled where !timeIsEnabled {
        label.textColor = UIColor.lighterGray()
      }
    }
  }

  @IBAction func didTapLabel(sender: AnyObject) {
    if (time!.enabled ?? false) {
      delegate?.timeViewDidSelectTimeView(self)
    }
  }

  var selected: Bool {
    get {
      return isSelected
    }

    set {
      isSelected = newValue
      if isSelected {
        if DeviceType.IS_IPHONE_5_OR_LESS {
          label.font = UIFont.boldBMCFont(size: 16)
        } else {
          label.font = UIFont.boldBMCFont(size: 18)
        }
      } else {
        label.font = UIFont.lightBMCFont(size: 15)
      }
    }
  }
}



protocol TimePickerViewDelegate {
  func timePickerViewDidSelectTime(time: ServiceTime?)
}
public class TimePickerView: UIView {

  @IBOutlet weak var verticalseparatorView: UIView!
  var splitStackView: ORSplitStackView!

  private var currentlySelectedViews: TimeView?

  var viewModel = TimePickerViewModel() {
    didSet {
      self.reloadView()
    }
  }

  var delegate: TimePickerViewDelegate?

  public override func awakeFromNib() {
    super.awakeFromNib()

    verticalseparatorView.backgroundColor = UIColor.lighterGray()

    splitStackView = ORSplitStackView(leftPredicate: "100@250",
      rightPredicate: "100@250")
    self.insertSubview(splitStackView, atIndex: 0)
    splitStackView.alignLeading("0", trailing: "0", toView: self)
    splitStackView.alignTopEdgeWithView(self, predicate: "0")
    self.alignBottomEdgeWithView(splitStackView, predicate: "0")
    splitStackView.leftStack?.backgroundColor = UIColor.clearColor()
    self.backgroundColor = UIColor.clearColor()

    reloadView()
  }

  public override func layoutSubviews() {
    super.layoutSubviews()
    let width = frame.width / 2.0
    splitStackView.leftStack?.constrainWidth("\(width)@1000")
    splitStackView.rightStack?.constrainWidth("\(width)@1000")
  }

  func reloadView() {
    setLeftStackView()
    setRightStackView()
  }

  private func setLeftStackView() {
    splitStackView.leftStack?.removeAllSubviews()

    let morningHeaderView = NSBundle.mainBundle().loadNibNamed("TimeHeaderView",
      owner: nil, options: nil)[0] as! TimeHeaderView
    morningHeaderView.text = "Morning"
    morningHeaderView.image = UIImage(named: "timeIcon")
    morningHeaderView.constrainHeight("60")
    splitStackView.leftStack?.addSubview(morningHeaderView, withPrecedingMargin: 0, sideMargin: 0)
    splitStackView.leftStack?.addSubview(separatorView(), withPrecedingMargin: 0, sideMargin: 0)

    for (index, time) in viewModel.morningTimes.enumerate() {
      let view = NSBundle.mainBundle().loadNibNamed("TimeView",
        owner: nil, options: nil)[0] as! TimeView
      view.time = time
      view.delegate = self
      view.selected = viewModel.shouldSelectTime(time)
      if view.selected {
        currentlySelectedViews = view
      }
      view.constrainHeight("60")
      if index > 0 {
        splitStackView.leftStack?.addSubview(separatorView(), withPrecedingMargin: 0, sideMargin: 40)
      }
      splitStackView.leftStack?.addSubview(view, withPrecedingMargin: 0, sideMargin: 40)

    }
  }

  private func setRightStackView() {
    splitStackView.rightStack?.removeAllSubviews()

    let afternoonHeaderView = NSBundle.mainBundle().loadNibNamed("TimeHeaderView",
      owner: nil, options: nil)[0] as! TimeHeaderView
    afternoonHeaderView.text = "Afternoon"
    afternoonHeaderView.image = UIImage(named: "timeIcon")
    afternoonHeaderView.constrainHeight("60")
    splitStackView.rightStack?.addSubview(afternoonHeaderView, withPrecedingMargin: 0, sideMargin: 0)
    splitStackView.rightStack?.addSubview(separatorView(), withPrecedingMargin: 0, sideMargin: 0)

    for (index, time) in viewModel.afternoonTimes.enumerate() {
      let view = NSBundle.mainBundle().loadNibNamed("TimeView",
        owner: nil, options: nil)[0] as! TimeView
      view.time = time
      view.delegate = self
      view.selected = viewModel.shouldSelectTime(time)
      if view.selected {
        currentlySelectedViews = view
      }
      view.constrainHeight("60")
      if index > 0 {
        splitStackView.rightStack?.addSubview(separatorView(), withPrecedingMargin: 0, sideMargin: 40)
      }
      splitStackView.rightStack?.addSubview(view, withPrecedingMargin: 0, sideMargin: 40)
    }
  }

  private func separatorView() -> UIView {
    let separator = BMCLineSeparatorView(frame: CGRectZero)
    separator.constrainHeight("1")
    return separator
  }
}

extension TimePickerView: TimeViewDelegate {
  func timeViewDidSelectTimeView(timeView: TimeView) {
    viewModel.selectedTime = nil
    currentlySelectedViews?.selected = false
    currentlySelectedViews = timeView
    currentlySelectedViews?.selected = true
    delegate?.timePickerViewDidSelectTime(timeView.time)
  }
}
