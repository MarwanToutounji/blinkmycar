//
//  SliderView.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout

public class SliderView: UIView {
  let animationDuration: NSTimeInterval = 0.44
  
  private var leadingConstraint: NSLayoutConstraint!
  public var selectedIndex: Int = 0
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    configureView()
  }
  
  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    configureView()
  }
  
  private func configureView() {
    self.clipsToBounds = true
  }
  
  public override func updateConstraints() {
    if leadingConstraint != nil {
      leadingConstraint.constant = -(self.frame.width * CGFloat(selectedIndex))
    }
    
    super.updateConstraints()
  }
  
  //==========================================================
  // MARK: - API
  //==========================================================
  
  public func setViewsControllersViews(viewControllers: [UIViewController],
                           parentViewController: UIViewController)
  {
    for viewController in viewControllers{
      // call before adding child view controller's view as subview
      parentViewController.addChildViewController(viewController)
      
      let view: UIView = viewController.view
      addView(view)

      // call after adding child view controller's view as subview
      viewController.didMoveToParentViewController(parentViewController.parentViewController)
    }
  }
  
  /// - Returns: Boolean indicating if the operation succeded or not
  public func next(animated: Bool = false) -> Bool {
    if selectedIndex == (subviews.count - 1) {
      return false
    }
    
    selectedIndex += 1
    refreshLayout(animated)
    
    return true
  }
  
  /// - Returns: Boolean indicating if the operation succeded or not
  public func previous(animated: Bool = false) -> Bool {
    if selectedIndex == 0 {
      return false
    }
    
    selectedIndex -= 1
    refreshLayout(animated)
    
    return true
  }
  
  public func goToFirstView(animated: Bool = false) {
    selectedIndex = 0
    refreshLayout(animated)
  }
  
  public func goToLast(animated: Bool = false) {
    selectedIndex = self.subviews.count - 1
    refreshLayout(animated)
  }
  
  //==========================================================
  // MARK: - HELPERS
  //==========================================================
  
  private func addView(view: UIView) {
    self.addSubview(view)
    
    // Set Same Width
    view.constrainWidthToView(self, predicate: "0")
    view.alignTop("0", bottom: "0", toView: self)
    
    // If that's the first subview then connect the leading 
    // constraint to the parent view
    // Otherwise connect it to the view that preceeds it
    if self.subviews.count == 1 {
      leadingConstraint = view.alignLeadingEdgeWithView(self, predicate: "0")[0] as! NSLayoutConstraint
    } else {
      view.constrainLeadingSpaceToView(
        self.subviews[self.subviews.count - 2],
        predicate: "0")
    }
  }
  
  func refreshLayout(animated: Bool) {
    leadingConstraint.constant = -(self.frame.width * CGFloat(selectedIndex))
    if animated {
      UIView.animateWithDuration(
        animationDuration,
        animations: {
          self.layoutIfNeeded()
      })
    } else {
      layoutIfNeeded()
    }
  }
}
