//
//  TableSectionView.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/8/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public class TableSectionView: UITableViewHeaderFooterView {
  @IBOutlet weak var container: UIView!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var label: UILabel!

  public class var nib: UINib {
    return  UINib(nibName: "TableSectionView" , bundle: nil)
  }
  public static let identifier: String = "TableSectionView"
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    self.contentView.clipsToBounds = true
    container.backgroundColor = UIColor.lighterMidGray()
  }
  
  func setTitle(title: String, imageName: String) {
    imageView.image = UIImage(named: imageName)
    label.text = title
  }
}
