//
//  CreateRideMakeModelCollectionViewCell.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class CreateRideMakeModelCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var textLabel: UILabel!
  @IBOutlet weak var separatorView: UIView!

  public static let cellReuseIdentifier = "CreateRideMakeModelCollectionViewCell"
  public static let cellHeight:CGFloat = 44.0

  public override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = UIColor.whiteColor()
    separatorView.backgroundColor = UIColor.lightGray()
  }
}
