//
//  MapCenterIndicator.swift
//  BlinkMyCar
//
//  Created by Marwan  on 4/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import FontAwesome

protocol MapCenterIndicatorDelegate {
  func mapCenterIndicatorDidReceiveTap()
}

enum MapCenterIndicatorViewState {
  case Normal
  case Collapsed
  case NotValid
}

public class MapCenterIndicator: UIView {
  var markerIcon: UIImageView!
  var useLocationView: BMCMapIndicatorButton!
  
  var delegate: MapCenterIndicatorDelegate?
  
  static func instance() -> MapCenterIndicator {
    return MapCenterIndicator(frame: CGRect(x: 0, y: 0, width: 230, height: 60))
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initView()
    addGesture()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initView()
    addGesture()
  }
  
  private func initView() {
    let markerIconWidth: CGFloat = 27
    let markerIconHeight: CGFloat = 34
    markerIcon = UIImageView(frame:
      CGRect(x: self.center.x - (markerIconWidth/2),
        y: self.frame.height - markerIconHeight,
        width: markerIconWidth,
        height: markerIconHeight)
    )
    self.addSubview(markerIcon)

    let useLocationViewHeight: CGFloat = 44
    useLocationView = BMCMapIndicatorButton(type: .Custom)
    useLocationView.fontSize = 18
    useLocationView.textColor = UIColor.whiteColor()
    useLocationView.setTitle(mapCenterIndicatorCarLocationServiceableText, forState: .Normal)
    useLocationView.frame = CGRect(x: 0,
      y: self.frame.height - (useLocationViewHeight + 15),
      width: self.frame.width,
      height: useLocationViewHeight)
    useLocationView.makeRounded()
    self.addSubview(useLocationView)

    markerIcon.image = UIImage(named: "markerIcon")?.imageWithRenderingMode(.AlwaysTemplate)
    markerIcon.tintColor = UIColor.orange()
    backgroundColor = UIColor.clearColor()
  }
  
  var state: MapCenterIndicatorViewState = .Normal {
    didSet {
      refreshViewForState()
    }
  }
  
  private func refreshViewForState() {
    switch state {
    case .Normal:
      useLocationView.hidden = false
      useLocationView.fontSize = 18
      useLocationView.fontAwesomeIcon = FAIcon.FAChevronRight
      useLocationView.textColor = UIColor.whiteColor()
      useLocationView.setTitle(mapCenterIndicatorCarLocationServiceableText, forState: .Normal)
    case .Collapsed:
      useLocationView.hidden = true
    case .NotValid:
      useLocationView.fontSize = 18
      useLocationView.fontAwesomeIcon = nil
      useLocationView.setTitle(mapCenterIndicatorCarLocationNotServiceableLocationText, forState: .Normal)
    }
  }
  
  private func addGesture() {
    let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(MapCenterIndicator.handleTap(_:)))
    tapGestureRecognizer.cancelsTouchesInView = true
    markerIcon.userInteractionEnabled = true
    markerIcon.addGestureRecognizer(tapGestureRecognizer)


    useLocationView.addTarget(self, action: #selector(MapCenterIndicator.handleTap(_:)),
      forControlEvents: .TouchUpInside)
  }
  
  func handleTap(sender: AnyObject) {
    if state != .NotValid {
      delegate?.mapCenterIndicatorDidReceiveTap()
    }
  }
}

//MARK: Localized Strings
extension MapCenterIndicator {
  var mapCenterIndicatorCarLocationServiceableText: String {
        return NSLocalizedString("AddressesTableViewController.mapCenterIndicatorServiceableLocationText", value: "SET CAR LOCATION",
      comment: "Text that appears in the middle of the map on a bubble when the point of the map is serviceable to set your car location")
  }

  var mapCenterIndicatorCarLocationNotServiceableLocationText: String {
    return NSLocalizedString("AddressesTableViewController.mapCenterIndicatorServiceableLocationText", value: "NO SERVICE HERE",
      comment: "Text that appears in the middle of the map on a bubble when the point of the map can not be used to set your car location")
  }
}
