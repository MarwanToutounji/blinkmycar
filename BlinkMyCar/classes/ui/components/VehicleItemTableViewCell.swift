//
//  VehicleItemTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 6/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

protocol VehicleItemTableViewCellDelegate: NSObjectProtocol {
  func didClickLocateLabel()
}

class VehicleItemTableViewCell: UITableViewCell {
  
  enum CellMode {
    case Header
    case Compact
    case AddVehicle
  }
  
  class var identifier: String {
    return "VehicleItemTableViewCell"
  }
  
  @IBOutlet weak var labelCarName: UILabel!
  @IBOutlet weak var labelLocateCar: UILabel!
  @IBOutlet weak var imageViewCar: UIImageView!
  
  //Should only be accessed through the custom setter function
  private var delegate: VehicleItemTableViewCellDelegate?
  
  var mode: CellMode = .Header {
    didSet {
      self.updateCellMode()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    initTapGesture()
    imageViewCar.cicularView()
  }
  
  func initTapGesture() {
    if (self.labelLocateCar.gestureRecognizers?.count ?? 0) == 0 {
      let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(VehicleItemTableViewCell.locateLabelTapped(_:)))
      tapGestureRecognizer.numberOfTapsRequired = 1
      self.labelLocateCar.addGestureRecognizer(tapGestureRecognizer)
      self.labelLocateCar.userInteractionEnabled = true
    }
  }
  
  func locateLabelTapped(tap: UITapGestureRecognizer) {
    //We are expecting that delegate would be nil sometimes, so callback parent only if delegate is not nil
    if self.mode != CellMode.AddVehicle {
      delegate?.didClickLocateLabel()
    }
  }
  
  func setLabelDelegate(delegate: VehicleItemTableViewCellDelegate) {
      self.delegate = delegate
  }
  
  private func updateCellMode() {
    switch (self.mode) {
    case .Header:
      labelCarName.alpha = 1
      labelLocateCar.alpha = 1
      imageViewCar.alpha = 1
      //TODO: update theme
    case .Compact:
      labelCarName.alpha = 1
      labelLocateCar.alpha = 0
      imageViewCar.alpha = 1
      //TODO: update theme
    case .AddVehicle:
      labelCarName.alpha = 1
      labelLocateCar.alpha = 0
      imageViewCar.alpha = 1
      //TODO: update theme
      
    }
  }
}
