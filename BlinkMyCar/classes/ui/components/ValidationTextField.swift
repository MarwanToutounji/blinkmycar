//
//  KWDTextField.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/18/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

public protocol ValidationTextFieldDelegate {
  func validationTextFieldDidBeginEditing(textField: UITextField)
  func validationTextFieldDidEndEditing(textField: UITextField)
  func validationTextFieldShouldReturn(textField: UITextField) -> Bool
  func validationTextFieldShouldLayoutIfNeeded(animationBlock: (()->Void) )
}

typealias ValidationMethod = ((text: String?) -> Bool)

// TODO: Implment the ability to have an external label
public class ValidationTextField: UIView, UITextFieldDelegate {
  @IBOutlet weak var textField: BMCTextField!
  @IBOutlet weak var internalLabel: BMCLabel!

  @IBOutlet weak var constraintBottomToTextField: NSLayoutConstraint!
  @IBOutlet weak var constraintBottomToInternalLabel: NSLayoutConstraint!

  static let nibName = "ValidationTextField"

  var validationTextFieldDelegate: ValidationTextFieldDelegate?

  var validationMethod: ValidationMethod?

  var errorMessage: String? = "Error"

  var animationDuration: NSTimeInterval = 0.5

  var returnKeyType: UIReturnKeyType? {
    didSet {
      textField.returnKeyType = returnKeyType ?? UIReturnKeyType.Default
    }
  }

  var keyboardType: UIKeyboardType = UIKeyboardType.Default {
    didSet {
      textField.keyboardType = keyboardType
    }
  }

  var adjustsFontSizeToFitWidth: Bool = false {
    didSet {
      textField.adjustsFontSizeToFitWidth = adjustsFontSizeToFitWidth
    }
  }

  var shouldDisplaySecureTextWhileEditing = false
  var secureTextEntry = false

  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  public override func awakeAfterUsingCoder(aDecoder: NSCoder) -> AnyObject? {
    return afterUsingCoderLoadNibIfNeeded(ValidationTextField.nibName)
  }

  public override func awakeFromNib() {
    super.awakeFromNib()
    textField.returnKeyType = returnKeyType ?? UIReturnKeyType.Default
    textField.adjustsFontSizeToFitWidth = adjustsFontSizeToFitWidth
    textField.keyboardType = keyboardType
    textField.delegate = self
    textField.constrainHeight("44")

    /**
    Without a default value for the text (hidden space in this case), the text
    field will show a weird animation when displaying the error text.
    We think it has to do with the text container being animated into place
    since it was not laid our correctly before setting the text.
    **/
    text = " "

    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(ValidationTextField.keyboardDidShow(_:)),
      name: UIKeyboardDidShowNotification, object: nil)
  }

  public override func resignFirstResponder() -> Bool {
    textField.resignFirstResponder()
    return true
  }

  public override func becomeFirstResponder() -> Bool {
    textField.becomeFirstResponder()
    return true
  }

  public override func isFirstResponder() -> Bool {
    return textField.isFirstResponder()
  }

  var text: String? {
    get {
      return textField.text
    }

    set {
      textField.text = newValue
    }
  }

  public func validate() -> Bool {
    textField.text = textField.text?.trim()
    let isValid = validationMethod?(text: textField.text) ?? true
    updateViewForValidationStatus(isValid)
    return isValid
  }

  //================================================================
  // MARK: - H E L P E R S
  //================================================================

  private func updateViewForValidationStatus(valid: Bool) {
    var animationBlock: (() -> Void)?
    switch valid {
    case false:
      self.removeConstraint(constraintBottomToTextField)
      self.addConstraint(constraintBottomToInternalLabel)
      animationBlock = { () -> Void in
        self.internalLabel.alpha = 1
        self.internalLabel.text = self.errorMessage
      }
    case true:
      self.removeConstraint(constraintBottomToInternalLabel)
      self.addConstraint(constraintBottomToTextField)

      animationBlock = { () -> Void in
        self.internalLabel.alpha = 0
        self.internalLabel.text = ""
      }
    }

    validationTextFieldDelegate?.validationTextFieldShouldLayoutIfNeeded(animationBlock!)
  }

  //================================================================
  // MARK: - U I   T E X T   F I E L D   D E L E G A T E
  //================================================================

  public func textFieldShouldReturn(textField: UITextField) -> Bool {
    return validationTextFieldDelegate?.validationTextFieldShouldReturn(textField) ?? true
  }

  public func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    textField.secureTextEntry = secureTextEntry

    return true
  }

  public func textFieldDidBeginEditing(textField: UITextField) {
    if textField.text == " " {
      textField.text = ""
    }

    validationTextFieldDelegate?.validationTextFieldDidBeginEditing(textField)
  }

  public func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    if textField.isFirstResponder() {
      textField.secureTextEntry = !shouldDisplaySecureTextWhileEditing && secureTextEntry
    }
    return true
  }

  public func textFieldDidEndEditing(textField: UITextField) {
    validate()
    textField.secureTextEntry = secureTextEntry

    validationTextFieldDelegate?.validationTextFieldDidEndEditing(textField)
  }

  func keyboardDidShow(notification: NSNotification) {
    if textField.isFirstResponder() {
      textField.secureTextEntry = !shouldDisplaySecureTextWhileEditing && secureTextEntry
    }
  }
}