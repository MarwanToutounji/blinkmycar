//
//  BookingHistoryTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/2/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout

public class BookingHistoryTableViewCell: UITableViewCell {
  public static let identifier: String = "BookingHistoryTableViewCell"
  
  var bookingStatusLabel: BMCBookingStatusAutoLabel!
  var dateTimeLabel: BMCBookingHistoryDateTimeLabel!
  var carInfoLabel: BMCBookingHistoryCarMakeModelLabel!
  var bookingIdLabel: BMCBookingHistoryBookingIdLabel!
  var addressLabel: BMCAddressLabel!
  var servicesView: UIView!
  var totalPriceLabel: BMCUpcomingServiceLabel!
  var totalPriceSeparatorView: UIView!
  var addressFooterSeparatorView: UIView!
  var separatorView: UIView!
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    bookingStatusLabel = BMCBookingStatusAutoLabel(frame: CGRectZero)
    dateTimeLabel = BMCBookingHistoryDateTimeLabel(frame: CGRectZero)
    carInfoLabel = BMCBookingHistoryCarMakeModelLabel(frame: CGRectZero)
    bookingIdLabel = BMCBookingHistoryBookingIdLabel(frame: CGRectZero)
    addressLabel = BMCAddressLabel(frame: CGRectZero)
    servicesView = UIView(frame: CGRectZero)
    totalPriceLabel = BMCUpcomingServiceLabel(frame: CGRect.zero)
    totalPriceSeparatorView = UIView(frame: CGRect.zero)
    addressFooterSeparatorView = UIView(frame: CGRectZero)
    separatorView = UIView(frame: CGRectZero)
    
    addressFooterSeparatorView.backgroundColor = UIColor.veryLightGray()
    separatorView.backgroundColor = UIColor.veryLightGray()
    totalPriceSeparatorView.backgroundColor = UIColor.veryLightGray()
    
    totalPriceLabel.fontSize = 15
    
    contentView.addSubview(bookingStatusLabel)
    contentView.addSubview(dateTimeLabel)
    contentView.addSubview(carInfoLabel)
    contentView.addSubview(bookingIdLabel)
    contentView.addSubview(addressLabel)
    contentView.addSubview(servicesView)
    contentView.addSubview(totalPriceSeparatorView)
    contentView.addSubview(totalPriceLabel)
    contentView.addSubview(addressFooterSeparatorView)
    contentView.addSubview(separatorView)
    
    bookingStatusLabel.alignLeading("30", trailing: "30", toView: contentView)
    bookingStatusLabel.alignTopEdgeWithView(contentView, predicate: "15")
    dateTimeLabel.alignLeading("30", trailing: "-30", toView: contentView)
    dateTimeLabel.constrainTopSpaceToView(bookingStatusLabel, predicate: "10")
    addressLabel.alignLeading("0", trailing: "0", toView: dateTimeLabel)
    addressLabel.constrainTopSpaceToView(dateTimeLabel, predicate: "10")
    
    carInfoLabel.alignLeading("0", trailing: "0", toView: dateTimeLabel)
    carInfoLabel.constrainTopSpaceToView(addressLabel, predicate: "10")
    bookingIdLabel.alignLeading("0", trailing: "0", toView: dateTimeLabel)
    bookingIdLabel.constrainTopSpaceToView(carInfoLabel, predicate: "5")
    
    servicesView.alignLeading("30", trailing: "-30", toView: contentView)
    servicesView.constrainTopSpaceToView(bookingIdLabel, predicate: "15")
    addressFooterSeparatorView.alignLeading("30", trailing: "-30", toView: contentView)
    addressFooterSeparatorView.constrainHeight("1")
    addressFooterSeparatorView.alignTopEdgeWithView(servicesView, predicate: "0")
    separatorView.constrainTopSpaceToView(totalPriceLabel, predicate: "10")
    separatorView.alignLeading("0", trailing: "0", toView: contentView)
    separatorView.constrainHeight("20")
    separatorView.alignBottomEdgeWithView(contentView, predicate: "0")
    
    totalPriceLabel.alignLeading("30", trailing: "-30", toView: contentView)
    totalPriceLabel.constrainTopSpaceToView(servicesView, predicate: "10")
    
    totalPriceSeparatorView.alignLeading("30", trailing: "-30", toView: contentView)
    totalPriceSeparatorView.constrainHeight("1")
    totalPriceSeparatorView.constrainTopSpaceToView(servicesView, predicate: "0")
  }
  
  public func setBooking(booking: Booking) {
    var services = booking.services ?? [Service]()
    if services.count == 0 {
      services = [
        Service(id: "", availability: ServiceAvailability.inStock, sku: "", name: "Exterior Wash", description: "", price: 12, category: "", needKeysLocation: false, isCore: true, vehicleType: booking.vehicle?.type ?? Make.VehicleType.Car, addOns: [Service](), parentId: nil)
      ]
    }
    
    // Set Status
    bookingStatusLabel.status = booking.status!
    
    // Set The date & time
    dateTimeLabel.attributedText(
      booking.deliveryDate?.displayTextForServiceDate(),
      time: booking.bookingTime?.startTimeStringValue)
    
    // Set Total Price
    var totalPrice = booking.totalPrice?.displayAsUsDollarsPrice
    if totalPrice == nil || totalPrice!.isEmpty {
      var price: Float = 0.0
      for service in services {
        price += service.price
      }
      totalPrice = price.displayAsUsDollarsPrice
    }
    totalPriceLabel.attributedText("Total", servicePrice: totalPrice)
    
    // Set Car Info
    carInfoLabel.attributedText(
      booking.vehicle?.make,
      model: booking.vehicle?.model)
    
    // Set Booking ID
    bookingIdLabel.text = (booking.bookingNumber ?? "").isEmpty ? "N/A" : booking.bookingNumber
    
    // Set the address
    addressLabel.text = booking.address?.fullAddress
    if (addressLabel.text ?? "").isEmpty {
      addressLabel.text = booking.address?.name
    }
    
    // Set services
    servicesView.clearSubviews()
    for (index, service) in services.enumerate() {
      var serviceLabel: BMCUpcomingServiceLabel!
      serviceLabel = BMCUpcomingServiceLabel(frame: CGRectZero)
      serviceLabel.attributedText(
        service.name,
        servicePrice: service.price.displayAsUsDollarsPrice)
      addServiceLabelToSuperview(serviceLabel, superview: servicesView, index: index, totalServicesCount: services.count)
    }
  }
  
  private func addServiceLabelToSuperview(serviceLabel: UILabel, superview: UIView, index: Int, totalServicesCount: Int) {
    superview.addSubview(serviceLabel)
    serviceLabel.alignLeading("0", trailing: "0", toView: superview)
    
    // Align Top constraint
    if index == 0 {
      serviceLabel.alignTopEdgeWithView(superview, predicate: "15")
    } else {
      serviceLabel.constrainTopSpaceToView(superview.subviews[index - 1], predicate: "10")
    }
    
    // align bottom constraint
    if index == totalServicesCount - 1 {
      serviceLabel.alignBottomEdgeWithView(superview, predicate: "-15")
    }
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
}
