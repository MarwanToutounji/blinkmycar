//
//  VehicleListingView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import ORStackView

protocol VehicleListingViewDelegate {
  func vehicleListingViewDidSelectVehicle(vehicle: Vehicle?)
}

class VehicleListingView: ORStackView, VehicleBasicInformationViewDelegate {
  private static var subViewsHeight: CGFloat = 120

  var vehicles: [Vehicle]? {
    didSet {
      clearView()
      reloadList()
    }
  }

  // Used only while the view is first initialising
  var selectedVehicle: Vehicle? {
    didSet {
      if let vehicleView =  vehicleViews.filter({ $0.vehicle == self.selectedVehicle }).first {
        selectedView?.selectedState = false
        vehicleView.selectedState = true
        selectedView = vehicleView
      }
    }
  }

  private var vehicleViews = [VehicleBasicInformationView]()
  private var selectedView: VehicleBasicInformationView?

  var delegate: VehicleListingViewDelegate?

  override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = UIColor.veryLightGray()
    reloadList()
  }

//  override func awakeAfterUsingCoder(aDecoder: NSCoder) -> AnyObject? {
//    return afterUsingCoderLoadNibIfNeeded("VehicleListingView")
//  }

  func reloadList() {
    if let vehicles = vehicles {
      for (index, vehicle) in vehicles.enumerate() {
        let vehicleBasicView = NSBundle.mainBundle().loadNibNamed("VehicleBasicInformationView",
          owner: nil, options: nil)[0] as! VehicleBasicInformationView
        vehicleBasicView.constrainHeight("\(VehicleListingView.subViewsHeight)")
        vehicleBasicView.vehicle = vehicle
        vehicleBasicView.delegate = self
        if let selectedVehicle = selectedVehicle {
          if selectedVehicle == vehicle {
            vehicleBasicView.selectedState = true
            selectedView = vehicleBasicView
          }
        }
        addSubview(vehicleBasicView, withPrecedingMargin: 0, sideMargin: 0)
        if index != (vehicles.count - 1) {
          let separatorView = BMCLineSeparatorView(frame: CGRectZero)
          separatorView.constrainHeight("1")
          addSubview(separatorView, withPrecedingMargin: 0, sideMargin: 0)
        }

        vehicleViews.append(vehicleBasicView)
      }
    }
  }

  func getVehicleInformationView(vehicle: Vehicle) -> VehicleBasicInformationView {
    return VehicleBasicInformationView()
  }

  var viewHeight: CGFloat {
     return CGFloat(vehicleViews.count) * VehicleListingView.subViewsHeight
  }

  //=======================================================================================
  // MARK: V E H I C L E   B A S I C   I N F O R M A T I O N   V I E  W   D E L E G A T E
  //=======================================================================================

  func vehicleBasicInfoDidSelectView(view: VehicleBasicInformationView) {
    selectedView?.selectedState = false
    view.selectedState = true
    selectedView = view

    delegate?.vehicleListingViewDidSelectVehicle(view.vehicle)
  }

  //============================================================
  // MARK: P R I V A T E   M E T H O D S
  //============================================================

  private func clearView() {
    vehicleViews.removeAll(keepCapacity: false)
    removeAllSubviews()
  }
}
