//
//  TitleTableViewHeaderSection.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/5/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public class TitleTableViewSectionView: UITableViewHeaderFooterView {
  @IBOutlet weak var container: UIView!
  @IBOutlet weak var label: UILabel!
  
  public class var nib: UINib {
    return  UINib(nibName: "TitleTableViewSectionView" , bundle: nil)
  }
  public static let identifier: String = "TitleTableViewSectionView"
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    self.contentView.clipsToBounds = true
    container.backgroundColor = UIColor.veryLightGray()
  }
  
  func setTitle(title: String) {
    label.text = title
  }
}
