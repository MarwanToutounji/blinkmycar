//
//  HorizontalRadioButtons.swift
//  BlinkMyCar
//
//  Created by Marwan on 3/14/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit



public typealias RadioButtonProperties = (title: String, subtitle: String?, item: Any?)

public protocol ServicesRadioButtonsViewDelegate {
  func radioButtonSelected(selectedService: Service?, unselectedService: Service?)
}

public class ServicesRadioButtonsView: HorizontalStackView, RadioButtonDelegate {
  public var services: [RadioButtonProperties]! {
    didSet {
      numberOfStackedViews = services.count
    }
  }
  
  var selectSingleOption = false
  
  private var haveSelectedOption = false {
    didSet {
      for subview in subviews {
        if let view = subview as? ServiceRadioButton {
          view.useInitialColors = !haveSelectedOption
        }
      }
    }
  }
  
  private var selectedRadioButton: RadioButton? {
    willSet {
      guard let selectedRadioButton = selectedRadioButton, let newValue = newValue else { return }
      if (selectedRadioButton as! ServiceRadioButton) != (newValue as! ServiceRadioButton) {
        self.selectedRadioButton?.selected = false
      }
    }
    
    didSet {
      if selectedRadioButton == nil {
        haveSelectedOption = false
      } else {
        haveSelectedOption = true
      }
    }
  }

  var delegate: ServicesRadioButtonsViewDelegate?

  public func getService() -> Service? {
    if let selectedRadioButton = selectedRadioButton as? ServiceRadioButton,
      let service = selectedRadioButton.properties.item as? Service{
      return service
    }
    
    return nil
  }
  
  private var selectedProperties: RadioButtonProperties? {
    if let selectedRadioButton = selectedRadioButton as? ServiceRadioButton {
      return selectedRadioButton.properties
    }
    return nil
  }
  
  func selectRadioForIndex(index: Int) {
    var radioButton = subviews[index] as! RadioButton
    radioButton.selected = true
    selectedRadioButton = radioButton
  }
  
  func selectedRadioForService(service: Service) {
    let selectionIndex = services.map({ $0.item as! Service }).indexOf(service)
    if let selectionIndex = selectionIndex {
      selectRadioForIndex(selectionIndex)
    }
  }
  
  override func subView(index: Int) -> UIView! {
    let radioButtonProperties = services[index]
    let view = ServiceRadioButton.serviceRadioButton(radioButtonProperties)
    view.delegate = self
    return view
  }
  
  public override func didFinishAddingSubviews() {
    if services.count == 1 && selectSingleOption {
      selectRadioForIndex(0)
    }
  }
  
  override func separatorViewTuple() -> (separatorView: UIView, viewWidth: Float)! {
    let separator = UIView(frame: CGRectZero)
    separator.backgroundColor = UIColor.lighterMidGray()
    
    return (separatorView: separator, viewWidth: 1)
  }
  
  public func radioButtonSelected(radioButton: RadioButton, selected: Bool) {
    let unselectedService = (selectedRadioButton as? ServiceRadioButton)?.properties.item as? Service
    var selectedService: Service? = nil
    if selected {
      selectedService = (radioButton as? ServiceRadioButton)?.properties.item as? Service
    }
    self.delegate?.radioButtonSelected(selectedService, unselectedService: unselectedService)
    
    selectedRadioButton = radioButton.selected ? radioButton : nil
  }
}



public protocol RadioButton {
  var selected: Bool { get set }
}

public protocol RadioButtonDelegate {
  func radioButtonSelected(radioButton: RadioButton, selected: Bool)
}

public class ServiceRadioButton: UIView, RadioButton {
  public static let nibName: String = "ServiceRadioButton"
  
  @IBOutlet weak var radioBox: BMCRadioButton!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subtitleLabel: UILabel!
  
  var textInitialColor = UIColor.darkGray()
  var textSelectedColor = UIColor.applicationSecondaryColor()
  var textUnselectedColor = UIColor.lighterMidGray()
  var radioBoxInitialColor = UIColor.lighterMidGray()
  var radioBoxSelectedColor = UIColor.applicationSecondaryColor()
  var radioBoxUnselectedColor = UIColor.lighterMidGray()
  
  var useInitialColors = true {
    didSet {
      refreshView()
    }
  }
  
  private var _selected: Bool = false
  public var delegate: RadioButtonDelegate?
  public var properties: RadioButtonProperties! {
    didSet {
      titleLabel.text = properties.title
      subtitleLabel.text = properties.subtitle
    }
  }
  
  public static func serviceRadioButton(properties: RadioButtonProperties) -> ServiceRadioButton {
    let serviceRadioButton = NSBundle.mainBundle().loadNibNamed(ServiceRadioButton.nibName,
      owner: nil, options: nil)[0] as! ServiceRadioButton
    serviceRadioButton.properties = properties
    return serviceRadioButton
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    configureView()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    configureView()
  }
  
  private func configureView() {
    initTapGesture()
    refreshView()
  }
  
  public override func layoutSubviews() {
    super.layoutSubviews()
    radioBox.userInteractionEnabled = false
    refreshView()
  }
  
  private func initTapGesture() {
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ServiceRadioButton.didTapView(_:)))
    self.addGestureRecognizer(tapGesture)
  }
  
  func didTapView(sender: AnyObject?) {
    selected = !selected
    delegate?.radioButtonSelected(self, selected: selected)
  }
  
  public var selected: Bool {
    get {
      return _selected
    }
    
    set {
      _selected = newValue
      refreshView()
    }
  }
  
  public func refreshView() {
    if let _ = titleLabel, _ = subtitleLabel, _ = radioBox {
      if _selected {
        titleLabel.textColor = textSelectedColor
        subtitleLabel.textColor = textSelectedColor
        radioBox.backgroundColor = radioBoxSelectedColor
      } else {
        if useInitialColors {
          titleLabel.textColor = textInitialColor
          subtitleLabel.textColor = textInitialColor
          radioBox.backgroundColor = radioBoxInitialColor
        } else {
          titleLabel.textColor = textUnselectedColor
          subtitleLabel.textColor = textUnselectedColor
          radioBox.backgroundColor = radioBoxUnselectedColor
        }
      }
    }
  }
}
