//
//  CarListingTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/6/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class CarListingTableViewCell: UITableViewCell {
  var vehicleInfoView: VehicleBasicInformationView!
  
  class var reusableIdentifier: String {
    return "CarListingTableViewCell"
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.lightWhite()
    self.addView()
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    let imageBackgroundColor = self.vehicleInfoView.imageView.backgroundColor
    super.setSelected(selected, animated: animated)
    self.vehicleInfoView.imageView.backgroundColor = imageBackgroundColor
    reload()
  }

  override func setHighlighted(highlighted: Bool, animated: Bool) {
    let imageBackgroundColor = self.vehicleInfoView.imageView.backgroundColor
    super.setHighlighted(highlighted, animated: animated)
    self.vehicleInfoView.imageView.backgroundColor = imageBackgroundColor
  }
  
  func reload() {
    self.backgroundColor = self.selected ? .darkPurple() : .lightWhite()
  }
  
  private func addView() {
    vehicleInfoView = NSBundle.mainBundle().loadNibNamed("VehicleBasicInformationView",
      owner: nil, options: nil)[0] as! VehicleBasicInformationView
    self.contentView.addSubview(vehicleInfoView)
    vehicleInfoView.cancelsTouches = false
    vehicleInfoView.selectionBackgroundColor = UIColor.darkPurple()
    
    vehicleInfoView.alignLeading("0", trailing: "0", toView: self.contentView)
    vehicleInfoView.alignTop("0", bottom: "0", toView: self.contentView)

  }
  
}
