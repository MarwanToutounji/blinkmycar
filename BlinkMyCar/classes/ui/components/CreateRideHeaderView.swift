//
//  CreateRideHeaderView.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class CreateRideHeaderView: UIView {
  @IBOutlet weak var cameraIconImageView: UIImageView!
  @IBOutlet weak var carImageView: UIImageView!
  @IBOutlet weak var carNameTextField: BMCTextField!
  @IBOutlet weak var topBlueView: UIView!
  @IBOutlet weak var triangleView: UIView!
  @IBOutlet weak var titleLabel: BMCBoldTitleLabel!

  public override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = UIColor.lightWhite()
    clipsToBounds = false
    topBlueView.backgroundColor = UIColor.darkPurple()
    triangleView.triangularView(color: topBlueView.backgroundColor!)
    carImageView.cicularView()
  }

  public override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.textColor = UIColor.purple()
  }

  public func fade(opacity: CGFloat) {
    cameraIconImageView.alpha = opacity
    carImageView.alpha = opacity
    carNameTextField.alpha = opacity
    titleLabel.alpha = opacity
  }
  
  public override func resignFirstResponder() -> Bool {
    if carNameTextField.isFirstResponder() {
      carNameTextField.resignFirstResponder()
      return true
    }
    return false
  }
}
