//
//  ServiceConfirmationTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/29/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout

public class ServiceConfirmationTableViewCell: UITableViewCell {
  
  public static let identifier: String = "ServiceConfirmationTableViewCell"
  
  public func setServiceBundle(serviceBundle: ServiceBundle) {
    clearContentViewSubviews()
    
    // Add the Edit Button
    let editButton = BMCEditIconButton(frame: CGRectZero)
    editButton.backgroundColor = UIColor.gray()
    editButton.userInteractionEnabled = false
    contentView.addSubview(editButton)
    editButton.constrainHeight("20")
    editButton.constrainWidth("20")
    editButton.alignTrailingEdgeWithView(contentView, predicate: "-30")
    editButton.alignTopEdgeWithView(contentView, predicate: "15")
    
    let mainServiceLabel = BMCServiceTitleLabel(frame: CGRectZero)
    mainServiceLabel.text = serviceBundle.mainService.name
    contentView.addSubview(mainServiceLabel)
    mainServiceLabel.alignLeadingEdgeWithView(contentView, predicate: "30")
    mainServiceLabel.alignTopEdgeWithView(contentView, predicate: "15@1000")
    mainServiceLabel.constrainTrailingSpaceToView(editButton, predicate: "-10")
    
    let mainServicePriceLabel = BMCServicePriceLabel(frame: CGRectZero)
    mainServicePriceLabel.text = serviceBundle.mainService.price.displayAsUsDollarsPrice
    contentView.addSubview(mainServicePriceLabel)
    mainServicePriceLabel.alignLeadingEdgeWithView(contentView, predicate: "30")
    mainServicePriceLabel.constrainTopSpaceToView(mainServiceLabel, predicate: "0")
    mainServicePriceLabel.constrainTrailingSpaceToView(editButton, predicate: "-10")
    
    
    if let addOns = serviceBundle.addOnsSerices {
      for (index, addOn) in addOns.enumerate() {
        let previousSubview = contentView.subviews.last!
        let addOnLabel = BMCSelectedAddOnServiceLabel(frame: CGRectZero)
        addOnLabel.attributedText(addOn.name, servicePrice: addOn.price.displayAsUsDollarsPrice)
        contentView.addSubview(addOnLabel)
        addOnLabel.alignLeadingEdgeWithView(contentView, predicate: "30")
        addOnLabel.alignTrailingEdgeWithView(contentView, predicate: "-30")
        if index == 0 {
          addOnLabel.constrainTopSpaceToView(previousSubview, predicate: "5")
        } else {
          addOnLabel.constrainTopSpaceToView(previousSubview, predicate: "0")
        }
      }
    }
    
    if let lastSubview = contentView.subviews.last {
      lastSubview.alignBottomEdgeWithView(contentView, predicate: "-15@1000")
    }
  }
  
  private func clearContentViewSubviews() {
    for subview in contentView.subviews {
      subview.removeFromSuperview()
    }
  }
  
  override public func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    switch selected {
    case true:
      for subview in contentView.subviews where subview is UILabel {
        if subview is BMCSelectedAddOnServiceLabel {
          continue
        }
        (subview as! UILabel).textColor = UIColor.applicationMainColor()
      }
    case false:
      for subview in contentView.subviews where subview is UILabel {
        if subview is BMCSelectedAddOnServiceLabel {
          continue
        }
        (subview as! UILabel).textColor = UIColor.darkGray()
      }
    }
    // Configure the view for the selected state
  }

}
