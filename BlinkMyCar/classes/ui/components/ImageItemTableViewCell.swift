//
//  ImageItemTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/31/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class ImageItemTableViewCell: UITableViewCell {

  class var identifier: String {
    return "ImageItemTableViewCell"
  }
  
  @IBOutlet weak var leftImageIcon: UIImageView!
  @IBOutlet weak var titleLabel: BMCSecondaryTitleLabel!
  
  override  func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    self.backgroundColor = UIColor.lightWhite()
  }
  
  var titleValue: String? {
    get {
      return titleLabel.text
    }
    set {
      titleLabel.text = newValue
    }
  }
}
