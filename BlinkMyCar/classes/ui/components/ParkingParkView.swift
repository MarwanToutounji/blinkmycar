//
//  ParkingParkView.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/4/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome

protocol ParkingParkViewDelegate: NSObjectProtocol {
  func didClickAddVoiceNote()
}

class ParkingParkView: UIView {
  var linkButtonAddVoiceNote: BMCImageLinkButton
  var floorLabel: BMCLightNoteLabel
  var spotLabel: BMCLightNoteLabel
  var noteLabel: BMCLightNoteLabel
  var floorPicker: PickerTextField
  var spotTextField: BMCAddressTextField
  var noteTextField: BMCTextView
  
  private let smallSpacing: CGFloat = 10.0
  private let mediumSpacing: CGFloat = 20.0
  
  var delegate: ParkingParkViewDelegate?
  
  override init(frame: CGRect) {
    spotTextField = BMCAddressTextField()
    noteTextField = BMCTextView()
    spotLabel = BMCLightNoteLabel()
    floorLabel = BMCLightNoteLabel()
    noteLabel = BMCLightNoteLabel()
    floorPicker = PickerTextField()
    linkButtonAddVoiceNote = BMCImageLinkButton()
    linkButtonAddVoiceNote.textColor = UIColor.applicationMainColor()
    linkButtonAddVoiceNote.fontAwesomeIcon = FAIcon.FAMicrophone
    
    spotTextField.returnKeyType = .Next
    noteTextField.returnKeyType = .Default
    
    super.init(frame: frame)
    initView()
  }

  required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
  func linkButtonAddVoiceNoteClick(sender: UIButton) {
    self.delegate?.didClickAddVoiceNote()
  }

  func initView() {
    linkButtonAddVoiceNote.addTarget(self, action: #selector(ParkingParkView.linkButtonAddVoiceNoteClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    spotTextField.delegate = self
    noteTextField.delegate = self
    
    floorPicker.constrainHeight("44")
    noteTextField.constrainHeight("100")
    //linkButtonAddVoiceNote.constrainHeight("20")
    //linkButtonAddVoiceNote.constrainHeight("160")
    
    self.addSubview(floorLabel)
    self.addSubview(floorPicker)
    self.addSubview(spotLabel)
    self.addSubview(spotTextField)
    self.addSubview(noteLabel)
    self.addSubview(noteTextField)
    self.addSubview(linkButtonAddVoiceNote)
    
    //
    floorLabel.alignTopEdgeWithView(self, predicate: "0")
    //
    floorLabel.alignLeading("0", trailing: "0", toView: self)
    
    floorPicker.constrainTopSpaceToView(floorLabel, predicate: "\(smallSpacing)")
    floorPicker.alignLeading("0", trailing: "0", toView: self)
    
    spotLabel.constrainTopSpaceToView(floorPicker, predicate: "\(mediumSpacing)")
    spotLabel.alignLeading("0", trailing: "0", toView: self)
    
    spotTextField.constrainTopSpaceToView(spotLabel, predicate: "\(smallSpacing)")
    spotTextField.alignLeading("0", trailing: "0", toView: self)
    
    noteLabel.constrainTopSpaceToView(spotTextField, predicate: "\(mediumSpacing)")
    noteLabel.alignLeading("0", trailing: "0", toView: self)
    
    noteTextField.constrainTopSpaceToView(noteLabel, predicate: "\(smallSpacing)")
    noteTextField.alignLeading("0", trailing: "0", toView: self)
    
    linkButtonAddVoiceNote.constrainTopSpaceToView(noteTextField, predicate: "\(smallSpacing)")
    linkButtonAddVoiceNote.alignLeadingEdgeWithView(self, predicate: "0")
    //
    self.alignBottomEdgeWithView(linkButtonAddVoiceNote, predicate: "0")
  }
  
  func resignResponder() {
    if self.spotTextField.isFirstResponder() {
      self.spotTextField.resignFirstResponder()
    } else if self.noteTextField.isFirstResponder() {
      self.noteTextField.resignFirstResponder()
    }
  }
}

extension ParkingParkView: UITextFieldDelegate, UITextViewDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch (textField) {
    case self.spotTextField:
      self.noteTextField.becomeFirstResponder()
      return false
    default: break
    }
    return true
  }
  
  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    self.addKeyboardToolBarDone(textView)
    if textView.text == " " {
      textView.text = ""
    }
    return true
  }
  
  func addKeyboardToolBarDone(textView: UITextView) {
    // Create a button bar for the number pad
    let keyboardDoneButtonView = UIToolbar()
    keyboardDoneButtonView.sizeToFit()
    
    // Setup the buttons to be put in the system.
    let item = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ParkingParkView.endEditingNow) )
    let spacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
    let toolbarButtons = [spacer,item]
    
    //Put the buttons into the ToolBar and display the tool bar
    keyboardDoneButtonView.setItems(toolbarButtons, animated: false)
    textView.inputAccessoryView = keyboardDoneButtonView
  }
  
  func endEditingNow() {
    resignResponder()
  }
}