//
//  CreateRidePlateNumberView.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/22/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

protocol CreateRidePlateNumberViewDelegate {
  func plateNumberTextFieldDidEndEditing(textField: UITextField)
  func saveAction()
  func skipAction()
}

public class CreateRidePlateNumberView: UIView {
  
  @IBOutlet weak var plateNumberTextField: UITextField!
  @IBOutlet weak var plateNumberLabel: UILabel!
  @IBOutlet weak var saveButton: UIButton!
  @IBOutlet weak var skipButton: UIButton!
  
  var delegate: CreateRidePlateNumberViewDelegate?
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    
    plateNumberTextField.returnKeyType = .Done
    
    plateNumberTextField.delegate = self
  }
  
  public func update(show show: Bool, animated: Bool, completionBlock: (() -> Void)?) {
    if show {
      self.show(animated, completionBlock: completionBlock)
    }
    else {
      self.hide(animated, completionBlock: completionBlock)
    }
  }
  
  public func show(animated: Bool, completionBlock: (() -> Void)?) {
    if self.alpha != 1.0 && animated {
      UIView.animateWithDuration(0.4, animations: { () -> Void in
        completionBlock?()
        self.alpha = 1.0
        self.layoutIfNeeded()
      })
    }
  }
  
  public func hide(animated: Bool, completionBlock: (() -> Void)?) {
    if self.alpha != 0.0 && animated {
      UIView.animateWithDuration(0.4, animations: { () -> Void in
        completionBlock?()
        self.alpha = 0.0
        self.layoutIfNeeded()
      })
    }
  }

  public override func resignFirstResponder() -> Bool {
    if plateNumberTextField.isFirstResponder() {
      plateNumberTextField.resignFirstResponder()
    }
    return super.resignFirstResponder()
  }
  
  public override func isFirstResponder() -> Bool {
    if plateNumberTextField.isFirstResponder() {
      return true
    }
    return false
  }
  
  @IBAction func saveAction(sender: UIButton!) {
    self.resignFirstResponder()
    self.delegate?.saveAction()
  }

  @IBAction func skipAction(sender: UIButton!) {
    self.resignFirstResponder()
    self.delegate?.skipAction()
  }
}

extension CreateRidePlateNumberView: UITextFieldDelegate {
  public func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch (textField) {
    case plateNumberTextField:
      plateNumberTextField.resignFirstResponder()
    default:
      break
    }
    
    return true
  }
  
  public func textFieldDidEndEditing(textField: UITextField) {
    switch (textField) {
    case plateNumberTextField:
      self.delegate?.plateNumberTextFieldDidEndEditing(plateNumberTextField)
    default:
      break
    }
  }
}