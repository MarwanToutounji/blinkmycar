//
//  ColorsCollectionView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/26/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

protocol ColorsCollectionViewDelegate {
  func colorsCollectionViewDidSelectColor(color: UIColor?)
}

class ColorsCollectionView: UICollectionView {

  var colorsCollection = [UIColor]()

  private var selectedColorIndex: Int? = nil

  var colorsDelegate: ColorsCollectionViewDelegate?

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.delegate = self
    self.dataSource = self
    self.registerNib(ColorsCollectionViewCell.nib,
      forCellWithReuseIdentifier: ColorsCollectionViewCell.reusableIdentifier)
  }

  var selectedColor: UIColor? {
    get {
      if selectedColorIndex != nil {
        return colorsCollection[selectedColorIndex!]
      }
      return nil
    }
    set {
      if newValue != nil {
        selectedColorIndex = colorsCollection.indexOf(newValue!)
        if selectedColorIndex != nil {
          let indexPath = NSIndexPath(forItem: selectedColorIndex!, inSection: 0)
          collectionView(self, shouldSelectItemAtIndexPath: indexPath)
        }
      }
    }
  }
}

extension ColorsCollectionView: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView,
    shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {

      let cell = collectionView.cellForItemAtIndexPath(indexPath)
        as! ColorsCollectionViewCell

      if let selectedIndexes = collectionView.indexPathsForSelectedItems() where selectedIndexes.count > 0 {
        if selectedIndexes[0].item == indexPath.item {
          cell.selected = false
          collectionView.deselectItemAtIndexPath(indexPath, animated: true)
          return false
        }
      }
      return true
  }

  func collectionView(collectionView: UICollectionView,
    didSelectItemAtIndexPath indexPath: NSIndexPath) {
      selectedColorIndex = indexPath.item
      colorsDelegate?.colorsCollectionViewDidSelectColor(colorsCollection[selectedColorIndex!])
  }
}

extension ColorsCollectionView: UICollectionViewDataSource {
  override func numberOfSections() -> Int {
    return (colorsCollection.count > 0) ? 1 : 0
  }

  func collectionView(collectionView: UICollectionView,
    numberOfItemsInSection section: Int) -> Int {
      return colorsCollection.count
  }

  func collectionView(collectionView: UICollectionView,
    cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCellWithReuseIdentifier(
        ColorsCollectionViewCell.reusableIdentifier,
        forIndexPath: indexPath) as! ColorsCollectionViewCell

      cell.color = colorsCollection[indexPath.row]
      
      return cell
  }
  
}