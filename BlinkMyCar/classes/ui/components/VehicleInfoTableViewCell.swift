//
//  VehicleInfoTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/25/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class VehicleInfoTableViewCell: UITableViewCell {
  @IBOutlet weak var labelInfo: UILabel!
  @IBOutlet weak var leftImageView: UIImageView!

  class var identifier: String {
    return "VehicleInfoTableViewCell"
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    leftImageView.cicularView()
  }

  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }

  var info: String? {
    get {
      return labelInfo.text
    }
    set {
      labelInfo.text = newValue
    }
  }
}
