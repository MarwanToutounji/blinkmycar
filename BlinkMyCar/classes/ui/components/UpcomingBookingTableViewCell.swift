//
//  UpcomingBookingTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 7/1/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout

public protocol UpcomingBookingTableViewCellDelegate {
  func upcomingBookingTableViewCellDidTapCancel(cell: UpcomingBookingTableViewCell)
  func upcomingBookingTableViewCellDidTapEdit(cell: UpcomingBookingTableViewCell)
  func upcomingBookingTableViewCellDidTapCallSpecialist(cell: UpcomingBookingTableViewCell)
}

public class UpcomingBookingTableViewCell: UITableViewCell {
  
  public static let identifier: String = "UpcomingBookingTableViewCell"
  
  public var state: BookingStatus = .Pending
  
  var delegate: UpcomingBookingTableViewCellDelegate?
  
  var bookingStatusLabel: BMCBookingStatusAutoLabel!
  var dateTimeLabel: BMCDateTimeLabel!
  var addressLabel: BMCAddressLabel!
  var servicesView: UIView!
  var totalPriceLabel: BMCUpcomingServiceLabel!
  var footerView: UIView!
  var totalPriceSeparatorView: UIView!
  var addressFooterSeparatorView: UIView!
  var servicesFooterSeparatorView: UIView!
  var separatorView: UIView!
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    bookingStatusLabel = BMCBookingStatusAutoLabel(frame: CGRectZero)
    dateTimeLabel = BMCDateTimeLabel(frame: CGRectZero)
    addressLabel = BMCAddressLabel(frame: CGRectZero)
    servicesView = UIView(frame: CGRectZero)
    totalPriceLabel = BMCUpcomingServiceLabel(frame: CGRect.zero)
    footerView = UIView(frame: CGRectZero)
    totalPriceSeparatorView = UIView(frame: CGRect.zero)
    addressFooterSeparatorView = UIView(frame: CGRectZero)
    servicesFooterSeparatorView = UIView(frame: CGRectZero)
    separatorView = UIView(frame: CGRectZero)
    
    totalPriceSeparatorView.backgroundColor = UIColor.veryLightGray()
    totalPriceLabel.fontSize = 15
    
    addressFooterSeparatorView.backgroundColor = UIColor.veryLightGray()
    servicesFooterSeparatorView.backgroundColor = UIColor.veryLightGray()
    separatorView.backgroundColor = UIColor.veryLightGray()
    
    contentView.addSubview(bookingStatusLabel)
    contentView.addSubview(dateTimeLabel)
    contentView.addSubview(addressLabel)
    contentView.addSubview(servicesView)
    contentView.addSubview(totalPriceSeparatorView)
    contentView.addSubview(totalPriceLabel)
    contentView.addSubview(footerView)
    contentView.addSubview(addressFooterSeparatorView)
    contentView.addSubview(servicesFooterSeparatorView)
    contentView.addSubview(separatorView)
    
    bookingStatusLabel.alignLeading("30", trailing: "30", toView: contentView)
    bookingStatusLabel.alignTopEdgeWithView(contentView, predicate: "15")
    dateTimeLabel.alignLeading("30", trailing: "-30", toView: contentView)
    dateTimeLabel.constrainTopSpaceToView(bookingStatusLabel, predicate: "10")
    addressLabel.alignLeading("30", trailing: "-30", toView: contentView)
    addressLabel.constrainTopSpaceToView(dateTimeLabel, predicate: "10")
    servicesView.alignLeading("30", trailing: "-30", toView: contentView)
    servicesView.constrainTopSpaceToView(addressLabel, predicate: "15")
    totalPriceLabel.alignLeading("30", trailing: "-30", toView: contentView)
    totalPriceLabel.constrainTopSpaceToView(servicesView, predicate: "10")
    footerView.alignLeading("0", trailing: "0", toView: contentView)
    footerView.constrainTopSpaceToView(totalPriceLabel, predicate: "10")
    totalPriceSeparatorView.alignLeading("30", trailing: "-30", toView: contentView)
    totalPriceSeparatorView.constrainHeight("1")
    totalPriceSeparatorView.constrainTopSpaceToView(servicesView, predicate: "0")
    addressFooterSeparatorView.alignLeading("30", trailing: "-30", toView: contentView)
    addressFooterSeparatorView.constrainHeight("1")
    addressFooterSeparatorView.alignTopEdgeWithView(servicesView, predicate: "0")
    servicesFooterSeparatorView.alignLeading("0", trailing: "0", toView: contentView)
    servicesFooterSeparatorView.constrainHeight("1")
    servicesFooterSeparatorView.alignTopEdgeWithView(footerView, predicate: "0")
    separatorView.constrainTopSpaceToView(footerView, predicate: "0")
    separatorView.alignLeading("0", trailing: "0", toView: contentView)
    separatorView.constrainHeight("20")
    separatorView.alignBottomEdgeWithView(contentView, predicate: "0")
  }
  
  public func setBooking(booking: Booking) {
    let services = booking.services ?? [Service]()
    
    // Set Status
    bookingStatusLabel.status = booking.status!
    
    // Set The date & time
    dateTimeLabel.attributedText(
      booking.deliveryDate?.displayTextForServiceDate(),
      time: booking.bookingTime?.startTimeStringValue)
    
    // Set the address
    addressLabel.text = booking.address?.fullAddress
    
    // Set services
    servicesView.clearSubviews()
    for (index, service) in services.enumerate() {
      var serviceLabel: BMCUpcomingServiceLabel!
      serviceLabel = BMCUpcomingServiceLabel(frame: CGRectZero)
      serviceLabel.attributedText(
        service.name,
        servicePrice: service.price.displayAsUsDollarsPrice)
      addServiceLabelToSuperview(serviceLabel, superview: servicesView, index: index, totalServicesCount: services.count)
    }
    
    // Set Total Price
    totalPriceLabel.attributedText("Total", servicePrice: booking.totalPrice?.displayAsUsDollarsPrice)
    
    // Add Action Buttons in footer for state
    layoutFooterSubviewsForBooking(booking)
  }
  
  func layoutFooterSubviewsForBooking(booking: Booking) {
    // clear footer
    footerView.clearSubviews()
    var footerTopView = footerView
    
    // Add Action Buttons in footer for state
    state = booking.status ?? .Pending
    switch state {
    case .Modified, .Pending, .Confirmed:
      // Add Call Specialist section in footer if available
      //------------------------------------------------------
      if let specialistName = booking.specialistName,
        specialistNumber = booking.specialistNumber
        where !specialistName.isEmpty && !specialistNumber.isEmpty {
        let callSpecialistButton = BMCCallSpecialistButton(frame: CGRect.zero)
        callSpecialistButton.addTarget(self, action: #selector(self.didTapCallSpecialistActionButton), forControlEvents: UIControlEvents.TouchUpInside)
        // TODO: localize
        callSpecialistButton.setTitle("Call Specialist: \(specialistName)", forState: .Normal)
        let callSpecialistBottomSeparator = UIView(frame: CGRect.zero)
        callSpecialistBottomSeparator.backgroundColor = UIColor.veryLightGray()
        footerView.addSubview(callSpecialistButton)
        footerView.addSubview(callSpecialistBottomSeparator)
        callSpecialistButton.constrainHeight("40")
        callSpecialistButton.alignTopEdgeWithView(footerView, predicate: "0")
        callSpecialistButton.constrainWidthToView(footerView, predicate: "<=*1")
        callSpecialistButton.alignCenterXWithView(footerView, predicate: "0")
        callSpecialistBottomSeparator.constrainHeight("1")
        callSpecialistBottomSeparator.alignLeading("0", trailing: "0", toView: footerView)
        callSpecialistBottomSeparator.alignBottomEdgeWithView(callSpecialistButton, predicate: "0")
        footerTopView = callSpecialistBottomSeparator
      }
      
      // Add Cancel and Edit booking action buttons
      //----------------------------------------------
      let cancelButton = BMCLinkButton(frame: CGRectZero)
      cancelButton.setTitle("Cancel", forState: .Normal) // TODO: localize
      cancelButton.addTarget(self, action: #selector(UpcomingBookingTableViewCell.didTapCancelActionButton), forControlEvents: .TouchUpInside)
      let editButton = BMCLinkButton(frame: CGRectZero)
      editButton.setTitle("Edit", forState: .Normal) // TODO: localize
      editButton.addTarget(self, action: #selector(UpcomingBookingTableViewCell.didTapEditActionButton), forControlEvents: .TouchUpInside)
      let buttonsSeparator = UIView(frame: CGRectZero)
      buttonsSeparator.backgroundColor = UIColor.veryLightGray()
      footerView.addSubview(cancelButton)
      footerView.addSubview(editButton)
      footerView.addSubview(buttonsSeparator)
      cancelButton.alignLeadingEdgeWithView(footerView, predicate: "0")
      cancelButton.alignTopEdgeWithView(footerTopView, predicate: "0")
      cancelButton.alignBottomEdgeWithView(footerView, predicate: "0")
      cancelButton.constrainHeight("40")
      cancelButton.constrainWidthToView(footerView, predicate: "==*0.5")
      editButton.constrainWidthToView(cancelButton, predicate: "0")
      editButton.constrainHeightToView(cancelButton, predicate: "0")
      editButton.alignTopEdgeWithView(cancelButton, predicate: "0")
      editButton.alignTrailingEdgeWithView(footerView, predicate: "0")
      buttonsSeparator.constrainWidth("1")
      buttonsSeparator.alignCenterXWithView(footerView, predicate: "0")
      buttonsSeparator.alignTopEdgeWithView(footerTopView, predicate: "0")
      buttonsSeparator.alignBottomEdgeWithView(footerView, predicate: "0")
    case .Done, .Completed:
      break
    case .Processing:
      break
    case .Rejected, .RejectedUnwashable, .RejectedWasherAvailability, .RejectedWasherObstruction, .RejectedWashStopped, .Canceled:
      break
    }
  }
  
  private func addServiceLabelToSuperview(serviceLabel: UILabel, superview: UIView, index: Int, totalServicesCount: Int) {
    superview.addSubview(serviceLabel)
    serviceLabel.alignLeading("0", trailing: "0", toView: superview)
    
    // Align Top constraint
    if index == 0 {
      serviceLabel.alignTopEdgeWithView(superview, predicate: "15")
    } else {
      serviceLabel.constrainTopSpaceToView(superview.subviews[index - 1], predicate: "10")
    }
    
    // align bottom constraint
    if index == totalServicesCount - 1 {
      serviceLabel.alignBottomEdgeWithView(superview, predicate: "-15")
    }
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override public func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }

  
  //=================================================================
  // MARK: Cancel
  //=================================================================
  
  func didTapCancelActionButton() {
    delegate?.upcomingBookingTableViewCellDidTapCancel(self)
  }
  
  func didTapEditActionButton() {
    delegate?.upcomingBookingTableViewCellDidTapEdit(self)
  }
  
  func didTapCallSpecialistActionButton() {
    delegate?.upcomingBookingTableViewCellDidTapCallSpecialist(self)
  }
}
