//
//  SwitchTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/3/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

protocol SwitchTableViewCellDelegate: NSObjectProtocol {
  func didToggle(cell: SwitchTableViewCell, value: Bool)
}

class SwitchTableViewCell: UITableViewCell {
  
  @IBOutlet weak var toggleView: UISwitch!
  @IBOutlet weak var itemTitleLabel: UILabel!
  
  var delegate: SwitchTableViewCellDelegate?
  
  class var reusableIdentifier: String {
    return "SwitchTableViewCell"
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.selectionStyle = .None
  }
  
}

//MARK: A C T I O N S
extension SwitchTableViewCell {
  @IBAction func switchValueChanged(sender: UISwitch) {
    self.delegate?.didToggle(self,value: sender.on)
  }
}
