//
//  SmallTableSectionView.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/29/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public class SmallTableSectionView: UITableViewHeaderFooterView {
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var label: BMCLabelForSmallSectionView!
  
  public class var nib: UINib {
    return  UINib(nibName: "SmallTableSectionView" , bundle: nil)
  }
  public static let identifier: String = "SmallTableSectionView"
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    self.contentView.clipsToBounds = true
    containerView.backgroundColor = UIColor.lighterMidGray()
  }
  
  func setTitle(title: String) {
    label.text = title
  }
}
