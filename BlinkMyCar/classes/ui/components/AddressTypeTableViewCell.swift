//
//  AddressTypeTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/23/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class AddressTypeTableViewCell: UITableViewCell {

  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var addressItemLabel: UILabel!
  
  class var reusableIdentifier: String {
    return "AddressTypeTableViewCell"
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}