//
//  ParkingStreetView.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/4/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome

protocol ParkingStreetViewDelegate: NSObjectProtocol {
  func didClickOnEditStreet(enabled: Bool)
  func didClickAddVoiceNote()
}

class ParkingStreetView: UIView {
  var linkButtonEditStreet: BMCImageLinkButton
  var linkButtonAddVoiceNote: BMCImageLinkButton
  var streetTextField: BMCMultiStateTextField
  var noteTextField: BMCTextView
  var noteLabel: BMCLightNoteLabel
  var streetLabel: BMCLightNoteLabel
  
  private let smallSpacing: CGFloat = 10.0
  private let mediumSpacing: CGFloat = 20.0
  
  var delegate: ParkingStreetViewDelegate?
  
  override init(frame: CGRect) {
    streetTextField = BMCMultiStateTextField()
    noteTextField = BMCTextView()
    noteLabel = BMCLightNoteLabel()
    streetLabel = BMCLightNoteLabel()
    linkButtonEditStreet = BMCImageLinkButton()
    linkButtonEditStreet.textColor = UIColor.applicationMainColor()
    linkButtonEditStreet.fontAwesomeIcon = FAIcon.FAMapMarker
    linkButtonAddVoiceNote = BMCImageLinkButton()
    linkButtonAddVoiceNote.textColor = UIColor.applicationMainColor()
    linkButtonAddVoiceNote.fontAwesomeIcon = FAIcon.FAMicrophone

    streetTextField.returnKeyType = .Next
    noteTextField.returnKeyType = .Default
    
    super.init(frame: frame)
    initView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    streetTextField = BMCMultiStateTextField()
    noteTextField = BMCTextView()
    noteLabel = BMCLightNoteLabel()
    streetLabel = BMCLightNoteLabel()
    linkButtonEditStreet = BMCImageLinkButton()
    linkButtonEditStreet.textColor = UIColor.applicationMainColor()
    linkButtonEditStreet.fontAwesomeIcon = FAIcon.FAMapMarker
    linkButtonAddVoiceNote = BMCImageLinkButton()
    linkButtonAddVoiceNote.textColor = UIColor.applicationMainColor()
    linkButtonAddVoiceNote.fontAwesomeIcon = FAIcon.FAMicrophone
    
    streetTextField.returnKeyType = .Next
    noteTextField.returnKeyType = .Done
    
    super.init(coder: aDecoder)
    initView()
  }
  
  func linkButtonAddVoiceNoteClick(sender: UIButton) {
    self.delegate?.didClickAddVoiceNote()
  }
  
  func linkButtonEditStreetClick(sender: UIButton) {
    streetTextField.enable(!streetTextField.enabled)
    if streetTextField.enabled {
      streetTextField.becomeFirstResponder()
    }
    //TODO: make sure action here is right
    self.delegate?.didClickOnEditStreet(streetTextField.enabled)
  }
  
  func initView() {
    streetTextField.enable(false)
    
    linkButtonAddVoiceNote.addTarget(self, action: #selector(ParkingStreetView.linkButtonAddVoiceNoteClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    linkButtonEditStreet.addTarget(self, action: #selector(ParkingStreetView.linkButtonEditStreetClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    streetTextField.delegate = self
    noteTextField.delegate = self

    noteTextField.constrainHeight("100")
    
    self.addSubview(streetLabel)
    self.addSubview(streetTextField)
    self.addSubview(noteLabel)
    self.addSubview(noteTextField)
    self.addSubview(linkButtonEditStreet)
    self.addSubview(linkButtonAddVoiceNote)

    
    //
    streetLabel.alignTopEdgeWithView(self, predicate: "0")
    //
    streetLabel.alignLeading("0", trailing: "0", toView: self)
    
    streetTextField.constrainTopSpaceToView(streetLabel, predicate: "\(smallSpacing)")
    streetTextField.alignLeading("0", trailing: "0", toView: self)
    
    //
    linkButtonEditStreet.constrainTopSpaceToView(streetTextField, predicate: "\(smallSpacing)")
    linkButtonEditStreet.alignLeadingEdgeWithView(self, predicate: "0")
    //
    
    noteLabel.constrainTopSpaceToView(linkButtonEditStreet, predicate: "\(mediumSpacing)")
    noteLabel.alignLeading("0", trailing: "0", toView: self)
    
    noteTextField.constrainTopSpaceToView(noteLabel, predicate: "\(smallSpacing)")
    noteTextField.alignLeading("0", trailing: "0", toView: self)
    //
    
    linkButtonAddVoiceNote.constrainTopSpaceToView(noteTextField, predicate: "\(smallSpacing)")
    linkButtonAddVoiceNote.alignLeadingEdgeWithView(self, predicate: "0")
    //
    self.alignBottomEdgeWithView(linkButtonAddVoiceNote, predicate: "0")
    //
  }
  
  func resignResponder() {
    if self.streetTextField.isFirstResponder() {
      self.streetTextField.resignFirstResponder()
    } else if self.noteTextField.isFirstResponder() {
      self.noteTextField.resignFirstResponder()
    }
  }
}

extension ParkingStreetView: UITextFieldDelegate, UITextViewDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    switch (textField) {
    case self.streetTextField:
      self.noteTextField.becomeFirstResponder()
      return false
    default: break
    }
    return true
  }
  
  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    self.addKeyboardToolBarDone(textView)
    if textView.text == " " {
      textView.text = ""
    }
    return true
  }
  
  func addKeyboardToolBarDone(textView: UITextView) {
    // Create a button bar for the number pad
    let keyboardDoneButtonView = UIToolbar()
    keyboardDoneButtonView.sizeToFit()
    
    // Setup the buttons to be put in the system.
    let item = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ParkingStreetView.endEditingNow) )
    let spacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
    let toolbarButtons = [spacer,item]
    
    //Put the buttons into the ToolBar and display the tool bar
    keyboardDoneButtonView.setItems(toolbarButtons, animated: false)
    textView.inputAccessoryView = keyboardDoneButtonView
  }
  
  func endEditingNow() {
    resignResponder()
  }
}
