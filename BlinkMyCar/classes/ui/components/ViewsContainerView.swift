//
//  ViewsContainerView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/24/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout

public class ViewsContainerView: UIView {

  private var _position: Int = 0

  private var leftConstraint: NSLayoutConstraint?

  public func setupViewWithViews(viewsArray: [UIView]) {
    for view in viewsArray {
      self.addSubview(view)
      view.alignTop("0", bottom: "0", toView: self)
      view.constrainWidthToView(self, predicate: "0")
    }

    addLeftSideForSubviews()
  }

  public func setupViewWithViewControllers(viewControllers: [UIViewController],
    parentViewController: UIViewController) {
      for viewController in viewControllers {
        // call before adding child view controller's view as subview
        parentViewController.addChildViewController(viewController)

        let view: UIView = viewController.view
        self.addSubview(view)
        view.alignTop("0", bottom: "0", toView: self)
        view.constrainWidthToView(self, predicate: "0")

        // call after adding child view controller's view as subview
        viewController.didMoveToParentViewController(parentViewController)
      }

      addLeftSideForSubviews()
  }

  // MARK: Controller Public METHODS and COMPUTED VARIABLES
  //------------------------------------------------------------
  public var position: Int {
    get {
      return _position
    }
    set {
      _position = newValue

      animateToPosition()
      // TODO: Send Notification When transition finishes
    }
  }

  public func next() -> Bool {
    if position == self.subviews.count - 1 {
      return false
    }

    position += 1
    return true
  }

  public func previous() -> Bool {
    if position == 0 {
      return false
    }

    position -= 1
    return true
  }

  // MARK: P R I V A T E   M E T H O D S
  //------------------------------------------------------------
  private func animateToPosition(animated: Bool = true) {
    leftConstraint?.constant = -(CGFloat(self.frame.width * CGFloat(position)))
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.layoutIfNeeded()
    })
  }

  private func addLeftSideForSubviews() {
    if subviews.count > 0 {
      leftConstraint = subviews[0].alignLeadingEdgeWithView(self, predicate: "0")[0]
        as? NSLayoutConstraint
    }

    if subviews.count > 1 {
      UIView.spaceOutViewsHorizontally(subviews, predicate: "0")
    }
  }
}
