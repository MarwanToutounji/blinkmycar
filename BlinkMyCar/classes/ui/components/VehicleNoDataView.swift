//
//  VehicleNoDataView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 9/18/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

public protocol VehicleNoDataViewDelegate {
  func didTapAddButton(text: String)
}

class HighlightView: UIView {
  var highlightColor = UIColor.lighterGray()

  private var originalBackgroundColor: UIColor?

  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesBegan(touches, withEvent: event)
    originalBackgroundColor = backgroundColor
    backgroundColor = highlightColor
  }

  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    backgroundColor = originalBackgroundColor
  }

  override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
    super.touchesCancelled(touches, withEvent: event)
    backgroundColor = originalBackgroundColor
  }
}

public class VehicleNoDataView: UIView {
  public enum State {
    case Make
    case Model
  }

  @IBOutlet weak var actionContainerView: UIView!
  @IBOutlet weak var topSeparatorView: UIView!
  @IBOutlet weak var bottomSeparator: UIView!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var actionLabel: BMCListItemLabel!
  @IBOutlet weak var label: UILabel!

  public var delegate: VehicleNoDataViewDelegate?

  public var addedItemName: String = "" {
    didSet {
      // Change all the fields that uses this variable here
      updateButtonText()
      refreshLabelContent()
    }
  }

  public var state: State = .Make {
    didSet {
      refreshView()
    }
  }

  public override func awakeAfterUsingCoder(aDecoder: NSCoder) -> AnyObject? {
    return afterUsingCoderLoadNibIfNeeded("VehicleNoDataView")
  }

  public override func awakeFromNib() {
    super.awakeFromNib()

    // Add Action to the action View
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(VehicleNoDataView.didTapButton(_:)))
    actionContainerView.addGestureRecognizer(tapGesture)

    // Set the image View Icon
    imageView.image = UIImage(named: "addIcon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    imageView.tintColor = UIColor.orange()

    // Set the separators Color
    let separatorsColor = UIColor.lightGray()
    topSeparatorView.backgroundColor = separatorsColor
    bottomSeparator.backgroundColor = separatorsColor

    self.layoutIfNeeded()

    refreshView()
  }

  public func refreshView() {
    addedItemName = ""
    refreshLabelContent()
  }

  public func refreshLabelContent() {
    switch state {
    case .Make:
      label.text = NSLocalizedString("CreateRideViewController.addCustomMake",
        value: "Add your vehicle's brand.",
        comment: "Short message that appears when the user is adding a cutom make")
    case .Model:
      label.text = NSLocalizedString("CreateRideViewController.addCustomModel",
        value: "Add your vehicle's model.",
        comment: "Short message that appears when the user is adding a cutom model")
    }
  }

  public func updateButtonText() {
    actionLabel.text = "Add \"\(addedItemName)\"".trim()
  }

  func didTapButton(sender: AnyObject) {
    delegate?.didTapAddButton(addedItemName)
  }
}
