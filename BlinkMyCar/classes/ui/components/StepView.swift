//
//  StepView.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/10/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public typealias StepInfo = (index: Int, description: String, isLast: Bool)

public class StepView: UIView {
  public struct Config {
    var linesColor: UIColor = UIColor.whiteColor()
    var numberIndicatorTextColor = UIColor.applicationMainColor()
    var descriptionTextColor: UIColor = UIColor.whiteColor()
    var backgroundColor: UIColor = UIColor.clearColor()
    var lineHeight: CGFloat = 1
    var descriptionLabelBackgroundColor: UIColor = UIColor.applicationMainColor()
    var numberIndicatorBackgroundColor: UIColor = UIColor.whiteColor()
    var numberIndicatorSelectedBackgroundColor: UIColor = UIColor.orange()
    var numberIndicatorSelectedTextColor: UIColor = UIColor.whiteColor()
    var displayDescriptionLabel: Bool = false
  }
  
  public static let nibName: String = "StepView"
  
  @IBOutlet weak var numberIndicatorLabel: BMCStepIndicatorLabel!
  @IBOutlet weak var descriptionLabel: BMCStepLabel!
  @IBOutlet weak var leftLine: UIView!
  @IBOutlet weak var rightLine: UIView!
  
  @IBOutlet weak var linesHeightConstraint: NSLayoutConstraint!
  
  var config = Config()
  
  var selected: Bool = false {
    didSet {
      refreshViewForSelection()
    }
  }
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    configureView()
  }
  
  func configureView() {
    self.backgroundColor = config.backgroundColor
    if let _ = linesHeightConstraint, _ = leftLine, _ = rightLine {
      linesHeightConstraint.constant = config.lineHeight
      leftLine.backgroundColor = config.linesColor
      rightLine.backgroundColor = config.linesColor
      numberIndicatorLabel.backgroundColor = config.numberIndicatorBackgroundColor
      descriptionLabel.backgroundColor = config.descriptionLabelBackgroundColor
      numberIndicatorLabel.textColor = config.numberIndicatorTextColor
      descriptionLabel.textColor = config.descriptionTextColor
      descriptionLabel.hidden = !config.displayDescriptionLabel
    }
    
    refreshViewForSelection()
  }
  
  func refreshViewForSelection() {
    if let _ = linesHeightConstraint, _ = leftLine, _ = rightLine {
      if selected {
        numberIndicatorLabel.backgroundColor = config.numberIndicatorSelectedBackgroundColor
        numberIndicatorLabel.textColor = config.numberIndicatorSelectedTextColor
      } else {
        numberIndicatorLabel.backgroundColor = config.numberIndicatorBackgroundColor
        numberIndicatorLabel.textColor = config.numberIndicatorTextColor
      }
    }
  }
  
  var info: StepInfo = (0, "", true) {
    didSet {
      numberIndicatorLabel.text = "\(info.index)"
      descriptionLabel.text = config.displayDescriptionLabel ? info.description : nil
      
      if info.index == 1 {
        leftLine.hidden = true
      }
      if info.isLast {
        rightLine.hidden = true
      }
    }
  }

  public static func stepView(info: StepInfo) -> StepView {
    let stepView = NSBundle.mainBundle().loadNibNamed(
      StepView.nibName,
      owner: nil, options: nil)[0] as! StepView
    stepView.info = info
    return stepView
  }
}
