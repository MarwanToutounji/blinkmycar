//
//  ConfirmationMinimalTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/28/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public class ConfirmationMinimalTableViewCell: UITableViewCell {
  @IBOutlet weak var label: BMCSingleLabelInCell!
  @IBOutlet weak var editButton: BMCEditIconButton!
  
  public class var nib: UINib {
    return  UINib(nibName: "ConfirmationMinimalTableViewCell" , bundle: nil)
  }
  public static let identifier: String = "ConfirmationMinimalTableViewCell"
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    editButton.userInteractionEnabled = false
  }
  
  override public func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
