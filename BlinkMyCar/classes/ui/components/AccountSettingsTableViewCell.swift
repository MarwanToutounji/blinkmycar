//
//  AccountSettingsTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/27/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

protocol AccountSettingsTableViewCellDelegate: NSObjectProtocol {
  func didToggle(cell: AccountSettingsTableViewCell, value: Bool)
}

class AccountSettingsTableViewCell: UITableViewCell {
  
  @IBOutlet weak var itemImage: UIImageView!
  @IBOutlet weak var itemTitleLabel: UILabel!
  //Right side Items
  @IBOutlet weak var toggleView: UISwitch!
  
  @IBOutlet weak var separator: UIView!
  var delegate: AccountSettingsTableViewCellDelegate?
  
  //default view: hide toggle show image
  var showToggle: Bool = false {
    didSet {
        toggleView.alpha = self.showToggle  ? 1.0 : 0.0
    }
  }
  
  var leftImage: UIImage? {
    didSet {
      itemImage.image = leftImage
    }
  }
  
  class var reusableIdentifier: String {
    return "AccountSettingsTableViewCell"
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    separator.backgroundColor = UIColor.lighterMidGray()
    itemImage.tintColor = UIColor.applicationSecondaryColor()
  }
  
}

//MARK: A C T I O N S
extension AccountSettingsTableViewCell {
  @IBAction func switchValueChanged(sender: UISwitch) {
    self.delegate?.didToggle(self,value: sender.on)
  }
}
