//
//  DetailsMenuTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class DetailsMenuTableViewCell: UITableViewCell {

  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var menuItemLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }

  var menuItemText: String? {
    get {
      return menuItemLabel.text
    }

    set {
      menuItemLabel.text = newValue
    }
  }

  var menuItemImage: UIImage? {
    get {
      return iconImageView.image
    }

    set {
      iconImageView.image = newValue
    }
  }

  var imageTintColor: UIColor? {
    get {
      return iconImageView.tintColor
    }

    set {
      var color = newValue
      if color == nil {
        color = UIColor.applicationSecondaryColor()
      }
      iconImageView.tintColor = color
    }
  }

  class var reusableIdentifier: String {
    return "DetailsMenuTableViewCell"
  }
}
