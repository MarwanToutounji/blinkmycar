//
//  PickerTextField.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/23/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

protocol PickerTextFieldDelegate {
  func didSelect(selectedText: String)
}

public class PickerTextField: BMCTextFieldWithDropDownIcon {
  
  private var spinner = UIPickerView()
  
  private var _viewModel: PickerTextFieldViewModel = PickerTextFieldViewModel()
  
  var pickerDelegate: PickerTextFieldDelegate?

  override init(frame: CGRect) {
    super.init(frame: frame)
    self.initView()
  }

  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.initView()
  }
  
  private func initView() {
  enableTheme()
  initInputProperties()
  
  NSNotificationCenter.defaultCenter().addObserver(self,
  selector: #selector(PickerTextField.didUpdateViewModelSelection(_:)),
  name: KWDPickerTextFieldNotificationsName.DidUpdateModelViewSelection,
  object: nil)
  
  //add right view tap gesture
  let rightViewTap :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PickerTextField.didTapOnRightView(_:)))
  rightViewTap.numberOfTapsRequired = 1
  rightViewTap.numberOfTouchesRequired = 1
  rightViewTap.cancelsTouchesInView = false
  self.rightView!.addGestureRecognizer(rightViewTap)
  self.rightView!.userInteractionEnabled = true
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  var viewModel: PickerTextFieldViewModel {
    get {
      return _viewModel
    }
    set {
      _viewModel = newValue
      spinner.reloadAllComponents()
    }
  }
  
  //===============================================================
  // MARK: ACTION
  //===============================================================
  
  func didTapOnRightView(tapGesture: UITapGestureRecognizer) {
    self.becomeFirstResponder()
  }
  
  func didClickKeyboardDoneButton(sender: AnyObject?) {
    viewModel.setSelectedComponentAtRow(spinner.selectedRowInComponent(0))
    self.text = viewModel.selectedText
    self.resignFirstResponder()
    self.pickerDelegate?.didSelect(self.text ?? "")
  }
  
  func didClickKeyboardCancelButton(sender: AnyObject?) {
    self.resignFirstResponder()
    spinner.selectRow(viewModel.selectedRow, inComponent: 0, animated: false)
  }
  
  func didUpdateViewModelSelection(sender: AnyObject?) {
    self.text = viewModel.selectedText
    spinner.selectRow(viewModel.selectedRow, inComponent: 0, animated: false)
  }
  
  //===============================================================
  // MARK: HELPERS
  //===============================================================
  func enableTheme() {
    //enableComboBoxStyling()
    
    spinner.backgroundColor = UIColor.whiteColor()
  }
  
  func setSideViews() {
    setLeftView()
    setRightView()
  }
  
  func setRightView() {
    rightViewMode = UITextFieldViewMode.Always
    let rightImageViewContainer = UIImageView(frame: CGRect(x: 0, y: 0,
      width: self.frame.height, height: self.frame.height))
    rightImageViewContainer.backgroundColor = UIColor.orange()
    let rightImageView = UIImageView(frame: UIEdgeInsetsInsetRect(
      rightImageViewContainer.frame,
      UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)))
    rightImageView.center = rightImageViewContainer.center
    rightImageView.contentMode = UIViewContentMode.ScaleAspectFit
    rightImageView.image = UIImage(named: "dropDownIcon")
    rightImageViewContainer.addSubview(rightImageView)
    rightView = rightImageViewContainer
  }
  
  func setLeftView() {
    leftViewMode = UITextFieldViewMode.Always
    let leftViewContainer = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
    leftView = leftViewContainer
  }
  
  func initInputProperties() {
    /*
    ** Add Input View
    */
    spinner.dataSource = self
    spinner.delegate = self
    inputView = spinner
    
    /*
    ** Add Input Accessory View
    */
    let keyboardToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,
      width: self.bounds.size.width, height: 44))
    keyboardToolbar.barStyle = UIBarStyle.Default
    keyboardToolbar.tintColor = UIColor.applicationMainColor()
    
    let flexSpace: UIBarButtonItem = UIBarButtonItem(
      barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace,
      target: nil,
      action: nil)
    let cancelButton: UIBarButtonItem = UIBarButtonItem(
      barButtonSystemItem: UIBarButtonSystemItem.Cancel,
      target: self,
      action: #selector(PickerTextField.didClickKeyboardCancelButton(_:)))
    let doneButton: UIBarButtonItem = UIBarButtonItem(
      barButtonSystemItem: UIBarButtonSystemItem.Done,
      target: self,
      action: #selector(PickerTextField.didClickKeyboardDoneButton(_:)))
    keyboardToolbar.setItems([flexSpace, cancelButton, doneButton], animated: false)
    
    inputAccessoryView = keyboardToolbar
  }
}

extension PickerTextField: UIPickerViewDataSource {
  public func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return viewModel.numberOfComponents
  }
  public func pickerView(pickerView: UIPickerView,
    numberOfRowsInComponent component: Int) -> Int {
      return viewModel.numberOfRows
  }
}

extension PickerTextField: UIPickerViewDelegate {
  public func pickerView(pickerView: UIPickerView, titleForRow row: Int,
    forComponent component: Int) -> String? {
      return viewModel.titleForRow(row)
  }
}