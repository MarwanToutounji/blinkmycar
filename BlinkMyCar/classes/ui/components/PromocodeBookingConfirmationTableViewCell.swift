//
//  PromocodeBookingConfirmationTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/30/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public class PromocodeBookingConfirmationTableViewCell: UITableViewCell {
  @IBOutlet weak var promocodeLabel: UILabel!
  
  public class var nib: UINib {
    return  UINib(nibName: "PromocodeBookingConfirmationTableViewCell" , bundle: nil)
  }
  
  class var identifier: String {
    return "PromocodeBookingConfirmationTableViewCell"
  }
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    
    self.tintColor = UIColor.applicationMainColor()
    self.accessoryType = .DisclosureIndicator
    let imageView = UIImageView(frame: CGRect(x: 0,y: 0,width: 20,height: 20))
    imageView.image = UIImage(named: "arrow")?.imageWithRenderingMode(.AlwaysTemplate)
    imageView.contentMode = UIViewContentMode.ScaleAspectFit
    self.accessoryView = imageView
  }
}
