//
//  PaymentListingTableViewCell.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 4/13/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import M13Checkbox
import Stripe

class PaymentListingTableViewCell: UITableViewCell {

  @IBOutlet weak var paymentToolImageView: UIImageView!
  @IBOutlet weak var cashToolImageView: UIImageView!
  @IBOutlet weak var descriptionLabel: BMCSmallFormLabel!
  @IBOutlet weak var checkBox: M13Checkbox!
  @IBOutlet weak var defaultLabel: BMCLightNoteLabel!

  class var identifier: String {
    return "PaymentListingTableViewCell"
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyle.None
    checkBox.tintColor = UIColor.applicationMainColor()
    checkBox.markType = .Checkmark
    checkBox.secondaryTintColor = UIColor.darkGray()
    checkBox.secondaryCheckmarkTintColor = UIColor.darkGray()
    checkBox.boxType = .Circle
    checkBox.checkmarkLineWidth = 3.0
    checkBox.hideBox = false
    checkBox.enabled = false
    
    paymentToolImageView.addObserver(
      self,
      forKeyPath: "image",
      options: NSKeyValueObservingOptions.New,
      context: nil)
    cashToolImageView.addObserver(
      self,
      forKeyPath: "image",
      options: NSKeyValueObservingOptions.New,
      context: nil)
  }
  
  deinit {
    paymentToolImageView.removeObserver(self, forKeyPath: "image")
    cashToolImageView.removeObserver(self, forKeyPath: "image")
  }
  
  override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
    if let imageView = object as? UIImageView {
      if imageView == paymentToolImageView && (keyPath ?? "") == "image" {
        cashToolImageView.hidden = true
        paymentToolImageView.hidden = false
      } else if imageView == cashToolImageView && (keyPath ?? "") == "image" {
        cashToolImageView.hidden = false
        paymentToolImageView.hidden = true
      }
    }
  }
}
