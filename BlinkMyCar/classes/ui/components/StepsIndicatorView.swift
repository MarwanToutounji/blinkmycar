//
//  StepsIndicatorView.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/10/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit

public class StepsIndicatorView: HorizontalStackView {
  public override var topPadding: CGFloat {
    return 20
  }
  
  var steps: [String]! {
    didSet {
      numberOfStackedViews = steps.count
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    configureView()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    configureView()
  }
  
  public override func layoutSubviews() {
    super.layoutSubviews()
    configureView()
  }
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    configureView()
  }
  
  private func configureView() {
    self.addSeparator = false
    self.backgroundColor = UIColor.applicationMainColor()
  }
  
  override func subView(index: Int) -> UIView! {
    let subview = StepView.stepView((index: (index+1), description: steps[index], isLast: index == (steps.count - 1)))
    return subview
  }
  
  private var selectedStepView: StepView? {
    willSet {
      guard let selectedStepView = selectedStepView, let newValue = newValue else { return }
      if selectedStepView != newValue {
        self.selectedStepView?.selected = false
      }
    }
  }

  func selectIndicatorForIndex(index: Int) {
    let subview = subviews[index] as! StepView
    subview.selected = true
    selectedStepView = subview
  }
}

//========================================================
// MARK: - HorizontalStackView methods
//========================================================
extension StepsIndicatorView {
}