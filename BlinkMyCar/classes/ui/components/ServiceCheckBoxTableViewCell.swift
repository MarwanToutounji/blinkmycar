//
//  ServiceCheckBoxTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/2/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout
import M13Checkbox

protocol ServiceCheckBoxTableViewCellDelegate {
  func serviceCheckBoxCellDidTapInfoIcon(cell: ServiceCheckBoxTableViewCell)
}

public class ServiceCheckBoxTableViewCell: UITableViewCell {
  let infoButtonWidthHeightValue: CGFloat = 30
  
  public static var textSelectedColor = UIColor.applicationSecondaryColor()
  public static var textUnselectedColor = UIColor.darkGray()
  public static var textDisabledColor = UIColor.lightGray()
  
  @IBOutlet var serviceNameLabel: BMCServiceTitleLabel!
  @IBOutlet var checkbox: M13Checkbox!
  @IBOutlet weak var infoButton: BMCInfoIconButton!
  
  @IBOutlet weak var infoButtonWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var labelsContainerLeadingToSuperviewLeading: NSLayoutConstraint!
  @IBOutlet weak var checkBoxWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var checkBoxTrailingToSuperviewTrailing: NSLayoutConstraint!
  @IBOutlet weak var labelsContainerTopToSuperviewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var labelsContainerBottomToSuperviewBottomConstraint: NSLayoutConstraint!
  
  private let checkBoxWidthForMainService: CGFloat = 30
  private let checkBoxWidthForAddOnService: CGFloat = 20
  private let checkBoxTrailingToSuperviewConstant: CGFloat = 30
  private let labelContainerVerticalSpacingValueToSuperview: CGFloat = 18
  
  var isAddOn: Bool = false {
    didSet {
      if isAddOn {
        labelsContainerLeadingToSuperviewLeading.constant = 40
        checkBoxWidthConstraint.constant = checkBoxWidthForAddOnService
        checkBoxTrailingToSuperviewTrailing.constant = -(checkBoxTrailingToSuperviewConstant + (checkBoxWidthForMainService - checkBoxWidthForAddOnService)/2)
        labelsContainerTopToSuperviewTopConstraint.constant = labelContainerVerticalSpacingValueToSuperview - 10
        labelsContainerBottomToSuperviewBottomConstraint.constant = labelContainerVerticalSpacingValueToSuperview - 10
      } else {
        labelsContainerLeadingToSuperviewLeading.constant = 30
        checkBoxWidthConstraint.constant = checkBoxWidthForMainService
        checkBoxTrailingToSuperviewTrailing.constant = -checkBoxTrailingToSuperviewConstant
        labelsContainerTopToSuperviewTopConstraint.constant = labelContainerVerticalSpacingValueToSuperview
        labelsContainerBottomToSuperviewBottomConstraint.constant = labelContainerVerticalSpacingValueToSuperview
      }
      self.layoutIfNeeded()
    }
  }
  
  var serviceAvailability: ServiceAvailability = .inStock {
    didSet {
      self.layoutIfNeeded()
    }
  }
  
  public override func layoutSubviews() {
    super.layoutSubviews()
    
    switch serviceAvailability {
    case .inStock:
      checkbox.hidden = false
      serviceNameLabel.textColor = ServiceCheckBoxTableViewCell.textUnselectedColor
      infoButton.setTitleColor(ServiceCheckBoxTableViewCell.textUnselectedColor, forState: .Normal)
    case .outOfStock:
      checkbox.hidden = true
      serviceNameLabel.textColor = ServiceCheckBoxTableViewCell.textDisabledColor
      infoButton.setTitleColor(ServiceCheckBoxTableViewCell.textUnselectedColor, forState: .Normal)
    }
  }
  
  var delegate: ServiceCheckBoxTableViewCellDelegate?
  
  public class var nib: UINib {
    return  UINib(nibName: "ServiceCheckBoxTableViewCell" , bundle: nil)
  }
  public static let identifier: String = "ServiceCheckBoxTableViewCell"
  public static let height: CGFloat = 70

  override public func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    self.selectionStyle = .None
    
    checkbox.tintColor = UIColor.applicationMainColor()
    checkbox.markType = .Checkmark
    checkbox.secondaryTintColor = UIColor.darkGray()
    checkbox.secondaryCheckmarkTintColor = UIColor.darkGray()
    checkbox.boxType = .Circle
    checkbox.checkmarkLineWidth = 3.0
    checkbox.hideBox = false
    checkbox.enabled = false
    
    hideInfoButton()
  }
  
  override public func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    setCellStyleForSelection(selected, animated: animated)
  }
  
  public func setCellStyleForSelection(selected: Bool, animated: Bool) {
    // Configure the view for the selected state
    if serviceAvailability == .inStock {
      switch selected {
      case true:
        checkbox.setCheckState(.Checked, animated: animated)
        serviceNameLabel.textColor = ServiceCheckBoxTableViewCell.textSelectedColor
        infoButton.setTitleColor(ServiceCheckBoxTableViewCell.textUnselectedColor, forState: .Normal)
      case false:
        checkbox.setCheckState(.Unchecked, animated: animated)
        serviceNameLabel.textColor = ServiceCheckBoxTableViewCell.textUnselectedColor
        infoButton.setTitleColor(ServiceCheckBoxTableViewCell.textUnselectedColor, forState: .Normal)
        
      }
    }
  }
  
  var hasDetails: Bool = false {
    didSet {
      if hasDetails {
        showInfoButton()
      } else {
        hideInfoButton()
      }
    }
  }
  
  //============================================================
  // MARK: - ACTIONS
  //============================================================
  
  @IBAction func didTapInfoButton(sender: AnyObject) {
    delegate?.serviceCheckBoxCellDidTapInfoIcon(self)
  }
  
  //============================================================
  // MARK: - HELPERS
  //============================================================
  
  func hideInfoButton() {
    if infoButtonWidthConstraint != nil {
      infoButtonWidthConstraint.constant = 0
      layoutIfNeeded()
    }
  }
  
  func showInfoButton() {
    if infoButtonWidthConstraint != nil {
      infoButtonWidthConstraint.constant = infoButtonWidthHeightValue
      layoutIfNeeded()
    }
  }
}
