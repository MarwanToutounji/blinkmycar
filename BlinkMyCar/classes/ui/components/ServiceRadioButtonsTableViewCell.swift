//
//  ServiceRadioButtonsTableViewCell.swift
//  BlinkMyCar
//
//  Created by Marwan  on 6/2/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import UIKit
import FLKAutoLayout

public protocol ServiceRadioButtonsTableViewCellDelegate {
  func radioButtonCellDidSelectService(
    cell: ServiceRadioButtonsTableViewCell,
    selectedService: Service?, unselectedService: Service?)
}

public class ServiceRadioButtonsTableViewCell: UITableViewCell {
  private var servicesRadioButtonsView: ServicesRadioButtonsView!
  var services: [Service] = [Service]() {
    didSet {
      var radioProperties = [RadioButtonProperties]()
      for service in services {
        radioProperties.append((title: service.radioDisplayName,
          subtitle: service.price.displayAsUsDollarsPrice, item: service))
      }
      radioButtonProperties = radioProperties
      
      // To be checked later where to populate the radio buttons
      servicesRadioButtonsView.services = radioButtonProperties
    }
  }
  private var radioButtonProperties = [RadioButtonProperties]()
  
  
  var delegate: ServiceRadioButtonsTableViewCellDelegate?
  
  
  public class var nib: UINib {
    return  UINib(nibName: "ServiceRadioButtonsTableViewCell" , bundle: nil)
  }
  public static let identifier: String = "ServiceRadioButtonsTableViewCell"
  public static let height: CGFloat = 142
  
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    
    self.selectionStyle = UITableViewCellSelectionStyle.None
    
    servicesRadioButtonsView = ServicesRadioButtonsView(frame: CGRectZero)
    servicesRadioButtonsView.backgroundColor = UIColor.whiteColor()
    servicesRadioButtonsView.delegate = self
    
    self.contentView.addSubview(servicesRadioButtonsView)
    servicesRadioButtonsView.alignTop(
      "20", leading: "0", bottom: "-20",
      trailing: "-0", toView: self.contentView)
  }
  
  override public func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func selectService(service: Service?) {
    if let service = service {
      servicesRadioButtonsView.selectedRadioForService(service)
    }
  }
}

extension ServiceRadioButtonsTableViewCell: ServicesRadioButtonsViewDelegate {
  public func radioButtonSelected(selectedService: Service?, unselectedService: Service?) {
    self.delegate?.radioButtonCellDidSelectService(
      self,
      selectedService: selectedService,
      unselectedService: unselectedService)
  }
}
