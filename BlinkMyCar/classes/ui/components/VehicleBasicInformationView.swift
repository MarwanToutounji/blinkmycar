//
//  VehicleBasicInformationView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import SDWebImage

protocol VehicleBasicInformationViewDelegate {
  func vehicleBasicInfoDidSelectView(view: VehicleBasicInformationView)
}

class VehicleBasicInformationView: UIView {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var label: UILabel!
  @IBOutlet weak var tapGestureRecognizer: UITapGestureRecognizer!


  @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
  
  var selectionBackgroundColor: UIColor = UIColor.applicationSecondaryColor()
  var selectionForegroundColor: UIColor = UIColor.whiteColor()
  var normalBackgroundColor: UIColor = UIColor.clearColor()
  var normalForegroundColor: UIColor = UIColor.gray()

  var isSelected: Bool = false
  var isCompact: Bool = false
  
  var vehicle: Vehicle? {
    didSet {
      setupViewWithVehicle()
    }
  }

  var delegate: VehicleBasicInformationViewDelegate?

  override func awakeAfterUsingCoder(aDecoder: NSCoder) -> AnyObject? {
    return afterUsingCoderLoadNibIfNeeded("VehicleBasicInformationView")
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    imageView.cicularView()
    setupViewWithVehicle()
    selectedState = isSelected
  }

  func compactView(compact: Bool) {
   self.constrainHeight(compact ? "80" : "120")
    if compact {
      imageHeightConstraint.constant = 60
      imageWidthConstraint.constant = 60
      (label as! BMCCarMenuLabel).compactSize()
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    imageView.cicularView()
  }
  
  private func setupViewWithVehicle() {
    if vehicle != nil && imageView != nil && label != nil {
      label.text = vehicle?.name
      let placeholderImageName = vehicle?.type == Make.VehicleType.Car ? "iconCar" : "iconMotorcycle"
      imageView.sd_setImageWithURL(
        APIProvider.mediaURLForPath(vehicle!.imagePath!),
        placeholderImage: UIImage(named: placeholderImageName),
        completed: { (image: UIImage!, error: NSError!, cash: SDImageCacheType, url: NSURL!) -> Void in
          if image != nil && error == nil {
            self.imageView.backgroundColor = UIColor.whiteColor()
          }
        }
      )
    }
  }

  @IBAction func didTapOnView(sender: AnyObject?) {
    delegate?.vehicleBasicInfoDidSelectView(self)
  }

  var selectedState: Bool {
    get {
      return isSelected
    }

    set {
      isSelected = newValue
      if isSelected {
        backgroundColor = selectionBackgroundColor
        label.textColor = selectionForegroundColor
      } else {
        backgroundColor = normalBackgroundColor
        label.textColor = normalForegroundColor
      }
    }
  }
  
  var cancelsTouches: Bool {
    get {
      return tapGestureRecognizer.cancelsTouchesInView
    }
    
    set {
      tapGestureRecognizer.cancelsTouchesInView = newValue
    }
  }
}
