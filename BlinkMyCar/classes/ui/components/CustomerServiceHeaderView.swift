//
//  CustomerServiceHeaderView.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/28/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

protocol CustomerServiceHeaderViewDelegate {
  func didTapCallButton()
}

public class CustomerServiceHeaderView: UIView {
  public static let nibName = "CustomerServiceHeaderView"
  
  @IBOutlet weak var labelPhoneNumber: BMCSecondaryTitleLabel!
  @IBOutlet weak var labelCallingPeriod: BMCServiceLabel!
  @IBOutlet weak var labelGiveUsACall: BMCServiceLabel!
  @IBOutlet weak var buttonCall: BMCGreenButton!
  
  var delegate: CustomerServiceHeaderViewDelegate?
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    self.applyTheme()
  }
  
  private func applyTheme() {
      self.backgroundColor = UIColor.veryLightGray()
  }
}

//MARK: ACTIONS
extension CustomerServiceHeaderView {
  @IBAction func didTapCallButton(sender: UIButton) {
    self.delegate?.didTapCallButton()
  }
}
