//
//  HorizontalStackView.swift
//  BlinkMyCar
//
//  Created by Marwan on 3/15/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import FLKAutoLayout

public class HorizontalStackView: UIView {
  public var topPadding: CGFloat {
    return 0
  }
  
  public var bottomPadding: CGFloat {
    return 0
  }
  
  public var addSeparator: Bool = true
  
  public var numberOfStackedViews: Int = 0 {
    didSet {
      refreshLayout()
    }
  }
  
  private func refreshLayout() {
    clearSubviews()
    addSubviews()
    self.layoutIfNeeded()
  }
  
  private func addSubviews() {
    for index in 0..<numberOfStackedViews {
      let subview = subView(index)
      self.addSubview(subview)
    }
    
    let widthMultiplier: Float = Float(1.0/Float(subviews.count))
    
    for (index, subview) in subviews.enumerate() {
      subview.alignTopEdgeWithView(self, predicate: "\(topPadding)")
      subview.constrainWidthToView(self, predicate: "==*\(widthMultiplier)")
      if index == 0 {
        self.alignBottomEdgeWithView(subview, predicate: "==\(bottomPadding)")
      }
      
      // Add Leading constraint
      if index == 0 {
        subview.alignLeadingEdgeWithView(self, predicate: "0")
      } else {
        if addSeparator {
          let separator = separatorViewTuple()
          self.addSubview(separator.separatorView)
          separator.separatorView.constrainWidth("\(separator.viewWidth)")
          separator.separatorView.alignTopEdgeWithView(self, predicate: "\(topPadding)")
          separator.separatorView.alignBottomEdgeWithView(self, predicate: "\(bottomPadding)")
          separator.separatorView.constrainLeadingSpaceToView(subviews[index-1], predicate: "-\(separator.viewWidth/2)")
        }
        subview.constrainLeadingSpaceToView(subviews[index-1], predicate: "0")
      }
    }
    
    // This view takes the Height of its first subview
    didFinishAddingSubviews()
  }
  
  public func didFinishAddingSubviews() {}
  
  func subView(index: Int) -> UIView! {
    return nil
  }
  
  func separatorViewTuple() -> (separatorView: UIView, viewWidth: Float)! {
    return nil
  }
}
