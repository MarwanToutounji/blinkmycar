//
//  ImageTitleView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/19/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

class ImageTitleView: UIView {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var label: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override func awakeAfterUsingCoder(aDecoder: NSCoder) -> AnyObject? {
    let isJustAPlaceholder = self.subviews.count == 0
    if isJustAPlaceholder {
      let view = NSBundle.mainBundle().loadNibNamed("ImageTitleView",
        owner: nil, options: nil)[0] as! ImageTitleView
      view.autoresizingMask = self.autoresizingMask
      view.translatesAutoresizingMaskIntoConstraints = self.translatesAutoresizingMaskIntoConstraints

      for constraint in self.constraints {
        var firstItem = constraint.firstItem as? NSObject
        if firstItem == self {
          firstItem = view
        }

        var secondItem = constraint.secondItem as? NSObject
        if secondItem == self {
          secondItem = view
        }

        let newConstraint = NSLayoutConstraint(
          item: firstItem!,
          attribute: constraint.firstAttribute,
          relatedBy: constraint.relation,
          toItem: secondItem,
          attribute: constraint.secondAttribute,
          multiplier: constraint.multiplier,
          constant: constraint.constant)

        view.addConstraint(newConstraint)
      }
      
      self.removeConstraints(self.constraints)
      
      return view
    }
    return self
  }

  var text: String? {
    get {
      return label.text
    }
    set {
      label.text = newValue
    }
  }

  var image: UIImage? {
    get {
      return imageView.image
    }
    set {
      imageView.image = newValue
    }
  }
}