//
//  StepsHeaderView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 8/20/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

class StepsHeaderView: UIView {
  @IBOutlet weak var descriptionLabel: UILabel!

  class var nibName: String {
    return "StepsHeaderView"
  }

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  var descriptionText: String? {
    get {
      return descriptionLabel.text
    }
    set {
      descriptionLabel.text = newValue
    }
  }
}
