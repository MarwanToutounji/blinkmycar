//
//  CreateRideMakeModelCollectionViewSectionHeaderCell.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class CreateRideMakeModelCollectionViewSectionHeaderCell: UICollectionReusableView {
  @IBOutlet weak var titleLabel: BMCLabel!
  public static let reuseIdentifier = "CreateRideMakeModelCollectionViewSectionHeaderCell"
  public static let cellHeight:CGFloat = 24

  public override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = UIColor.purple()
  }
}
