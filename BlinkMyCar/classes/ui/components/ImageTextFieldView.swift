//
//  VehiculeProfileHeaderView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/19/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

protocol ImageTextFieldViewImageDelegate {
  func headerViewDidTapImage()
}

enum ImageTextFieldViewNotification: String {
  case FinishEditing = "ImageTextFieldViewNotificationFinishEditing"
}

class ImageTextFieldView: UIView {

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var textField: UITextField!

  var imageDelegate: ImageTextFieldViewImageDelegate?
  var tintImage: Bool = false
  
  override func awakeAfterUsingCoder(aDecoder: NSCoder) -> AnyObject? {
    return afterUsingCoderLoadNibIfNeeded("ImageTextFieldView")
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    applyCustomStyle()
    applyTheme()
    
    self.userInteractionEnabled = true;
    imageView.userInteractionEnabled = true;

    textField.delegate = self
  }

  // MARK: -  U I
  //--------------------------
  func applyCustomStyle() {
    imageView.cicularView()
  }

  func applyTheme() {
    // TODO: Customize the view to match the app theme here
  }

  // MARK: -  A P I 
  //--------------------------

  var image: UIImage? {
    get {
      return imageView.image
    }

    set {
      if tintImage {
        //Set Image and apply Tint color to white
        imageView.image = newValue?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imageView.tintColor = UIColor.whiteColor()
      }else{
        imageView.image = newValue
      }
    }
  }

  var text: String? {
    get {
      return textField.text
    }
    set {
      textField.text = newValue
    }
  }

  var textPlaceholder: String? {
    didSet {
      textField.placeholder = textPlaceholder
    }
  }

  // MARK: -  A C T I O N S
  //--------------------------
  @IBAction func didTapOnImageView(sender: AnyObject) {
    imageDelegate?.headerViewDidTapImage()
  }
}

extension ImageTextFieldView: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }

  func textFieldDidEndEditing(textField: UITextField) {
    textField.resignFirstResponder()
    NSNotificationCenter.defaultCenter().postNotificationName(
      ImageTextFieldViewNotification.FinishEditing.rawValue,
      object: textField.text)
  }
}
