//
//  BMCBarButtonItem.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {
  class func applyTheme() {
    UIBarButtonItem.appearance().setTitleTextAttributes(
      [NSFontAttributeName : UIFont.boldBMCFont(size: 15)],
      forState: UIControlState.Normal)
  }
}