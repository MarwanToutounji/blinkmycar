//
//  BMCTextField.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 6/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class BMCTextField: UITextField {
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override public init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  override public func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }

  func setup() {
    //TODO: Customize the button's layout based on the designs
    self.backgroundColor = UIColor.lightWhite()
    self.borderStyle = UITextBorderStyle.None
    self.font = UIFont.lightBMCFont(size: 20)
    self.textColor = UIColor.darkGray()
    super.makeCurvedEdges(5)
  }

  public override func textRectForBounds(bounds: CGRect) -> CGRect {
    return CGRectInset(bounds, 10, 10)
  }

  public override func editingRectForBounds(bounds: CGRect) -> CGRect {
    return CGRectInset(bounds, 10, 10)
  }

  public override func layoutSubviews() {
    super.layoutSubviews()
    if let rightView = self.rightView {
      rightView.frame.size = CGSizeMake(frame.size.height, frame.size.height)
    }
  }

  func enable(enabled: Bool) {
    self.enabled = enabled
    if enabled {
      self.backgroundColor = UIColor.lightWhite()
      hideBorder()
    }
    else {
      self.backgroundColor = UIColor.whiteColor()
      border(UIColor.veryLightGray())
    }
  }
  
  func border(color: UIColor) {
    self.layer.borderColor = color.CGColor
    self.layer.borderWidth = 1.0
  }
  
  func hideBorder() {
    self.borderStyle = UITextBorderStyle.None
  }
}


class BMCTextFieldGeneral: BMCTextField {
  override func setup() {

  }
}

public class BMCSmallTextField: BMCTextField {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 17)
  }
}

public class BMCTextFieldWithEditIcon: BMCTextField {
  override func setup() {
    super.setup()

    self.borderStyle = .None

    let searchIcon: UIImageView = UIImageView(image: UIImage(named: "editIcon"))
    self.leftViewMode = UITextFieldViewMode.Always
    self.leftView = UIView(frame: CGRectMake(0.0, 0.0, self.frame.size.height, self.frame.size.height))
    self.leftView?.addSubview(searchIcon)
    self.leftView?.backgroundColor = UIColor.orange()

    if self.leftView?.gestureRecognizers?.count <= 0 {
      self.leftView?.addGestureRecognizer(tapGestureRecognizer)
    }

    searchIcon.alignTop("2", leading: "2", bottom: "-2", trailing: "-2", toView: self.leftView!)

    adjustsFontSizeToFitWidth = false
  }

  var tapGestureRecognizer: UITapGestureRecognizer {
    return UITapGestureRecognizer(target: self, action: #selector(BMCTextFieldWithEditIcon.tapEditIcon))
  }

  func tapEditIcon() {
    self.becomeFirstResponder()
  }

  public override func textRectForBounds(bounds: CGRect) -> CGRect {
    if leftView != nil {
      let x = bounds.origin.x + (leftView?.frame.size.width ?? 0) + 5
      return CGRectMake(x, bounds.origin.y, bounds.size.width - x - 5, bounds.size.height)
    }else {
      return super.textRectForBounds(bounds)
    }
  }

  public override func editingRectForBounds(bounds: CGRect) -> CGRect {
    if leftView != nil {
      let x = bounds.origin.x + (leftView?.frame.size.width ?? 0) + 5
      return CGRectMake(x, bounds.origin.y, bounds.size.width - x - 20, bounds.size.height)
    }else {
      return super.editingRectForBounds(bounds)
    }
  }

  public func hideStyle() {
    self.enabled = false
    self.leftView = nil
    self.backgroundColor = UIColor.clearColor()
    self.textColor = UIColor.whiteColor()
    self.font = UIFont.boldBMCFont(size: 24)
    self.textAlignment = .Center
  }
  
  public func showStyle() {
    self.enabled = true
    self.textAlignment = .Left
    self.textColor = UIColor.darkGray()
    setup()
  }
}

public class BMCSearchTextField: BMCTextField {
  override func setup() {
    self.textColor = UIColor.darkGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 15)

    //add search right icon
    let searchIcon: UIImageView = UIImageView(image: UIImage(named: "searchIcon"))
    self.rightViewMode = UITextFieldViewMode.Always
    self.rightView = UIView(frame: CGRectMake(0.0, 0.0, self.frame.size.height, self.frame.size.height))
    self.rightView?.addSubview(searchIcon)
    self.rightView?.backgroundColor = UIColor.purple()

    searchIcon.alignTop("13", leading: "13", bottom: "-13", trailing: "-13", toView: self.rightView!)

    //Apply Corners on the Right, since adding rightView removed them
    let rect: CGRect = self.bounds;
    let cornerRadius: CGFloat = 4.0 //TODO: why is this not working: self.layer.cornerRadius it defaults to Zero
    let path: UIBezierPath = UIBezierPath( roundedRect: rect,
      byRoundingCorners:[UIRectCorner.BottomRight, UIRectCorner.TopRight],
      cornerRadii:CGSizeMake(cornerRadius, cornerRadius));
    let layers: CAShapeLayer = CAShapeLayer(layer: self)
    layers.frame = rect
    layers.path = path.CGPath
    self.layer.mask = layers

    adjustsFontSizeToFitWidth = false
  }

  func hideSearchIcon() {
    self.rightView = UIView(frame: CGRectZero)
  }

  func showSearchIcon() {
    //add search right icon
    let searchIcon: UIImageView = UIImageView(image: UIImage(named: "searchIcon"))
    self.rightViewMode = UITextFieldViewMode.Always
    self.rightView = UIView(frame: CGRectMake(0.0, 0.0, self.frame.size.height, self.frame.size.height))
    self.rightView?.addSubview(searchIcon)
    self.rightView?.backgroundColor = UIColor.purple()
    searchIcon.alignTop("13", leading: "13", bottom: "-13", trailing: "-13", toView: self.rightView!)
  }

  public override func textRectForBounds(bounds: CGRect) -> CGRect {
    let rightPadding = rightView!.frame.size.width + 5
    return CGRectMake(bounds.origin.x + 5, bounds.origin.y, bounds.size.width - rightPadding, bounds.size.height)
  }

  public override func editingRectForBounds(bounds: CGRect) -> CGRect {
    return textRectForBounds(bounds)
  }
}

public class BMCTextFieldWithDropDownIcon: BMCAddressTextField {
  override func setup() {
    super.setup()
    
    let dropDownIcon: UIImageView = UIImageView(image: UIImage(named: "dropDownIcon"))
    self.rightViewMode = UITextFieldViewMode.Always
    self.rightView = UIView(frame: CGRectMake(0.0, 0.0, self.frame.size.height, self.frame.size.height))
    self.rightView?.addSubview(dropDownIcon)
    self.rightView?.backgroundColor = UIColor.applicationMainColor()
    
    dropDownIcon.alignTop("16", leading: "13", bottom: "-16", trailing: "-13", toView: self.rightView!)
    
    adjustsFontSizeToFitWidth = false
  }
}

public class BMCAddressTextField: BMCTextField {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 15)
    self.textColor = UIColor.darkGray()
    self.backgroundColor = UIColor.lightWhite()
  }
  
  func styledPlaceholder(placeholder: String) {
    let attributesDictionary = [NSForegroundColorAttributeName: UIColor.lightGray()]
    self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: attributesDictionary)
  }
}

public class BMCMultiStateTextField: BMCTextField {
  override func setup() {
    self.font = UIFont.lightBMCFont(size: 15)
    self.textColor = UIColor.darkGray()
    self.makeRounded()
    
    enable(self.enabled)
  }
  
  func styledPlaceholder(placeholder: String) {
    let attributesDictionary = [NSForegroundColorAttributeName: UIColor.lightGray()]
    self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: attributesDictionary)
  }
}
