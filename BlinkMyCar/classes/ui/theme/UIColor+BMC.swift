//
//  UIColor+BMC.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/8/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

extension UIColor {

  class func applicationMainColor() -> UIColor {
    return purpleShade()
  }

  class func applicationSecondaryColor() -> UIColor {
    return purple()
  }

  //App Colors
  class func purpleShade() -> UIColor {
    return UIColor(red:67/255.0, green:40/255.0, blue:239/255.0, alpha:255/255.0)
  }

  class func lightWhite() -> UIColor {
    return UIColor(red:239/255.0, green:239/255.0, blue:239/255.0, alpha:255/255.0)
  }

  class func gray() -> UIColor {
    return UIColor(red: 115/255.0, green: 115/255.0, blue: 115/255.0, alpha: 255/255.0)
  }

  class func lightGray() -> UIColor {
    return UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 255/255.0)
  }

  class func lighterGray() -> UIColor {
    return UIColor(red: 191/255.0, green: 191/255.0, blue: 191/255.0, alpha: 255/255.0)
  }
  
  class func lighterMidGray() -> UIColor {
    return UIColor(red: 224/255.0, green: 224/255.0, blue: 224/255.0, alpha: 255/255.0)
  }
  
  class func veryLightGray() -> UIColor {
    return UIColor(red: 239/255.0, green: 239/255.0, blue: 239/255.0, alpha: 255/255.0)
  }

  class func orange() -> UIColor {
    return UIColor(red: 255/255.0, green: 110/255.0, blue: 0/255.0, alpha: 255/255.0)
  }

  class func lightOrange() -> UIColor {
    return UIColor(red: 255/255.0, green: 197/255.0, blue: 153/255.0, alpha: 255/255.0)
  }

  class func purple() -> UIColor {
    return UIColor(red: 73/255.0, green:  40/255.0, blue: 244/255.0, alpha: 255/255.0)
  }

  class func darkPurple() -> UIColor {
    return UIColor(red: 0.176, green: 0.000, blue: 0.776, alpha: 1.000)
  }

  class func darkGray() -> UIColor {
    return UIColor(red: 76/255.0, green:  76/255.0, blue: 76/255.0, alpha: 255/255.0)
  }

  class func bmcGreen() -> UIColor {
    return UIColor(red:0.239, green:0.851, blue:0.294, alpha:1.000)
  }

  class func bmcRed() -> UIColor {
    return UIColor(red: 255/255.0, green: 61/255.0, blue: 45/255.0, alpha: 255/255.0)
  }

  class func textFieldTextColor() -> UIColor {
    return UIColor.darkGray()
  }

  class func textFieldPlaceHolderTextColor() -> UIColor {
    return UIColor.lighterGray()
  }
}
