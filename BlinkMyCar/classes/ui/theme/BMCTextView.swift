//
//  BMCTextView.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/28/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit


protocol TextViewWithPlaceholderDelegate: NSObjectProtocol {
  func textViewShouldBeginEditing(textView: UITextView) -> Bool
  func textViewDidBeginEditing(textView: UITextView)
  func textViewDidEndEditing(textView: UITextView)
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
  func textViewShouldReturn(textField: UITextField) -> Bool
}

public class BMCTextView: UITextView {
  var textFieldTextColor: UIColor = UIColor.textFieldTextColor()
  var placeholderColor: UIColor = UIColor.textFieldPlaceHolderTextColor()
  var placeholderText: String = "" {
    didSet {
      if text.isEmpty && !placeholderText.isEmpty {
        textColor = placeholderColor
        text = placeholderText
      }
    }
  }
  override public var text: String! {
    didSet {
      if !text.isEmpty && text == placeholderText {
        textColor = placeholderColor
      }
      else if text.isEmpty && oldValue != placeholderText {
        //WARNING: This line will call-> placeholderText which in turn will again call/set (here) text
        text = placeholderText
      }
      else {
        textColor = textFieldTextColor
      }
    }
  }

  var delegateForTextViewWithPlaceholder: TextViewWithPlaceholderDelegate?

  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override public init(frame: CGRect, textContainer: NSTextContainer?) {
    super.init(frame: frame, textContainer: textContainer)
    setup()

  }
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }
  
  func setup() {
    self.delegate = self

    //TODO: Customize the textview's layout based on the designs
    self.backgroundColor = UIColor.lightWhite()
    self.layer.borderWidth = 0.0
    self.font = UIFont.lightBMCFont(size: 15)
    self.textContainerInset = UIEdgeInsetsMake(10,10,10,10);
    makeCurvedEdges(5)

    self.placeholderColor = UIColor.textFieldPlaceHolderTextColor()
    self.placeholderText = ""
  }

  private func isDisplayingPlaceholder() -> Bool {
    return self.textColor == self.placeholderColor
  }

  func enable(enabled: Bool) {
    self.editable = enabled
    if enabled {
      self.backgroundColor = UIColor.lightWhite()
      hideBorder()
    }
    else {
      self.backgroundColor = UIColor.whiteColor()
      addBorder(color: UIColor.veryLightGray())
    }
  }

  func hideBorder() {
    self.layer.borderColor = backgroundColor?.CGColor
    self.layer.borderWidth = 0.0
  }

  func valueForText() -> String {
    return (text == placeholderText) ? "" : text
  }
}

// TODO: Obsolete
public class BMCAddressTextView: BMCTextView {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 15)
    self.textColor = textFieldTextColor
    self.backgroundColor = UIColor.lightWhite()
  }
}

public class BMCAdditionalInfoTextView: BMCTextView {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 15)
    self.textColor = textFieldTextColor
    self.backgroundColor = UIColor.whiteColor()
  }
}

extension BMCTextView: UITextViewDelegate {

  public func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    return delegateForTextViewWithPlaceholder?.textViewShouldBeginEditing(textView) ?? false
  }

  public func textViewDidBeginEditing(textView: UITextView) {
    if isDisplayingPlaceholder() {
      textView.text = nil
    }
    delegateForTextViewWithPlaceholder?.textViewDidBeginEditing(textView)
  }

  public func textViewDidEndEditing(textView: UITextView) {
    if textView.text.isEmpty {
      textView.text = self.placeholderText
      textView.textColor = self.placeholderColor
    }
    delegateForTextViewWithPlaceholder?.textViewDidEndEditing(textView)
  }

  public func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    if textView.textColor != textFieldTextColor {
      textView.textColor = textFieldTextColor
    }
    return delegateForTextViewWithPlaceholder?.textView(textView, shouldChangeTextInRange: range, replacementText: text) ?? false
  }

  func textViewShouldReturn(textField: UITextField) -> Bool {
    return delegateForTextViewWithPlaceholder?.textViewShouldReturn(textField) ?? false
  }
}
