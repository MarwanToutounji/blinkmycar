//
//  UIFont+BMC.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 6/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome

extension UIFont {

  class func thinBMCFont(size size: CGFloat) -> UIFont! {
    return UIFont(name: "Campton-Thin", size: size)
  }

  class func boldBMCFont(size size: CGFloat) -> UIFont! {
    return UIFont(name: "Campton-Bold", size: size)
  }

  class func lightBMCFont(size size: CGFloat) -> UIFont! {
    return UIFont(name: "Campton-Light", size: size)
  }

  class func awesomeBMCFont(size size: CGFloat) -> UIFont! {
    return UIFont(awesomeFontOfSize: size)
  }
}
