//
//  BMCView.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

public class BMCView: UIView {
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override public init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  override public func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }

  func setup() {
    //TODO: Customize the button's layout based on the designs
  }
}


public class BMCLineSeparatorView: BMCView {
  public override func setup() {
    backgroundColor = UIColor.lighterGray()
  }
}

public class BMCLightBackgroundView: BMCView {
  public override func setup() {
    backgroundColor = UIColor.lightWhite()
  }
}

public class BMCBackgroundView: BMCView {
  public override func setup() {
    backgroundColor = UIColor.applicationMainColor()
  }
}