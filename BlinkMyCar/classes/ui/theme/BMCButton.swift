//
//  BMCButton.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 6/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome

public class BMCButton: UIButton {

  public override func layoutSubviews() {
    super.layoutSubviews()
    setup()
  }

  func setup() {
    //TODO: Customize the button's layout based on the designs
    self.titleLabel?.minimumScaleFactor = 0.5;
    self.titleLabel?.adjustsFontSizeToFitWidth = true;
  }
}

public class BMCRadioButton: BMCButton {
  override func setup() {
    makeRounded()
  }
}

class BMCWhiteButton: BMCButton {

  override func setup() {
    super.setup()
    
    //set Background color
    self.backgroundColor = UIColor.whiteColor()
    //set text color
    self.setTitleColor(UIColor.purpleShade(), forState: .Normal)
    //TODO: add disabled and highlighted

    //Set Font
    self.titleLabel!.font = UIFont.lightBMCFont(size: 22)

    self.makeRounded()
  }
  
}

class BMCBlueWhiteBorderButton: BMCButton {

  override func setup() {
    super.setup()
    //set Background color
    self.backgroundColor = UIColor.purpleShade()
    //set text color
    self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    //TODO: add disabled and highlighted

    //Set Font
    self.titleLabel!.font = UIFont.lightBMCFont(size: 22)

    self.makeRounded()
    self.addBorder(color: UIColor.whiteColor())
  }

}

public class BMCMapTranslucentButton: BMCButton {
  public var textColor: UIColor?
  
  override func setup() {
    super.setup()
    
    self.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.6)
  }
  
  override public func setTitle(title: String?, forState state: UIControlState) {
    if let title = title {
      textColor = UIColor.applicationSecondaryColor()
      self.setAttributedTitle(attributedString(title), forState: state)
    }
  }
  
  private func attributedString(text: String) -> NSMutableAttributedString {
    return NSMutableAttributedString(string: text,
      attributes: [
        NSUnderlineStyleAttributeName: NSNumber(integer:NSUnderlineStyle.StyleSingle.rawValue),
        NSForegroundColorAttributeName: textColor ?? UIColor.orange(),
        NSFontAttributeName: UIFont.lightBMCFont(size: 16)
      ])
  }
}

class BMCOrangeButton: BMCButton {
  let disabledColor: UIColor = UIColor.lightOrange()
  let enabledColor: UIColor = UIColor.orange()
  let disabledTextColor: UIColor = UIColor.whiteColor()

  override func setup() {
    super.setup()
    
    //set Background color
    self.backgroundColor = enabled ? enabledColor : disabledColor
    //set text color
    self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    //TODO: add disabled
    self.setTitleColor(UIColor(white: 170/255, alpha: 0.7), forState: UIControlState.Highlighted)
    self.setTitleColor(disabledTextColor, forState: UIControlState.Disabled)

    //Set Font
    self.titleLabel!.font = UIFont.lightBMCFont(size: 22)

    self.makeRounded()
    self.addBorder(color: UIColor.whiteColor())
  }

  func enable(enable: Bool) {
    self.enabled = enable
    self.backgroundColor = enabled ? enabledColor : disabledColor
  }
}

public class BMCLinkButton: BMCButton {
  public var textColor: UIColor?
  public var fontSize: CGFloat?
  public var fontText: UIFont?

  override func setup() {
    super.setup()
    
    //set Background color
    self.backgroundColor = UIColor.clearColor()
  }

  override public func setTitle(title: String?, forState state: UIControlState) {
    if let title = title {
      self.setAttributedTitle(attributedString(title), forState: state)
    }
  }

  private func attributedString(text: String) -> NSMutableAttributedString {
    return NSMutableAttributedString(string: text,
      attributes: [
        NSUnderlineStyleAttributeName: NSNumber(integer:NSUnderlineStyle.StyleSingle.rawValue),
        NSForegroundColorAttributeName: textColor ?? UIColor.orange(),
        NSFontAttributeName: fontText ?? UIFont.lightBMCFont(size: fontSize ?? 16)
      ])
  }
}

public class BMCCallSpecialistButton: BMCButton {
  public var textColor: UIColor? = UIColor.bmcGreen()
  public var fontSize: CGFloat? = 16
  public var fontText: UIFont? = UIFont.lightBMCFont(size: 16)
  
  override func setup() {
    super.setup()
    
    //set Background color
    self.backgroundColor = UIColor.clearColor()
  }
  
  override public func setTitle(title: String?, forState state: UIControlState) {
    if let title = title {
      self.setAttributedTitle(attributedString(title), forState: state)
    }
  }
  
  private func attributedString(text: String) -> NSMutableAttributedString {
    let myString = NSMutableAttributedString(string: "")
    
    let phoneIconFAString = NSString.fontAwesomeIconStringForEnum(FAIcon.FAPhone)
    let symbolString = NSAttributedString(
      string: phoneIconFAString,
      attributes: [
        NSFontAttributeName: UIFont.awesomeBMCFont(size: (fontSize ?? 16) - 1),
        NSForegroundColorAttributeName: textColor ?? UIColor.bmcGreen(),
        NSUnderlineStyleAttributeName: NSNumber(integer:NSUnderlineStyle.StyleSingle.rawValue)
      ])
    myString.appendAttributedString(symbolString)
    
    // Add a space before the specialist Name
    let text = " " + text
    let specialistNameString = NSAttributedString(
      string: text,
      attributes: [
        NSUnderlineStyleAttributeName: NSNumber(integer:NSUnderlineStyle.StyleSingle.rawValue),
        NSForegroundColorAttributeName: textColor ?? UIColor.bmcGreen(),
        NSFontAttributeName: fontText ?? UIFont.lightBMCFont(size: fontSize ?? 16)
      ])
    
    myString.appendAttributedString(specialistNameString)
    
    return myString
  }
}

public class BMCImageLinkButton: BMCLinkButton {
  public var fontAwesomeIcon: FAIcon?

  override public func setTitle(title: String?, forState state: UIControlState) {
    if let title = title {
      let iconString = NSString.fontAwesomeIconStringForEnum(fontAwesomeIcon ?? FAIcon.FAMicrophone) + " "
      let icon = NSMutableAttributedString(string: iconString,
        attributes: [
          NSFontAttributeName: UIFont.awesomeBMCFont(size: 16),
          NSForegroundColorAttributeName: textColor ?? UIColor.applicationMainColor()
        ])
      let text = self.attributedString(title)
      icon.appendAttributedString(text)
      setAttributedTitle(icon, forState: UIControlState.Normal)
    }
  }
}

public class BMCInfoIconButton: BMCButton {
  public var fontAwesomeIcon: FAIcon = FAIcon.FAInfoCircle
  
  override func setup() {
    self.titleLabel?.font = UIFont.awesomeBMCFont(size: 20)
    setTitle(nil, forState: UIControlState.Normal)
  }
  
  public override func setTitle(title: String?, forState state: UIControlState) {
    let iconString = NSString.fontAwesomeIconStringForEnum(fontAwesomeIcon)
    super.setTitle(iconString, forState: UIControlState.Normal)
  }
}

class BMCMapButton: BMCButton {
  
  override func setup() {
    super.setup()
    
    //set text color
    self.setTitleColor(UIColor.applicationMainColor(), forState: .Normal)
    //Set Font
    self.titleLabel!.font = UIFont.lightBMCFont(size: 20)
    self.backgroundColor = UIColor(white: 170, alpha: 0.5)
  }
}

public class BMCGreenButton: BMCButton {
  override func setup() {
    super.setup()
    titleLabel?.font = UIFont.lightBMCFont(size: 20)
    setTitleColor(UIColor.whiteColor(), forState: .Normal)
    backgroundColor = UIColor.bmcGreen()
    makeRounded()
  }
}

public class BMCRedButton: BMCButton {
  override func setup() {
    super.setup()
    titleLabel?.font = UIFont.lightBMCFont(size: 20)
    setTitleColor(UIColor.whiteColor(), forState: .Normal)
    backgroundColor = UIColor.bmcRed()
    makeRounded()
  }
}

public class BMCEditButton: BMCButton {
  override func setup() {
    super.setup()
    titleLabel?.font = UIFont.lightBMCFont(size: 20)
    setTitleColor(UIColor.whiteColor(), forState: .Normal)
    backgroundColor = UIColor.darkPurple()

    self.layer.cornerRadius = 10
    self.clipsToBounds = true
  }
}

public class BMCEditIconButton: BMCButton {
  override func setup() {
    super.setup()
    setImage(UIImage(named: "pencilIcon"), forState: .Normal)
    backgroundColor = UIColor.clearColor()
  }
}

public class BMCCancelButton: BMCButton {
  override func setup() {
    super.setup()
    titleLabel?.font = UIFont.lightBMCFont(size: 20)
    setTitleColor(UIColor.whiteColor(), forState: .Normal)
    backgroundColor = UIColor.lightGray()

    self.layer.cornerRadius = 10
    self.clipsToBounds = true
  }
}

class BMCAudioRecordViewButton: BMCButton {
  override func setup() {
    super.setup()
    
    self.titleLabel?.font = UIFont.lightBMCFont(size: 13)
    self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
  }
}

class BMCTranseculantButton: BMCButton {
  override func setup() {
    super.setup()
    
    self.backgroundColor = UIColor.clearColor()
    self.addBorder(color: UIColor.whiteColor())
    self.titleLabel?.font = UIFont.lightBMCFont(size: 22)
    self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
  }
}

class BMCMapIndicatorButton: UIButton {
  //Note: BMCMapIndicatorButton extends UIButton and not BMCButton since BMCButton overrides layoutSubviews to call setup
  // which was causing problems for taps on button.
  var fontAwesomeIcon: FAIcon?
  var textColor: UIColor?
  var fontSize: CGFloat?
  var fontText: UIFont?

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  required override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }

  func setup() {
    //set Background color
    self.backgroundColor = UIColor.orange()
    //Need to set textColor before setting text
    self.makeRounded()
  }

  override func setTitle(title: String?, forState state: UIControlState) {
    if let title = title {
      let iconString = (fontAwesomeIcon != nil ) ? ("  " + NSString.fontAwesomeIconStringForEnum(fontAwesomeIcon!)) : ""
      let icon = NSMutableAttributedString(string: iconString,
        attributes: [
          NSFontAttributeName: UIFont.awesomeBMCFont(size: fontSize ?? 18),
          NSForegroundColorAttributeName: textColor ?? UIColor.applicationMainColor()
        ])
      let text = self.attributedString(title)
      text.appendAttributedString(icon)
      setAttributedTitle(text, forState: .Normal)
    }
  }

  private func attributedString(text: String) -> NSMutableAttributedString {
    return NSMutableAttributedString(string: text,
      attributes: [
        NSForegroundColorAttributeName: textColor ?? UIColor.applicationMainColor(),
        NSFontAttributeName: UIFont.lightBMCFont(size: 18)
      ])
  }

  func noBorder() {
    self.layer.borderWidth = 0
  }
}
