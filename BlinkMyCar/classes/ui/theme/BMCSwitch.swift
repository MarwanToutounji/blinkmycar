//
//  BMCSwitch.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/28/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class BMCSwitch: UISwitch {
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }
  
  func setup() {
    //TODO: Customize the switch's layout based on the designs
  }

}

public class BMCSettingsSwitch: BMCSwitch {

  override func setup() {
    super.setup()
    self.onTintColor = UIColor.applicationSecondaryColor()
    self.tintColor = UIColor.lighterGray()
  }

}