//
//  String+Style.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 7/2/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

extension String {

  public func underlinedAttributedString() -> NSMutableAttributedString {
    let underlinedString = NSMutableAttributedString(string: self)
    underlinedString.addAttribute(NSUnderlineStyleAttributeName,
      value: NSNumber(integer:NSUnderlineStyle.StyleSingle.rawValue),
      range: NSMakeRange(0, underlinedString.length))
    return underlinedString
  }
}
