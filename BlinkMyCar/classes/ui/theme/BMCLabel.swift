//
//  BMCLabel.swift
//  BlinkMyCar
//
//  Created by Elie Soueidy on 6/30/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class BMCLabel: UILabel {
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override public init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  override public func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }

  func setup() {
    //TODO: Customize the button's layout based on the designs
  }

  func underlinedString(text: String) {
    self.attributedText = NSMutableAttributedString(string: text,
      attributes: [
        NSUnderlineStyleAttributeName: NSNumber(integer:NSUnderlineStyle.StyleSingle.rawValue),
        NSForegroundColorAttributeName: textColor ?? UIColor.orange()
      ])
  }
}

class BMCTitleLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.lightWhite()
    //Set Font
    self.font = UIFont.thinBMCFont(size: 20)
  }
}

class BMCBoldTitleLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.whiteColor()
    //Set Font
    self.font = UIFont.boldBMCFont(size: 22)
  }
}

class BMCAnimationInfoLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.lightGray()
    self.font = UIFont.lightBMCFont(size: 22)
  }
}

class BMCSecondaryTitleLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    //Set Font
    self.font = UIFont.boldBMCFont(size: 24)
  }
}

class BMCSecondaryTitleLightLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.whiteColor()
    //Set Font
    self.font = UIFont.boldBMCFont(size: 24)
  }
}

class BMCErrorLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.orange()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 16)
  }
}

class BMCFormLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    self.font = UIFont.lightBMCFont(size: 20)
  }

  func setRequired(required: Bool) {
    if !required {
      return
    }
    if let text = self.text {
      let attributedText = NSMutableAttributedString(string: text)
      let attributedStar = NSMutableAttributedString(string: " *", attributes: [
        NSForegroundColorAttributeName: UIColor.bmcRed()
        ])
      attributedText.appendAttributedString(attributedStar)
      self.attributedText = attributedText
    }
  }
}

class BMCAddressFormLabel: BMCFormLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCAddressSubtitleLabel: BMCFormLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    self.font = UIFont.lightBMCFont(size: 15)
  }
}

class BMCAddressTitleLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    self.font = UIFont.boldBMCFont(size: 18)
  }
}

class BMCSmallFormLabel: BMCFormLabel {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 15)
  }
}

class BMCMenuLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    self.font = UIFont.lightBMCFont(size: 16)
  }
}

class BMCNotificationLabel: BMCFormLabel {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 16)
  }
}

class BMCCarMenuLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.gray()
    self.font = UIFont.boldBMCFont(size: 24)
  }
  
  func compactSize() {
    self.font = UIFont.boldBMCFont(size: 22)
  }
}

class BMCListingSectionHeaderLabel: BMCLabel {
  override func setup() {
    super.setup()
    textColor = UIColor.whiteColor()
    font = UIFont.boldBMCFont(size: 18)
  }
}

class BMCListItemLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCOrangeListItemLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.orange()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCSmallListItemLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 15)
  }
}

class BMCLightNoteLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.lightGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 15)
  }
}

class BMCOrangeLightNoteLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.orange()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 15)
  }
}

class BMCSubsectionLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCServiceLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.gray()
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCAddressHeaderLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.whiteColor()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCCarProfileName: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    self.font = UIFont.boldBMCFont(size: 18)
  }
}

class BMCCarProfileValue: BMCLabel {
  override func setup() {
    self.textColor = UIColor.gray()
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCHistoryTitleLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    self.font = UIFont.boldBMCFont(size: 18)
  }
}

class BMCHistorySubTitleLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    self.font = UIFont.boldBMCFont(size: 15)
  }
}

class BMCHistoryTimeLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    self.font = UIFont.lightBMCFont(size: 12)
  }
}

class BMCBoldLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    //Set Font
    self.font = UIFont.boldBMCFont(size: 22)
  }
}

class BMCAboutUsBoldLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    //Set Font
    self.font = UIFont.boldBMCFont(size: 22)
  }
}

class BMCSecondaryTitleLightWeightLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.whiteColor()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 24)
  }
}

class BMCServiceTitleLabel: BMCLabel {
  let fontSize: CGFloat = 18
  
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    //Set Font
    self.font = UIFont.boldBMCFont(size: fontSize)
  }
  
  func mainServiceAttributedText(serviceName: String?, servicePrice: String?, separator: String = ", ") {
    attributedText(nil, serviceName: serviceName, servicePrice: servicePrice,
                   fontSize: fontSize, separator: separator)
  }
  
  func addOnAttributedText(serviceName: String?, servicePrice: String?, separator: String = ", ") {
    attributedText("+", serviceName: serviceName, servicePrice: servicePrice,
                   fontSize: fontSize-2, separator: separator)
  }
  
  private func attributedText(symbol: String?, serviceName: String?, servicePrice: String?, fontSize: CGFloat, separator: String = ", ") {
    let myString = NSMutableAttributedString(string: "")
    
    if let symbol = symbol {
      let aString = NSAttributedString(
        string: symbol,
        attributes: [
          NSFontAttributeName: UIFont.boldBMCFont(size: fontSize+1)
        ])
      myString.appendAttributedString(aString)
      myString.appendAttributedString(NSAttributedString(string: " "))
    }
    
    if let serviceName = serviceName {
      let aString = NSAttributedString(
        string: serviceName,
        attributes: [
          NSFontAttributeName: UIFont.boldBMCFont(size: fontSize)
        ])
      
      myString.appendAttributedString(aString)
    }
    
    if let servicePrice = servicePrice {
      let aString = NSAttributedString(
        string: servicePrice,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: fontSize),
        ])
      myString.appendAttributedString(NSAttributedString(string: separator))
      myString.appendAttributedString(aString)
    }
    
    self.attributedText = myString
  }
}

class BMCServicePriceLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 15)
  }
}

class BMCBookingStatusLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    self.textAlignment = NSTextAlignment.Center
    //Set Font
    self.font = UIFont.lightBMCFont(size: 15)
  }
}

class BMCAddressLabel: BMCServicePriceLabel {
  override func setup() {
    super.setup()
    self.textColor = UIColor.darkGray()
  }
}

class BMCDateTimeLabel: BMCLabel {
  var dateTextFontSize: CGFloat = 18
  var timeTextFontSize: CGFloat = 18
  var dateTextColor: UIColor = UIColor.applicationSecondaryColor()
  var timeTextColor: UIColor = UIColor.darkGray()
  
  override func setup() {
    self.textColor = UIColor.darkGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: dateTextFontSize)
  }
  
  func attributedText(date: String?, time: String?) {
    let myString = NSMutableAttributedString(string: "")
    
    if let date = date {
      let dateString = "\(date)."
      let aString = NSAttributedString(
        string: dateString,
        attributes: [
          NSFontAttributeName: UIFont.boldBMCFont(size: dateTextFontSize),
          NSForegroundColorAttributeName: dateTextColor
        ])
      myString.appendAttributedString(aString)
    }
    
    if let time = time {
      let timeString = " \(time)"
      let aString = NSAttributedString(
        string: timeString,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: timeTextFontSize),
          NSForegroundColorAttributeName: timeTextColor
        ])
      myString.appendAttributedString(aString)
    }
    
    self.attributedText = myString
  }
}

class BMCBookingHistoryDateTimeLabel: BMCDateTimeLabel {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    timeTextColor = UIColor.applicationSecondaryColor()
    timeTextFontSize = 15
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    timeTextColor = UIColor.applicationSecondaryColor()
    timeTextFontSize = 15
  }
}

class BMCBookingHistoryCarMakeModelLabel: BMCLabel {
  var makeTextFontSize: CGFloat = 18
  var modelTextFontSize: CGFloat = 18
  var makeTextColor: UIColor = UIColor.applicationSecondaryColor()
  var modelTextColor: UIColor = UIColor.applicationSecondaryColor()
  
  override func setup() {
    self.textColor = UIColor.darkGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: makeTextFontSize)
  }
  
  func attributedText(make: String?, model: String?) {
    let myString = NSMutableAttributedString(string: "")
    
    if let make = make {
      let makeString = "\(make) "
      let aString = NSAttributedString(
        string: makeString,
        attributes: [
          NSFontAttributeName: UIFont.boldBMCFont(size: makeTextFontSize),
          NSForegroundColorAttributeName: makeTextColor
        ])
      myString.appendAttributedString(aString)
    }
    
    if let model = model {
      let modelString = "\(model)"
      let aString = NSAttributedString(
        string: modelString,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: modelTextFontSize),
          NSForegroundColorAttributeName: modelTextColor
        ])
      myString.appendAttributedString(aString)
    }
    
    self.attributedText = myString
  }
}

class BMCBookingHistoryBookingIdLabel: BMCLabel {
  var symbolTextFontSize: CGFloat = 18
  var defaultTextFontSize: CGFloat = 18
  var symbolColor: UIColor = UIColor.applicationSecondaryColor()
  var defaultTextColor: UIColor = UIColor.darkGray()
  
  override func setup() {
    self.textColor = defaultTextColor
    self.font = UIFont.lightBMCFont(size: defaultTextFontSize)
  }
  
  override var text: String? {
    didSet {
      attributedText(text)
    }
  }
  
  func attributedText(id: String?) {
    let myString = NSMutableAttributedString(string: "")
    
    let symbolString = "# "
    let aString = NSAttributedString(
      string: symbolString,
      attributes: [
        NSFontAttributeName: UIFont.boldBMCFont(size: symbolTextFontSize),
        NSForegroundColorAttributeName: symbolColor
      ])
    myString.appendAttributedString(aString)
    
    
    if let id = id {
      let modelString = "\(id)"
      let aString = NSAttributedString(
        string: modelString,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: defaultTextFontSize),
          NSForegroundColorAttributeName: defaultTextColor
        ])
      myString.appendAttributedString(aString)
    }
    
    self.attributedText = myString
  }
}

class BMCUpcomingServiceLabel: BMCLabel {
  var fontSize: CGFloat = 14
  
  override func setup() {
    self.textColor = UIColor.darkGray()
    // Set Font
    self.font = UIFont.lightBMCFont(size: fontSize)
  }
  
  func attributedText(serviceName: String?, servicePrice: String?) {
    let myString = NSMutableAttributedString(string: "")
    
    if let serviceName = serviceName {
      let aString = NSAttributedString(
        string: serviceName,
        attributes: [
          NSFontAttributeName: UIFont.boldBMCFont(size: fontSize),
          NSForegroundColorAttributeName: textColor ?? UIColor.darkGray()
        ])
      myString.appendAttributedString(aString)
    }
    
    if let servicePrice = servicePrice {
      let aString = NSAttributedString(
        string: servicePrice,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: fontSize),
          NSForegroundColorAttributeName: textColor ?? UIColor.darkGray()
        ])
      myString.appendAttributedString(NSAttributedString(string: ", "))
      myString.appendAttributedString(aString)
    }
    
    self.attributedText = myString
  }
}

class BMCBookingStatusAutoLabel: BMCLabel {
  
  var status: BookingStatus = .Pending {
    didSet {
      self.textColor = status.statusColor
      attributedText(status.fontAwsomeSymbol, bookingStatus: status.displayedStatus)
    }
  }
  
  func attributedText(symbol: String, bookingStatus: String) {
    let myString = NSMutableAttributedString(string: "")
    
    let symbolString = NSAttributedString(
      string: symbol,
      attributes: [
        NSFontAttributeName: UIFont.awesomeBMCFont(size: 15)
      ])
    myString.appendAttributedString(symbolString)
    myString.appendAttributedString(NSAttributedString(string: " "))
    
    let statusString = NSAttributedString(
      string: bookingStatus,
      attributes: [
        NSFontAttributeName: UIFont.boldBMCFont(size: 15)
      ])
    myString.appendAttributedString(statusString)
    
    self.attributedText = myString
  }
}

class BMCSelectedAddOnServiceLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 12)
  }
  
  func attributedText(serviceName: String?, servicePrice: String?) {
    let myString = NSMutableAttributedString(string: "")
    
    let aString = NSAttributedString(
      string: "+",
      attributes: [
        NSFontAttributeName: UIFont.boldBMCFont(size: 15),
        NSForegroundColorAttributeName: UIColor.applicationMainColor()
      ])
    myString.appendAttributedString(aString)
    
    
    if let serviceName = serviceName {
      let aString = NSAttributedString(
        string: serviceName,
        attributes: [
          NSFontAttributeName: UIFont.boldBMCFont(size: 14),
          NSForegroundColorAttributeName: textColor ?? UIColor.darkGray()
        ])
      myString.appendAttributedString(NSAttributedString(string: " "))
      myString.appendAttributedString(aString)
    }
    
    if let servicePrice = servicePrice {
      let aString = NSAttributedString(
        string: servicePrice,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: 14),
          NSForegroundColorAttributeName: textColor ?? UIColor.darkGray()
        ])
      myString.appendAttributedString(NSAttributedString(string: ", "))
      myString.appendAttributedString(aString)
    }
    
    self.attributedText = myString
  }
}

class BMCSingleLabelInCell: BMCLabel {
  override func setup() {
    self.textColor = UIColor.darkGray()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCLabelForSmallSectionView: BMCLabel {
  override func setup() {
    self.textColor = UIColor.applicationSecondaryColor()
    //Set Font
    self.font = UIFont.lightBMCFont(size: 18)
  }
}

class BMCStepIndicatorLabel: BMCLabel {
  let foregroundColor: UIColor = UIColor.applicationMainColor()
  
  override func setup() {
    self.textColor = foregroundColor
    self.font = UIFont.boldBMCFont(size: 18)
    self.textAlignment = NSTextAlignment.Center
    makeRounded()
  }
}

class BMCStepLabel: BMCLabel {
  let foregroundColor: UIColor = UIColor.whiteColor()
  
  override func setup() {
    self.textColor = foregroundColor
    self.font = UIFont.lightBMCFont(size: 18)
    self.textAlignment = NSTextAlignment.Center
  }
}

class BMCPriceInfoLabel: BMCLabel {
  override func setup() {
    self.textColor = UIColor.lightWhite()
    self.font = UIFont.lightBMCFont(size: 22)
    self.textAlignment = NSTextAlignment.Center
    self.numberOfLines = 0
  }

  func attributedText(actualPrice: String?, discountedPrice: String?, note: String?) {
    let myString = NSMutableAttributedString(string: "")
    
    if let discountedPrice = discountedPrice {
      let aString = NSAttributedString(
        string: discountedPrice,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: 15),
          NSStrikethroughStyleAttributeName: 2,
          NSForegroundColorAttributeName: textColor ?? UIColor.orange()
        ])
      
      myString.appendAttributedString(aString)
      myString.appendAttributedString(NSAttributedString(string: " "))
    }
    
    if let actualPrice = actualPrice {
      let aString = NSAttributedString(
        string: actualPrice,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: 22),
          NSForegroundColorAttributeName: textColor ?? UIColor.orange()
        ])
      myString.appendAttributedString(aString)
    }
    
    if let note = note {
      let aString = NSAttributedString(
        string: note,
        attributes: [
          NSFontAttributeName: UIFont.lightBMCFont(size: 10),
          NSForegroundColorAttributeName: textColor ?? UIColor.orange()
        ])
      
      myString.appendAttributedString(NSAttributedString(string: "\n"))
      myString.appendAttributedString(aString)
    }
    
    self.attributedText = myString
  }
}

class BMCRatingDateTimeLabel: BMCLabel {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 15)
    self.textColor = UIColor.darkGray()
  }
}

class BMCAdditonalInfoTitleLabel: BMCLabel {
  override func setup() {
    super.setup()
    self.font = UIFont.boldBMCFont(size: 24)
    self.textColor = UIColor.whiteColor()
  }
}

class BMCAdditonalInfoSubtitleLabel: BMCLabel {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 24)
    self.textColor = UIColor.whiteColor()
  }
}

class BMCAdditonalInfoQuestionLabel: BMCLabel {
  override func setup() {
    super.setup()
    self.font = UIFont.lightBMCFont(size: 18)
    self.textColor = UIColor.applicationSecondaryColor()
  }
}
