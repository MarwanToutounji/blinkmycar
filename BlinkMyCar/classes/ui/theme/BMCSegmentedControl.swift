//
//  BMCSegmentedControl.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 8/4/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

public class BMCSegmentedControl: UISegmentedControl {
  
  override init(items: [AnyObject]?) {
    super.init(items: items)
    setup()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }
  
  func setup() {
    //TODO: Customize the button's layout based on the designs
    self.tintColor = UIColor.applicationMainColor()
    //setup font and text colors according to states
    let attrNormal: [NSObject : AnyObject] = [NSFontAttributeName : UIFont.lightBMCFont(size: 15),
      NSForegroundColorAttributeName : UIColor.applicationMainColor()]
    let attrSelected: [NSObject : AnyObject] = [NSFontAttributeName : UIFont.lightBMCFont(size: 15),
      NSForegroundColorAttributeName : UIColor.whiteColor()]
    
    self.setTitleTextAttributes(attrNormal, forState: .Normal)
    self.setTitleTextAttributes(attrSelected, forState: .Selected)
  }
  
}