//
//  UINavigationBar+BMC.swift
//  BlinkMyCar
//
//  Created by Shafic Hariri on 7/8/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit

extension UINavigationBar {
  
  class func applyTheme() {
    let appearance = UINavigationBar.appearance()
    appearance.barTintColor = UIColor.purpleShade()
    appearance.tintColor = UIColor.whiteColor()
    appearance.translucent = false
    appearance.titleTextAttributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.boldBMCFont(size: 18)
    ]
    appearance.backIndicatorImage = UIImage(named: "backBarButtonItem")
    appearance.backIndicatorTransitionMaskImage = UIImage(named: "backBarButtonItem")
  }
  
  class func closeBarButton(target: AnyObject, selector: String) -> UIBarButtonItem {
    let backButton: UIButton = UIButton(type: UIButtonType.Custom)
    backButton.frame = CGRectMake(17, 17, 17, 17)
    backButton.setImage(UIImage(named: "closeBarIcon"), forState: UIControlState.Normal)
    backButton.addTarget(target, action: Selector(selector), forControlEvents: UIControlEvents.TouchUpInside)
    let leftBarButton = UIBarButtonItem(customView: backButton)
    return leftBarButton
  }

}
