//
//  UniversalLinksManager.swift
//  BlinkMyCar
//
//  Created by Marwan  on 8/29/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

final class UniversalLinksManager {
  /// The deep link scheme of the application
  static let deepLinkScheme: String = "blinkmycar"
}