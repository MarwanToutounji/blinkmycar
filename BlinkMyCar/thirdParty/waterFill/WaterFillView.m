//
//  WaterFillView.m
//  WaterFillView
//
//  Created by Wang Yandong on 4/21/14.
//  Copyright (c) 2014 wangyandong@outlook.com. All rights reserved.
//

#import "WaterFillView.h"

@interface WaterFillView ()
{
  CADisplayLink *_displayLink;
  CGFloat _phrase;
  CGFloat _y;
}

@end


@implementation WaterFillView

- (void) dealloc {
  [self stopAnimation];
}

- (void) startAnimation {
  if (!_displayLink) {
    _y = 0;
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(update:)];
    _displayLink.frameInterval = 2;
    [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  }
}

- (void) pauseAnimation {
  [_displayLink removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void) stopAnimation {
  [_displayLink invalidate];
  _displayLink = nil;
}

- (void) update:(CADisplayLink *)displayLink {
  _phrase += _horizontalSpeed;

  _y = (_startDate - [[NSDate date] timeIntervalSince1970]) * self.bounds.size.height / (_slotDuration * 3600);

  if (-_y >= CGRectGetHeight(self.bounds)) {
    _y = -CGRectGetHeight(self.bounds);
    [self stopAnimation];
  }

  [self setNeedsDisplay];
}

- (void) drawRect:(CGRect)rect {
  CGRect bounds = self.bounds;
  CGFloat halfHeight = CGRectGetHeight(bounds) / 2;

  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextTranslateCTM(context, 0.0, halfHeight);

  NSUInteger times = (NSUInteger)CGRectGetWidth(bounds);
  CGPoint start;
  CGFloat y = 0;
  for (NSUInteger t = 0; t <= times; ++t) {
    y = (CGFloat)(_amplitude * sin(t * _frequency + _phrase));

    if (0 == t) {
      CGContextMoveToPoint(context, 0.0, y + halfHeight + _y);
      start = CGPointMake(0, y);
    } else {
      CGContextAddLineToPoint(context, t, y + halfHeight + _y);
    }
  }

  CGContextAddLineToPoint(context, CGRectGetWidth(bounds), CGRectGetHeight(bounds));
  CGContextAddLineToPoint(context, 0, CGRectGetHeight(bounds));
  CGContextAddLineToPoint(context, start.x, start.y);

  CGContextSetFillColorWithColor(context, _waterColor.CGColor);
  CGContextFillPath(context);

  if (_delegate && [_delegate respondsToSelector:@selector(waterFillViewDidUpdate:)]) {
    CGFloat percentage = -_y / (2 * halfHeight);
    [_delegate waterFillViewDidUpdate:percentage];
  }
}

@end
