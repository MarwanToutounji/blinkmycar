//
//  WaterFillView.h
//  WaterFillView
//
//  Created by Wang Yandong on 4/21/14.
//  Copyright (c) 2014 wangyandong@outlook.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WaterFillViewDelegate;

@interface WaterFillView : UIView

@property (nonatomic) CGFloat amplitude;
@property (nonatomic) CGFloat frequency;
@property (nonatomic) CGFloat verticalSpeed;
@property (nonatomic) CGFloat horizontalSpeed;
@property (nonatomic) CGFloat startingFrom;
@property (nonatomic) CFTimeInterval startDate;
@property (nonatomic) CGFloat slotDuration;
@property (nonatomic, strong) UIColor *waterColor;

- (void) startAnimation;
- (void) pauseAnimation;
- (void) stopAnimation;

@property (nonatomic, weak) id<WaterFillViewDelegate> delegate;

@end

@protocol WaterFillViewDelegate <NSObject>

@optional
- (void)waterFillViewDidUpdate:(CGFloat)percentage;

@end
