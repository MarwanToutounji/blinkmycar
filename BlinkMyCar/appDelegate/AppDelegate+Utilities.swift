//
//  AppDelegate+Utilities.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/18/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate {
  func animateRootViewTransition(options: UIViewAnimationOptions,
                                 viewController: UIViewController) {
    
    // Remove the current rootVC as observer if he is
    NSNotificationCenter.defaultCenter().removeObserver(self.window!.rootViewController!)
    
//    viewController.view.layoutIfNeeded()
//    
//    // Perform the transition
//    UIView.transitionWithView(
//      self.window!,
//      duration: 0.5,
//      options: options,
//      animations: { () -> Void in
//        self.window!.rootViewController = viewController;
//    }) { (finished: Bool) -> Void in
//    }
    
    // Implemented on 16/8/2016 - Testing
    changeRootViewController(viewController)
  }
  
  
  func changeRootViewController(viewController: UIViewController) {
    
    let snapshot:UIView = (self.window?.snapshotViewAfterScreenUpdates(true))!
    viewController.view.addSubview(snapshot);
    
    self.window?.rootViewController = viewController;
    
    UIView.animateWithDuration(0.4, animations: {() in
      snapshot.layer.opacity = 0;
      }, completion: {
        (value: Bool) in
        snapshot.removeFromSuperview();
    });
  }
}
