//
//  AppDelegate+SetupApp.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/7/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

enum BlinkMyCarErrorCodes: Int {
  case SignInFailed
  case SignInCredentialsInvalid
  case ProfileAPIFailed
  case UploadFailed
  case GetAllBookingsFailed
  case GetAllBookingsNeedsRating
  case GetAllBookingsForVehicleFailed
  case RetrieveDatesAndTimesFailed
  case RetrieveAreasFailed
  case RetrieveAddressesFailed
  case RetrieveVehiclesFailed
  case CreateBookingFailed
  case CreateAddressFailed
  case SubmitBookingAdditionalInformationFailed
  case UpdateBookingFailed
  case UpdateAddressFailed
  case DeleteBookingFailed
  case DeleteAddressFailed
  case DeleteVehicleFailed
  case DeletePaymentToolFailed
  case UpdateBookingAddressFailed
  case UpdateBookingVehicleInformationFailed
  case RetrieveCountryInformationFailed
  case RetrieveCardsFailed
  case SetPaymentToolAsDefaultFailed
  case MissingDefaultPaymentMethod
  case RetrieveServicesFailed
  case GetTotalFailed
}

extension AppDelegate {
  func getRootViewController() -> UIViewController {
    // If there is a token then the app is signed in
    if AccessToken().hasToken {
      if !hasAtLeastOneVehicle {
        let vc = mainStoryBoard.instantiateViewControllerWithIdentifier(
          ViewControllerStoryboardIdentifier.ChooseYourRideViewController.rawValue)
          as! ChooseYourRideViewController
        vc.viewModel.analyticsCategory = AnalyticsCategories.RegistrationChooseVehicle
        return UINavigationController(rootViewController: vc)
      } else {
        return mainStoryBoard.instantiateViewControllerWithIdentifier(
          ViewControllerStoryboardIdentifier.HomeViewController.rawValue)
          as! HomeViewController
      }
    }

    return mainStoryBoard.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.WelcomeViewController.rawValue)
      as! WelcomeViewController
  }
  
  func resetAccessTokenFlags() {
    //Reset AccessToken Updating Flag on Start and End App
    var accessToken = AccessToken()
    accessToken.isUpdating = false
  }

  func setupLoader() {
    var config : SwiftLoader.Config = SwiftLoader.Config()
    config.size = 135.0
    config.coverBackgroundColor = UIColor(white: 0.1, alpha: 0.4)
    SwiftLoader.setConfig(config)
  }

  func applyTheme() {
    UIBarButtonItem.applyTheme()
  }
  
  func initiateGAnalytics() {
    // Configure tracker from GoogleService-Info.plist.
    var configureError:NSError?
    GGLContext.sharedInstance().configureWithError(&configureError)
    assert(configureError == nil, "Error configuring Google services: \(configureError)")
    
    // Optional: configure GAI options.
    let gai = GAI.sharedInstance()
    gai.trackUncaughtExceptions = true  // report uncaught exceptions
    configureGoogleAnalytics()
    
    gai.defaultTracker.allowIDFACollection = true
  }

  var hasAtLeastOneVehicle: Bool {
    return Vehicle.getSavedVehicles().count > 0
  }
}
