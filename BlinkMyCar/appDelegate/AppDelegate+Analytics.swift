//
//  AppDelegate+Analytics.swift
//  BlinkMyCar
//
//  Created by Marwan  on 10/12/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation

public enum AnalyticsCategories: String {
  case WelcomeScreen = "Welcome Screen"
  case Signin = "Signin"
  case Register = "Register"
  case RegistrationChooseVehicle = "Registration Choose Vehicle"
  case InappChooseVehicle = "In-App Choose Vehicle"
  case BookingProcess = "Booking Process"
  case Upcoming = "Upcoming"
  case RightMenu = "Right Menu"
  case LeftMenu = "Left Menu"
  case InMenuPayment = "In-Menu Payment"
  case MyAccount = "My Account"
  case InMenuMyLocations = "In-Menu My Locations"
}
