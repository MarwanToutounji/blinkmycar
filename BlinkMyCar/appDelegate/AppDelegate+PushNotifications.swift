//
//  AppDelegate+PushNotifications.swift
//  BlinkMyCar
//
//  Created by Marwan on 3/1/16.
//  Copyright © 2016 Keeward. All rights reserved.
//

import Foundation
import Keys

// MARK: UIApplication Delegate
//------------------------------
extension AppDelegate {
  
  func application(application: UIApplication,
    didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
    fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
      
      print("Notification received: \(userInfo)")
      
      // This works only if the app started the GCM service
      GCMService.sharedInstance().appDidReceiveMessage(userInfo);
      
      // Handle the received message
      // Invoke the completion handler passing the appropriate UIBackgroundFetchResult value
      NSNotificationCenter.defaultCenter().postNotificationName(
        BMCNotificationNames.RefreshApplication.rawValue, object: nil)
      completionHandler(UIBackgroundFetchResult.NoData);
  }
  
  func application(application: UIApplication,
    didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
      // Create a config and set a delegate that implements the GGLInstaceIDDelegate protocol.
      let instanceIDConfig = GGLInstanceIDConfig.defaultConfig()
      instanceIDConfig.delegate = self
      
      // Start the GGLInstanceID shared instance with that config and request a registration
      // token to enable reception of notifications
      GGLInstanceID.sharedInstance().startWithConfig(instanceIDConfig)
      let sandboxOption = BlinkmycarKeys().blinkMyCarAPNSServerTypeSandboxOption().boolValue()!
      registrationOptions = [kGGLInstanceIDRegisterAPNSOption:deviceToken,
        kGGLInstanceIDAPNSServerTypeSandboxOption:sandboxOption]
      GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
        scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
  }
  
  func application( application: UIApplication, didFailToRegisterForRemoteNotificationsWithError
    error: NSError ) {
      print("Registration for remote notification failed with error: \(error.localizedDescription)")
  }
}

// MARK: CUSTOM METHODS
//----------------------
extension AppDelegate {
  func registerForNotifications() {
    // Configure the Google context: parses the GoogleService-Info.plist, and initializes
    // the services that have entries in the file
    var configureError:NSError?
    GGLContext.sharedInstance().configureWithError(&configureError)
    assert(configureError == nil, "Error configuring Google services: \(configureError)")
    gcmSenderID = GGLContext.sharedInstance().configuration.gcmSenderID
    
    enablePushNotificationAlerts()
    
    // start GCM service
    let gcmConfig = GCMConfig.defaultConfig()
    gcmConfig.receiverDelegate = self
    GCMService.sharedInstance().startWithConfig(gcmConfig)
  }
  
  func unregisterForNotifications() {
    /*
    Unregistering for notifications occures on logout
    And since the logout action is not blocking
    all actions happen at the same time (delete GCM token, delete token from server).
    **/
    
    //~ Disable registering to alert locally
    disablePushNotificationAlerts()
    
    
    if UIApplication.sharedApplication().isRegisteredForRemoteNotifications() {
      //~ Delete Token from GCM
      GGLInstanceID.sharedInstance().deleteTokenWithAuthorizedEntity(gcmSenderID,
        scope: kGGLInstanceIDScopeGCM, handler: nil)
      
      //~ Delete Token from our servers
      if let savedNotificationToken = NotificationToken.getSavedToken() {
        signedRequest(BlinkMyCarAPI.UnregisterPushNotification(serverTokenID: savedNotificationToken.id), completion: { (data, statusCode, response, error) -> () in
          //~ Delete the token locally
          NotificationToken.deleteNotificationToken()
        })
      }
    }
  }
  
  func registrationHandler(registrationToken: String!, error: NSError!) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
      if (registrationToken != nil) {
        print("Registration Token: \(registrationToken)")
        self.registrationToken = registrationToken
        if let registrationToken = self.registrationToken {
          signedRequest(
            BlinkMyCarAPI.RegisterPushNotification(
              deviceNotificationToken: registrationToken),
            completion: { (data, statusCode, response, error) -> () in
              if statusCode != 201 {
                return
              }
              
              if let data = data,
                jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data,
                  options: NSJSONReadingOptions.AllowFragments) where jsonResult is [String : AnyObject] {
                let notificationToken = NotificationToken.fromJSON(jsonResult as! [String : AnyObject]) as! NotificationToken
                NotificationToken.saveToken(notificationToken)
              }
          })
        }
      } else {
        print("Registration to GCM failed with error: \(error.localizedDescription)")
      }
    }
  }
  
  //======================
  // MARK: HELPERS
  //======================
  private func enablePushNotificationAlerts() {
    // Register for remote notifications
    let settings: UIUserNotificationSettings =
    UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
    let application = UIApplication.sharedApplication()
    application.registerUserNotificationSettings(settings)
    application.registerForRemoteNotifications()
  }
  
  func disablePushNotificationAlerts() {
    let application = UIApplication.sharedApplication()
    application.unregisterForRemoteNotifications()
  }
}

// MARK: GCMReceiverDelegate
//---------------------------
extension AppDelegate: GCMReceiverDelegate {
  func willSendDataMessageWithID(messageID: String!, error: NSError!) {
    print("willSendDataMessageWithID => \(messageID)")
    if (error != nil) {
      // Failed to send the message.
    } else {
      // Will send message, you can save the messageID to track the message
    }
  }
  
  func didSendDataMessageWithID(messageID: String!) {
    // Did successfully send message identified by messageID
  }
  
  func didDeleteMessagesOnServer() {
    // Some messages sent to this device were deleted on the GCM server before reception, likely
    // because the TTL expired. The client should notify the app server of this, so that the app
    // server can resend those messages.
  }
}

// MARK: GGLInstanceIDDelegate
//-----------------------------
extension AppDelegate: GGLInstanceIDDelegate {
  func onTokenRefresh() {
    // A rotation of the registration tokens is happening, so the app needs to request a new token.
    print("The GCM registration token needs to be changed.")
    GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
      scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
  }
}
