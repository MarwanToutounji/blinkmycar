//
//  AppDelegate.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/8/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import Fabric
import TwitterKit
import Crashlytics
import SDWebImage
import FBSDKCoreKit
import SwiftLoader
import GoogleMaps
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  internal static var appID: String {
    return "1027861843"
  }

  var stripeID: String? {
    get {
      return Stripe.defaultPublishableKey()
    }
    set {
      if let id = newValue {
        Stripe.setDefaultPublishableKey(id)
      }
    }
  }

  var window: UIWindow?
  var mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  
  var connectedToGCM = false
  var gcmSenderID: String?
  var registrationToken: String?
  var registrationOptions = [String: AnyObject]()

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    initiateGAnalytics()
    
    resetAccessTokenFlags()

    // Initialize Fabric
    Twitter.sharedInstance().startWithConsumerKey("Tf2kvm1ReGKEJ9piTlyZ07q4a",
      consumerSecret: "WQcxNcekMkcl2ZpkNujaQaz9UsLwPs65jXZhKeDxas90liWC1y")
    Fabric.with([Crashlytics(), Twitter.sharedInstance()])

    // For statusBarStyle to work info.plist must contain "view controller bases status bar appearance = NO"
    application.statusBarStyle = .LightContent
    UINavigationBar.applyTheme()

    self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
    let rootViewController = getRootViewController()
    self.window!.rootViewController = rootViewController
    self.window!.makeKeyAndVisible()

    setupNotifications()
    setupLoader()

    applyTheme()

    SDWebImageDownloader.sharedDownloader().setValue(userAgentValue, forHTTPHeaderField: "User-Agent")

    FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    
    GMSServices.provideAPIKey("AIzaSyAekAU5rn2tBVfr6wSdDE73ElgSD6bCL9Q")

    return true
  }

  func applicationWillResignActive(application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

    GAI.sharedInstance().dispatch()
    
    NSNotificationCenter.defaultCenter().postNotificationName(
      BMCNotificationNames.AppWillEnterBackground.rawValue,
      object: nil)
  }

  func applicationDidEnterBackground(application: UIApplication) {
    GCMService.sharedInstance().disconnect()
    self.connectedToGCM = false
  }

  func applicationWillEnterForeground(application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.\
    SwiftLoader.refreshIfNeeded()
  }

  func applicationDidBecomeActive(application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    FBSDKAppEvents.activateApp()
    
    // Connect to the GCM server to receive non-APNS notifications
    GCMService.sharedInstance().connectWithHandler({
      (error) -> Void in
      if error != nil {
        print("Could not connect to GCM: \(error.localizedDescription)")
      } else {
        self.connectedToGCM = true
        print("Connected to GCM")
      }
    })
  }

  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    resetAccessTokenFlags()
  }

  func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
    // Facebook Handeling check point
    var handled = FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    
    // Deep linking check point
    if !handled && url.scheme == UniversalLinksManager.deepLinkScheme {
      handled = true
      // Do Additional stuff here
    }
    
    
    // GA campaigns check point
    if !handled {
      handled = true
      
      let urlString = url.absoluteString
      
      let tracker = GAI.sharedInstance().defaultTracker
      
      // setCampaignParametersFromUrl: parses Google Analytics campaign ("UTM")
      // parameters from a string url into a Map that can be set on a Tracker.
      let hitParams = GAIDictionaryBuilder()
      
      // Set campaign data on the map, not the tracker directly because it only
      // needs to be sent once.
      hitParams.setCampaignParametersFromUrl(urlString)
      
      // Campaign source is the only required campaign field. If previous call
      // did not set a campaign source, use the hostname as a referrer instead.
      if let urlHost = url.host where hitParams.get(kGAICampaignSource) == nil && !urlHost.isEmpty {
        // Set campaign data on the map, not the tracker.
        hitParams.set("referrer", forKey: kGAICampaignMedium)
        hitParams.set(urlHost, forKey: kGAICampaignSource)
      }
      
      let hitParamsDict = hitParams.build()
      
      // A screen name is required for a screen view.
      tracker.set(kGAIScreenName, value: "Campaign Screen View")
      
      tracker.send(GAIDictionaryBuilder.createScreenView().setAll(hitParamsDict as [NSObject : AnyObject]).build() as [NSObject : AnyObject])
    }
    
    
    return handled
  }
}

