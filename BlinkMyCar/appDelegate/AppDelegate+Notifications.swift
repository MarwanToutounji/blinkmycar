//
//  AppDelegate+Notifications.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/8/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader

enum BMCNotificationNames : String {
  case RefreshApplication = "BMCNotificationNamesRefreshApplication"
  case FailToSignInAfterRegistration = "BMCNotificationNamesFailToSignInAfterRegistration"
  case DidTapRegisterFromSignInVC = "BMCNotificationNamesDidTapRegisterFromSignInVC"
  case DidTapLoginFromRegisterVC = "BMCNotificationNamesDidTapLoginFromRegisterVC"
  case ShouldShowHomeVC = "BMCNotificationNamesShouldShowHomeVC"
  case LoginSuccess = "BMCNotificationNamesLoginSuccess"
  case DidChangeCurrentlySelectedVehicle = "BMCNotificationDidChangeCurrentlySelectedVehicle"
  case DidUpdateAddresses = "BMCNotificationNamesDidUpdateAddresses"
  case Logout = "BMCNotificationNamesLogout"
  case DidUpdateBookings = "BMCNotificationNamesDidUpdateBookings"
  case ShouldRefreshApplicationRootVC = "BMCNotificationNamesShouldRefreshApplicationRootVC"
  case DidUpdateVehicleListing = "BMCNotificationNamesDidUpdateVehicleListing"
  case DidChangeSendUsageData = "BMCNotificationNamesDidChangeSendUsageData"
  case FailToRefreshToken = "BMCNotificationNamesFailToRefreshToken"
  case BookingProcessTimeOut = "BMCNotificationNamesbookingProcessTimeOut"
  case shouldStopBookingFlowTimer = "BMCNotificationNamesShouldStopBookingFlowTimer"
  case AppNeedsUpdate = "BMCNotificationNamesAppNeedsUpdate"
  case DidUpdateCardsListing = "BMCNotificationNamesDidUpdateCardsListing"
  case DidAddNewCard = "BMCNotificationNamesDidAddNewCard"
  case DidUpdateSavedServices = "BMCNotificationNamesDidUpdateSavedServices"
  case ShouldReselectDate = "BMCNotificationNamesShouldReselectDate"
  case ShouldReselectAddress = "BMCNotificationNamesShouldReselectAddress"
  case AppWillEnterBackground = "BMCNotificationNamesAppWillEnterBackground"
  case DisplayBookingProcessTimeOutAlert = "BMCNotificationNamesDisplayBookingProcessTimeOutAlert"
}

enum BMCNotificationDataKeys : String {
  case BMCUserEmailKey = "BMCUserEmail"
}

extension AppDelegate {
  func setupNotifications() {
    resetNotifications()
  }

  func setHomeViewController(notification: NSNotification?) {
    if !hasAtLeastOneVehicle {
      setCreateRideViewController(nil)
    } else {
      let vc = mainStoryBoard.instantiateViewControllerWithIdentifier(
        ViewControllerStoryboardIdentifier.HomeViewController.rawValue)
        as! HomeViewController

      self.animateRootViewTransition(UIViewAnimationOptions.TransitionCrossDissolve,
        viewController: vc)
    }
  }

  func resetNotifications() {
    NSNotificationCenter.defaultCenter().removeObserver(self)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(AppDelegate.setHomeViewController(_:)),
      name: BMCNotificationNames.ShouldShowHomeVC.rawValue,
      object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(AppDelegate.logout),
      name: BMCNotificationNames.Logout.rawValue,
      object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(AppDelegate.configureGoogleAnalytics),
      name: BMCNotificationNames.DidChangeSendUsageData.rawValue,
      object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(AppDelegate.failToRefreshToken),
      name: BMCNotificationNames.FailToRefreshToken.rawValue,
      object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: #selector(AppDelegate.appNeedsUpdateHandler(_:)),
      name: BMCNotificationNames.AppNeedsUpdate.rawValue,
      object: nil)
  }

  func failToRefreshToken() {
    let alertTitle = NSLocalizedString("AppDelegate.failToRefreshTokenAlertTitle",
      value: "Account Verification",
      comment: "Alert View title, shows up when a logged in user is desabled from the backend ")

    let alertMessage = NSLocalizedString("AppDelegate.failToRefreshTokenAlertMessage",
      value: "There seems to be an issue with your user and you no longer have access to the app.\nPlease sign in again.",
      comment: "Alert View Message, shows up when a logged in user is desabled from the backend ")

    let alertActionTitle = NSLocalizedString("AppDelegate.failToRefreshTokenAlertTActionTitle",
      value: "Ok",
      comment: "Alert View Action Button Title, shows up when a logged in user is desabled from the backend ")

    let alertController = UIAlertController(
      title: alertTitle,
      message: alertMessage,
      preferredStyle: UIAlertControllerStyle.Alert)

    let okAction = UIAlertAction(title: alertActionTitle, style: UIAlertActionStyle.Default)
      { (UIAlertAction) -> Void in
        self.logout()
    }

    alertController.addAction(okAction)

    self.window?.rootViewController?.dismissViewControllerAnimated(false, completion: nil)
    self.window?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
  }

  func logout(){
    resetNotifications()
    
    // Unregister from push notifications
    (UIApplication.sharedApplication().delegate as! AppDelegate).unregisterForNotifications()

    let vc = mainStoryBoard.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.WelcomeViewController.rawValue)
      as! WelcomeViewController

    self.animateRootViewTransition(UIViewAnimationOptions.TransitionCrossDissolve,
      viewController: vc)
    AccessToken().deleteToken()
    User.deleteSavedUser()
    Vehicle.deleteSavedVehicles()
    Vehicle.deleteCurrentVehicle()
    Vehicle.deleteVehiclesIDsForUpdatingRelatedBookings()
    Booking.deleteBookings()
    Address.deleteAddresses()
    Address.deleteAddressesIDsForUpdatingRelatedBookings()
    Country.deleteCountry()
    PaymentMethod.deleteDefaultPaymentMethod()
  }

  func setCreateRideViewController(notification: NSNotification?) {
    let vc = mainStoryBoard.instantiateViewControllerWithIdentifier(
      ViewControllerStoryboardIdentifier.ChooseYourRideViewController.rawValue)
      as! ChooseYourRideViewController
    vc.viewModel.analyticsCategory = AnalyticsCategories.RegistrationChooseVehicle
    let navVC = UINavigationController(rootViewController: vc)
    self.animateRootViewTransition(UIViewAnimationOptions.TransitionCrossDissolve,
      viewController: navVC)
  }

  func loginSuccess() {
    if hasAtLeastOneVehicle {
      setHomeViewController(nil)
    } else {
      setCreateRideViewController(nil)
    }
  }
  
  func configureGoogleAnalytics() {
    GAI.sharedInstance().optOut = !GeneralSettings().sendUsageData
  }
  
  func bookingDateInvalidAlarm() {
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      NSNotificationCenter.defaultCenter().postNotificationName(
        BMCNotificationNames.BookingProcessTimeOut.rawValue, object: nil)
    })
  }
  
  func appNeedsUpdateHandler(notification: NSNotification) {
    PersistenceManager.sharedManager.stopSynching()
    SwiftLoader.hide()
    displayUpdateAppAlert(notification.object as! String)
  }
  
  private func displayUpdateAppAlert(appStoreURL: String) {
    let alertTitle = "Blink My Car"
    let alertMessage = "Update the app! The new one rocks!"
    let alertActionButtonTitle = "Update"
    
    if let rootViewController = window?.rootViewController {
      let alertController = UIAlertController(
        title: alertTitle,
        message: alertMessage,
        preferredStyle: .Alert)
      let alertAction = UIAlertAction(
        title: alertActionButtonTitle,
        style: UIAlertActionStyle.Default,
        handler: { (_) -> Void in
          // TODO: Go to app store here
          if let url = NSURL(string: appStoreURL)
            where UIApplication.sharedApplication().canOpenURL(url) {
              UIApplication.sharedApplication().openURL(url)
          }
      })
      alertController.addAction(alertAction)
      
      let visibleVC = UIViewController.getVisiblePresentedViewController(rootViewController)
      visibleVC.presentViewController(alertController, animated: true, completion: nil)
    }
  }
}
