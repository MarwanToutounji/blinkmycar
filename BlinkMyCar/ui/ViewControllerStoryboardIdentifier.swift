//
//  StoryboardIdentifiers.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/29/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import Foundation

public enum ViewControllerStoryboardIdentifier: String {
  case RegisterViewController = "RegisterViewController"
  case AddressBookViewController = "AddressBookViewController"
  case AddressProfileViewController = "AddressProfileViewController"
  case ChooseYourRideViewController = "ChoseYourRideViewController"
  case CreateRideViewController = "CreateRideViewController"
  case VehicleInfoTableViewController = "VehicleInfoTableViewController"
  case VehicleInfoCollectionViewController = "VehicleInfoCollectionViewController"
  case WelcomeViewController = "WelcomeViewController"
  case MenuViewController = "MenuViewController"
  case HomeViewController = "HomeViewController"
  case SignInViewController = "SignInViewController"
  case DetailsMenuTableViewController = "DetailsMenuTableViewController"
  case VehiclesMenuViewController = "VehiclesMenuViewController"
  case BookServicesViewController = "BookServicesViewController"
  case AddressesTableViewController = "AddressesTableViewController"
  case DatePickerTableViewController = "DatePickerTableViewController"
  case UserProfileViewController = "UserProfileViewController"
  case AccountSettingsViewController = "AccountSettingsViewController"
  case CustomerServiceViewController = "CustomerServiceViewController"
  case CallMeViewController = "CallMeViewController"
  case DiagnosticsAndUsageViewController = "DiagnosticsAndUsageViewController"
  case UpcomingBookingsViewController = "UpcomingBookingsViewController"
  case VehicleProfileViewController = "VehicleProfileViewController"
  case VehicleListingViewController = "VehicleListingViewController"
  case BookingHistoryViewController = "BookingHistoryViewController"
  case AboutUsViewController = "AboutUsViewController"
  case WashInProgressViewController = "WashInProgressViewController"
  case WebViewController = "WebViewController"
  case AcknowledgementsTableViewController = "AcknowledgementsTableViewController"
  case AcknowledgementViewController = "AcknowledgementViewController"
  case LocationsMapViewController = "LocationsMapViewController"
  case PaymentListingViewController = "PaymentListingViewController"
  case AddCardViewController = "AddCardViewController"
  case ServicesViewController = "ServicesViewController"
  case TimePickerViewController = "TimePickerViewController"
  case BookingConfirmationViewController = "BookingConfirmationViewController"
  case UpcomingBookingsListingViewController = "UpcomingBookingsListingViewController"
  case RateBookingViewController = "RateBookingViewController"
  case AdditionalInstructionsViewController = "AdditionalInstructionsViewController"
  case AddPromocodeViewController = "AddPromocodeViewController"
}

