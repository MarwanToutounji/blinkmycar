/* 
  Localizable.strings
  BlinkMyCar

  Created by Shafic Hariri on 7/8/15.
  Copyright (c) 2015 Keeward. All rights reserved.
*/

"WelcomeViewViewController.WelcomeLabel" = "Car Care\nAnytime, Anywhere.";
"ChooseYourRideViewController.titleLabel" = "Choose your vehicle";
"ChooseYourRideViewController.carTitleLabel" = "Car";
"ChooseYourRideViewController.bikeTitleLabel" = "Bike";
"ChooseYourRideViewController.fleetTitleLabel" = "Fleet";
"RegisterViewController.WelcomeLabelText" = "Welcome;";
"RegisterViewController.form.emailLabelText" = "Email Address";
"RegisterViewController.form.phoneLabelText" = "Mobile Number";
"RegisterViewController.form.passwordLabel" = "Password";
"RegisterViewController.form.registrationButton" = "Register";
"RegisterViewController.form.emailErrorMessage" = "Woops. Please check your email again.";
"RegisterViewController.form.passwordErrorMessage" = "Password is empty";
"RegisterViewController.form.phoneErrorMessage" = "Woops. Please check your mobile number again.";
"RegisterViewController.termsAndConditionsLabelText" = "By registering you agree to our";
"RegisterViewController.termsAndConditionsButtonText" = "Terms And Conditions";
"RegisterViewController.form.loginLinkButton" = "Already have an account? Sign in";

/* Register view controller title that appears in the navigation Bar */
"RegisterViewController.ViewControllerTitle" = "Register";
/* Text That is displayed above the email text field */
"SignInViewController.LabelEmail" = "Email";
/* Text That is displayed above the password text field */
"SignInViewController.LabelPasswordText" = "Password";
/* Text That is displayed In the Sign In button */
"SignInViewController.ButtonSignInTitle" = "Sign In";
/* Error Message that is displayed in case the Email field content is not valid */
"SignInViewController.TextFieldEmailErrorMessage" = "Woops. Please check your email again.";
/* Error Message that is displayed in case the Password field content is empty */
"SignInViewController.TextFieldPasswordErrorMessage" = "Woops. Please check your password again.";
/* Text that is displayed in the button that takes the user to the registration View */
"SignInViewController.RegisterButtonText" = "Register";
/* Text that is displayed in the label above the register button */
"SignInViewController.RegisterLabelText" = "Don't have an account yet?";
/* Sign In View Controller Title that appears in the navigation controller */
"SignInViewController.viewControllerTitle" = "Sign In";
/* Alert view title, the alert view holds error messages related to the Sign in process */
"SignInViewController.AlertViewTitle" = "Sign In";
/* Error message from the server informing user that he couldn't login. Username or password are wrong */
"SignInViewController.AlertViewServerErrorMessage" = "Failed to login! try again later";
/* Error message that is displayed after checking the validation of the email and password fields before calling the server */
"SignInViewController.AlertViewInvalidEmailAndOrPassword" = "Username or password are invalid!";
/* Alert view dismiss button */
"SignInViewController.AlertViewDismissActionButtonText" = "Ok";

"SignInViewController.buttonForgotPasswordTitle" = "Forgot Password?";

/* Title for alert view that is displayed while confirming a user phone number while registering an account */
"RegisterViewController.alertViewConfirmPhoneNumberTitle" = "Is this your number?";
/* Negative button text, for not confirming a phone number */
"RegisterViewController.alertViewConfirmPhoneNumberNegativeActionText" = "No";
/* Positive button text, for confirming a phone number */
"RegisterViewController.alertViewConfirmPhoneNumberPositiveActionText" = "Yes";
/* Message that is displayed in alert view for having invalid fields */
"RegisterViewController.alertViewFailRegistrationAlertInvalidFieldsMessage" = "One or more fields are invalid!";
/* Message that is displayed in alert view indicating that there was a some kind of error and we couldn't register user */
"RegisterViewController.alertViewFailRegistrationAlertRegistrationErrorMessage" = "Fail to register";
/* Action Button Text for Error alert views in registration */
"RegisterViewController.alertViewFailRegistrationAlertActionButtonText" = "Ok";

"RegisterViewController.alertViewFailRegistrationAlertSignInActionButtonText" = "Sign in";

/* Error message that is displayed after having a server error (invalid username or password) and is hidden after starting editing the usernam pr password fields */
"SignInViewController.tempFormErrorMessage" = "Invalid username or password";
/* The details menu button text */
"HomeViewController.leftMenuBarButtonText" = "Menu";
/* Text That appears in the Add Vehicle Button in the side menu */
"VehiclesMenuViewController.addVehicleButtonText" = "Add a Vehicle";
/* Text that appears in the navigation Bar when the Vehicle listing VC is displayed */
"VehiclesListingViewController.vehicleListingViewControllerTitle" = "My Vehicles";

"VehicleInfoViewController.myCarName" = "My Car";
"VehicleInfoViewController.myMotorcycleName" = "My Bike";
"VehicleInfoViewController.searchTextPlaceHolder" = "Start typing here";
"VehicleInfoViewController.chooseBrandQuestion" = "What brand is it?";
"VehicleInfoViewController.chooseBrand" = "Choose your brand";

"VehicleInfoViewController.chooseModelQuestion" = "What model is it?";
"VehicleInfoViewController.chooseModel" = "Choose your model";

"VehicleInfoViewController.chooseColourQuestion" = "What colour is it?";
"VehicleInfoViewController.chooseColour" = "Choose your colour";

"VehicleInfoViewController.plateNumber" = "Plate Number";
"VehicleInfoViewController.plateNumberQuestion" = "Please enter your plate number";
"VehicleInfoViewController.saveButton" = "Save";
"VehicleInfoViewController.skipButton" = "Skip";
"VehicleInfoViewController.myVehicleName" = "My";
"VehicleInfoViewController.plateNumberPlaceHolder" = "Your plate number";
"VehicleInfoViewController.plateNumberLabel" = "Plate Number";

/* Title in alert view that shows If registering the ride failed */
"CreateRideViewController.FailToAddRideAlertViewTitle" = "Add A Vehicle";

/* Message of alert view that shows If registering the ride failed */
"CreateRideViewController.FailToAddRideAlertViewMessage" = "Error Adding Vehicle";

/* Action Button text of alert view that shows If registering the ride failed */
"CreateRideViewController.FailToAddRideAlertViewActionButtonText" = "Ok";

"CreateRideViewController.alertViewFailToLoadModelsAlertTitle" = "Load Models";
"CreateRideViewController.alertViewFailToLoadModelsAlertMessage" = "Failed to load data, please check the internet connectivity.";
"CreateRideViewController.alertViewFailToLoadModelsAlertActionButtonText" = "Load Models";

/* The Add Addresses button Text */
"AddressesTableViewController.addAddressesButtonText" = "Add New Location";

"AddressesTableViewController.addAddressesNoRecordsMessage" = "No saved locations yet";

/* The Sticky header title that is present while choosing an time */
"BookAWashTimeViewController.stickyTitleText" = "Pick a Time Interval";

/* The Sticky header title that is present while choosing an date */
"BookAWashDateViewController.stickyTitleText" = "Pick a Date";

/* The Sticky header title that is present while choosing an address */
"BookAWashAddressViewController.stickyTitleText" = "Pick a Location";

"AddressTypeViewModel.viewControllerTitle" = "Add a Location";
"AddressTypeViewModel.descriptionText" = "What type of location is it?";

"AddressProfileViewModel.areaLabelText" = "Area";
"AddressProfileViewModel.streetLabelText" = "Street / Landmark / Neighborhood";
"AddressProfileViewModel.buildingLabelText" = "Building";
"AddressProfileViewModel.carLocationLabelText" = "Car Location/Parking";
"AddressProfileViewModel.areaTextFieldPlaceholderText" = "Choose your area";
"AddressProfileViewModel.streetTextFieldPlaceholderText" = "Damascus Road";
"AddressProfileViewModel.buildingTextFieldPlaceholderText" = "Berytech Building";
"AddressProfileViewModel.carLocationTextFieldPlaceholderText" = "Street, Indoor -1, Outdoor parking";
"AddressProfileViewModel.saveButtonText" = "Save";
"AddressProfileViewModel.addressNameFieldPlaceholder" = "My place";
"AddressProfileViewModel.viewControllerTitle" = "Add a location";
"AddressProfileViewModel.viewControllerTitleMapFullScreenMode" = "Locate your vehicle";
"AddressProfileViewModel.otherAreaTextFieldPlaceholderText" = "Other Area";
"AddressProfileViewModel.otherAreaLabelText" = "We do not currently service areas outside of the ones listed in the drop down. If you tell us where you would like to have your car washed, we'll notify you as soon as the service is available in your area.";
"AddressProfileViewModel.carLocationInformationLabel" = "You can keep this field empty if you park in a different location every day.";

"AddressProfileViewModel.alertViewServiceAreaNotificationAlertTitle" = "Service Area";
"AddressProfileViewModel.alertViewServiceAreaNotificationAlertMessage" = "We have saved your location. We do not service this area yet but we will notify you as soon as we do.";
"AddressProfileViewModel.alertViewServiceAreaNotificationAlertButtonText" = "Ok";

"AddressProfileViewModel.alertViewMissingDataAlertMessage" = "One or more fields are invalid!";
"AddressProfileViewModel.alertViewMissingDataAlertTitle" = "Could not update your Location";
"AddressProfileViewModel.alertViewMissingDataAlertButtonText" = "Ok";

/* Title that appears when this view controller is present inside a UINavigationController */
"AddressesTableViewController.viewControllerTitle" = "My Locations";

"AddressesTableViewController.alertViewErrorLoadingAddressesTitle" = "Ooops";
"AddressesTableViewController.alertViewErrorLoadingAddressesMessage" = "Couldn't load your locations. Please make sure you are connected to the internet.";
"AddressesTableViewController.alertViewErrorLoadingAddressesButtonTitle" = "Ok";

/* Alert action button text that shows when error occures */
"AddressProfileViewModel.alertViewFailToAddOrUpdateAddressActionButtonText" = "Ok";

/* Alert message that shows when error occures */
"AddressProfileViewModel.alertViewFailToAddOrUpdateAddressAlertMessage" = "Could not Add/Update your location";

/* Alert title that shows when error occures */
"AddressProfileViewModel.alertViewFailToAddOrUpdateAddressAlertTitle" = "Location";


"UserProfileViewController.ViewControllerTitle" = "Profile";
"UserProfileViewController.form.firstNameLabelText" = "First Name";
"UserProfileViewController.form.lastNameLabelText" = "Last Name";
"UserProfileViewController.form.emailLabelText" = "Email";
"UserProfileViewController.form.phoneLabelText" = "Mobile Number";
"UserProfileViewController.form.emailErrorMessage" = "Woops. Please check your email again.";
"UserProfileViewController.form.phoneErrorMessage" = "Woops. Please check your mobile number again.";
"UserProfileViewController.api.alertViewFailToUpdateUserProfileAlertActionButtonText" = "Ok";
"UserProfileViewController.api.alertViewFailToUpdateUserProfileAlertTitle" = "Update Profile";
"UserProfileViewController.api.alertViewFailToUpdateUserProfileAlertMessage" = "Could not update your profile";
"UserProfileViewController.form.alertViewInvalidFormDataAlertMessage" = "One or more fields are invalid!";
"UserProfileViewController.form.alertViewInvalidFormDataAlertTitle" = "Update Profile";
"UserProfileViewController.form.alertViewInvalidFormDataAlertActionButtonText" = "Ok";
"UserProfileViewController.submitButtonTitle" = "Save";

"RatingViewController.title" = "Rating";
"RatingViewController.messageLabel" = "We hope you enjoyed our service. Let us know how it went";
"RatingViewController.ratingTitleLabel" = "Rate The Service";
"RatingViewController.submitButtonTitle" = "Submit";
/* Label that appears just before the social icons */
"RatingViewController.socialSharingLabel" = "Share The Love";

"DiagnosticsAndUsageViewController.sendDataUsageText" = "Send usage data";
"DiagnosticsAndUsageViewController.viewControllerTitle" = "Diagnostics And Usage";
"DiagnosticsAndUsageViewController.headerDetailedMessage" = "Help us improve our application and service by automatically sending us usage data. This data my include details about your device and your iOS version, information about how you use the app, more details about possible issues and crashes that you may encounter, and location information.\n\nNone of the collected data identifies you personally. For more information about the collected data, you may check our Privacy Policy.";

"CustomerServiceViewController.giveUsACallLabelText" = "Give Us a Call";
"CustomerServiceViewController.callNowButtonTitle" = "Call Now";
"CustomerServiceViewController.saveButtonTitle" = "Submit";
"CustomerServiceViewController.requestTypeLabelText" = "Request Type";
"CustomerServiceViewController.descriptionLabelText" = "Description";
"CustomerServiceViewController.descriptionTextFieldPlaceHolder" = "Write your message here";

"CustomerServiceViewController.cantSubmitAlertTitle" = "No Data";
"CustomerServiceViewController.cantSubmitAlertMessage" = "Can't submit an empty form";
"CustomerServiceViewController.cantSubmitAlertOkButton" = "Ok";

/* Steps separator - ex: 1 of 3" */
"BookAWashViewController.stepsSeprator" = "of";

/* Save A Booking Button Title */
"BookAWashConfirmationViewController.saveButtonTitle" = "Confirm";
"BookAWashConfirmationViewController.saveButtonEditTitle" = "Save";

/* Discard A Booking Button Title*/
"BookAWashConfirmationViewController.discardButtonTitle" = "Cancel Booking";

/* Cancel A Booking Button Title */
"BookAWashConfirmationViewController.cancelButtonTitle" = "Cancel";

/* Booking Confirmation View Controller Title */
"BookAWashConfirmationViewController.viewControllerTitle" = "Confirm";

/* Title for alert view that shows up when booking a wash is successful */
"BookAWashConfirmationViewController.bookingSuccessAlertViewTitle" = "Blink My Car";

/* Message for alert view that shows up when booking a wash is successful */
"BookAWashConfirmationViewController.bookingSuccessAlertViewMessage" = "We got your request. We will confirm your booking shortly.";

/* Dismiss Button title for alert view that shows up when booking a wash is successful */
"BookAWashConfirmationViewController.bookingSuccessAlertViewPostiveButtonTitle" = "Ok";

"CallMeViewController.ViewControllerTitle" = "Call Me";
"CallMeViewController.headerLabelText" = "We will call you in less than one minute";
"CallMeViewController.okButton" = "Ok";
"CallMeViewController.cancelButton" = "Cancel";

/* Upcoming bookings view controller title */
"UpcomingBookingsViewController.viewControllerTitle" = "Blink My Car";

/* Call Us button Text in the Upcoming Bookings View Controller */
"UpcomingBookingsViewController.callUsButtonText" = "Call Us!";

/* Text that appears before the call us button in the upcoming Bookings VC */
"UpcomingBookingsViewController.informationLabelText" = "Need Assistance?";

/* Edit booking button Text */
"UpcomingBookingsViewController.editButtonText" = "Edit this booking";

/* Cancel booking button Text */
"UpcomingBookingsViewController.cancelButtonText" = "Cancel booking";

/* Title of the alert view that appears upon tapping Discard booking button */
"BookAWashConfirmationViewController.discardBookingAlertViewTitle" = "Cancel Booking";

/* Message of the alert view that appears upon tapping Discard booking button */
"BookAWashConfirmationViewController.discardBookingAlertViewTitleMessage" = "Are you sure you want to discard your booking?";

/* Title of negative button in the alert view that appears upon tapping Discard booking button */
"BookAWashConfirmationViewController.discardBookingAlertViewNegativeButtonTitle" = "No";

/* Title of positive button in the alert view that appears upon tapping Discard booking button */
"BookAWashConfirmationViewController.discardBookingAlertViewPostiveButtonTitle" = "Yes";

"VehicleProfileViewController.ViewControllerTitle" = "Vehicle Profile";
"VehicleProfileViewController.brandTableItem" = "Brand";
"VehicleProfileViewController.modelTableItem" = "Model";
"VehicleProfileViewController.colorTableItem" = "Color";
"VehicleProfileViewController.plateNumberTableItem" = "Plate";

"VehicleProfileViewModel.alertViewFailToLoadModelsAlertTitle" = "Load Models";
"VehicleProfileViewModel.alertViewFailToLoadModelsAlertMessage" = "Failed to load data, please check the internet connectivity.";
"VehicleProfileViewModel.alertViewFailToLoadModelsAlertActionButtonText" = "Load Models";


"VehiclesListingViewController.deleteAlertTitle" = "Delete Confirmation";
"VehiclesListingViewController.deleteAlertMessage" = "Are you sure you want to delete this vehicle?";
"VehiclesListingViewController.deleteAlertNegativeButton" = "No";
"VehiclesListingViewController.deleteAlertPostionButton" = "Yes";

"VehiclesListingViewController.alertViewCantDeleteTitle" = "Ooops";
"VehiclesListingViewController.alertViewCantDeleteMessage" = "You already booked a wash for this vehicle. Please cancel your booking before deleting this vehicle.";
"VehiclesListingViewController.alertViewCantDeleteButtonTitle" = "Ok";

"VehiclesListingViewController.alertViewCantDeleteLastVehicleTitle" = "Ooops";
"VehiclesListingViewController.alertViewCantDeleteLastVehicleMessage" = "Unfortunately we can't delete your last vehicle";
"VehiclesListingViewController.alertViewCantDeleteLastVehicleButtonTitle" = "Ok";

"VehiclesListingViewController.deleteVehicleErrorMessage" = "Could not Delete vehicle";

"VehiclesListingViewController.alertViewErrorDeletingTitle" = "Ooops";
"VehiclesListingViewController.alertViewErrorDeletingButtonTitle" = "Ok";

"BookingHistoryViewController.viewControllerTitle" = "Booking History";
"BookingHistoryViewController.bookingHistoryNoRecordsMessage" = "No bookings yet.";

"AboutUsViewController.title" = "About Us";
"AboutUsViewController.playButtonTitle" = "Play Video";
"AboutUsViewController.mainText" = "Car Care\nAnytime, Anywhere.";
"AboutUsViewController.secondaryParagraphText" = "With the pace of technology now faster than ever, urban living has never been simpler. How is it that we still spend hours waiting at a car wash, wasting both time and hundreds of liters of water? Meet Blink My Car.\n\nBlink My Car is the easiest way to get your car or motorbike washed quickly by our team of Specialists, anywhere in Beirut. Just tell us a bit about your vehicle so we can spot it easily and pick a location, date and time.\n\nOur Specialists will find your car and wash it without moving it. A wash takes 40 minutes and you pay securely right from your phone when we are done. Help us maintain quality service by rating your experience with Blink My Car.";
"AboutUsViewController.termsAndConditions" = "Terms And Conditions";
"AboutUsViewController.privacyPolicy" = "Privacy Policy";
"AboutUsViewController.legalNotices" = "Legal Notices";

"CustomerServiceViewController.successAlertTitle" = "Request submitted";
"CustomerServiceViewController.successAlertMessage" = "Thank you for getting in touch. We will get back to you shortly.";
"CustomerServiceViewController.failAlertTitle" = "Ooops";
"CustomerServiceViewController.failAlertMessage" = "We faced an error saving your request. Please try again later.";

"RegisterViewController.registerationErrorInvalidField" = "Fields you submitted are invalid";
"RegisterViewController.registerationErrorEmailAlreadyExists" = "An account already exists with the same email.";

/* A text that comes between the washer name and the car name stating that the washer is pampering your vehicle */
"WashInProgressViewController.washerActionDescription" = "We are pampering your";

/* Wash In Progress View Controller Title */
"WashInProgressViewController.viewControllerTitle" = "Wash In Progress";

"CallMeViewController.successAlertTitle" = "Request submitted";
"CallMeViewController.successAlertMessage" = "We have received your request. We will get in touch shortly.";
"CallMeViewController.failAlertTitle" = "Ooops";
"CallMeViewController.failAlertMessage" = "There was an error receiving your request. Please try again later.";

/* Alert View Message, Shown When we fail to cancel a booking */
"BookAWashConfirmationViewController.failToDiscardBookingAlertMessage" = "Failed to Cancel your booking";

/* Alert View Discard button title, Shown When we fail to cancel a booking */
"BookAWashConfirmationViewController.failToDiscardBookingAlertOkButtonTitle" = "Ok";

/* Alert View Title, Shown When we fail to cancel a booking */
"BookAWashConfirmationViewController.failToDiscardBookingAlertTitle" = "Cancel Booking";

/* Alert View Message, Shown When we fail to Update a booking */
"BookAWashConfirmationViewController.failToUpdateBookingAlertMessage" = "Failed to Update your booking";

/* Alert View Discard button title, Shown When we fail to Update a booking */
"BookAWashConfirmationViewController.failToUpdateBookingAlertOkButtonTitle" = "Ok";

/* Alert View Title, Shown When we fail to Update a booking */
"BookAWashConfirmationViewController.failToUpdateBookingAlertTitle" = "Update Booking";

/* Alert View Message, Shown When we fail to Create a booking */
"BookAWashConfirmationViewController.failToCreateBookingAlertMessage" = "Failed to Create your booking";

/* Alert View Discard button title, Shown When we fail to Create a booking */
"BookAWashConfirmationViewController.failToCreateBookingAlertOkButtonTitle" = "Ok";

/* Alert View Title, Shown When we fail to Create a booking */
"BookAWashConfirmationViewController.failToCreateBookingAlertTitle" = "Create Booking";


/* Text that appears under the address name to notify the user that this address is not serviceable */
"AddressesTableViewController.addressNotServiceableAreaNote" = "This location is in an area that we do not service yet.";

/* Short message that appears when the user is adding a cutom make */
"CreateRideViewController.addCustomMake" = "Add your vehicle's brand.";
/* Short message that appears when the user is adding a custom model */
"CreateRideViewController.addCustomModel" = "Add your vehicle's model.";

/* Alert View Title, appears when we fail to load the addresses from server in the Addresses listing */
"AddressesTableViewCOntroller.alertViewTitleCouldNotLoadAddresses" = "Locations";
/* Alert View Message, appears when we fail to load the addresses from server in the Addresses listing */
"AddressesTableViewCOntroller.alertViewMessageCouldNotLoadAddresses" = "Sorry we couldn't load your locations would you like to try again?";
/* Alert View Cancel button, just dismisses the alert view */
"AddressesTableViewCOntroller.alertViewCancelButtonTitle" = "Cancel";
/* Alert View Try Again button */
"AddressesTableViewCOntroller.alertViewTryAgainButtonTitle" = "Try again";
/* Alert View Ok button */
"AddressesTableViewCOntroller.alertViewOkButtonTitle" = "Ok";
/* Alert View Title, appears when we fail to load the areas from server in the Addresses listing */
"AddressesTableViewCOntroller.alertViewTitleCouldNotLoadAreas" = "Locations";
/* Alert View Message, appears when we fail to load the areas from server in the Addresses listing */
"AddressesTableViewCOntroller.alertViewMessageCouldNotLoadAreas" = "Sorry we couldn't load the serviceable areas would you like to try again?";
/* Alert View Title, appears when we an unknown error occures while fetching info from server */
"AddressesTableViewCOntroller.alertViewTitleGeneralError" = "Addresses";
/* Alert View Message, appears when we an unknown error occures while fetching info from server */
"AddressesTableViewCOntroller.alertViewMessageGeneralError" = "Something went wrong while loading addresses, try again ?";
/* Alert View Title, appears when we fail to delete an address */
"AddressesTableViewCOntroller.alertViewTitleCouldNotDeleteAddress" = "Location";
/* Alert View Message, appears when we fail to delete an address */
"AddressesTableViewCOntroller.alertViewMessageCouldNotDeleteAddress" = "Something went wrong we couldn't delete your Location!";

/* Alert View Message, Shown When we fail to cancel a booking */
"UpcomingBookingsViewController.failToDiscardBookingAlertMessage" = "Failed to Cancel your booking";

/* Alert View Discard button title, Shown When we fail to cancel a booking */
"UpcomingBookingsViewController.failToDiscardBookingAlertOkButtonTitle" = "Ok";

/* Alert View Title, Shown When we fail to cancel a booking */
"UpcomingBookingsViewController.failToDiscardBookingAlertTitle" = "Cancel Booking";

/* Alert View Title, Shown When we the user taps on call us */
"CustomerServiceViewController.callAlertTitle" = "Call Us";

/* Alert View Message, Shown When we the user taps on call us */
"CustomerServiceViewController.callAlertMessage" = "You are about to call the customer service. Would you like to proceed?";

/* Alert View no button, Shown When we the user taps on call us */
"CustomerServiceViewController.callAlertNoButton" = "No";

/* Alert View yes button, Shown When we the user taps on call us */
"CustomerServiceViewController.callAlertYesButton" = "Yes";

/* A text that comes under the amount of water saved */
"WashInProgressViewController.descriptionText" = "Liters of water saved so far";

/* Legal terms acknowledgements view controller title */
"AcknowledgementsTableViewController.viewControllerTitle" = "Legal Notices";

/* The address picker view controller title that appears in the navigation view */\
"BookAWashAddressPickerViewController.viewControllerTitle" = "Book A Wash";

/* The date picker view controller title that appears in the navigation view */
"BookAWashDateViewController.viewControllerTitle" = "Book A Wash";

/* The time picker view controller title that appears in the navigation view */
"BookAWasTimeViewController.viewControllerTitle" = "Book A Wash";

}
