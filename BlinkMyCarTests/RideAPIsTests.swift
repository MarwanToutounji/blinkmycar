//
//  RideAPIsTests.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import XCTest
import BlinkMyCar
import SwiftyJSON

class RideAPIsTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

  func testAddVehicleAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.AddVehicle(color: "#ffffff", make: "Keeward", model: "Mazaratti", name: "Marwan", plateNumber: nil, type: "car", year: nil),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 201, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testUpdateVehicleAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.UpdateVehicle(id: "4", color: nil, make: nil, model: nil, name: "Zouzou", plateNumber: nil, type: nil, year: nil),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testAllVehiclesAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.AllVehicles,
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testVehicleAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.Vehicle(id: "5"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testDeleteVehicleAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.DeleteVehicle(id: "6"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 204, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })
    
    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }
}
