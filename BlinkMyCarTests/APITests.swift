//
//  APITests.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 6/16/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import XCTest
import BlinkMyCar
import SwiftyJSON

class APITests: XCTestCase {

  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  func testOAuthAPICall() {

    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    // TODO: Replace username and password
    APIProvider.sharedProvider.request(
      BlinkMyCarAPI.OAuth(username: "danny.hajj@keeward.com", password: "123123"),
      completion: { (data, statusCode, response, error) -> () in
        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var message = NSString(data: data!, encoding: NSUTF8StringEncoding) as? String ?? ""
        XCTAssertNotNil(message, "Data is not a string")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testUserAPICall() {

    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    // TODO: Replace username and password
    signedRequest(
      BlinkMyCarAPI.Profile,
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        let user: User! = User.fromJSON(jsonDictionary!) as! User

        // Fulfill the expectation
        readyExpectation.fulfill()
    })


    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testMakeAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    APIProvider.sharedProvider.request(BlinkMyCarAPI.CarMakesAndModels,
      completion: { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertNil(error, "OAuth API call Error")
        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        var makeArray = Make.arrayMakeFromJSON(jsonDictionary!)
        XCTAssertTrue(((jsonDictionary?.isEmpty ?? true) == makeArray.isEmpty), "")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }
}
