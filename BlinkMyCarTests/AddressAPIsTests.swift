//
//  AddressAPIsTests.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/17/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import XCTest
import BlinkMyCar
import SwiftyJSON

class AddressAPIsTests: XCTestCase {

  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  func testExample() {
    // This is an example of a functional test case.
    XCTAssert(true, "Pass")
  }

  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measureBlock() {
      // Put the code you want to measure the time of here.
    }
  }

  func testAllAddressesAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.AllAddresses,
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        let addresses = Address.addressesFromJSON(jsonDictionary!)
        XCTAssertTrue(((jsonDictionary?.isEmpty ?? true) == addresses.isEmpty), "")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testAddAddressAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.AddAddress(
        area: nil, city: "Beirut", comment: "No Comment", coordinates: "20.00,23.43",
        country: "Lebanon", street: nil, building: nil, userFloorNumber: nil, floor: nil, isIndoor: nil, landmark: "Berytech",
        locationType: nil, name: "Work", spotNumber: nil, type: nil),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 201, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testDeleteAddressAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.DeleteAddress(id: "2"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 204, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testAddressAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.Address(id: "2"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testUpdateAddressAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.UpdateAddress(id: "1", area: "Batroun", city: nil, comment: nil,
        coordinates: nil, country: nil, street: nil, building: nil, userFloorNumber: nil, floor: nil, isIndoor: nil, landmark: nil,
        locationType: nil, name: nil, spotNumber: nil, type: nil),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testCountryAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.Country,
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        let countries = Country.countriesFromJSON(jsonDictionary!)
        XCTAssertTrue(((jsonDictionary?.isEmpty ?? true) == countries.isEmpty), "")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testCityAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.City(countryId: "LB"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        let cities = City.citiesFromJSON(jsonDictionary!)
        XCTAssertTrue(((jsonDictionary?.isEmpty ?? true) == cities.isEmpty), "")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testAreasAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.Area(countryId: "LB", cityId: "beirut"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        let areas = Area.areasFromJSON(jsonDictionary!)
        XCTAssertTrue(((jsonDictionary?.isEmpty ?? true) == areas.isEmpty), "")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }


  func testServiceDatesAndTimesAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(
      BlinkMyCarAPI.ServiceDateTime(countryId: "LB", cityId: "beirut", areaId: "1"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        let serviceDate = ServiceDate.serviceDatesFromJSON(jsonDictionary!)
        XCTAssertTrue(((jsonDictionary?.isEmpty ?? true) == serviceDate.isEmpty), "")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }
}
