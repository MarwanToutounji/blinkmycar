//
//  BookingAPIsTests.swift
//  BlinkMyCar
//
//  Created by Marwan Toutounji on 7/29/15.
//  Copyright (c) 2015 Keeward. All rights reserved.
//

import UIKit
import XCTest
import BlinkMyCar
import SwiftyJSON

class BookingAPIsTests: XCTestCase {

  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  func testExample() {
    // This is an example of a functional test case.
    XCTAssert(true, "Pass")
  }

  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measureBlock() {
      // Put the code you want to measure the time of here.
    }
  }

  func testAllBookingsAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.AllBookings,
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        let bookings = Booking.bookingsFromJSON(jsonDictionary!)
        XCTAssertTrue(((jsonDictionary?.isEmpty ?? true) == bookings.isEmpty), "")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testCancelBookingAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.CancelBooking(bookingId: "5"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testGetABookingAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.Booking(bookingId: "5"),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }

  func testUpdateBookingAPICall() {
    // Declare our expectation
    let readyExpectation = expectationWithDescription("ready")

    signedRequest(BlinkMyCarAPI.UpdateBooking(bookingId: "4", addressId: nil, deliveryDate: NSDate(timeIntervalSince1970: 1496880000), deliveryTime: nil, rideId: nil),
      { (data: NSData?, statusCode: Int?, response: NSURLResponse?, error: NSError?) -> () in
        XCTAssertTrue((statusCode ?? 0) == 200, "Request unsuccessful")

        XCTAssertNil(error, "OAuth API call Error")

        XCTAssertNotNil(data, "OAuth API return no data")

        var error: NSError? = nil
        let jsonDictionary = NSJSONSerialization.JSONObjectWithData(data!,
          options: NSJSONReadingOptions.AllowFragments, error: &error) as? [String:AnyObject]
        XCTAssertNotNil(jsonDictionary, "Response is not a dictionary")

        // Fulfill the expectation
        readyExpectation.fulfill()
    })

    // Loop until the expectation is fulfilled
    waitForExpectationsWithTimeout(10,
      handler: { (error: NSError!) -> Void in
        XCTAssertNil(error, "Error While ")
    })
  }
  
  
}
